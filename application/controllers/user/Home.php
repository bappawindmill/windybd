<?php


class Home extends MY_Controller {

    public function __construct() {
        parent::__construct();
        if ($this->session->userdata('employeeId') == "") {
            redirect('hr/emp_login');
        }
    }

    function index() {
        $this->data['title'] = 'Home';
        $this->middle = 'hr/emp_home'; // passing middle to function. change this for different views.
        $this->layout();
    }

}

