<?php

class Payslip extends MY_Controller {

    public function __construct() {
        parent::__construct();
        if ($this->session->userdata('employeeId') == "") {
            redirect('hr/emp_login');
        }
    }


    function view_form() {
        $this->data['year'] = "";
        $this->data['month'] = "";
        $this->data['title'] = 'Leave Report';
        $this->middle = 'user/payslip/form';
        $this->layout();
    }

    function viewed() {
        $this->form_validation->set_rules('year', 'Year', 'trim|required');
        $this->form_validation->set_rules('month', 'Month', 'trim|required');

        if ($this->form_validation->run() == FALSE) {
            $this->data['year'] = $this->input->post('year');
            $this->data['month'] = $this->input->post('month');
            $this->data['title'] = 'payslip';
            $this->middle = 'user/payslip/form';
            $this->layout();
        } else {
            $empId = $this->session->userdata("employeeId");
            $year = $this->input->post("year");
            $month = $this->input->post("month");
            $this->data['montyly_salary_info'] = $this->payslip_model->getMonthlySalaryInfo($empId, $month, $year);
            $this->data['title'] = 'payslip';
            $this->middle = 'user/payslip/payslip';
            $this->layout();
        }
    }

    function pf_viewed() { 
        $this->data['title'] = 'provident_fund';
        $empId = $this->session->userdata("employeeId");
        $this->data['emp_profile_info'] = $this->provident_fund_model->getProfileInfo($empId);
        foreach ($this->data['emp_profile_info'] as $empInfo):
            $empId = $empInfo->id;
            $confirmation_date = $empInfo->confirmation_date;
        endforeach;
        $this->data['allData'] = $this->provident_fund_model->getEmpProvidentAmount($empId,'pf_amount');
        $providentAr = $this->provident_fund_model->getEmpProvidentInfo($empId);
        /*echo '<pre>';
        var_dump($providentAr);
        echo '</pre>';
        exit;*/
        $maturity = 1095;
        if ($providentAr) {
            foreach ($providentAr as $row) {
                $maturity = $row->maturity;
            }
        }
        $c_date = date("Y-m-d");
        //$confirmation_date = "2010-10-01";
        $days = (strtotime($c_date) - strtotime($confirmation_date)) / (60 * 60 * 24);

        if ($days > $maturity) {
            $this->middle = 'hr/providentFund/report/view';
            $this->layout();
        } else {
            $this->middle = 'hr/providentFund/report/view1';
            $this->layout();
        }
    }


}

