<?php

//include_once 'BaseController.php';

class Attendence extends MY_Controller {

    function __construct() {
        parent::__construct();
        if ($this->session->userdata('employeeId') == "") {
            redirect('hr/emp_login');
        }
    }

    function attendence_request() {
        $id = $this->session->userdata('employeeId');
        $this->data['attendence_request_info'] = $this->attendence_model->get_attendence_request($id);
        $this->data['title'] = 'Attendence Request';
        $this->middle = 'user/attendence/application/application_view';
        $this->layout();
    }

    function details($empId = NULL, $year = NULL, $month = NULL) {
        //$query = $this->db->query("select * from employeeinfo where id='" . $empId . "'");
        $emp_info = $this->attendence_model->getEmployeeInfo($empId);
        /*echo '<pre>';
        var_dump($query);
        echo '</pre>';
        exit;*/
        if ($emp_info) {
            $this->data['title'] = 'attendence_request';
            $this->data['empId'] = $empId;
            $this->data['year'] = $year;
            $this->data['month'] = $month;
            $this->middle = 'user/attendence/application/application_details';
            $this->layout();
        } else {
            redirect("user/attendence/attendence_request");
        }
    }

    function view() {
        $this->data['year'] = "";
        $this->data['month'] = "";
        $this->data['title'] = 'View Attendance';
        $this->middle = 'user/attendence/r_form';
        $this->layout();
    }

    function viewed() {

        $this->form_validation->set_rules('year', 'Year', 'trim|required');
        $this->form_validation->set_rules('month', 'Month', 'trim|required');

        if ($this->form_validation->run() == FALSE) {
            $this->data['year'] = $this->input->post('year');
            $this->data['month'] = $this->input->post('month');
            $this->data['title'] = 'View Attendance';
            $this->middle = 'user/attendence/r_form';
            $this->layout();
        } else {
            $empId = $this->session->userdata("employeeId");
            $this->data['year'] = $this->input->post("year");
            $this->data['month'] = $this->input->post("month");
            $checkAttendence = $this->attendence_model->checkSubmission($this->data['year'], $this->data['month'], $empId);
            if (count($checkAttendence)):
//                echo '<pre>';                print_r($checkAttendence); echo '</pre>';;
//                echo 'details1';
//                exit;

                $this->data['title'] = 'View Attendance';
                $this->middle = 'user/attendence/details1';
                $this->layout();
            else:
                //echo 'details';
                //exit;
                $this->data['title'] = 'View Attendance';
                $this->middle = 'user/attendence/details';
                $this->layout();
            endif;
        }
    }

    function update() {
        $this->data['title'] = 'attendence_request';
        $emp_id = $this->input->post("emp_id");
        $year = $this->input->post("year");
        $month = $this->input->post("month");
        $att_date = $this->input->post("att_date");
        $timein = $this->input->post("timein");
        $timeout = $this->input->post("timeout");
        $sup_comment = $this->input->post("sup_comment");
        $sup_approve = $this->input->post("sup_status");
        for ($i = 0; $i < sizeof($att_date); $i++):
            $AttData =
                array(
                    'timein' => $timein[$i],
                    'timeout' => $timeout[$i],
                    'sup_comment' => $sup_comment[$i],
                    'sup_approve' => $sup_approve[$i]
                );
            $this->attendence_model->updateAttendence($AttData, $emp_id, $att_date[$i]);
        endfor;
        //update supervisor status in attendence_request table
        $supStatus = array(
            'sup_status' => 2
        );
        $this->attendence_model->updateSupStatus($supStatus, $year, $month, $emp_id);

        redirect("user/attendence/attendence_request");
    }

    function viewLog() {
        $emp_id = $this->session->userdata("employeeId");
        $secreteNo = $this->session->userdata("secreteNo");
        $year = $this->input->post("year");
        $month = $this->input->post("month");
        $att_date = $this->input->post("att_date");
        $timein = $this->input->post("timein");
        $timeout = $this->input->post("timeout");
        $status = $this->input->post("status");
        $comment = $this->input->post("comment");
        for ($i = 0; $i < sizeof($att_date); $i++):
            $AttData = array(
                'emp_id' => $emp_id,
                'card_no' => $secreteNo,
                'year' => $year,
                'month' => $month,
                'att_date' => $att_date[$i],
                'timein' => $timein[$i],
                'timeout' => $timeout[$i],
                'emp_comment' => $comment[$i],
                'status' => $status[$i]
            );
            $this->attendence_model->attendenceInsert($AttData);
        endfor;
        $supervisor = "";
        $emp_info = $this->attendence_model->getEmployeeInfo($emp_id);
        foreach ($emp_info as $row):
            $supervisor = $row->supervisor;
        endforeach;
        if ($supervisor == "" || $supervisor == 0):
            $requestData = array(
                'emp_id' => $emp_id,
                'year' => $year,
                'month' => $month,
                'sup_status' => 3,
                'hr_status' => 1
            );
        else:
            $requestData = array(
                'emp_id' => $emp_id,
                'supervisor_id' => $supervisor,
                'year' => $year,
                'month' => $month,
                'sup_status' => 1,
                'hr_status' => 1
            );
        endif;
        $this->attendence_model->att_request_insert($requestData);
        redirect("user/attendence/view");
    }

    function addLog() {
        $empId = $this->session->userdata('employeeId');
        $this->data['dailyLogInfo'] = $this->attendence_model->viewDailyLog($empId);
        $this->pagination->initialize($this->pagination_library->getArray());
        $this->data['title'] = 'View DailyLog';
        $this->middle = 'user/DailyLog/log';
        $this->layout();
    }

    function added_log_form() {
        $this->data['log_date'] = "";
        $this->data['comment'] = "";
        $this->data['title'] = 'Add Log';
        $this->middle = 'user/DailyLog/logform';
        $this->layout();
    }

    function addedLog($start = 0) {
        $this->data['title'] = 'dailylog';
        $this->load->library('form_validation');
        $this->form_validation->set_rules('log_date', 'Log Date', 'trim|required');
        $this->form_validation->set_rules('comment', 'Comment', 'trim|required');
        if ($this->form_validation->run() == FALSE) {
            $this->data['log_date'] = $this->input->post('log_date');
            $this->data['comment'] = $this->input->post('comment');
            /*echo '<pre>';
            var_dump($this->data['comment']);
            echo '</pre>';
            exit;*/
            $this->data['title'] = 'Add Log';
            $this->middle = 'user/DailyLog/logform';
            $this->layout();
        } else {
            $data = array(
                'emp_id' => $this->session->userdata('employeeId'),
                'log_date' => $this->input->post('log_date'),
                'comment' => $this->input->post('comment')
            );
            $this->attendence_model->DailyLogInsert($data);
            redirect('user/attendence/addLog', 'location', 301);
        }
    }

    function log_delete($id = NULL) {
        $this->data['title'] = 'dailylog';
        $this->attendence_model->deleteDailyLog($id);
        redirect('user/attendence/addLog', 'location', 301);
    }

    function dayLogSave() {
        $comment   = $this->input->get_post('comment');
        $emp_id    = $this->input->get_post('emp_id');
        $log_date  = $this->input->get_post('log_date');

        $log = $this->db->from('DailyLog')
                ->where('emp_id', $emp_id)
                ->where('log_date', $log_date)
                ->get()->row_array();
        if(!empty($log)) {
            $log['comment'] = $comment;
            $this->db->where('id', $log['id']);
            $this->db->update('DailyLog', $log);
        } else {
            $log['emp_id']   = $emp_id;
            $log['log_date'] = $log_date;
            $log['comment']  = $comment;
            $this->db->insert('DailyLog', $log);
        }
        exit;
    }
    
}

