<?php

class Leave extends MY_Controller {

    function __construct() {
        parent::__construct();


        if ($this->session->userdata('employeeId') == "") {
            redirect('hr/emp_login');
        }


    }

    function leave_report($data = NULL) {
        $this->data['title'] = 'Leave Report';
        $this->middle = 'user/leave/leave_details';
        $this->layout();
    }

    function leave_details() {

        $this->form_validation->set_rules('year', 'Year', 'trim|required');

        if ($this->form_validation->run() == FALSE) {
            $this->data['title'] = 'Leave Details';
            $this->middle = 'user/leave/leave_details';
            $this->layout();

        } else {
            $year = $this->input->post("year");
            $emp_id = $this->session->userdata('employeeId');
            $this->data['leave_details_info'] = $this->leave_model->report_leave_details($emp_id, $year);
            //echo '<pre>';
            //var_dump($this->data['$leave_details_info']);
            //echo '</pre>';
            //var_dump($this->data['$leave_details_info'][0]->hr_status);
            //exit;

            // leave package
            $this->data['leave_package_id'] = $this->leave_model->report_leave_package($emp_id);
            $leave_package_id = $this->data['leave_package_id'][0]->leave_name;
            $this->data['leave_package_info'] = $this->leave_model->all_leave_package_info($leave_package_id);

            // pass value
            $this->data['title'] = 'Leave Details';
            $this->data['year'] = $year;
            $this->middle = 'user/leave/leave_slip';
            $this->layout();
        }
    }

    function schedule() {
        $this->data['schedule_info'] = $this->leave_model->get_schedule();
        $this->data['title'] = 'Leave Schedule';
        $this->middle = 'user/leave/schedule';
        $this->layout();
    }

    function show() {
        $emp_id = $this->session->userdata('employeeId');
        $this->data['leave_status_info'] = $this->leave_model->show_leave_status($emp_id);
        $this->data['title'] = 'View Leave Application';
        $this->middle = 'user/leave/show';
        $this->layout();
    }

    function view_application(){
        $emp_id = $this->session->userdata('employeeId');
        $this->data['view_leave_application_request_info'] = $this->leave_model->view_leave_application_request($emp_id);
        $this->data['title'] = 'View Application Request';
        $this->middle = 'user/leave/application_view';
        $this->layout();
    }

    function details($id = NULL, $empId) {
        //$query = $this->db->query("select * from employeeinfo where id='" . $empId . "'");
        $emp = $this->leave_model->get_emp($empId);
        if ($emp) {
            $this->data['application_info'] = $this->leave_model->get_application_info($id);
            $this->data['title'] = 'View Application Details';
            $this->middle = 'user/leave/application_details';
            $this->layout();
        } else {
            redirect("user/leave/view_application");
        }
    }

    function holiday(){
        $this->data['holiday_info'] = $this->leave_model->view_holiday();
        $this->pagination->initialize($this->pagination_library->getArray());
        $this->data['title'] = 'View Holiday';
        $this->middle = 'user/holiday/holiday';
        $this->layout();
    }

    function holiday_list_calendar(){
        $this->data['title'] = 'Holiday List';
        $this->data['mainTab'] = 'configuration';
        $this->data['activeTab'] = 'leave';
        $this->db->order_by('start_date','desc');
        $query = $this->db->get('add_holiday');

        //$this->data['query1'] = $this->holiday_package_model->view_holiday();
        $this->data['query1'] = $query->result();
        $this->middle = 'user/holiday/view_holiday_calendar';
        $this->layout();
    }

    function weekend(){
        $this->data['weekend_info'] = $this->leave_model->view_weekend();
        $this->pagination->initialize($this->pagination_library->getArray());
        $this->data['title'] = 'View Holiday';
        $this->middle = 'user/weekend/weekend';
        $this->layout();
    }

    function weekend_calendar(){
        $bName = $this->session->userdata('bName');
        $this->data['weekend_info'] = $this->leave_model->view_weekend_calendar($bName);
        $this->data['title'] = 'View Holiday';
        $this->middle = 'user/weekend/weekend_view';
        $this->layout();
    }

    function apply() {  
        $this->data['start_date'] = "";
        $this->data['end_date'] = "";
        $this->data['leave_type'] = "";
        $this->data['duration'] = "";
        $this->data['sup_designation'] = "";
        $this->data['nominated_person'] = "";
        $this->data['title'] = 'Leave Application';

        $year = date('Y');
        $emp_id = $this->session->userdata('employeeId');
        $this->data['leave_details_info'] = $this->leave_model->report_leave_details($emp_id, $year);
        $this->data['leave_package_id'] = $this->leave_model->report_leave_package($emp_id);
        $leave_package_id = $this->data['leave_package_id'][0]->leave_name;
        $this->data['leave_package_info'] = $this->leave_model->all_leave_package_info($leave_package_id);
        $this->data['schedule_info'] = $this->leave_model->get_schedule();

        $this->middle = 'user/leave/application';
        $this->layout();
    }

    function getDateDifference($sdate = NULL, $edate = NULL) {
        date_default_timezone_set("Asia/Dhaka");
        $start = strtotime($sdate);
        $end = strtotime($edate);
        $days_between = ceil(abs($end - $start) / 86400);
        if ($sdate == $edate):
            ?>
            <input  type="text" readonly="readonly" class="form-control" name="duration" value="<?php echo $days_between + 1; ?>" />
        <?php else:
            ?>
            <input  type="text" readonly="readonly" class="form-control" name="duration" value="<?php echo $days_between + 1; ?>" />
            <?php
        endif;
    }

    function get_supervisor($id = NULL) {
        $empDesignation  = $this->input->get_post("empDesignation");
        $nominated_person = $this->input->get_post("nominated_person");
        $id = $empDesignation;
        if ($id == "") {
            echo "Not Found";
        } else {
            //$res = $this->db->query("SELECT id, name FROM employeeinfo WHERE designation='" . $id . "'");
            $res = $this->leave_model->get_supInfo_for_leave_apply($id);
            if ($res == 0) {
                echo "<select id='nominated_person' name='nominated_person' class='form-control'>";
                echo "<option value=''></option>";
                echo '</select>';
                die;
            }

            echo "<select id='nominated_person' name='nominated_person' class='form-control'>";
            foreach ($res as $row3) {
                $str = '';
                if($nominated_person == $row3->id ){
                    $str = 'selected';
                }
                echo "<option value='" . $row3->id . "' ".$str."      >" . $row3->name . "</option>";
            }
            echo '</select>';
        }
    }

    function confirmation() {
        $start_date = $this->input->post("start_date");
        $end_date = $this->input->post("end_date");
        $pay_type = $this->input->post("pay_type");
        $leave_duration1 = $this->input->post("leave_duration");
        $extra_days1 = $this->input->post("extra_days");
        $tDuration = $leave_duration1 - $extra_days1;

        $tDuration = $this->input->post('leave_duration');

        //with pay
        if ($pay_type == "1") {
            $data = array(
                'card_no' => $this->session->userdata("card_no"),
                'emp_id' => $this->session->userdata("employeeId"),
                'leave_duration' => $tDuration,
                'leave_type' => $this->input->post("leave_type"),
                'category' => 2,
                'start_date' => $this->input->post("start_date"),
                'end_date' => $this->input->post("end_date"),
                'app_date' => date("Y-m-d"),
                'year' => $this->input->post("year"),
                'nominated_person' => $this->input->post("nominated_person"),
                'extra_days' => $this->input->post("extra_days"),
                'pay_type' => $this->input->post("pay_type"),
                'quarter_id' => 0,
                'supervisor_status' => 1,
                'hr_status' => 1
            );
            $this->shows->EntryAll("leave_management", $data);

            $authorized_name = "";
            if ($this->input->post("nominated_person") != " ") {
                $nid = $this->input->post("nominated_person");
                $query = $this->leave_model->get_emp_info1($nid);
                foreach ($query as $info):
                    $authorized_name = $info->name;
                endforeach;
            }
            //Start mail sent to supervisor
            $sid = $this->session->userdata("employeeId");
            $supInfo = $this->leave_model->get_emp_info1($sid);
            $supervisor = "";
            foreach ($supInfo as $sInfo):
                $supervisor = $sInfo->supervisor;
            endforeach;
            $supMail = $this->leave_model->get_emp_info1($supervisor);
            $supmail = "";
            $supname = "";
            foreach ($supMail as $sMail):
                $supmail = $sMail->email;
                $supname = $sMail->name;
            endforeach;
            if ($supmail != "") {
                $applicant_name = $this->session->userdata('name');
                $to = $supmail;
                $subject = "Leave Notification";
                $sup_message = "                            
                              Dear Mr./Ms. ($supname) ,";

                $sup_message.="<br/>
							  You are being requested by (Mr./Ms $applicant_name) for granting his/her leave application. 
Please review the leave status of Mr./Ms. ($applicant_name) and convey a 
decision regarding this within 24 hours. 
Please also check with designated alternative official, Mr./Ms $authorized_name about his/her availability and consent of the additional responsibilities.
PLEASE DO NOT CONSIDER THIS LEAVE APPLICATION AS CONFIRM TILL WORK FORCE MANAGEMENT NOTIFICATION EMAIL.";
                $message.="<br/>This is auto generated mail. Pls donot reply to this mail";
                $headers = 'MIME-Version: 1.0' . "\r\n";
                $headers .= 'Content-type: text/html; charset=UTF-8' . "\r\n";
                // Additional headers
                $headers .= 'To: Customer <' . $to . "\r\n";
                $headers .= 'From: info@charcoalboard.com <info@charcoalboard.com>' . "\r\n";
                // Mail it
                mail($to, $subject, $sup_message, $headers);
            }
            //End supervisor mail sent
            //Start Send mail to applicant
            $applicant_name = $this->session->userdata('name');
            $sid = $this->session->userdata("employeeId");
            $EInfo = $this->leave_model->get_emp_info1($sid);
            $empmail = "";
            foreach ($EInfo as $eInfo):
                $empmail = $eInfo->email;
            endforeach;


            $applicant_name = $this->session->userdata('name');
            $to = $empmail;
            $subject = "Leave Notification";
            $applicant_message = "                            
                              Dear Mr./Ms. ($applicant_name) ,";

            $applicant_message.="<br/>
							 Thanks for your leave application. Your leave application will be forwarded to your immediate supervisor for approval. 
              After the approval of the supervisor the application will be forwarded to the WORK FORCE MANAGEMENT Department for further process. 
              PLEASE DON'T CONSIDER THIS MAIL AS AN APPROVAL OR REJECTION OF THE LEAVE REQUEST. WAIT FOR THE FINAL CONFIRMATION EMAIL FROM WORK FORCE MANAGEMENT";
            $message.="<br/>This is auto generated mail. Pls donot reply to this mail";
            $headers = 'MIME-Version: 1.0' . "\r\n";
            $headers .= 'Content-type: text/html; charset=UTF-8' . "\r\n";
            // Additional headers
            $headers .= 'To: Customer <' . $to . "\r\n";
            $headers .= 'From: info@charcoalboard.com <info@charcoalboard.com>' . "\r\n";
            // Mail it
            mail($to, $subject, $applicant_message, $headers);

            $nominatedPerson = $this->input->post("nominated_person");
            if ($nominatedPerson > 0) {
                $nEmail = "";
                $nName = "";
                $niminatedInfo = $this->leave_model->get_emp_info1($nominatedPerson);
                foreach ($niminatedInfo as $nTemp):
                    $nEmail = $nTemp->email;
                    $nName = $nTemp->name;
                endforeach;
                $applicant_name = $this->session->userdata('name');
                $to2 = $nEmail;
                $subject2 = "Leave Notification";
                $message2 = "Dear Concern,";
                $message2.="Mr./Mrs ('$applicant_name') has applied for a leave from ('$start_date') to ('$end_date'). His/Her application is still pending for approval. 
                                        You are mentioned as the alternative person in charge by the applicant during his/her absent. Please talk to the the applicant or his/her line manager, if you are not available or unable to take this responsibility by 24 hours. Otherwise, it will be effected. This e-mail has been generated for your information and necessary action. Don’t reply to this email address.";

                $headers2 = 'MIME-Version: 1.0' . "\r\n";
                $headers2 .= 'Content-type: text/html; charset=UTF-8' . "\r\n";
                // Additional headers
                $headers2 .= 'To: Customer <' . $to2 . "\r\n";
                $headers2 .= 'From: info@charcoalboard.com <info@charcoalboard.com>' . "\r\n";

                // Mail it
                mail($to2, $subject2, $message2, $headers2);
            }
            //end
            $this->session->set_flashdata('message', '<div id="message">Leave Info saved successfully</div>');
        }
        //without pay
        elseif ($pay_type == "2") {
            $data = array(
                'card_no' => $this->session->userdata("card_no"),
                'emp_id' => $this->session->userdata("employeeId"),
                'leave_duration' => $tDuration,
                'leave_type' => $this->input->post("leave_type"),
                'category' => 2,
                'start_date' => $this->input->post("start_date"),
                'end_date' => $this->input->post("end_date"),
                'app_date' => date("Y-m-d"),
                'year' => $this->input->post("year"),
                'nominated_person' => $this->input->post("nominated_person"),
                'extra_days' => $this->input->post("extra_days"),
                'pay_type' => $this->input->post("pay_type"),
                'quarter_id' => $this->input->post("quarter_id"),
                'supervisor_status' => 1,
                'hr_status' => 1
            );
            $this->shows->EntryAll("leave_management", $data);
            $authorized_name = "";
            if ($this->input->post("nominated_person") != " ") {
                $nid = $this->input->post("nominated_person");
                $query = $this->leave_model->get_emp_info1($nid);
                foreach ($query as $info):
                    $authorized_name = $info->name;
                endforeach;
            }

            //Start mail sent to supervisor
            $sid = $this->session->userdata("employeeId");
            $supInfo = $this->leave_model->get_emp_info1($sid);
            $supervisor = "";
            foreach ($supInfo as $sInfo):
                $supervisor = $sInfo->supervisor;
            endforeach;
            $supMail = $this->leave_model->get_emp_info1($supervisor);
            $supmail = "";
            $supname = "";
            foreach ($supMail as $sMail):
                $supmail = $sMail->email;
                $supname = $sMail->name;
            endforeach;
            if ($supmail != "") {

                $applicant_name = $this->session->userdata('name');
                $to = $supmail;
                $subject = "Leave Notification";
                $sup_message = "                            
                              Dear Mr./Ms. ($supname) ,";

                $sup_message.="<br/>
							  You are being requested by (Mr./Ms $applicant_name) for granting his/her leave application. 
Please review the leave status of Mr./Ms. ($applicant_name) and convey a 
decision regarding this within 24 hours. 
Please also check with designated alternative official, Mr./Ms $authorized_name about his/her availability and consent of the additional responsibilities.
PLEASE DO NOT CONSIDER THIS LEAVE APPLICATION AS CONFIRM TILL WORK FORCE MANAGEMENT NOTIFICATION EMAIL.";
                //$message.="<br/>This is auto generated mail. Pls donot reply to this mail";
                $headers = 'MIME-Version: 1.0' . "\r\n";
                $headers .= 'Content-type: text/html; charset=UTF-8' . "\r\n";
                // Additional headers
                $headers .= 'To: Customer <' . $to . "\r\n";
                $headers .= 'From: info@charcoalboard.com <info@charcoalboard.com>' . "\r\n";
                // Mail it
                mail($to, $subject, $sup_message, $headers);
            }
            //End supervisor mail sent
            //Start Send mail to applicant
            $applicant_name = $this->session->userdata('name');
            $sid = $this->session->userdata("employeeId");
            $EInfo = $this->leave_model->get_emp_info1($sid);
            $empmail = "";
            foreach ($EInfo as $eInfo):
                $empmail = $eInfo->email;
            endforeach;


            $to = $empmail;
            $subject = "Leave Notification";
            $applicant_message = "                            
                              Dear Mr./Ms. ($applicant_name) ,";

            $applicant_message.="<br/>
							     Thanks for your leave application. Your leave application will be forwarded to your immediate supervisor for approval. 
              After the approval of the supervisor the application will be forwarded to the WORK FORCE MANAGEMENT Department for further process. 
              PLEASE DON'T CONSIDER THIS MAIL AS AN APPROVAL OR REJECTION OF THE LEAVE REQUEST. WAIT FOR THE FINAL CONFIRMATION EMAIL FROM WORK FORCE MANAGEMENT.         
  ";
            $headers = 'MIME-Version: 1.0' . "\r\n";
            $headers .= 'Content-type: text/html; charset=UTF-8' . "\r\n";
            // Additional headers
            $headers .= 'To: Customer <' . $to . "\r\n";
            $headers .= 'From: info@charcoalboard.com <info@charcoalboard.com>' . "\r\n";
            // Mail it
            mail($to, $subject, $applicant_message, $headers);
            //nominated person

            $nominatedPerson = $this->input->post("nominated_person");
            if ($nominatedPerson > 0) {
                $nEmail = "";
                $nName = "";
                $niminatedInfo = $this->leave_model->get_emp_info1($nominatedPerson);
                foreach ($niminatedInfo as $nTemp):
                    $nEmail = $nTemp->email;
                    $nName = $nTemp->name;
                endforeach;
                $applicant_name = $this->session->userdata('name');
                $to2 = $nEmail;
                $subject2 = "Leave Notification";
                $message2 = "Dear Concern,";
                $message2.="Mr./Mrs ('$applicant_name') has applied for a leave from ('$start_date') to ('$end_date'). His/Her application is still pending for approval. 
                                        You are mentioned as the alternative person in charge by the applicant during his/her absent. Please talk to the the applicant or his/her line manager, if you are not available or unable to take this responsibility by 24 hours. Otherwise, it will be effected. This e-mail has been generated for your information and necessary action. Don’t reply to this email address.";

                $headers2 = 'MIME-Version: 1.0' . "\r\n";
                $headers2 .= 'Content-type: text/html; charset=UTF-8' . "\r\n";
                // Additional headers
                $headers2 .= 'To: Customer <' . $to2 . "\r\n";
                $headers2 .= 'From: info@charcoalboard.com <info@charcoalboard.com>' . "\r\n";

                // Mail it
                mail($to2, $subject2, $message2, $headers2);
            }

            //end
            $this->session->set_flashdata('message', '<div id="message">Leave Info saved successfully</div>');
        } else {

        }
        redirect('user/leave/apply', 'location', 301);
    }


    /*public function date_check(){
        $start_date = $this->input->get_post("start_date");
        $start_date_query=$this->db->query("SELECT q_id FROM leave_schedule WHERE DATE('$start_date') between start_date and end_date") ;
        $data = $start_date_query->result_array();
        if ($start_date == '2017-01-12'){
            $this->form_validation->set_message('date_check', 'The {field} field can not be the word  '.$data[0]['q_id']);
            return FALSE;
        } else {
            $this->form_validation->set_message('date_check', 'The {field} field can not be the word "test123"');
            return true;
        }
    }*/

    function applied() {

        $year = date('Y');
        $emp_id = $this->session->userdata('employeeId');
        $this->data['leave_details_info'] = $this->leave_model->report_leave_details($emp_id, $year);
        $this->data['leave_package_id'] = $this->leave_model->report_leave_package($emp_id);
        $leave_package_id = $this->data['leave_package_id'][0]->leave_name;
        $this->data['leave_package_info'] = $this->leave_model->all_leave_package_info($leave_package_id);
        $this->data['schedule_info'] = $this->leave_model->get_schedule();

        $this->data['title'] = 'Leave Application';
        $this->form_validation->set_rules('start_date', 'Start Date', 'trim|required');
        $this->form_validation->set_rules('end_date', 'End Date', 'trim|required');
        $this->form_validation->set_rules('duration', 'Duration', 'trim|required');
        $this->form_validation->set_rules('leave_type', 'Leave Type', 'trim|required');
        //show($this->input->post("leave_type"));
        $this->data['start_date'] = $this->input->post("start_date");
        $this->data['end_date'] = $this->input->post("end_date");
        $this->data['leave_type'] = $this->input->post("leave_type");
        $this->data['duration'] = $this->input->post("duration");
        $this->data['sup_designation'] = $this->input->post("sup_designation");
        $this->data['nominated_person'] = $this->input->post("nominated_person");

        if ($this->form_validation->run() == FALSE) {
            //$this->data['start_date'] = $this->input->post("start_date");
            //$this->data['end_date'] = $this->input->post("end_date");
            //$this->data['leave_type'] = $this->input->post("leave_type");
            $this->middle = 'user/leave/application';
            $this->layout();
        } else {

            $id = $this->session->userdata("employeeId");
            $branch_name = $this->session->userdata("bName");
            $leavePackage = $this->session->userdata("leave_package");
            $start_date = $this->input->post("start_date");
            $end_date = $this->input->post("end_date");
            $sexplode = explode("-", $start_date);
            $syear = $sexplode[0];
            $eexplode = explode("-", $end_date);
            $eyear = $eexplode[0];
            if ($syear != $eyear || ($end_date < $start_date)) {
                $this->session->set_flashdata('message', '<div id="message">Incorrect Date Combination.</div>');
                redirect('user/leave/apply', 'location', 301);
            } else {
                $leave_type = $this->input->post("leave_type");
                if ($leave_type != "2"){

                    date_default_timezone_set("Asia/Dhaka");
                    $sysdate = date("Y-m-d");
                    $timestamp = strtotime($sysdate);
                    $submission_approve_date = date("Y-m-d", strtotime("+2 Day", $timestamp));
                    // $sysdate = date("Y-m-d");
                    //  $date = new DateTime($sysdate);
                    //  $date->add(new DateInterval('P2D')); //adding two days
                    //   $submission_approve_date = $date->format('Y-m-d');

                    if ($start_date < $submission_approve_date){
                        $this->session->set_flashdata('message', '<div id="message">Sorry! Date must be at least two days greater from current date.</div>');

                        $this->middle = 'user/leave/application';
                        $this->layout();
                        return false;
                    };
                };
                //$holiday = $this->db->query("select * from add_holiday where start_date between '$start_date' and '$end_date'");
                $holiday = $this->leave_model->get_holiday_info_for_leave_application($start_date,$end_date);
                //show($holiday);
                if ($holiday){
                    $this->session->set_flashdata('message', '<div id="message">Sorry! Holidays Are not allowed for leave.</div>');
                    $this->middle = 'user/leave/application';
                    $this->layout();
                    return false;
                    /*redirect('user/leave/apply', 'location', 301);
                    exit;*/
                };
                $query = $this->db->query("select * from add_weekend where weekend_date between '$start_date' and '$end_date' and branch_name='" . $this->session->userdata("bName") . "'");
                $weekend=$query->result();
                //$weekend = $this->leave_model->get_weekend_info_for_leave_application($start_date,$end_date,$branch_name);

                if ($weekend) {
                    $this->session->set_flashdata('message', '<div id="message">Sorry! Weekend Are not allowed for leave.</div>');
                    $this->middle = 'user/leave/application';
                    $this->layout();
                    return false;
                    /*redirect('user/leave/apply', 'location', 301);
                    exit;*/
                };

                $lduration = $this->input->post("duration");
                //$query = $this->db->query("select category from employeeinfo where id='" . $id . "'");
                $category_info = $this->leave_model->get_category_info_for_leave_application($id);
                $category = "";
                foreach ($category_info as $info):$category = $info->category;
                endforeach;

                //$q1 = $this->db->query("select leave_duration from leave_management where year='" . $syear . "' and leave_type='" . $leave_type . "'and emp_id='" . $this->session->userdata("employeeId") . "' and hr_status=2");
                $leave_duration_info = $this->leave_model->get_leave_duration_info_for_leave_application($syear,$leave_type,$id);
                $duration = 0;
                foreach ($leave_duration_info as $leave) {
                    $duration = $duration + $leave->leave_duration;
                };
                //show($duration);
                $package_name = "";
                $casual = "";
                $medical = "";
                $earned = "";
                //$allocate = $this->db->query("select * from add_leave where id='" . $this->session->userdata("leave_package") . "'");
                //show($leavePackage);
                $allocate_info = $this->leave_model->get_leave_package_info_for_leave_application($leavePackage);
                foreach ($allocate_info as $row1) {
                    $package_name = $row1->package_name;
                    $casual = $row1->casual;
                    $medical = $row1->medical;
                    $earned = $row1->earned;
                }
                if ($leave_type == '1') {
                    $l_exists = $casual - $duration;
                } elseif ($leave_type == '2') {
                    $l_exists = $medical - $duration;
                } else if ($leave_type == '3') {
                    $l_exists = $earned - $duration;
                } else {
                    $l_exists = 0;
                }
                if ($l_exists <= 0) {
                    $l_exists = 0;
                }
                $cNo = $this->session->userdata("employeeId");
                $leaveSchedule = $this->db->query("select * from leave_schedule where '$start_date' between start_date and end_date");
                //$leaveSchedule_info = $this->leave_model->get_leave_schedule_info_for_leave_application($start_date);
                /*echo '<pre>';
                var_dump($leaveSchedule);
                echo '<pre>';
                exit;*/
                foreach ($leaveSchedule->result() as $scheduleInfo){
                    $first_id = $scheduleInfo->q_id;
                };
                $leaveSchedule1 = $this->db->query("select * from leave_schedule where '$end_date' between start_date and end_date");
                //$leaveSchedule1_info = $this->leave_model->get_leaveSchedule1_info_for_leave_application($end_date);
                foreach ($leaveSchedule1->result() as $scheduleInfo1){
                    $last_id = $scheduleInfo1->q_id;
                };
                if ($first_id != $last_id) {
                    $this->session->set_flashdata('message', '<div id="message">Date Range must be in same quarter</div>');
                    redirect('user/leave/apply', 'location', 301);
                } else {
                    $ltype = $this->input->post("leave_type");
                    $f1 = 0;
                    $f2 = 0;
                    $f3 = 0;
                    //$firstQuarter = $this->db->query("select * from leave_management where year='" . $syear . "'and emp_id='" . $cNo . "'and quarter_id=1 and hr_status=2 and leave_type='" . $ltype . "'");
                    $firstQuarter_info = $this->leave_model->get_firstQuarter_info_for_leave_application($syear,$cNo,$ltype);
                    foreach ($firstQuarter_info as $t1){
                        $f1 = $f1 + ($t1->leave_duration);
                    };
                    //$secondQuarter = $this->db->query("select * from leave_management where year='" . $syear . "'and emp_id='" . $cNo . "'and quarter_id=2 and hr_status=2 and leave_type='" . $ltype . "'");
                    $secondQuarter_info = $this->leave_model->get_secondQuarter_info_for_leave_application($syear,$cNo,$ltype);
                    foreach ($secondQuarter_info as $t2){
                        $f2 = $f2 + ($t2->leave_duration);
                    };
                    //$thirdQuarter = $this->db->query("select * from leave_management where year='" . $syear . "'and emp_id='" . $cNo . "'and quarter_id=3 and hr_status=2 and leave_type='" . $ltype . "'");
                    $thirdQuarter_info = $this->leave_model->get_thirdQuarter_info_for_leave_application($syear,$cNo,$ltype);
                    foreach ($thirdQuarter_info as $t3) {
                        $f3 = $f3 + ($t3->leave_duration);
                    };
                    // echo $f1 . "," . $f2 . ":" . $f3;
                    // die();
                    //Calculation for casual
                    if ($ltype == "1") {
                        if ($first_id == "1") {
                            if ($f1 >= 2) {
                                $remain_leave_in_quarter = 0;
                            } else {
                                $remain_leave_in_quarter = 2 - $f1;
                            }
                        } elseif ($first_id == "2") {
                            if ($f1 < 2) {
                                $f1_remain = 2 - $f1;
                            } else {
                                $f1_remain = 0;
                            }
                            $total_f2 = 2 + $f1_remain;
                            if ($f2 >= $total_f2) {
                                $remain_leave_in_quarter = 0;
                            } else {
                                $remain_leave_in_quarter = $total_f2 - $f2;
                            }
                        } elseif ($first_id == "3") {
                            if ($f1 < 2) {
                                $f1_remain = 2 - $f1;
                            } else {
                                $f1_remain = 0;
                            }

                            $total_f2 = 2 + $f1_remain;
                            if ($f2 >= $total_f2) {
                                $f2_remain = 0;
                            } else {
                                $f2_remain = $total_f2 - $f2;
                            }
                            $total_f3 = 3 + $f2_remain;
                            if ($f3 >= $total_f3) {
                                $remain_leave_in_quarter = 0;
                            } else {
                                $remain_leave_in_quarter = $total_f3 - $f3;
                            }
                        } else {
                            $remain_leave_in_quarter = 0;
                        }
                    }
                    //end casual calculation
                    //Calculation for sick
                    else if ($ltype == "2") {
                        if ($first_id == "1") {
                            if ($f1 >= 3) {
                                $remain_leave_in_quarter = 0;
                            } else {
                                $remain_leave_in_quarter = 3 - $f1;
                            }
                        } elseif ($first_id == "2") {
                            if ($f1 < 3) {
                                $f1_remain = 3 - $f1;
                            } else {
                                $f1_remain = 0;
                            }
                            $total_f2 = 2 + $f1_remain;
                            if ($f2 >= $total_f2) {
                                $remain_leave_in_quarter = 0;
                            } else {
                                $remain_leave_in_quarter = $total_f2 - $f2;
                            }
                        } elseif ($first_id == "3") {
                            if ($f1 < 3) {
                                $f1_remain = 3 - $f1;
                            } else {
                                $f1_remain = 0;
                            }

                            $total_f2 = 2 + $f1_remain;
                            if ($f2 >= $total_f2) {
                                $f2_remain = 0;
                            } else {
                                $f2_remain = $total_f2 - $f2;
                            }
                            $total_f3 = 2 + $f2_remain;
                            if ($f3 >= $total_f3) {
                                $remain_leave_in_quarter = 0;
                            } else {
                                $remain_leave_in_quarter = $total_f3 - $f3;
                            }
                        } else {
                            $remain_leave_in_quarter = 0;
                        }
                    }
                    //end sick calculation
                    //Calculation for earned
                    elseif ($ltype == "3") {
                        if ($first_id == "1") {
                            if ($f1 >= 2) {
                                $remain_leave_in_quarter = 0;
                            } else {
                                $remain_leave_in_quarter = 2 - $f1;
                            }
                        } elseif ($first_id == "2") {
                            if ($f1 < 2) {
                                $f1_remain = 2 - $f1;
                            } else {
                                $f1_remain = 0;
                            }
                            $total_f2 = 3 + $f1_remain;
                            if ($f2 >= $total_f2) {
                                $remain_leave_in_quarter = 0;
                            } else {
                                $remain_leave_in_quarter = $total_f2 - $f2;
                            }
                        } elseif ($first_id == "3") {
                            if ($f1 < 2) {
                                $f1_remain = 2 - $f1;
                            } else {
                                $f1_remain = 0;
                            }

                            $total_f2 = 3 + $f1_remain;
                            if ($f2 >= $total_f2) {
                                $f2_remain = 0;
                            } else {
                                $f2_remain = $total_f2 - $f2;
                            }
                            $total_f3 = 2 + $f2_remain;
                            if ($f3 >= $total_f3) {
                                $remain_leave_in_quarter = 0;
                            } else {
                                $remain_leave_in_quarter = $total_f3 - $f3;
                            }
                        } else {
                            $remain_leave_in_quarter = 0;
                        }
                    } else {
                        $remain_leave_in_quarter = 0;
                    }

                    if ($lduration > $remain_leave_in_quarter) {
                        //$this->session->set_flashdata('message', '<div id="message">Sorry! Leave is  not available for apply.</div>');
                        // redirect('user/leave/apply', 'location', 301);
                        // exit();
                        $extra_days = $lduration - $remain_leave_in_quarter;
                        $this->data['message'] = "Your remaining leave days  in This Quarter is $remain_leave_in_quarter. You applied for '$extra_days' Days extra leave.";
                        $this->data['start_date'] = $this->input->post('start_date');
                        $this->data['end_date'] = $this->input->post('end_date');
                        $this->data['leave_type'] = $this->input->post('leave_type');
                        $this->data['leave_duration'] = $this->input->post('duration');
                        $this->data['extra_days'] = $extra_days;
                        $this->data['nominated_person'] = $this->input->post("nominated_person");
                        $this->data['year'] = $syear;
                        $this->data['quater_id'] = $first_id;
                        $this->data['status'] = 1;
                        $this->middle = 'user/leave/confirmation';
                        $this->layout();
                    } else {
                        if ($lduration > $l_exists) {
                            //$this->session->set_flashdata('message', '<div id="message">Sorry! Leave is  not available for apply.</div>');
                            // redirect('user/leave/apply', 'location', 301);
                            // exit();
                            $extra_days = $lduration - $l_exists;
                            $this->data['message'] = "Sorrry! According to your leave package no enough leave remain. You applied for '$extra_days' Days extra leave.";
                            $this->data['start_date'] = $this->input->post('start_date');
                            $this->data['end_date'] = $this->input->post('end_date');
                            $this->data['leave_type'] = $this->input->post('leave_type');
                            $this->data['leave_duration'] = $this->input->post('duration');
                            $this->data['extra_days'] = $extra_days;
                            $this->data['nominated_person'] = $this->input->post("nominated_person");
                            $this->data['year'] = $syear;
                            $this->data['quater_id'] = $first_id;
                            $this->data['status'] = 0;
                            $this->middle = 'user/leave/confirmation';
                            $this->layout();
                        } else {
                            $data = array(
                                'card_no' => $this->session->userdata("card_no"),
                                'emp_id' => $this->session->userdata("employeeId"),
                                'leave_duration' => $lduration,
                                'leave_type' => $this->input->post("leave_type"),
                                'category' => 2,
                                'start_date' => $this->input->post("start_date"),
                                'end_date' => $this->input->post("end_date"),
                                'app_date' => date("Y-m-d"),
                                'year' => $syear,
                                'quarter_id' => $first_id,
                                'nominated_person' => $this->input->post("nominated_person"),
                                'supervisor_status' => 1,
                                'hr_status' => 1
                            );
                            $this->shows->EntryAll("leave_management", $data);

                            $authorized_name = "";
                            if ($this->input->post("nominated_person") != " ") {
                                $nid = $this->input->post("nominated_person");
                                $query = $this->leave_model->get_emp_info1($nid);
                                foreach ($query as $info){
                                    $authorized_name = $info->name;
                                }
                            }

                            //Start mail sent to supervisor
                            $sid = $this->session->userdata("employeeId");
                            $supInfo = $this->leave_model->get_emp_info1($sid);
                            $supervisor = "";
                            foreach ($supInfo as $sInfo){
                                $supervisor = $sInfo->supervisor;
                            }
                            $supMail = $this->leave_model->get_emp_info1($supervisor);
                            $supmail = "";
                            $supname = "";
                            foreach ($supMail as $sMail){
                                $supmail = $sMail->email;
                                $supname = $sMail->name;
                            }
                            if ($supmail != "") {
                                $applicant_name = $this->session->userdata('name');
                                $to = $supmail;
                                $subject = "Leave Notification";
                                $message = "                            
                              Dear Mr./Ms. ($supname) ,";

                                $message.="<br/>
							  You are being requested by (Mr./Ms $applicant_name) for granting his/her leave application. 
   Please review the leave status of Mr./Ms. ($applicant_name) and convey a 
       decision regarding this within 24 hours. 
       Please also check with designated alternative official, Mr./Ms $authorized_name about his/her availability and consent of the additional responsibilities.
         PLEASE DO NOT CONSIDER THIS LEAVE APPLICATION AS CONFIRM TILL WORK FORCE MANAGEMENT NOTIFICATION EMAIL.";
                                $message.="<br/>This is auto generated mail. Pls donot reply to this mail";
                                $headers = 'MIME-Version: 1.0' . "\r\n";
                                $headers .= 'Content-type: text/html; charset=UTF-8' . "\r\n";
                                // Additional headers
                                $headers .= 'To: Customer <' . $to . "\r\n";
                                $headers .= 'From: info@charcoalboard.com <info@charcoalboard.com>' . "\r\n";
                                // Mail it
                                mail($to, $subject, $message, $headers);
                            }
                            //End supervisor mail sent
                            //Start Send mail to applicant

                            $applicant_name = $this->session->userdata('name');
                            $sid = $this->session->userdata("employeeId");
                            $EInfo = $this->leave_model->get_emp_info1($sid);
                            $empmail = "";
                            foreach ($EInfo as $eInfo):
                                $empmail = $eInfo->email;
                            endforeach;
                            $to1 = $empmail;
                            $subject1 = "Leave Notification";
                            $message1 = "                            
                              Dear Mr./Ms. ($applicant_name) ,";

                            $message1.="<br/>
							  Thanks for your leave application. Your leave application will be forwarded to your immediate supervisor for approval. 
                                After the approval of the supervisor the application will be forwarded to the WORK FORCE MANAGEMENT Department for further process. 
                                PLEASE DON'T CONSIDER THIS MAIL AS AN APPROVAL OR REJECTION OF THE LEAVE REQUEST. WAIT FOR THE FINAL CONFIRMATION EMAIL FROM WORK FORCE MANAGEMENT.         ";
                            $message1.="<br/>This is auto generated mail. Pls donot reply to this mail";
                            $headers1 = 'MIME-Version: 1.0' . "\r\n";
                            $headers1 .= 'Content-type: text/html; charset=UTF-8' . "\r\n";
                            // Additional headers
                            $headers1 .= 'To: Customer <' . $to1 . "\r\n";
                            $headers1 .= 'From: info@charcoalboard.com <info@charcoalboard.com>' . "\r\n";
                            // Mail it
                            mail($to1, $subject1, $message1, $headers1);
                            //end applicant message
                            //alternative official mail
                            $nominatedPerson = $this->input->post("nominated_person");
                            if ($nominatedPerson > 0) {
                                $nEmail = "";
                                $nName = "";
                                $niminatedInfo = $this->leave_model->get_emp_info1($nominatedPerson);
                                foreach ($niminatedInfo as $nTemp):
                                    $nEmail = $nTemp->email;
                                    $nName = $nTemp->name;
                                endforeach;
                                $applicant_name = $this->session->userdata('name');
                                $to2 = $nEmail;
                                $subject2 = "Leave Notification";
                                $message2 = "Dear Concern,";
                                $message2.="Mr./Mrs ('$applicant_name') has applied for a leave from ('$start_date') to ('$end_date'). His/Her application is still pending for approval. 
                                        You are mentioned as the alternative person in charge by the applicant during his/her absent. Please talk to the the applicant or his/her line manager, if you are not available or unable to take this responsibility by 24 hours. Otherwise, it will be effected. This e-mail has been generated for your information and necessary action. Don’t reply to this email address.";


                                $headers2 = 'MIME-Version: 1.0' . "\r\n";
                                $headers2 .= 'Content-type: text/html; charset=UTF-8' . "\r\n";
                                // Additional headers
                                $headers2 .= 'To: Customer <' . $to2 . "\r\n";
                                $headers2 .= 'From: info@charcoalboard.com <info@charcoalboard.com>' . "\r\n";

                                // Mail it
                                mail($to2, $subject2, $message2, $headers2);
                            }
                            //end alternative official mail


                            $this->session->set_flashdata('message', '<div id="message">Your Application Submitted Successfully.</div>');
                            redirect('user/leave/apply', 'location', 301);
                        }
                    }
                }
            }
        }
    }

    function approve($id = NULL) {
        $obj_leave_manage = $this->leave_model->get_leave_manage($id);
        $emp_id =  $obj_leave_manage[0]->emp_id;

        $this->data['title'] = 'application';
        $data = array(
            'supervisor_status' => 2
        );
        $this->leave_model->update_leave_management($data, $id);
        $emp_info = $this->leave_model->get_emp($emp_id);
        $name = "";
        $email = "";
        foreach ($emp_info as $info):
            $name = $info->name;
            $email = $info->email;

        endforeach;

        //start mail to applicant
        $to1 = $email;
        $subject1 = "Leave Notification";
        $message1 = "                            
          Dear Mr./Ms. ( $name ) ,";
        $message1.="<br/>
          Thanks for your leave application. Your leave application has been approved by the supervisor and now the application is forwarded to the WORK FORCE MANAGEMENT Department for final approval. WAIT FOR THE FINAL CONFIRMATION EMAIL FROM WORK FORCE MANAGEMENT.       ";
        $message1.="<br/>Thanks for your cooperation";
        $message1.="<br/>This is auto generated mail. Pls donot reply to this mail";
        $headers1 = 'MIME-Version: 1.0' . "\r\n";
        $headers1 .= 'Content-type: text/html; charset=UTF-8' . "\r\n";
        // Additional headers
        $headers1 .= 'To: Customer <' . $to1 . "\r\n";
        $headers1 .= 'From: info@charcoalboard.com <info@charcoalboard.com>' . "\r\n";
        // Mail it
        mail($to1, $subject1, $message1, $headers1);
        //end  mail to applicant
        //start mail to hr
        $hr_email = "hr@windmill.com.bd";
        $to = $hr_email;
        $subject = "Leave Notification";
        $message = " Dear recipient,";

        $message.="<br/>A leave request of   Mr./Ms. ( $name ) is pending in the leave approval process. Pls approve/deny the leave after checking the following details: ";
        $message.="<br/>  Duration of leave";
        $message.="<br/>Approval from the immediate supervisor ";
        $message.="<br/> Availability of leaves of the official";
        $message.="<br/> Arrangement of alternative official during leave";
        $message.="<br/> Notification period before ty he leave enjoyed";
        $message.="<br/> Consumed leave or future leave";

        $message.="<br/>Pls note that as early the leave will be approved the concern employees will get notification for that.";
        $message.="<br/>This is auto generated mail. Pls donot reply to this mail";
        $headers = 'MIME-Version: 1.0' . "\r\n";
        $headers .= 'Content-type: text/html; charset=UTF-8' . "\r\n";
        // Additional headers
        $headers .= 'To: Customer <' . $to . "\r\n";
        $headers .= 'From: info@charcoalboard.com <info@charcoalboard.com>' . "\r\n";
        // Mail it
        mail($to, $subject, $message, $headers);

        //end hr
        //start nominated person mail
        $startDate = "";
        $endDate = "";
        $leave_duration = "";
        $nominated_person = "";
        //$lDetails = $this->leave_model->getLeaveDetails($id);
        $lDetails = $this->leave_model->get_leave_manage($id);
        /*echo '<pre>';
        var_dump($lDetails);
        echo '</pre>';
        exit;*/
        foreach ($lDetails as $l_details):
            $startDate = $l_details->start_date;
            $endDate = $l_details->end_date;
            $leave_duration = $l_details->leave_duration;
            $nominated_person = $l_details->nominated_person;
        endforeach;
        if ($nominated_person != " ") {
            $nid = $nominated_person;
            $nominated_info = $this->leave_model->get_emp_info1($nid);
            $n_name = "";
            $n_email = "";
            foreach ($nominated_info as $info):
                $n_name = $info->name;
                $n_email = $info->email;
            endforeach;


            $to = $n_email;
            $subject = " Notification";
            $message = "Dear Concern,";
            $message.="Mr./Mrs ('$name') has applied for a leave from ('$startDate') to ('$endDate'). His/Her application has been approved by his/her line manager and waiting for management approval. You are mentioned as the alternative person in charge by the applicant during his/her absent. Please talk to the the applicant or his/her line manager, if you are not available or unable to take this responsibility by 24 hours. Otherwise, it will be effected. This e-mail has been generated for your information and necessary action. Don’t reply to this email address.";
            $headers = 'MIME-Version: 1.0' . "\r\n";
            $headers .= 'Content-type: text/html; charset=UTF-8' . "\r\n";
            // Additional headers
            $headers .= 'To: Customer <' . $to . "\r\n";
            $headers .= 'From: info@charcoalboard.com <info@charcoalboard.com>' . "\r\n";

            // Mail it
            mail($to, $subject, $message, $headers);
        }
        redirect('user/leave/view_application', 'location', 301);
    }

    function deny($id = NULL, $emp_id = NULL) {
        $this->data['title'] = 'application';
        $data = array(
            'supervisor_status' => 3,
        );

        $this->leave_model->update_leave_management($data, $id);
        $emp_info = $this->leave_model->get_emp($emp_id);
        $name = "";
        $email = "";
        foreach ($emp_info as $info):
            $name = $info->name;
            $email = $info->email;

        endforeach;
        $to = $email;
        $subject = "Leave Notification";
        $message = "Dear Mr./Ms. ( $name )";
        $message.="<br/> Thanks for your leave application. Please not that your leave has been DECLINED from management. Please consult your line manager and WORK FORCE MANAGEMENT to understand the reasoning. Your available leave till today can be checked by log on to your leave status.";
        $message.="<br/>  Please note that a notification mail has also been sent to your proposed assigned alternative official, your line manager and management for their record";
        $message.="<br/>Management express regrets for any inconvenience caused.";
        $message.="<br/>This is auto generated mail. Pls donot reply to this mail";
        $headers = 'MIME-Version: 1.0' . "\r\n";
        $headers .= 'Content-type: text/html; charset=UTF-8' . "\r\n";
        // Additional headers
        $headers .= 'To: Customer <' . $to . "\r\n";
        $headers .= 'From: info@charcoalboard.com <info@charcoalboard.com>' . "\r\n";
        // Mail it
        mail($to, $subject, $message, $headers);
        redirect('user/leave/view_application', 'location', 301);
    }

    function  change_password() {
        $this->data['title'] = 'Change Password';
        $this->middle = 'user/password';
        $this->layout();
    }

    function changed_password() {
        $this->data['title'] = 'Change Password';

        //$this->load->library('form_validation');
        $this->form_validation->set_rules('old_password', 'Old Password', 'trim|required');
        $this->form_validation->set_rules('new_password', 'New Password', 'trim|required|min_length[3]|max_length[20]|matches[confirm_password]');
        $this->form_validation->set_rules('confirm_password', 'Confirm Password', 'trim|required');

        if ($this->form_validation->run() == FALSE) {
            $this->middle = 'user/password';
            $this->layout();
        } else {
            $old_password = $this->input->post('old_password');
            $id = $this->session->userdata("employeeId");
            $query = $this->db->query("select * from employeeinfo where  id='" . $this->session->userdata("employeeId") . "'and password='" . $old_password . "'");
            if ($query->num_rows() == 0) {
                $this->session->set_flashdata('message', '<div id="message">Old Password doesnt match.</div>');
                redirect('user/leave/change_password');
            } else {
                $data = array(
                    'password' => $this->input->post('new_password')
                );
                $this->leave_model->password_update($data, $id);
                $this->session->set_flashdata('message', '<div id="message"> Password  Changed Successfully.</div>');
                redirect('user/leave/change_password');
            }
        }
    }

}
