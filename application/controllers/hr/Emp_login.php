<?php
defined('BASEPATH') OR exit('No direct script access allowed');


class Emp_login extends CI_Controller {

	function test1(){
		$this->load->view("hr/emp_login/test2");
	}

	function index() {
		
		if ($this->session->userdata('employeeId') != "") {
			//redirect('hr/emp_login/index');
			//redirect('user/home/index');
			redirect('../user/home/index');
		}
		$data = array(
			'title' => 'Login'
		);
		$this->load->view("hr/emp_login/login",$data);
	}

	function process_login() {
		$username = $this->input->post('username');
		$password = $this->input->post('password');
		$query1 = $this->db->query("SELECT * FROM employeeinfo WHERE username='$username' AND password='$password' AND status=0");
		if ($query1->num_rows() == 1) {
			foreach ($query1->result() as $info):
				$id = $info->id;
				$name = $info->name;
				$card_no = $info->card_no;
				$leavePackage = $info->leave_name;
				$branch_name = $info->branch_name;
				$secreteNo = $info->secrete_no;
			endforeach;
			$data = array(
				'name' => $name,
				'employeeId' => $id,
				'card_no' => $card_no,
				'leave_package' => $leavePackage,
				'bName' => $branch_name,
				'secreteNo' => $secreteNo,
				'loggedin' => TRUE
			);
			$this->session->set_userdata($data);
			redirect('../user/home/index');
		} else {
			$this->session->set_flashdata('message', '<div id="message">Username or password is incorrect, please try again.</div>');
			redirect('../hr/emp_login/index');
		}
	}

	function logout() {
		$this->session->sess_destroy();
		redirect('../hr/emp_login/index');
	}

}


