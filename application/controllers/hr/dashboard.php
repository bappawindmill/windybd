<?php

class Dashboard extends MY_Controller {

    public function __construct() {
        parent::__construct();
        if ($this->session->userdata('logged_in') != TRUE) {
            redirect('hr/login');
        }
        if ($this->session->userdata('leave_prev') != "1") {
            redirect('hr/hr/index');
        }
        $this->load->model('hr/dashboard_model');
    }

    public function index(){
        redirect('hr/dashboard/dashboard_list');
    }

    function dashboard_list() {
        $this->data['mainTab'] = 'leave';
        $this->data['activeTab'] = 'dashboard_list';
        $this->data['query'] = $this->dashboard_model->get_all();
        $this->pagination->initialize($this->pagination_library->getArray());
        $this->middle = 'hr/dashboard/view_dashboard';
        $this->layout();
    }

    function added_dashboard($start = 0) {
        $this->data['mainTab'] = 'leave';
        $this->data['activeTab'] = 'dashboard';
        $this->load->library('form_validation');
        $this->form_validation->set_rules('message', 'Message', 'trim|required');
        $this->form_validation->set_rules('id', 'id', 'trim');

        if ($this->form_validation->run() == FALSE) {
            $this->data['query'] = $this->dashboard_model->get_all();
            $this->middle = 'hr/dashboard/add_dashboard';
            $this->layout();
        } else {
            $data = array(
                'title' => $this->input->post('title'),
                'message' => $this->input->post('message')
            );
            $this->dashboard_model->message_insert($data);
            redirect('hr/dashboard/dashboard_list', 'location', 301);
        }
    }

    function department_edit($id = NULL) {
        $this->data['mainTab'] = 'configuration';
        $this->data['activeTab'] = 'add_department';
        $this->load->model('hr/department_model');
        if ($this->input->post('mysubmit')) {
            if ($this->input->post('id')) {
                $insert = array('name' => $this->input->post('name')
                );

                $this->department_model->edit_department($insert);
                redirect('hr/department/add_department', 'location', 301);
            }
        }

        if ($id != NULL) {
            $query = $this->department_model->get_id($id);
            $this->data['fid']['value'] = $query['id'];
            $this->data['fname']['value'] = $query['name'];
            $this->data['dynamicView'] = 'hr/department/department_edit';
            $this->_commonPageLayout('frontend_viewer');

            // $this->load->view('hr/department/department_edit', $data);
        }
    }

    function delete($id = NULL) {
        $this->data['mainTab'] = 'leave';
        $this->data['activeTab'] = 'dashboard';
        $this->dashboard_model->delete($id);
        redirect('hr/dashboard/dashboard_list', 'location', 301);
    }


}

?>