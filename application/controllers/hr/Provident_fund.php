<?php

class Provident_fund extends MY_Controller {

    public function __construct() {
        parent::__construct();
        if ($this->session->userdata('logged_in') != TRUE) {
            redirect('hr/login');
        }

        $this->load->model('hr/provident_fund_model');
    }

    function index() {
        $this->load->view('hr/home');
    }

    function add() {
        if ($this->session->userdata('conf_prev') != "1") {
            redirect('hr/hr/index');
            exit();
        }
        $this->load->library('form_validation');
        $this->form_validation->set_rules('employee_rate', 'Employee Rate', 'trim|required');
        $this->form_validation->set_rules('employeer_rate', 'Employeer Rate', 'trim|required');
        $this->form_validation->set_rules('maturity', 'maturity', 'trim|required');
        $this->data['title'] = 'PF Rate';
        $this->data['mainTab'] = 'configuration';
        $this->data['activeTab'] = 'provident_fund_rate';
        $this->data['query'] = $this->provident_fund_model->getAll();
        if ($this->form_validation->run() == FALSE) {
            $this->middle = 'hr/providentFund/edit_pf_rate';
            $this->layout();
        } else {

            if (count($this->data['query'])):
                $this->middle = 'hr/providentFund/edit_pf_rate';
                $this->layout();
            else:
                $this->middle = 'hr/providentFund/entry_pf_rate';
                $this->layout();
            endif;
        }
    }


    function entry() {
        if ($this->session->userdata('conf_prev') != "1") {
            redirect('hr/hr/index');
            exit();
        }
        $this->load->library('form_validation');
        $this->form_validation->set_rules('employee_rate', 'Employee Rate', 'trim|required');
        $this->form_validation->set_rules('employeer_rate', 'Employeer Rate', 'trim|required');
        $this->form_validation->set_rules('maturity', 'maturity', 'trim|required');
        $this->data['mainTab'] = 'configuration';
        $this->data['activeTab'] = 'provident_fund';
        if ($this->form_validation->run() == FALSE) {
            $this->middle = 'hr/providentFund/entry_pf_rate';
            $this->layout();
        } else {
            $data = array(
                'employee_rate' => $this->input->post('employee_rate'),
                'employeer_rate' => $this->input->post('employeer_rate'),
                'maturity' => $this->input->post('maturity')
            );
            $this->provident_fund_model->insert_pf($data);
            redirect('hr/provident_fund/add', 'location', 301);
        }
    }

    function update($id = NULL) {
        if ($this->session->userdata('conf_prev') != "1") {
            redirect('hr/hr/index');
            exit();
        }
        $this->data['mainTab'] = 'configuration';
        $this->data['activeTab'] = 'provident_fund';
        $insert = array(
            'employee_rate' => $this->input->post('employee_rate'),
            'employeer_rate' => $this->input->post('employeer_rate'),
            'maturity' => $this->input->post('maturity')
        );
        $this->load->model('hr/provident_fund_model');
        $this->provident_fund_model->edit_provident_fund($insert);
        redirect('hr/provident_fund/add', 'location', 301);
    }

    /* Provident Fund Report */

    function pf_form() {
        if ($this->session->userdata('report_prev') != "1") {
            redirect('hr/hr/index');
            exit();
        }
        $this->data['title'] = 'PF Report';
        $this->data['mainTab'] = 'report';
        $this->data['activeTab'] = 'provident_fund';
        $this->middle = 'hr/providentFund/report/pf_form';
        $this->layout();
    }

    function pf_viewed() {
        $this->data['title'] = 'PF Report';
        $this->data['mainTab'] = 'report';
        $this->data['activeTab'] = 'provident_fund';
        if ($this->session->userdata('report_prev') != "1") {
            redirect('hr/hr/index');
            exit();
        }
        $this->load->library('form_validation');
        $this->form_validation->set_rules('card_no', 'Card No', 'trim|required');
        if ($this->form_validation->run() == FALSE) {
            $this->data['mainTab'] = 'report';
            $this->data['activeTab'] = 'provident_fund';
            $this->middle = 'hr/providentFund/report/pf_form';
            $this->layout();
        } else {
            $card_no = $this->input->post("card_no");
            $this->data['query'] = $this->provident_fund_model->checkCardNo($card_no);
            if (count($this->data['query'])):
                foreach ($this->data['query'] as $empInfo):
                    $empId = $empInfo->id;
                    $confirmation_date = $empInfo->confirmation_date;
                endforeach;
                $this->data['allData'] = $this->provident_fund_model->getEmpProvidentAmount($empId, 'pf_amount');
                $providentAr = $this->provident_fund_model->getEmpProvidentAmount($empId, 'provident_fund');
                $maturity = 1095;
                if (count($providentAr) > 0) {
                    foreach ($providentAr as $row) {
                        $maturity = $row->maturity;
                    }
                }
                $c_date = date("Y-m-d");
               // $confirmation_date = "2010-10-01";
                $days = (strtotime($c_date) - strtotime($confirmation_date)) / (60 * 60 * 24);
                if ($days > $maturity) {
                    $this->middle = 'hr/providentFund/report/hr_pf_view';
                    $this->layout();
                } else {
                    $this->middle = 'hr/providentFund/report/hr_pf_view';
                    $this->layout();
                }
            else:
                $this->session->set_flashdata('message', '<div id="message">Invalid ID No.</div>');
                redirect('hr/provident_fund/pf_form', 'location', 301);
            endif;
        }
    }

}