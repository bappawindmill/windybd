<?php


class Monthly_salary extends MY_Controller {

    public function __construct() {
        parent::__construct();
        if ($this->session->userdata('logged_in') != TRUE) {
            redirect('hr/login');
        }
       if ($this->session->userdata('salary_prev') != "1") {
            redirect('hr/hr/index');
        }
        $this->load->model('hr/salary_model');
    }

    function index() {
        $this->load->view('hr/home');
    }
    function view_cash_sheet() {
        $this->data['title'] = 'View Cash Salary';
        $this->data['mainTab'] = 'salary';
        $this->data['activeTab'] = 'cash_sheet';
        $this->middle = 'hr/monthly_salary/cash/view_form';
        $this->layout();
    }

    function viewed_cash_sheet() {
        $this->data['title'] = 'Salary';
        $this->data['mainTab'] = 'salary';
        $this->data['activeTab'] = 'cash_sheet';
        $this->load->library('form_validation');
        $this->form_validation->set_rules('branch_name', 'Branch Name', 'trim|required');
        $this->form_validation->set_rules('year', 'Year', 'trim|required');
        $this->form_validation->set_rules('month', 'Month', 'trim|required');
        if ($this->form_validation->run() == FALSE) {
            $this->middle = 'hr/monthly_salary/cash/view_form';
            $this->layout();
        } else {
            $this->data['sbranch'] = $this->input->post("branch_name");
            $this->data['syear'] = $this->input->post("year");
            $this->data['month'] = $this->input->post("month");
            $year = $this->input->post("year");
            $month = $this->input->post("month");
            $dept = $this->input->post("dept");
            $this->data['sdept'] = $this->input->post("dept");
            if ($dept != "") {
                $this->data['query'] = $this->salary_model->getDeptWiseSalary($dept, $year, $month);
                $this->middle = 'hr/monthly_salary/cash/view_details_dept';
            } else {
                $branch = $this->input->post("branch_name");
                $this->data['query'] = $this->salary_model->getBranchWiseSalary($branch, $year, $month);
                $this->middle = 'hr/monthly_salary/cash/view_details_all';
            }
            $this->layout();
        }
    }


    function add_hourly_salary($start = 0) {
        $this->data['title'] = 'Monthly Salary';
        $this->data['mainTab'] = 'salary';
        $this->data['activeTab'] = 'create_salary_hourly';
        $this->middle = 'hr/salary/hourly_form';
        $this->layout();
    }

    function added_hourly_salary() {
        $this->load->library('form_validation');
        $this->form_validation->set_rules('year', 'Year', 'trim|required');
        $this->form_validation->set_rules('branch_name', 'Branch Name', 'trim|required');
        //$this->form_validation->set_rules('dept', 'Department', 'trim|required');
        $this->form_validation->set_rules('month', 'Month', 'trim|required');
        if ($this->form_validation->run() == FALSE) {
            $this->data['title'] = 'Monthly Salary';
            $this->data['mainTab'] = 'salary';
            $this->data['activeTab'] = 'create_salary_hourly';
            $this->middle = 'hr/salary/hourly_form';
            $this->layout();
        } else {
            set_time_limit("3000");
            $month = $this->input->post("month");
            $year = $this->input->post("year");
            $dept = $this->input->post("dept");
            $branch_name = $this->input->post("branch_name");
            if ($dept != "") {
                $this->shows->DeleteAll("dept = '$dept' and year = '$year' and month = '$month'", 'monthly_salary');
            } else {
                $this->shows->DeleteAll("branch_name = '$branch_name' and year = '$year' and month = '$month'", 'monthly_salary');
            }

            if ($month == '01') {
                $total_month_day = 31;
            } elseif ($month == '02') {
                $total_month_day = 28;
            } elseif ($month == '03') {
                $total_month_day = 31;
            } elseif ($month == '04') {
                $total_month_day = 30;
            } elseif ($month == "05") {
                $total_month_day = 31;
            } elseif ($month == "06") {
                $total_month_day = 30;
            } elseif ($month == "07") {
                $total_month_day = 31;
            } elseif ($month == "08") {
                $total_month_day = 31;
            } elseif ($month == "09") {
                $total_month_day = 30;
            } elseif ($month == "10") {
                $total_month_day = 31;
            } elseif ($month == "11") {
                $total_month_day = 30;
            } elseif ($month == "12") {
                $total_month_day = 31;
            } else {
                $total_month_day = 30;
            }
             if ($dept != "") {
                $query = $this->db->query("select * from employeeinfo where dept='" . $dept . "' and status=0");
            } else {
                $query = $this->db->query("select * from employeeinfo where branch_name='" . $branch_name . "'and status=0");
            }

            //start salary calculation
            foreach ($query->result() as $e_info) {
                $present = 0;
                $absent = 0;
                $late = 0;
                $leave = 0;
                for ($i = 1; $i <= $total_month_day; $i++) {
                    if ($i <= 9) {
                        $day = "0" . $i;
                    } else {
                        $day = $i;
                    }
                    $sdate = $year . "-" . $month . "-" . $day . "";
                    //check leave for this day
                    $empAttn = $this->db->query("select * from Attendence_emp where emp_id='" . $e_info->id . "'and att_date='" . $sdate . "'");

                    if ($empAttn->num_rows() > 0) {
                        foreach ($empAttn->result() as $temp) {
                            $attStatus = strtolower($temp->status);
                            $hrStatus = $temp->hr_approve;
                        }
                        if ($attStatus == "present" || $attStatus == "weekend" || $attStatus == "holiday") {
                           // $present = $present + 1; before added

							// edite start by joy
							if($hrStatus ==2){
								$absent = $absent + 1;

							}else{
								$present = $present + 1;
							}
							// edit end
                        } elseif ($attStatus == "late") {
                            $present = $present + 1;
                            if ($hrStatus == '2') {
                                $late = $late + 1;
                            }
                        } elseif ($attStatus == "absent") {
                            if ($hrStatus == '2') {
                                $absent = $absent + 1;
                            } elseif ($hrStatus == '1') {
                                $present = $present + 1;
                            } else {
                                $absent = $absent + 1;
                            }
                        } elseif ($attStatus == "leave") {

							if($e_info->category  == 2)
							{
								$absent = $absent + 1;

							}
							else
							{
								$present = $present + 1;
								$leave = $leave + 1;
							}
							/* $present = $present + 1;
							$leave = $leave + 1; */
                        } else {
                            $absent = $absent + 1;
                        }
                    } else {
                        $absent = $absent + 1;
                    }
                }
                //	echo "<br/>absent=" . $absent;
                //  echo "<br/>present=" . $present;
                // 	echo "<br/>late=" . $late;

				// edit by joy
				$grossMobileBill=$e_info->mobile_bill;
				$perDayMobileBill=$e_info->mobile_bill/$total_month_day;
				/* if($absent==0 AND ($total_month_day==31 OR $total_month_day==28)){
					$mobileBillPresent=30;
				}else{
					$mobileBillPresent=$present;
				} */

				$netMobileBill=$perDayMobileBill*$present;
                $mobileBill = $netMobileBill;
				$mobileBillDeduction=$perDayMobileBill*$absent;


                //$mobileBill = $e_info->mobile_bill;

                //arrear Info
                $arrearBill = 0;
                $arrearInfo = $this->salary_model->getArrear($e_info->id, $year, $month);
                foreach ($arrearInfo as $empArrear):
                    $arrearBill = $empArrear->amount;
                endforeach;
                //kpi Info
                $kpiBill = 0;
                $kpiInfo = $this->salary_model->getKpi($e_info->id, $year, $month);
                foreach ($kpiInfo as $empKpi):
                    $kpiBill = $empKpi->amount;
                endforeach;
                $kpiAdd = 0;
                $kpiInfo1 = $this->salary_model->getKpi1($e_info->id, $year, $month);
                foreach ($kpiInfo1 as $empKpi1):
                    $kpiAdd = $empKpi1->amount;
                endforeach;
                //advance info
                $advanceBill = 0;
                $advanceInfo = $this->salary_model->getAdvance($e_info->id, $year, $month);
                foreach ($advanceInfo as $empAdvance):
                    $advanceBill = $empAdvance->advance;
                endforeach;

                //retrive pf rate
                $empRate = 0;
                $employerRate = 0;
                $pfInfo = $this->salary_model->getPfRate();
                foreach ($pfInfo as $pInfo):
                    $empRate = $pInfo->employee_rate;
                    $employerRate = $pInfo->employeer_rate;
                endforeach;
                //tax deduction
                $taxBill = 0;
                if ($e_info->tax_deduction == '2') {
                    $taxBill = $e_info->tax_percentage;
                }
                //salary info
                $basic = $e_info->basic;
                $house_rent = $e_info->house_rent;
                $medical = $e_info->medical;
                $conveyance = $e_info->conveyance;
                $special = $e_info->special;
                $gross_salary = $basic + $house_rent + $medical + $conveyance + $special;

                // $gross_without_pf = $gross_salary - $totalpvovident_fund;
                $lateDays = 0;
                if ($late >= 3) {
                    $lateDays = intval($late / 3);
                }
                $loan_deduction = 0;
                $loanInfo = $this->salary_model->get_loan_info($e_info->id);
                //print_r($loanInfo);die();
                if (count($loanInfo)) {
                    $lDeduction = $this->salary_model->loanInfo($e_info->id, $year, $month);
                    if (count($lDeduction)) {
                        foreach ($lDeduction as $t1):
                            $loan_deduction = $t1->amount;
                        endforeach;
                    } else {
                        foreach ($loanInfo as $iInfo):
                            $loan_id = $iInfo->id;
                            $total_loan_amount = $iInfo->loan_amount;
                            $monthly_installment = $iInfo->monthly_installment;
                            $loan_year = $iInfo->year;
                            $loan_month = $iInfo->month;
                            $total_deduct = $iInfo->total_deduct;
                            $remain_amount = $iInfo->remain_amount;
                        endforeach;

                        if ($year > $loan_year) {
                            if ($remain_amount > 0) {
                                if ($remain_amount < $monthly_installment) {
                                    $loan_deduction = $remain_amount;
                                } else {
                                    $loan_deduction = $monthly_installment;
                                }
                                $loanData = array(
                                    'loan_id' => $loan_id,
                                    'emp_id' => $e_info->id,
                                    'year' => $year,
                                    'month' => $month,
                                    'amount' => $loan_deduction,
                                );
                                $this->shows->EntryAll("loan_deduction", $loanData);
                                $total_loan_deduct = $total_deduct + $loan_deduction;
                                $total_loan_remain = $total_loan_amount - $total_loan_deduct;
                                if ($total_loan_remain < 0) {
                                    $total_loan_remain = 0;
                                }
                                $loanUpdate = array(
                                    'total_deduct' => $total_loan_deduct,
                                    'remain_amount' => $total_loan_remain
                                );
                                $this->shows->UpdateAll("id = '$iInfo->id'", "loan", $loanUpdate);
                            }
                        } elseif ($year == $loan_year) {

                            if ($month >= $loan_month) {

                                if ($remain_amount > 0) {
                                    if ($remain_amount < $monthly_installment) {
                                        $loan_deduction = $remain_amount;
                                    } else {
                                        $loan_deduction = $monthly_installment;
                                    }
                                    $loanData = array(
                                        'loan_id' => $loan_id,
                                        'emp_id' => $e_info->id,
                                        'year' => $year,
                                        'month' => $month,
                                        'amount' => $loan_deduction,
                                    );
                                    $this->shows->EntryAll("loan_deduction", $loanData);

                                    $total_loan_deduct = $total_deduct + $loan_deduction;
                                    $total_loan_remain = $total_loan_amount - $total_loan_deduct;
                                    if ($total_loan_remain < 0) {
                                        $total_loan_remain = 0;
                                    }
                                    $loanUpdate = array(
                                        'total_deduct' => $total_loan_deduct,
                                        'remain_amount' => $total_loan_remain
                                    );
                                    $this->shows->UpdateAll("id = '$iInfo->id'", "loan", $loanUpdate);
                                }
                            }
                        } else {

                        }
                    }
                }
                //end loan Calculation
                //calculate bonus
                $totalDeductionDays = $absent + $lateDays;
                $perDayDeduction = $gross_salary / $total_month_day;
                $dayDeductionAmount = round($perDayDeduction * $totalDeductionDays);

                //pf calculation
                $emp_pf_amout = 0;
                $employeer_pf_amout = 0;
                $totalPf = 0;
                if ($e_info->provident_fund == '2'):
                    if ($e_info->confirmation_date > 0):
                        $emp_pf_amout = ($basic * $empRate) / 100;
                        $employeer_pf_amout = ($basic * $employerRate) / 100;
                        $totalPf = $emp_pf_amout + $employeer_pf_amout;
                        $checkEntry = $this->salary_model->getPfInfo($e_info->id, $year, $month);
                        if (!count($checkEntry)):
                            $pfData = array(
                                'emp_id' => $e_info->id,
                                'year' => $year,
                                'month' => $month,
                                'employee_amount' => $emp_pf_amout,
                                'employeer_amount' => $employeer_pf_amout,
                                'total_pf_amount' => $totalPf
                            );
                            $this->shows->EntryAll("pf_amount", $pfData);
                        endif;
                    endif;
                endif;

                //ot Calculation
                $ot = 0;
                $otInfo = $this->salary_model->get_ot_info($e_info->id, $year, $month);
                if (count($otInfo)):
                    foreach ($otInfo as $otData):
                        $ot = $ot + $otData->total_amount;
                    endforeach;
                endif;
                //bonus Calculation
                $bonusBill = 0;
                if ($e_info->confirmation_date > 0):
                    $current_date = $year . "-" . $month . "-30";
                    if ($current_date > $e_info->confirmation_date) {

			$date1 = date(strtotime($e_info->confirmation_date));
			$date2 = date(strtotime($current_date));
			$difference = $date2 - $date1;
			$yMonth = floor($difference / 86400 / 30 );

                      if ($yMonth == 0) {
                            $bonusBill = 0;
                        } else {
                            if ($yMonth % 6 == 0) {
                                $bonusBill = $e_info->basic;
                            }
                        }
                    }
                endif;
                //$grossPayable = $gross_salary + $grossMobileBill + $arrearBill + $bonusBill + $employeer_pf_amout + $kpiAdd + $ot;
                $grossPayableOnly = $gross_salary + $grossMobileBill + $arrearBill + $bonusBill + $employeer_pf_amout + $kpiAdd + $ot;
                $total_deduction_amount = $dayDeductionAmount + $advanceBill + $loan_deduction + $taxBill + $totalPf + $kpiBill+$mobileBillDeduction;
                $net_salary = $grossPayableOnly - $total_deduction_amount;
                $salaryData = array(
                    'branch_name' => $e_info->branch_name,
                    'dept' => $e_info->dept,
                    'year' => $year,
                    'month' => $month,
                    'emp_id' => $e_info->id,
                    'card_no' => $e_info->card_no,
                    'name' => $e_info->name,
                    'designation' => $e_info->designation,
                    'join_date' => $e_info->join_date,
                    'gross_salary' => $gross_salary,
                    'basic' => $e_info->basic,
                    'house_rent' => $e_info->house_rent,
                    'medical' => $e_info->medical,
                    'conveyance' => $e_info->conveyance,
                    'special' => $e_info->special,
                    'arrear' => $arrearBill,
                    'mobile_bill' => $e_info->mobile_bill,
                    'kpi' => $kpiBill,
                    'kpi_add' => $kpiAdd,
                    'gross_payable' => $grossPayableOnly,
                    'total_month_day' => $total_month_day,
                    'present' => $present,
                    'late' => $late,
                    'absent' => $absent,
                    'pf_employee' => $emp_pf_amout,
                    'pf_employeer' => $employeer_pf_amout,
                    'total_pf' => $totalPf,
                    'tax' => $taxBill,
                    'm_bill_d' => $mobileBillDeduction,
                    'total_deduction_amount' => $total_deduction_amount,
                    'absent_deduction' => $dayDeductionAmount,
                    'loan' => $loan_deduction,
                    'advance' => $advanceBill,
                    'bonus' => $bonusBill,
                    'ot' => $ot,
                    'net_salary' => round($net_salary)
                );
                $this->shows->EntryAll("monthly_salary", $salaryData);
            }
            $this->session->set_flashdata('message', '<div id="message">Salary Created Successfully.</div>');
            redirect('hr/monthly_salary/add_hourly_salary', 'location', 301);
        }
    }

    function view_hourly_salary() {
        $this->data['title'] = 'Salary';
        $this->data['mainTab'] = 'salary';
        $this->data['activeTab'] = 'view_hourly_salary';
        $this->middle = 'hr/monthly_salary/view_form';
        $this->layout();
    }

    function viewed_hourly_salary() {
        $this->data['title'] = 'Salary';
        $this->data['mainTab'] = 'salary';
        $this->data['activeTab'] = 'view_hourly_salary';
        $this->load->library('form_validation');
        $this->form_validation->set_rules('branch_name', 'Branch Name', 'trim|required');
        $this->form_validation->set_rules('year', 'Year', 'trim|required');
        $this->form_validation->set_rules('month', 'Month', 'trim|required');
        if ($this->form_validation->run() == FALSE) {
            $this->middle = 'hr/monthly_salary/view_form';
            $this->layout();
        } else {
            $this->data['sbranch'] = $this->input->post("branch_name");
            $this->data['syear'] = $this->input->post("year");
            $this->data['month'] = $this->input->post("month");
            $year = $this->input->post("year");
            $month = $this->input->post("month");
            $dept = $this->input->post("dept");
            $this->data['sdept'] = $this->input->post("dept");
            if($dept != "") {
                $this->data['query'] = $this->salary_model->getDeptWiseSalary($dept, $year, $month);
                $this->middle = 'hr/monthly_salary/view_details_dept';
            } else {
                $branch = $this->input->post("branch_name");
                $this->data['query'] = $this->salary_model->getBranchWiseSalary($branch, $year, $month);
                $this->middle = 'hr/monthly_salary/view_details_all';
            }
            $this->layout();
        }
    }

    function view_banksheet() {
        $this->data['title'] = 'View Bank Sheet';
        $this->data['mainTab'] = 'salary';
        $this->data['activeTab'] = 'banksheet';
        $this->middle = 'hr/monthly_salary/bank/view_form';
        $this->layout();
    }

    function viewed_banksheet() {
        $this->data['title'] = 'View Bank Sheet';
        $this->data['mainTab'] = 'salary';
        $this->data['activeTab'] = 'banksheet';
        $this->load->library('form_validation');
        $this->form_validation->set_rules('branch_name', 'Branch Name', 'trim|required');

        $this->form_validation->set_rules('year', 'Year', 'trim|required');
        $this->form_validation->set_rules('month', 'Month', 'trim|required');
        if ($this->form_validation->run() == FALSE) {
            $this->middle = 'hr/monthly_salary/bank/view_form';
            $this->layout();
        } else {
            $this->data['sbranch'] = $this->input->post("branch_name");
            $this->data['syear'] = $this->input->post("year");
            $this->data['month'] = $this->input->post("month");
            $year = $this->input->post("year");
            $month = $this->input->post("month");
            $dept = $this->input->post("dept");
            $this->data['sdept'] = $this->input->post("dept");
            if ($dept != "") {
                $this->data['query'] = $this->salary_model->getDeptWiseSalary($dept, $year, $month);
                $this->middle = 'hr/monthly_salary/bank/view_details_dept';
            } else {
                $branch = $this->input->post("branch_name");
                $this->data['query'] = $this->salary_model->getBranchWiseSalary($branch, $year, $month);
                $this->middle = 'hr/monthly_salary/bank/view_details_all';
            }

            $this->layout();
        }
    }


}