<?php



class Temp extends MY_Controller {

    public function __construct() {
        parent::__construct();
        if ($this->session->userdata('logged_in') != TRUE) {
            redirect('hr/login');
        }
        $this->load->model('hr/temp_model');
    }

    function index() {
        $this->load->view('hr/home');
    }

    function edit($id = NULL, $cardNo = NULL) {
        $this->data['query1'] = $this->temp_model->getallAcademic($id);
        $this->data['cno'] = $cardNo;
        $this->middle = 'hr/temp/academic';
        $this->layout();
    }

    function update() {
        $cardNo = $this->input->post("cno");
        $id = $this->input->post("id");
        $data = array(
            'exam_name' => $this->input->post("exam_name"),
            'institute_name' => $this->input->post('institute_name'),
            'result' => $this->input->post('result'),
            'passing_year' => $this->input->post('passing_year'),
            'note' => $this->input->post('note')
        );
        $this->shows->updateThisValue($data, "academic", "id = '$id'");
        redirect("hr/employee/empreport/" . $cardNo);
    }

    function qedit($id = NULL, $cardNo = NULL) {
        $this->data['query1'] = $this->temp_model->getallQualification($id);
        //print_r($this->data['query1']);die();
        $this->data['cno'] = $cardNo;
        $this->data['dynamicView'] = 'hr/temp/qualification';
        $this->_commonPageLayout('frontend_viewer');
    }

    function qupdate() {
        $cardNo = $this->input->post("cno");
        $id = $this->input->post("id");
        $data = array(
            'qualification_name' => $this->input->post("qualification_name"),
            'institute_name' => $this->input->post('institute_name'),
            'result' => $this->input->post('result'),
            'year' => $this->input->post('year'),
            'note' => $this->input->post('note')
        );
        $this->shows->updateThisValue($data, "qualification", "id = '$id'");
        redirect("hr/employee/empreport/" . $cardNo);
    }

    function hedit($id = NULL, $cardNo = NULL) {
        $this->data['query1'] = $this->temp_model->getallHistory($id);
        $this->data['cno'] = $cardNo;
        $this->middle = 'hr/temp/history';
        $this->layout();
    }

    function hupdate() {
        $cardNo = $this->input->post("cno");
        $id = $this->input->post("id");
        $data = array(
            'company_name' => $this->input->post("company_name"),
            'designation' => $this->input->post('designation'),
            'start' => $this->input->post('start_date'),
            'end_date' => $this->input->post('end_date'),
            'note' => $this->input->post('note')
        );
        $this->shows->updateThisValue($data, "career_history", "id = '$id'");
        redirect("hr/employee/empreport/" . $cardNo);
    }

    function acDelete($id = NULL, $cardNo = NULL) {
        $this->temp_model->delete_academic($id);
        redirect("hr/employee/empreport/" . $cardNo);
    }

    function hsDelete($id = NULL, $cardNo = NULL) {
        $this->temp_model->delete_history($id);
        redirect("hr/employee/empreport/" . $cardNo);
    }

    function quDelete($id = NULL, $cardNo = NULL) {
        $this->temp_model->delete_qualification($id);
        redirect("hr/employee/empreport/" . $cardNo);
    }

    function docDelete($id = NULL, $cardNo = NULL) {
        $this->temp_model->delete_doc($id);
        redirect("hr/employee/empreport/" . $cardNo);
    }

    //appraisal
    function appraisal_edit($id = NULL, $cardNo = NULL) {
        $this->data['query1'] = $this->temp_model->getallAppraisal($id);
        //print_r($this->data['query1']);die();
        $this->data['cno'] = $cardNo;
        $this->middle = 'hr/temp/appraisal';
        $this->layout();
    }

    function appraisal_update() {
        $cardNo = $this->input->post("cno");
        $id = $this->input->post("id");
        $data = array(
            'status' => $this->input->post("status"),
            'effective_date' => $this->input->post('effective_date'),
            'designation' => $this->input->post('designation'),
            'pay_scale' => $this->input->post('pay_scale'),
            'note' => $this->input->post('note'),
        );

        $this->shows->updateThisValue($data, "appraisal", "id = '$id'");
        redirect("hr/employee/empreport/" . $cardNo);
    }
    
    function appraisal_delete($id = NULL, $cardNo = NULL) {
        $this->temp_model->delete_appraisal($id);
        redirect("hr/employee/empreport/" . $cardNo);
    }


}