<?php

class Login extends CI_Controller {

    function index() {
        if ($this->session->userdata('logged_in') == TRUE) {
            redirect('../hr/hr/index');
        }
        $data = array(
            'title' => 'Admin Login'
        );
        $this->load->view("hr/login/login",$data);
    }

    function process_login() {
        $username = $this->input->post('username');
        $password = md5($this->input->post('password'));
        $query1 = $this->db->query("SELECT * FROM user_info WHERE username='$username' AND password='$password'");
        if ($query1->num_rows() == 1) {
            foreach ($query1->result() as $info):
                $branch_id = $info->access_branch;
                $user_type = $info->user_type;
                $conf_prev = $info->conf_prev;
                $leave_prev = $info->leave_prev;
                $salary_prev = $info->salary_prev;
                $hr_prev = $info->hr_prev;
                $finance_prev = $info->finance_prev;
                $attendance_prev = $info->attendance_prev;
                $report_prev = $info->report_prev;
            endforeach;
            $data = array(
                'username' => $username,
                'bid' => $branch_id,
                'utype' => $user_type,
                'conf_prev' => $conf_prev,
                'leave_prev' => $leave_prev,
                'salary_prev' => $salary_prev,
                'hr_prev' => $hr_prev,
                'finance_prev' => $finance_prev,
                'attendance_prev' => $attendance_prev,
                'report_prev' => $report_prev,
                'userCategory' => 'admin',
                'logged_in' => TRUE
            );
            $this->session->set_userdata($data);
            redirect('../hr/hr/index');
        } else {
            $this->session->set_flashdata('message', '<div id="message">Username or password is incorrect, please try again.</div>');
            redirect('../hr/login/index');
        }
    }

    function logout() {
        $this->session->sess_destroy();
        redirect('../hr/login/index');
    }

}

?>
