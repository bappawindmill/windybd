<?php

class Leave extends MY_Controller {

    public function __construct() {
        parent::__construct();
        if ($this->session->userdata('logged_in') != TRUE) {
            redirect('hr/login');
        }
        if ($this->session->userdata('leave_prev') != "1") {
            redirect('hr/hr/index');
        }
        $this->load->model('hr/leave_model');
    }

    function index() {
        $this->load->view('hr/home');
    }

    function added_leave_info($start = 0) {
        $this->data['mainTab'] = 'leave';
        $this->data['activeTab'] = 'manage_leave';

        $emp_id = $this->input->post("emp_id");
        if ($emp_id == "") {
            $this->session->set_flashdata('message', '<div id="message"><font color="red">Plese fill the * field.</font></div>');
            redirect('hr/leave/leave_manage', 'location', 301);
        }
        $this->load->model('hr/leave_model');
        $leave_type = $this->input->post("leave_type");
        $duration = $this->input->post("duration");
        $month = $this->input->post('month');
        $year = $this->input->post('year');
        $purpose = $this->input->post('purpose');
        $dept = $this->input->post('dept');
        $emp_id = $this->input->post("emp_id");
        for ($i = 0; $i < sizeof($month); $i++):
            $data = array(
                'emp_id' => $emp_id[$i],
                'leave_type' => $leave_type[$i],
                'dept' => $dept[$i],
                'month' => $month[$i],
                'year' => $year[$i],
                'purpose' => $purpose[$i],
                'duration' => $duration[$i]
            );
            $this->leave_model->leave_info_insert($data);
        endfor;
        $this->empty_cart();
        redirect('hr/leave/leave_manage', 'location', 301);
    }

    /* Start Add Leave Management */

    function entry() {
        $this->data['mainTab'] = 'leave';
        $this->data['activeTab'] = 'manage_leave';

        $insert = array(
            'id' => $this->input->post('leave_type'),
            'emp_id' => $this->input->post('emp_id'),
            'dept' => $this->input->post('dept'),
            'branch_name' => $this->input->post('branch_name'),
            'duration' => $this->input->post('duration'),
            'year' => $this->input->post('year'),
            'month' => $this->input->post('month'),
            'purpose' => $this->input->post('purpose'),
            'qty' => '1',
            'price' => '10',
            'name' => 'test'
        );
        $this->cart->insert($insert);
        redirect('hr/leave/leave_manage', 'location', 301);
    }

    function remove($rowid) {
        $this->data['mainTab'] = 'leave';
        $this->data['activeTab'] = 'manage_leave';

        $this->cart->update(array(
            'rowid' => $rowid,
            'qty' => 0
        ));
        redirect('hr/leave/leave_manage');
    }

    function empty_cart() {
        $this->data['mainTab'] = 'leave';
        $this->data['activeTab'] = 'manage_leave';

        $qs = $this->cart->destroy();
        return $qs;
    }

    function leave_manage($start = 0) {
        $this->data['mainTab'] = 'leave';
        $this->data['activeTab'] = 'manage_leave';
        $this->data['dynamicView'] = 'hr/leave/manage_leave';
        $this->_commonPageLayout('frontend_viewer');
    }

    function leave_managed() {
        $this->data['mainTab'] = 'leave';
        $this->data['activeTab'] = 'manage_leave';
        $this->load->library('form_validation');
        $this->form_validation->set_rules('branch_name', 'Branch name', 'trim|required');
        $this->form_validation->set_rules('card_no', 'Card No', 'trim|required');
        $this->form_validation->set_rules('start_date', 'Date', 'trim|required');
        $this->form_validation->set_rules('category', 'Category', 'trim|required');

        if ($this->form_validation->run() == FALSE) {
            $this->data['dynamicView'] = 'hr/leave/manage_leave';
            $this->_commonPageLayout('frontend_viewer');
        } else {
            $card_no = $this->input->post("card_no");
            $this->load->model('hr/leave_model');
            $this->data['record'] = $this->leave_model->check_card_no($card_no);
            if (count($this->data['record'])) {
                if ($this->input->post('category') == '1') {
                    if ($this->input->post('leave_hour') == '') {
                        $this->session->set_flashdata('message', '<div id="message">Leave Hour is Required</div>');
                        redirect('hr/leave/leave_manage', 'location', 301);
                    } else {
                        $start_date = $this->input->post('start_date');
                        $next_day = explode("-", $start_date);
                        $y = $next_day[0];
                        $m = $next_day[1];
                        $d = $next_day[2];
                        $data = array(
                            'branch_name' => $this->input->post('branch_name'),
                            'card_no' => $this->input->post('card_no'),
                            'start_date' => $this->input->post('start_date'),
                            'note' => $this->input->post('note'),
                            'leave_hour' => $this->input->post('leave_hour'),
                            'category' => 1,
                            'month' => $m,
                            'year' => $y
                        );
                        $this->leave_model->leave_manage_insert($data);
                        $this->session->set_flashdata('message', '<div id="message">Leave Info saved successfully</div>');
                        redirect('hr/leave/leave_manage', 'location', 301);
                    }
                }
                if ($this->input->post('category') == '2') {
                    if ($this->input->post('leave_type') == '') {
                        $this->session->set_flashdata('message', '<div id="message">Leave Type is Required</div>');
                        redirect('hr/leave/leave_manage', 'location', 301);
                    } else {
                        $start_date = $this->input->post('start_date');
                        $next_day = explode("-", $start_date);
                        $y = $next_day[0];
                        $m = $next_day[1];
                        $d = $next_day[2];
                        $data = array(
                            'branch_name' => $this->input->post('branch_name'),
                            'card_no' => $this->input->post('card_no'),
                            'start_date' => $this->input->post('start_date'),
                            'note' => $this->input->post('note'),
                            'leave_type' => $this->input->post('leave_type'),
                            'leave_duration' => 1,
                            'category' => 2,
                            'month' => $m,
                            'year' => $y
                        );
                        $this->leave_model->leave_manage_insert($data);
                        $this->session->set_flashdata('message', '<div id="message">Leave Info saved successfully</div>');
                        redirect('hr/leave/leave_manage', 'location', 301);
                    }
                }
            } else {
                $this->session->set_flashdata('message', '<div id="message">Invalid Card No</div>');
                redirect('hr/leave/leave_manage', 'location', 301);
            }
        }
    }

    /* End Add Leave Management */

    /* Start Edit Leave Management */

    function leave_manage_edit($id = 0, $emid = 0) {
        $this->load->model('hr/leave_model');
        $this->load->library('parser');

        if ($this->input->post('mysubmit')) {
            if ($this->input->post('id')) {
                $update = array('emp_id' => $this->input->post('emp_id'),
                    'leave_type' => $this->input->post('leave_type'),
                    'duration' => $this->input->post('duration'),
                    'year' => $this->input->post('year'),
                    'month' => $this->input->post('month'),
                    'purpose' => $this->input->post('purpose'),
                    'note' => $this->input->post('note'),
                );

                $this->leave_model->edit_leave_manage($update);
                redirect('hr/leave/leave_manage', 'location', 301);
            }
        } else {

            $query3 = $this->db->query("SELECT id,name FROM employeeinfo");

            $dataStr3 = "<tr><th>Employee :</th><td><select name='emp_id'>";

            foreach ($query3->result() as $row3) {
                $dataStr3.="<option value='" . $row3->id . "'";

                if ($row3->id == $emid) {
                    $dataStr3.=" Selected";
                }

                $dataStr3.=">" . $row3->name . "</option>";
            }

            $dataStr3.='</select></td></tr>';

            $data = array(
                'EMPLOYEE' => $dataStr3
            );

            $query = $this->leave_model->get_leave_manage($id);
            $data['fid']['value'] = $query['id'];
            $data['fduration']['value'] = $query['duration'];
            $data['fleave_type']['value'] = $query['leave_type'];
            $data['fdeparture']['value'] = $query['year'];
            $data['freturn']['value'] = $query['month'];
            $data['fpurpose']['value'] = $query['purpose'];
            $data['fnote']['value'] = $query['note'];

            $this->parser->parse('hr/leave/leave_manage_edit', $data);
        }
    }

    /* End Edit Leave Management */


    function leave_management_delete($id = NULL) {
        $this->load->model('hr/leave_model');
        $this->leave_model->delete_leave_management($id);
        redirect('hr/leave/leave_manage', 'location', 301);
    }


    function leave_report($data = NULL) {
        $this->data['title'] = 'Leave Report';
        $this->data['mainTab'] = 'leave';
        $this->data['activeTab'] = 'leave_report';
        $this->middle = 'hr/leave/yearly/view_leave_details';
        $this->layout();
    }

    function leave_details() {
        $this->data['title'] = 'Leave Details';
        $this->data['mainTab'] = 'leave';
        $this->data['activeTab'] = 'leave_report';
        $this->load->library('form_validation');
        $this->form_validation->set_rules('card_no', 'Card No', 'trim|required');
        if ($this->form_validation->run() == FALSE) {
            $this->middle = 'hr/leave/yearly/view_leave_details';
            $this->layout();
        } else {
            $card_no = $this->input->post("card_no");
            $year = $this->input->post("year");
            $this->load->model('hr/leave_hr_model');
            $this->data['record'] = $this->leave_hr_model->check_card_no($card_no);
            if (count($this->data['record'])) {
                $this->data['query10'] = $this->leave_hr_model->report_leave_details1($card_no, $year);
                $this->data['fyear'] = $this->input->post('year');
                $this->data['c'] = $this->input->post('card_no');
                $this->middle = 'hr/leave/yearly/leave_report_individual';
                $this->layout();
            } else {
                $this->session->set_flashdata('message', '<div id="message">Invalid Card No</div>');
                redirect('hr/leave/leave_report', 'location', 301);
            }
        }
    }

    function view_application() {
        $this->data['title'] = 'View Leave Application';
        $this->data['mainTab'] = 'leave';
        $this->data['activeTab'] = 'view_application';
        //$this->load->model('hr/leave_hr_model');

        $limit  = $this->pagination_library->per_page;
        $search = $this->input->get_post('search');
        $start  = ($this->input->get_post('start')) ? $this->input->get('start') : 0;

        $this->db->like('card_no',$search);
        $this->db->limit($limit,$start);
        $this->db->where('hr_status',1);
        $this->db->where('supervisor_status',2);
        $this->db->order_by('id','desc');
        $query = $this->db->get('leave_management');
        $this->db->like('card_no',$search);

        $this->db->limit(0);

        $this->db->where('hr_status',1);
        $this->db->where('supervisor_status',2);
        $rowCountQuery = $this->db->get('leave_management');

        $this->pagination_library->total_rows =  $rowCountQuery->num_rows();
        $search_q = (!empty($t_array)) ? "?".implode("&",$t_array) : '';
        $this->pagination_library->base_url = site_url('hr/leave/view_application').$search_q;

        //$this->data['query'] = $this->leave_hr_model->leaveApplication();
        $this->data['query'] = $query; //show($query->result());
        $this->pagination->initialize($this->pagination_library->getArray());
        $this->middle = 'hr/leave/application/leave_application_view';
        $this->layout();
    }

    function details($id = NULL, $empId) {
        $this->load->model("hr/leave_hr_model");
        $query = $this->db->query("select * from employeeinfo where id='" . $empId . "'");
        if ($query->num_rows() > 0) {
            $this->data['mainTab'] = 'leave';
            $this->data['activeTab'] = 'view_application';
            $this->data['query10'] = $this->leave_hr_model->get_application_info($id);
            $this->middle = 'hr/leave/application/leave_application_details';
            $this->layout();
        } else {
            redirect("hr/leave/view_application");
        }
    }

    function approve($id = NULL) {
        $this->load->model("hr/leave_hr_model");
        $obj_leave_manage = $this->leave_hr_model->get_leave_manage($id);
        $emp_id =  $obj_leave_manage['emp_id'];


        $employeeType=$this->leave_hr_model->checkEmployee($emp_id);
        $this->data['mainTab'] = 'leave';
        $this->data['activeTab'] = 'application';
        /*if($employeeType[0]['category']==2){
                $data = array(
                    'hr_status' => 3
                );
        }else{
                $data = array(
                    'hr_status' => 2
                );
        }*/

        $data = array(
            'hr_status' => 2
        );
        $this->leave_hr_model->update_leave_management($data, $id);

        //start email send
        //start email sending to applicant
        $query = $this->leave_hr_model->get_emp_info($emp_id);
        $name = "";
        $email = "";
        foreach ($query as $info):
            $name = $info->name;
            $email = $info->email;
        endforeach;
        //start email sending to applicant
        $to = $email;
        $subject = " Notification";
        $message = "Dear Mr./Ms. $name ,";
        $message.="<br/> Thanks for your leave application. Please not that your leave has been GRANTED from  management and it has been adjusted to your leave account. In your absent your nominated person will be in charge of your assigned responsibilities. Also note that your remaining leave has been adjusted. To know more please visit your leave status.";
        $message.="<br/>  Please note that a notification mail has also been sent to your assigned alternative official.";
        $message.="<br/>See you again on your assigned joining date.";
        $message.="<br/>This is auto generated mail. Pls donot reply to this mail";
        $headers = 'MIME-Version: 1.0' . "\r\n";
        $headers .= 'Content-type: text/html; charset=UTF-8' . "\r\n";
        // Additional headers
        $headers .= 'To: Customer <' . $to . "\r\n";
        $headers .= 'From: info@charcoalboard.com <info@charcoalboard.com>' . "\r\n";
        // Mail it
        mail($to, $subject, $message, $headers);

        //end email sending to applicant
        //send mail to asigned person

        $startDate = "";
        $endDate = "";
        $leave_duration = "";
        $nominated_person = "";
        $lDetails = $this->leave_hr_model->getLeaveInfo($id);
        foreach ($lDetails as $l_details):
            $startDate = $l_details->start_date;
            $endDate = $l_details->end_date;
            $leave_duration = $l_details->leave_duration;
            $nominated_person = $l_details->nominated_person;
        endforeach;
        if ($nominated_person != " ") {
            $nid = $nominated_person;
            $query = $this->leave_hr_model->get_emp_info1($nid);
            $n_name = "";
            $n_email = "";

            foreach ($query as $info):
                $n_name = $info->name;
                $n_email = $info->email;
            endforeach;

            $to = $n_email;
            $subject = " Notification";
            $message = "Dear Concern,";
            $message.="Mr./Mrs ('$name') has applied for a leave from ('$startDate') to ('$endDate'). His/her application has been approved by the management. You are mentioned as the alternative person in charge by the applicant during his/her absent. PLEASE NOTIFY ALL CONCERN REGARDING YOUR AVAILABILITY AND YOUR RESPONSIBILITIES. This e-mail has been generated for your information and necessary action.If you are not the appropriate person, please talk to HR Department.  Don’t reply to this email address.";

            $headers = 'MIME-Version: 1.0' . "\r\n";
            $headers .= 'Content-type: text/html; charset=UTF-8' . "\r\n";
            // Additional headers
            $headers .= 'To: Customer <' . $to . "\r\n";
            $headers .= 'From: info@charcoalboard.com <info@charcoalboard.com>' . "\r\n";
            // Mail it
            mail($to, $subject, $message, $headers);
        }

        //end sending mail to assigned person
        //Start send email to top management

        $to = "mosharaf@windmillbd.net";
        $subject = " Notification";
        $message = "Dear Concern ,";
        $message.="<br/>  Leave Notification: Mr./Ms. $name has applied for a leave from ($startDate) to ($endDate) for $leave_duration days. The leave has been approved and confirmed by concerned department. This email is only for your information and action.";

        $headers = 'MIME-Version: 1.0' . "\r\n";
        $headers .= 'Content-type: text/html; charset=UTF-8' . "\r\n";
        // Additional headers
        $headers .= 'To: Customer <' . $to . "\r\n";
        $headers .= 'From: info@charcoalboard.com <info@charcoalboard.com>' . "\r\n";
        // Mail it
        mail($to, $subject, $message, $headers);


        $to = "bushra@windmillbd.net";
        $subject = " Notification";
        $message = "Dear Concern,";
        $message.="<br/> Leave Notification: Mr./Ms. $name has applied for a leave from ($startDate) to ($endDate) for $leave_duration days. The leave has been approved and confirmed by concerned department. This email is only for your information and action..";

        $headers = 'MIME-Version: 1.0' . "\r\n";
        $headers .= 'Content-type: text/html; charset=UTF-8' . "\r\n";
        // Additional headers
        $headers .= 'To: Customer <' . $to . "\r\n";
        $headers .= 'From: info@charcoalboard.com <info@charcoalboard.com>' . "\r\n";
        // Mail it
        mail($to, $subject, $message, $headers);

        $to = "hr@windmill.com.bd";
        $subject = " Notification";
        $message = "Dear Concern ,";
        $message.="<br/> Leave Notification: Mr./Ms. $name has applied for a leave from ($startDate) to ($endDate) for $leave_duration days. The leave has been approved and confirmed by concerned departments. This email is only for your information and action..";

        $headers = 'MIME-Version: 1.0' . "\r\n";
        $headers .= 'Content-type: text/html; charset=UTF-8' . "\r\n";
        // Additional headers
        $headers .= 'To: Customer <' . $to . "\r\n";
        $headers .= 'From: info@charcoalboard.com <info@charcoalboard.com>' . "\r\n";
        // Mail it
        mail($to, $subject, $message, $headers);

        //end sending email to top management
        //start sending email to supervisor

        $supInfo = $this->leave_hr_model->get_emp_info1($emp_id);
        $supervisor = "";
        foreach ($supInfo as $sInfo):
            $supervisor = $sInfo->supervisor;
        endforeach;
        $supMail = $this->leave_hr_model->get_emp_info1($supervisor);
        $supmail = "";
        $supname = "";
        foreach ($supMail as $sMail):
            $supmail = $sMail->email;
            $supname = $sMail->name;
        endforeach;

        $to = $supmail;
        $subject = " Notification";
        $message = "Dear Mr./Ms. $supname  ,";
        $message.="<br/> Leave Notification: Mr./Ms. $name has applied for a leave from ($startDate) to ($endDate) for $leave_duration days. The leave has been approved and confirmed by concerned departments. This email is only for your information and action.";
        $message.="<br/>This is auto generated mail. Please do not reply to this mail..";
        $headers = 'MIME-Version: 1.0' . "\r\n";
        $headers .= 'Content-type: text/html; charset=UTF-8' . "\r\n";
        // Additional headers
        $headers .= 'To: Customer <' . $to . "\r\n";
        $headers .= 'From: info@charcoalboard.com <info@charcoalboard.com>' . "\r\n";
        // Mail it
        mail($to, $subject, $message, $headers);


        // $this->sup_recursive($supervisor, $name, $startDate, $endDate, $leave_duration);
        //end sending email to super
        redirect('hr/leave/view_application', 'location', 301);
    }

    function sup_recursive($emp_id = NULL, $name = NULL, $startDate = NULL, $endDate = NULL, $leave_duration = NULL) {
        if ($emp_id == "") {
            redirect('hr/leave/view_application', 'location', 301);
        } else {
            $supInfo = $this->leave_model->get_emp_info1($emp_id);
            $supervisor = "";
            foreach ($supInfo as $sInfo):
                $supervisor = $sInfo->supervisor;
            endforeach;
            $supMail = $this->leave_model->get_emp_info1($supervisor);
            $supmail = "";
            $supname = "";
            foreach ($supMail as $sMail):
                $supmail = $sMail->email;
                $supname = $sMail->name;
            endforeach;
            $sup_message = "
            <html>
            <body>
               <p>Dear Mr./Ms. $supname ,</p>
               <p>Leave Notification: Mr./Ms. $name has applied for a leave from ($startDate) to ($endDate) for $leave_duration days. The leave has been approved and confirmed by concerned departments. This email is only for your information and action.
            </p>
	      <p>This is auto generated mail. Pls donot reply to this mail..</p>
            </body>
            </html>
            ";
            $this->load->library('email');
            $this->email->from('info@charcoalboard.com', 'Leave Notification');
            $this->email->to("$supmail");
            $this->email->subject('Leave Notification');
            $this->email->message($sup_message);
            $this->email->send();
            $this->sup_recursive($supervisor, $name, $startDate, $endDate, $leave_duration);
        }
    }

    function deny($id = NULL, $emp_id = NULL) {
        $this->load->model("hr/leave_hr_model");
        $this->data['mainTab'] = 'leave';
        $this->data['activeTab'] = 'application';
        $data = array(
            'hr_status' => 3
        );
        $this->leave_hr_model->update_leave_management($data, $id);
        $lDetails = $this->leave_hr_model->getLeaveInfo($id);
        $startDate = "";
        $endDate = "";
        $leave_duration = "";
        foreach ($lDetails as $l_details):
            $startDate = $l_details->start_date;
            $endDate = $l_details->end_date;
            $leave_duration = $l_details->leave_duration;
        endforeach;
        $query = $this->leave_hr_model->get_emp_info($emp_id);
        $name = "";
        $email = "";
        foreach ($query as $info):
            $name = $info->name;
            $email = $info->email;
        endforeach;


        $to = $email;
        $subject = " Notification";
        $message = "Dear Mr./Ms. ( $name ),";
        $message.="<br/> Thanks for your leave application. Please not that your leave has been DECLINED from management. Please consult your line manager and HR Department to understand the reasoning. Your available leave status till today can be checked by log on to your leave status..";
        $message.="<br/>Please note that a notification mail has also been sent to your proposed assigned alternative official, your line manager and management for their record";
        $message.="<br/>This is auto generated mail. Pls donot reply to this mail..";
        $headers = 'MIME-Version: 1.0' . "\r\n";
        $headers .= 'Content-type: text/html; charset=UTF-8' . "\r\n";
        // Additional headers
        $headers .= 'To: Customer <' . $to . "\r\n";
        $headers .= 'From: info@charcoalboard.com <info@charcoalboard.com>' . "\r\n";
        // Mail it
        mail($to, $subject, $message, $headers);





        //Start Sending Mail to supervisor
        $supInfo = $this->leave_hr_model->get_emp_info1($emp_id);
        $supervisor = "";
        foreach ($supInfo as $sInfo):
            $supervisor = $sInfo->supervisor;
        endforeach;
        $supMail = $this->leave_hr_model->get_emp_info1($supervisor);
        $supmail = "";
        $supname = "";
        foreach ($supMail as $sMail):
            $supmail = $sMail->email;
            $supname = $sMail->name;
        endforeach;

        if ($supmail != "") {
            $applicant_name = $name;

            $to = $supmail;
            $subject = " Notification";
            $message = "Dear Mr./Ms. $supname ,";
            $message.="<br/> Leave Notification: Mr./MS $applicant_name has applied for a leave from ($startDate) to ($endDate) for $leave_duration days. The leave has been rejected by HR Department. This email is only for your information and action";

            $message.="<br/>This is auto generated mail. Pls donot reply to this mail..";
            $headers = 'MIME-Version: 1.0' . "\r\n";
            $headers .= 'Content-type: text/html; charset=UTF-8' . "\r\n";
            // Additional headers
            $headers .= 'To: Customer <' . $to . "\r\n";
            $headers .= 'From: info@charcoalboard.com <info@charcoalboard.com>' . "\r\n";
            // Mail it
            mail($to, $subject, $message, $headers);
        }
        //end sending mail to supervisor


        redirect('hr/leave/view_application', 'location', 301);
    }

    function leave_all() {
        $this->data['title'] = 'Leave';
        $this->data['mainTab'] = 'leave';
        $this->data['activeTab'] = 'leave_report_all';
        $this->middle = 'hr/leave/all/leave_form_all';
        $this->layout();
    }

    function leave_report_all() {
        $this->data['title'] = 'Leave Report';
        $this->data['mainTab'] = 'leave';
        $this->load->model('hr/leave_hr_model');
        $this->data['activeTab'] = 'leave_report_all';
        $dept= $this->input->get_post("dept");
        $branch_name = $this->input->get_post("branch_name");
        $this->data['fyear'] = $this->input->post("year");
        if($branch_name || $dept)
        {
            if ($dept != ""):
                $this->data['empInfo'] = $this->leave_hr_model->getDeptWiseEmployee($dept);
            else:
                $this->data['empInfo'] = $this->leave_hr_model->getBranchWiseEmployee($branch_name);
            endif;
            $this->data['fyear'] = $this->input->get_post("year");

            $this->pagination->initialize($this->pagination_library->getArray());
            $this->middle = 'hr/leave/all/leave_report_all_details';
            $this->layout();
        }
        else{
            $this->middle = 'hr/leave/all/leave_form_all';
            $this->layout();
        }

    }

    function dateWiseLeave() {
        $this->data['title'] = 'Date Wise Leave';
        $this->data['mainTab'] = 'leave';
        $this->data['activeTab'] = 'dateWiseLeave';
        $this->middle = 'hr/leave/datewise/datewise_leave_form';
        $this->layout();

    }


    function dateWiseView() {
        $this->data['mainTab'] = 'leave';
        $this->data['activeTab'] = 'dateWiseLeave';
        $this->load->model('hr/leave_hr_model');
        $this->data['title'] = 'dateWiseLeave';
        $this->form_validation->set_rules('start_date', 'Start Date', 'trim|required');
        $this->form_validation->set_rules('end_date', 'End Date', 'trim|required');
        $sdate = $this->input->get_post("start_date");
        if($sdate){
            $this->data['start_date'] = $this->input->get_post("start_date");
            $this->data['end_date'] = $this->input->get_post("end_date");
            $this->data['empInfo'] = $this->leave_hr_model->getEmp($this->data['start_date'],$this->data['end_date']);
            $this->pagination->initialize($this->pagination_library->getArray());
            $this->middle = 'hr/leave/datewise/datewise_report_view';
            $this->layout();
        }else if($this->form_validation->run() == FALSE){
            $this->middle = 'hr/leave/datewise/datewise_leave_form';
            $this->layout();
        }
    }

    function view_summary() {
        $this->data['title'] = 'Leave Summary';
        $this->data['mainTab'] = 'leave';
        $this->data['activeTab'] = 'leave_summary';
        $this->middle = 'hr/leave/summary/leave_summary_form';
        $this->layout();
    }

    function viewed_summary() {
        $this->data['title'] = 'Viewed Summary';
        $this->data['mainTab'] = 'leave';
        $this->load->model("hr/leave_hr_model");
        $this->data['activeTab'] = 'leave_summary';
        $this->load->library('form_validation');
        $this->form_validation->set_rules('branch_name', 'SBU Name', 'trim|required');
        $this->form_validation->set_rules('year', 'Year', 'trim|required');
        $branch = $this->input->get_post("branch_name");
        if($branch){
            $year = $this->input->post("year");
            $this->data['syear'] = $year;
            $this->data['empInfo'] = $this->leave_hr_model->getEmp_concern($branch);
            $this->pagination->initialize($this->pagination_library->getArray());
            $this->middle = 'hr/leave/summary/summary_report_details';
            $this->layout();
        } else if($this->form_validation->run() == FALSE) {
            $this->middle = 'hr/leave/summary/leave_summary_form';
            $this->layout();
        }
    }

    function branchWiseView($branch = NULL, $start_date = NULL, $end_date = NULL) {
        $this->load->model("hr/leave_hr_model");
        $this->data['mainTab'] = 'leave';
        $this->data['activeTab'] = 'dateWiseLeave';
        $this->data['sdate'] = $start_date;
        $this->data['edate'] = $end_date;
        $this->data['empInfo'] = $this->leave_hr_model->getEmpBranchWise($branch);
        $this->middle = 'hr/leave/datewise/branchwise_leave_view';
        $this->layout();
    }

    function nameWiseView($cardNo = NULL, $start_date = NULL, $end_date = NULL) {
        $this->load->model("hr/leave_hr_model");
        $this->data['mainTab'] = 'leave';
        $this->data['activeTab'] = 'dateWiseLeave';
        $this->data['sdate'] = $start_date;
        $this->data['edate'] = $end_date;
        $this->data['empInfo'] = $this->leave_hr_model->getEmpNameWise($cardNo);
        $this->middle = 'hr/leave/datewise/namewise_leave_view';
        $this->layout();
    }

    function leaveTypeWiseView($ltype = NULL, $start_date = NULL, $end_date = NULL) {
        $this->load->model("hr/leave_hr_model");
        $this->data['mainTab'] = 'leave';
        $this->data['activeTab'] = 'dateWiseLeave';
        $this->data['sdate'] = $start_date;
        $this->data['edate'] = $end_date;
        $this->data['leaveType'] = $ltype;
        $this->data['empInfo'] = $this->leave_hr_model->getEmp();
        $this->middle = 'hr/leave/datewise/typewise_leave_view';
        $this->layout();
    }

}
