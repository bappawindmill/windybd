<?php


class Info extends MY_Controller {

    public function __construct() {
        parent::__construct();
        if ($this->session->userdata('logged_in') != TRUE) {
            redirect('hr/login');
        }
        if ($this->session->userdata('hr_prev') != "1") {
            redirect('hr/hr/index');
        }
        $this->load->model('hr/info_model');
    }

    function index() {
        $this->load->view('hr/home');
    }
    function change_status($id = NULL) {
        $this->load->model('hr/info_model');
        $this->data['mainTab'] = 'employee';
        $this->data['activeTab'] = 'general_info';
        $this->load->library('form_validation');
        $this->form_validation->set_rules('name', 'name', 'trim');

        if ($this->input->post('mysubmit')) {
            $id = $this->input->post('id');
            if ($this->session->userdata("utype") == '1'):
                $data = array(
                    'status' => $this->input->post('status')
                );

                $this->info_model->employee_general_info_update($data);
            endif;
            redirect('hr/employee/get_all_employee', 'location', 301);
        }

        if ($id != NULL) {
            $this->data['query'] = $this->info_model->get_employee_general_info($id);
            $this->middle = 'hr/empinfo/status_edit_l';
            $this->layout();
        }
    }

    /* 1684 */

    function print_card() {
        $this->data['mainTab'] = 'report';
        $this->data['activeTab'] = 'print_card';
        $this->data['dynamicView'] = 'hr/card/card_form';
        $this->_commonPageLayout('frontend_viewer');
    }

    function printed_card() {
        $this->data['mainTab'] = 'report';
        $this->data['activeTab'] = 'print_card';
        $this->load->library('form_validation');
        $this->form_validation->set_rules('branch_name', 'Branch Name', 'trim|required');
        $this->form_validation->set_rules('dept', 'Department', 'trim|required');
        $this->form_validation->set_rules('proffession', 'Section', 'trim|required');
        if ($this->form_validation->run() == FALSE) {
            $this->data['dynamicView'] = 'hr/card/card_form';
            $this->_commonPageLayout('frontend_viewer');
        } else {
            $this->data['query1'] = $this->info_model->get_all_employee();
            $this->data['dynamicView'] = 'hr/card/report_details';
            $this->_commonPageLayout('frontend_viewer');
        }
    }

    function add_employee_general_info($start = 0) {
        $this->data['title'] = 'Employee Entry';
        $this->data['optionDesignation'] = getOptionsDesignation();
        $this->data['optionDepartment'] = getOptionsDepartment();
        $this->data['mainTab'] = 'employee';
        $this->data['activeTab'] = 'general_info';
        $this->load->library('pagination');
        $this->load->library('parser');
        $config['base_url'] = site_url("hr/info/add_employee_general_info");
        $config['total_rows'] = $this->db->count_all('employeeinfo');
        $config['per_page'] = '500';
        $this->pagination->initialize($config);
        $this->load->model('hr/info_model');
        $this->data['query1'] = $this->info_model->view_employee_general_info($start, $config['per_page']);
        $this->middle = 'hr/empinfo/employee_general_info';
        $this->layout();
    }

    function check_card_no($card_no) {

        $result = $this->dx_auth->is_card_no_available($card_no);
        if (!$result) {
            $this->form_validation->set_message('check_card_no', 'Card No exists. ');
        }

        return $result;
    }

    function check_secrete_no($secrete_no) {

        $result = $this->dx_auth->is_secrete_no_available($secrete_no);
        if (!$result) {
            $this->form_validation->set_message('check_secrete_no', 'Secrete No exists.');
        }

        return $result;
    }

    function check_user($username) {

        $result = $this->dx_auth->is_user_available($username);
        if (!$result) {
            $this->form_validation->set_message('check_user', 'Username exists.');
        }

        return $result;
    }

	
	
	function check_unique_employee_id($field_value)
	{		
		$rv = true;
		$carNo = $field_value;
		$employees = $this->info_model->get_employees($carNo);
		
		if( count($employees) > 0  ){
			$this->form_validation->set_message('check_unique_employee_id', 'Duplicate ID');
				$rv = false;				
		}
		return $rv;
	}


    function added_employee_general_info($start = 0) {
		$this->load->library('form_validation');
		
        $this->form_validation->set_rules('firstname', 'Firstname', 'trim|required');
        $this->form_validation->set_rules('lastname', 'Lastname', 'trim|required');
        // $this->form_validation->set_rules('secrete_no', 'Secret No', 'trim|required|callback_check_secrete_no');
         $this->form_validation->set_rules('secrete_no', 'Secret No', 'trim|required');
       //$this->form_validation->set_rules('card_no', 'ID No', 'trim|required|callback_check_card_no');
	   
	   $this->form_validation->set_rules('card_no', 'ID No', 'trim|required|callback_check_unique_employee_id');
       
        $this->form_validation->set_rules('branch_name', 'SBU Name', 'trim|required');
        $this->form_validation->set_rules('dept', 'Department', 'trim|required');
        $this->form_validation->set_rules('designation', 'Designation', 'trim|required');
        $this->form_validation->set_rules('username', 'Username', 'trim|required');
        $this->form_validation->set_rules('password', 'Password', 'trim|required');
        $this->form_validation->set_rules('email', 'Email', 'trim|required|valid_email');
        $this->form_validation->set_rules('category', 'Category', 'trim|required');
		
		
        if ($this->form_validation->run() == FALSE) {
            $this->data['optionDesignation'] = getOptionsDesignation();
            $this->data['mainTab'] = 'employee';
            $this->data['activeTab'] = 'general_info';
            $this->load->library('pagination');
            $this->load->library('parser');
            $config['base_url'] = site_url("hr/info/add_employee_general_info");
            $config['total_rows'] = $this->db->count_all('employeeinfo');
            $config['per_page'] = '500';
            $this->pagination->initialize($config);
            $this->load->model('hr/info_model');
            $this->data['query1'] = $this->info_model->view_employee_general_info($start, $config['per_page']);
            $this->middle = 'hr/empinfo/employee_general_info';
            $this->layout();
        } else {
            $image_name = "";
            $config['upload_path'] = './uploads/';
            $config['allowed_types'] = 'gif|jpg|png';
            //$config['max_size']	= '10000';
            //$config['max_width']  = '1024';
            //$config['max_height']  = '768';
            $this->load->library('upload');
            $this->upload->initialize($config);
            if ($this->upload->do_upload()) {
                $data = $this->upload->data();
                $image_name = $data['file_name'];
            }
            $this->load->model('hr/info_model');
            //$this->load->view('upload_success', $data);
            if ($this->input->post('cash_payment') == 1) {
                $data = array('name' => $this->input->post('firstname') . " " . $this->input->post('lastname'),
                    'card_no' => $this->input->post('card_no'),
                    'dept' => $this->input->post('dept'),
                    'branch_name' => $this->input->post('branch_name'),
                    'leave_name' => $this->input->post('leave_name'),
                    'dob' => $this->input->post('year') . '-' . $this->input->post('month') . '-' . $this->input->post('day'),
                    'gender' => $this->input->post('gender'),
                    'photo' => $image_name,
                    'marital_status' => $this->input->post('marital_status'),
                    'nationality' => $this->input->post('nationality'),
                    'blood' => $this->input->post('blood'),
                    'telephone' => $this->input->post('telephone'),
                    'contact_person' => $this->input->post('contact_person'),
                    'national_id' => $this->input->post('national_id'),
                    'email' => $this->input->post('email'),
                    'emergency_no' => $this->input->post('emergency_no'),
                    'relation' => $this->input->post('relation'),
                    'join_date' => $this->input->post('join_date'),
                    'notice_period' => $this->input->post('notice_period'),
                    'attendence_rules' => $this->input->post('attendence_rules'),
                    'provident_fund' => $this->input->post('provident_fund'),
                    'payment_mode' => $this->input->post('cash_payment'),
                    'note' => $this->input->post('note'),
                    'secrete_no' => $this->input->post('secrete_no'),
                    'emp_type' => $this->input->post('emp_type'),
                    'designation' => $this->input->post('designation'),
                    'category' => $this->input->post("category"),
                    'username' => $this->input->post("username"),
                    'password' => $this->input->post("password"),
                    'sup_designation' => $this->input->post("sup_designation"),
                    'supervisor' => $this->input->post("supervisor"),
                    'confirmation_date' => $this->input->post("confirmation_date"),
                    'reference_name' => $this->input->post("reference_name"),
                    'referee_contact_no' => $this->input->post("referee_contact_no"),
                    'sapackage' => 0,
                    'basic' => $this->input->post('basic'),
                    'house_rent' => $this->input->post('house_rent'),
                    'medical' => $this->input->post('medical'),
                    'conveyance' => $this->input->post('conveyance'),
                    'special' => $this->input->post('special'),
                    'salary_step' => $this->input->post('salary_step'),
                    'salary_band' => $this->input->post('salary_band'),
                    'mobile_bill' => $this->input->post('mobile_bill'),
                    'tax_deduction' => $this->input->post('tax_deduction'),
                    'tax_percentage' => $this->input->post('tax_percentage'),
                    'present_address' => $this->input->post('present_address'),
                    'permanent_address' => $this->input->post('permanent_address'),
                    'personal_email' => $this->input->post('personal_email'),
                    'employeer_pf_balance' => $this->input->post('employeer_pf_balance'),
                    'employee_pf_balance' => $this->input->post('employee_pf_balance'),
                    'timeframe' => $this->input->post('timeframe'),
                    'duty_schedule' => $this->input->post('duty_schedule')
                );
            } elseif ($this->input->post('bank_payment') == 2) {

                $data = array(
                    'name' => $this->input->post('firstname') . " " . $this->input->post('lastname'),
                    'card_no' => $this->input->post('card_no'),
                    'dept' => $this->input->post('dept'),
                    'branch_name' => $this->input->post('branch_name'),
                    'proffession' => $this->input->post('proffession'),
                    'leave_name' => $this->input->post('leave_name'),
                    'dob' => $this->input->post('year') . '-' . $this->input->post('month') . '-' . $this->input->post('day'),
                    'gender' => $this->input->post('gender'),
                    'photo' => $image_name,
                    'marital_status' => $this->input->post('marital_status'),
                    'nationality' => $this->input->post('nationality'),
                    'blood' => $this->input->post('blood'),
                    'telephone' => $this->input->post('telephone'),
                    'contact_person' => $this->input->post('contact_person'),
                    'national_id' => $this->input->post('national_id'),
                    'email' => $this->input->post('email'),
                    'emergency_no' => $this->input->post('emergency_no'),
                    'relation' => $this->input->post('relation'),
                    'join_date' => $this->input->post('join_date'),
                    'notice_period' => $this->input->post('notice_period'),
                    'attendence_rules' => $this->input->post('attendence_rules'),
                    'provident_fund' => $this->input->post('provident_fund'),
                    'payment_mode' => $this->input->post('bank_payment'),
                    'bank_name' => $this->input->post('bank_name'),
                    'bank_branch' => $this->input->post('bank_branch'),
                    'account_number' => $this->input->post('account_number'),
                    'note' => $this->input->post('note'),
                    'secrete_no' => $this->input->post('secrete_no'),
                    'emp_type' => $this->input->post('emp_type'),
                    'designation' => $this->input->post('designation'),
                    'category' => $this->input->post("category"),
                    'username' => $this->input->post("username"),
                    'password' => $this->input->post("password"),
                    'sup_designation' => $this->input->post("sup_designation"),
                    'supervisor' => $this->input->post("supervisor"),
                    'confirmation_date' => $this->input->post("confirmation_date"),
                    'reference_name' => $this->input->post("reference_name"),
                    'referee_contact_no' => $this->input->post("referee_contact_no"),
                    'sapackage' => 0,
                    'basic' => $this->input->post('basic'),
                    'house_rent' => $this->input->post('house_rent'),
                    'medical' => $this->input->post('medical'),
                    'conveyance' => $this->input->post('conveyance'),
                    'special' => $this->input->post('special'),
                    'salary_step' => $this->input->post('salary_step'),
                    'salary_band' => $this->input->post('salary_band'),
                    'mobile_bill' => $this->input->post('mobile_bill'),
                    'tax_deduction' => $this->input->post('tax_deduction'),
                    'tax_percentage' => $this->input->post('tax_percentage'),
                    'present_address' => $this->input->post('present_address'),
                    'permanent_address' => $this->input->post('permanent_address'),
                    'personal_email' => $this->input->post('personal_email'),
                    'employeer_pf_balance' => $this->input->post('employeer_pf_balance'),
                    'employee_pf_balance' => $this->input->post('employee_pf_balance'),
                    'timeframe' => $this->input->post('timeframe'),
                    'duty_schedule' => $this->input->post('duty_schedule')
                );
            } else {

                $data = array(
                    'name' => $this->input->post('firstname') . " " . $this->input->post('lastname'),
                    'card_no' => $this->input->post('card_no'),
                    'dept' => $this->input->post('dept'),
                    'branch_name' => $this->input->post('branch_name'),
                    'proffession' => $this->input->post('proffession'),
                    'leave_name' => $this->input->post('leave_name'),
                    'dob' => $this->input->post('year') . '-' . $this->input->post('month') . '-' . $this->input->post('day'),
                    'gender' => $this->input->post('gender'),
                    'photo' => $image_name,
                    'marital_status' => $this->input->post('marital_status'),
                    'nationality' => $this->input->post('nationality'),
                    'blood' => $this->input->post('blood'),
                    'telephone' => $this->input->post('telephone'),
                    'contact_person' => $this->input->post('contact_person'),
                    'national_id' => $this->input->post('national_id'),
                    'email' => $this->input->post('email'),
                    'emergency_no' => $this->input->post('emergency_no'),
                    'relation' => $this->input->post('relation'),
                    'join_date' => $this->input->post('join_date'),
                    'notice_period' => $this->input->post('notice_period'),
                    'attendence_rules' => $this->input->post('attendence_rules'),
                    'provident_fund' => $this->input->post('provident_fund'),
                    'note' => $this->input->post('note'),
                    'secrete_no' => $this->input->post('secrete_no'),
                    'emp_type' => $this->input->post('emp_type'),
                    'designation' => $this->input->post('designation'),
                    'category' => $this->input->post("category"),
                    'username' => $this->input->post("username"),
                    'password' => $this->input->post("password"),
                    'sup_designation' => $this->input->post("sup_designation"),
                    'supervisor' => $this->input->post("supervisor"),
                    'confirmation_date' => $this->input->post("confirmation_date"),
                    'reference_name' => $this->input->post("reference_name"),
                    'referee_contact_no' => $this->input->post("referee_contact_no"),
                    'sapackage' => 0,
                    'basic' => $this->input->post('basic'),
                    'house_rent' => $this->input->post('house_rent'),
                    'medical' => $this->input->post('medical'),
                    'conveyance' => $this->input->post('conveyance'),
                    'special' => $this->input->post('special'),
                    'salary_step' => $this->input->post('salary_step'),
                    'salary_band' => $this->input->post('salary_band'),
                    'mobile_bill' => $this->input->post('mobile_bill'),
                    'tax_deduction' => $this->input->post('tax_deduction'),
                    'tax_percentage' => $this->input->post('tax_percentage'),
                    'present_address' => $this->input->post('present_address'),
                    'permanent_address' => $this->input->post('permanent_address'),
                    'personal_email' => $this->input->post('personal_email'),
                    'employeer_pf_balance' => $this->input->post('employeer_pf_balance'),
                    'employee_pf_balance' => $this->input->post('employee_pf_balance'),
                    'timeframe' => $this->input->post('timeframe'),
                    'duty_schedule' => $this->input->post('duty_schedule')
                );
            }
            $this->info_model->employee_info_insert($data);
            $e_id = $this->db->insert_id();
            $data = array(
                'emp_id' => $e_id,
                'month' => date('j'),
                'year' => date('Y'),
                'status' => $this->input->post('provident_fund'),
            );
            $this->info_model->pf_insert($data);
            $this->session->set_flashdata('message', '<div id="message">Employee Info saved Successfully.</div>');
            redirect('hr/info/add_employee_general_info', 'location', 301);
        }
    }

	
		function check_unique_employee_id_for_update_employee_info($field_value){
			$rv = true;
			$carNo = $field_value;
			$employees = $this->info_model->get_employees($carNo);
            //var_dump($employees[0]->id, $this->input->post('id')); exit;
			if( count($employees) > 0 AND ($employees[0]->id != $this->input->post('id')) ){ 
				$this->form_validation->set_message('check_unique_employee_id_for_update_employee_info', 'Duplicate ID');
					$rv = false;
			}
			return $rv;
		}
	
	
    function employee_general_info_edit($id = NULL) {
		$this->load->library('form_validation');
        $this->data['mainTab'] = 'employee';
        $this->data['activeTab'] = 'general_info';

	    $this->form_validation->set_rules('card_no', 'ID No', 'trim|required|callback_check_unique_employee_id_for_update_employee_info');
	    $this->form_validation->set_rules('secrete_no', 'Card No', 'trim|required');
        $this->form_validation->set_rules('name', 'Name', 'trim|required');
		$this->form_validation->set_rules('branch_name', 'SBU Name', 'trim|required');
        $this->form_validation->set_rules('dept', 'Department', 'trim|required');
        $this->form_validation->set_rules('designation', 'Designation', 'trim|required');
        $this->form_validation->set_rules('username', 'Username', 'trim|required');
        $this->form_validation->set_rules('password', 'Password', 'trim|required');
        $this->form_validation->set_rules('email', 'Email', 'trim|required|valid_email');
        $this->form_validation->set_rules('category', 'Category', 'trim|required');

        if ($this->input->post('mysubmit') && $this->form_validation->run() == true) {
            $id = $this->input->post('id');
            $idno = $this->input->post('card_no');
            $query = $this->db->query("select * from employeeinfo where card_no='" . $idno . "'and id !='" . $id . "'");
            if ($query->num_rows() > 0) {
                $this->session->set_flashdata('message', '<div id="message">ID No exists.</div>');
                redirect('hr/info/employee_general_info_edit/' . $id);
            } else {
                $this->load->model('hr/info_model');
                $config['upload_path'] = './uploads/';
                $config['allowed_types'] = 'gif|jpg|png';
                $this->load->library('upload');
                $this->upload->initialize($config);
                if ($this->upload->do_upload()) {
                    $data = $this->upload->data();
                    $image_name = $data['file_name'];
                    $fileData = array(
                        'photo' => $image_name
                    );
                    $this->info_model->employee_general_info_update($fileData);
                }

                $data = array(
                    'name' => $this->input->post('name'),
                    'card_no' => $this->input->post('card_no'),
                    'dept' => $this->input->post('dept'),
                    'branch_name' => $this->input->post('branch_name'),
                    'proffession' => $this->input->post('proffession'),
                    'leave_name' => $this->input->post('leave_name'),
                    'dob' => $this->input->post('date_of_birth'),
                    'gender' => $this->input->post('gender'),
                    'marital_status' => $this->input->post('marital_status'),
                    'nationality' => $this->input->post('nationality'),
                    'blood' => $this->input->post('blood'),
                    'telephone' => $this->input->post('telephone'),
                    'contact_person' => $this->input->post('contact_person'),
                    'national_id' => $this->input->post('national_id'),
                    'email' => $this->input->post('email'),
                    'emergency_no' => $this->input->post('emergency_no'),
                    'relation' => $this->input->post('relation'),
                    'join_date' => $this->input->post('join_date'),
                    'notice_period' => $this->input->post('notice_period'),
                    'attendence_rules' => $this->input->post('attendence_rules'),
                    'provident_fund' => $this->input->post('provident_fund'),
                    'note' => $this->input->post('note'),
                    'secrete_no' => $this->input->post('secrete_no'),
                    'emp_type' => $this->input->post('emp_type'),
                    'designation' => $this->input->post('designation'),
                    'category' => $this->input->post("category"),
                    'username' => $this->input->post("username"),
                    'password' => $this->input->post("password"),
                    'sup_designation' => $this->input->post("sup_designation"),
                    'supervisor' => $this->input->post("supervisor"),
                    'confirmation_date' => $this->input->post("confirmation_date"),
                    'reference_name' => $this->input->post("reference_name"),
                    'referee_contact_no' => $this->input->post("referee_contact_no"),
                    'sapackage' => 0,
                    'basic' => $this->input->post('basic'),
                    'house_rent' => $this->input->post('house_rent'),
                    'medical' => $this->input->post('medical'),
                    'conveyance' => $this->input->post('conveyance'),
                    'special' => $this->input->post('special'),
                    'salary_step' => $this->input->post('salary_step'),
                    'salary_band' => $this->input->post('salary_band'),
                    'mobile_bill' => $this->input->post('mobile_bill'),
                    'tax_deduction' => $this->input->post('tax_deduction'),
                    'tax_percentage' => $this->input->post('tax_percentage'),
                    'present_address' => $this->input->post('present_address'),
                    'permanent_address' => $this->input->post('permanent_address'),
                    'personal_email' => $this->input->post('personal_email'),
                    'employeer_pf_balance' => $this->input->post('employeer_pf_balance'),
                    'employee_pf_balance' => $this->input->post('employee_pf_balance'),
                    'timeframe' => $this->input->post('timeframe'),
                    'duty_schedule' => $this->input->post('duty_schedule')
                );

                $this->info_model->employee_general_info_update($data);
                if (($this->input->post('cash_payment') == 1) && $this->input->post('bank_payment') == 2) {
                    
                } elseif ($this->input->post('cash_payment') == 1 && $this->input->post('bank_payment') != 2) {
                    $data = array(
                        'payment_mode' => $this->input->post('cash_payment')
                    );
                    $this->info_model->employee_general_info_update($data);
                } elseif ($this->input->post('bank_payment') == 2 && $this->input->post('cash_payment') != 1) {
                    $data = array(
                        'payment_mode' => $this->input->post('bank_payment'),
                        'bank_name' => $this->input->post('bank_name'),
                        'bank_branch' => $this->input->post('bank_branch'),
                        'account_number' => $this->input->post('account_number')
                    );
                    $this->info_model->employee_general_info_update($data);
                } else {
                    
                }
                redirect('hr/employee/get_all_employee', 'location', 301);
            }
        }
        if ($id != NULL) {
            $this->load->model('hr/info_model');
            $this->data['query'] = $this->info_model->get_employee_general_info($id);
			$this->data['id'] = $id;
            $this->middle = 'hr/empinfo/employee_general_info_edit';
            $this->layout();

        }
		
    }

    /* End Edit employee_info */

    /* Start Delete employee_info */

    function employee_general_info_delete($id = NULL) {
        $this->load->model('hr/info_model');
        if ($this->session->userdata("utype") == '1'):
            $this->info_model->delete_employee_general_info($id);
        endif;
        redirect('hr/employee/get_all_employee', 'location', 301);
    }


    function update_secrete_no() {
        $query = $this->db->query("select * from employeeinfo");
        foreach ($query->result() as $info):
            $id = $info->id;
            $p_sl = $info->secrete_no;
            $aIdlen = strlen($p_sl);
            if ($aIdlen == 1): $asIdlen = "000000000";
            elseif ($aIdlen == 2): $asIdlen = "00000000";
            elseif ($aIdlen == 3): $asIdlen = "0000000";
            elseif ($aIdlen == 4): $asIdlen = "000000";
            elseif ($aIdlen == 5): $asIdlen = "00000";
            elseif ($aIdlen == 6): $asIdlen = "0000";
            elseif ($aIdlen == 7): $asIdlen = "000";
            elseif ($aIdlen == 8): $asIdlen = "00";
            elseif ($aIdlen == 9): $asIdlen = "0";
            elseif ($aIdlen == 10): $asIdlen = "";
            endif;
            $en_id = $asIdlen . $p_sl;
            $data = array(
                'secrete_no' => $en_id
            );
            $this->info_model->update_secrete_no($data, $id);
        endforeach;
    }

    function view_summery() {
        $this->data['mainTab'] = 'report';
        $this->data['activeTab'] = 'view_summery';
        $this->data['dynamicView'] = 'hr/summery/summery_form';
        $this->_commonPageLayout('frontend_viewer');
    }

    function viewed_summery() {
        $this->data['mainTab'] = 'report';
        $this->data['activeTab'] = 'view_summery';
        $this->load->library('form_validation');
        $this->form_validation->set_rules('branch_name', 'Branch Name', 'trim|required');
        if ($this->form_validation->run() == FALSE) {
            $this->data['dynamicView'] = 'hr/summery/summery_form';
            $this->_commonPageLayout('frontend_viewer');
        } else {
            if ($this->input->post("dept") != "" && $this->input->post("proffession") == "") {
                $dept = $this->input->post("dept");
                $this->data['query1'] = $this->info_model->get_section_dept($dept);
            } else if ($this->input->post("dept") != "" && $this->input->post("proffession") != "") {
                $section = $this->input->post("proffession");
                $this->data['query1'] = $this->info_model->get_section($section);
            } else {
                $this->data['query1'] = $this->info_model->get_all_section();
            }
            $this->data['dynamicView'] = 'hr/summery/details';
            $this->_commonPageLayout('frontend_viewer');
        }
    }

    //Start Other Info
    function add_other() {
        $this->data['title'] = 'Employee Info';
        $this->data['mainTab'] = 'employee';
        $this->data['activeTab'] = 'other_info';
        $this->middle = 'hr/other/add';
        $this->layout();
    }

    function added() {
        $this->data['mainTab'] = 'employee';
        $this->data['activeTab'] = 'other_info';

        $this->load->library('form_validation');
        $this->form_validation->set_rules('card_no', 'ID No', 'trim|required');
        if ($this->form_validation->run() == FALSE) {
            $this->middle = 'hr/other/add';
            $this->layout();
        } else {
            $query = $this->db->query("select card_no from employeeinfo where card_no='" . $this->input->post('card_no') . "'");
            if ($query->num_rows() > 0):
                $this->data['cardNo'] = $this->input->post('card_no');
                $this->middle = 'hr/other/entry';
                $this->layout();
            else:
                $this->session->set_flashdata("message", "Invalid ID no");
                redirect("hr/info/add_other");
            endif;
        }
    }

    function add($id = NULL) {
        $this->data['mainTab'] = 'employee';
        $this->data['activeTab'] = 'other_info';
        $this->data['cardNo'] = $id;
        $this->middle = 'hr/other/entry';
        $this->layout();
    }

    function academicEntry() {
        $id = $this->input->post('cno');
        $data = array(
            'exam_name' => $this->input->post("exam_name"),
            'institute_name' => $this->input->post('institute_name'),
            'result' => $this->input->post('result'),
            'passing_year' => $this->input->post('passing_year'),
            'note' => $this->input->post('note'),
            'emp_id' => $this->input->post('empid'),
            'card_no' => $this->input->post('cno'),
            'status' => 1
        );
        $this->shows->insertThisValue($data, 'temp');
        redirect("hr/info/add/" . $id);
    }

    function extraEntry() {
        $id = $this->input->post('cno');
        $data = array(
            'exam_name' => $this->input->post("exam_name1"),
            'institute_name' => $this->input->post('institute_name1'),
            'result' => $this->input->post('result1'),
            'passing_year' => $this->input->post('passing_year1'),
            'note' => $this->input->post('note1'),
            'emp_id' => $this->input->post('empid'),
            'card_no' => $this->input->post('cno'),
            'status' => 2
        );
        $this->shows->insertThisValue($data, 'temp');
        redirect("hr/info/add/" . $id);
    }

    function historyEntry() {
        $id = $this->input->post('cno');
        $data = array(
            'company_name' => $this->input->post("company_name"),
            'designation' => $this->input->post('designation'),
            'start_date' => $this->input->post('start_date'),
            'end_date' => $this->input->post('end_date'),
            'note' => $this->input->post('note2'),
            'emp_id' => $this->input->post('empid'),
            'card_no' => $this->input->post('cno'),
            'status' => 3
        );
        $this->shows->insertThisValue($data, 'temp');
        redirect("hr/info/add/" . $id);
    }

    function save() {
        $this->data['mainTab'] = 'employee';
        $this->data['activeTab'] = 'other_info';
        $emp_id = $this->input->post('empid');
        $this->data['query'] = $this->info_model->get_temp_info($emp_id);
        foreach ($this->data['query'] as $info):
            $status = $info->status;
            if ($status == "1") {
                $data = array(
                    'emp_id' => $info->emp_id,
                    'exam_name' => $info->exam_name,
                    'institute_name' => $info->institute_name,
                    'result' => $info->result,
                    'passing_year' => $info->passing_year,
                    'note' => $info->note
                );
                $this->shows->insertThisValue($data, 'academic');
            } else if ($status == "2") {
                $data = array(
                    'emp_id' => $info->emp_id,
                    'qualification_name' => $info->exam_name,
                    'institute_name' => $info->institute_name,
                    'result' => $info->result,
                    'year' => $info->passing_year,
                    'note' => $info->note
                );
                $this->shows->insertThisValue($data, 'qualification');
            } else if ($status == "3") {
                $data = array(
                    'emp_id' => $info->emp_id,
                    'company_name' => $info->company_name,
                    'designation' => $info->designation,
                    'start' => $info->start_date,
                    'end_date' => $info->end_date,
                    'note' => $info->note
                );
                $this->shows->insertThisValue($data, 'career_history');
            } else {
                
            }
        endforeach;
        $id = $this->input->post('cno');
        $this->shows->deleteThisValue("emp_id = '$emp_id'", "temp");
        $this->session->set_flashdata("message", "Data Saved Successfully.");
        redirect("hr/info/add/" . $id);
    }

    function remove($id = NULL) {
        $cardNo = "";
        $query = $this->db->query("select * from temp where id='" . $id . "'");
        foreach ($query->result() as $t):
            $cardNo = $t->card_no;
        endforeach;
        $this->shows->deleteThisValue("id = '$id'", "temp");
        redirect("hr/info/add/" . $cardNo);
    }

    //End Other Info



}