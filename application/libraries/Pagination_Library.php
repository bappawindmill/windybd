<?php

    defined('BASEPATH') OR exit('No direct script access allowed');

class Pagination_Library {


    public $full_tag_open ;
    public $full_tag_close ;
    public $first_link ;
    public $first_tag_open ;
    public $first_tag_close ;
    public $last_link ;
    public $last_tag_open ;
    public $last_tag_close ;
    public $next_link ;
    public $next_tag_open ;
    public $next_tag_close ;
    public $prev_link ;
    public $prev_tag_open ;
    public $prev_tag_close;
    public $cur_tag_open ;
    public $cur_tag_close ;
    public $num_tag_open ;
    public $num_tag_close ;
    public $per_page ;
    public $total_rows ;
    public $base_url ;

    public function __construct()
    {
        $this->full_tag_open= '<div class"text-center"><ul class="pagination">';
        $this->full_tag_close= '</ul></div>';
        $this->first_link=  "&laquo;";
        $this->first_tag_open= "<li>";
        $this->first_tag_close=  "</li>";
        $this->last_link=  "&raquo;";
        $this->last_tag_open= "<li>";
        $this->last_tag_close= "</li>";
        $this->next_link= 'Next';
        $this->next_tag_open= '<li>';
        $this->next_tag_close= "</li>";
        $this->prev_link= 'Previous';
        $this->prev_tag_open= '<li>';
        $this->prev_tag_close= "</li>";
        $this->cur_tag_open= '<li class="active"><a href="#">';
        $this->cur_tag_close= '</a></li>';
        $this->num_tag_open= "<li>";
        $this->num_tag_close= "</li>";
        $this->per_page= 10;
        $this->total_rows= 0;
        $this->base_url= '';



    }

    public function getArray()
    {
        $config["full_tag_open"] = $this->full_tag_open;
        $config["full_tag_close"] = $this->full_tag_close;
        $config["first_link"] = $this->first_link;
        $config["first_tag_open"] = $this->first_tag_open;
        $config["first_tag_close"] = $this->first_tag_close;
        $config["last_link"] = $this->last_link;
        $config["last_tag_open"] = $this->last_tag_open;
        $config["last_tag_close"] = $this->last_tag_close;
        $config['next_link'] = $this->next_link;
        $config['next_tag_open'] = $this->next_tag_open;
        $config['next_tag_close'] = $this->next_tag_close;
        $config['prev_link'] = $this->prev_link;
        $config['prev_tag_open'] = $this->prev_tag_open;
        $config['prev_tag_close'] = $this->prev_tag_close;
        $config['cur_tag_open'] = $this->cur_tag_open;
        $config['cur_tag_close'] = $this->cur_tag_close;
        $config['num_tag_open'] = $this->num_tag_open;
        $config['num_tag_close'] = $this->num_tag_close;
        $config['per_page'] = $this->per_page;
        $config['total_rows'] = $this->total_rows;
        $config['base_url'] = $this->base_url;
        
        $config['page_query_string'] = TRUE;
        $config['query_string_segment'] = 'start';

        return $config;

    }

}