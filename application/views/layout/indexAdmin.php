<!DOCTYPE html>
<html class="no-js">
<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta name="description" content="">
    <meta name="author" content="Dashboard">
    <meta name="keyword" content="Dashboard, Bootstrap, Admin, Template, Theme, Responsive, Fluid, Retina">

    <title><?php echo $title;?></title>


    <script src="<?php echo base_url(); ?>assets/js/jquery-1.8.3.min.js"></script>

    
    <link rel="shortcut icon" href="<?php echo base_url(); ?>assets/img/w_fav_icon.png">
    <!-- Bootstrap core CSS -->
    <link href="<?php echo base_url(); ?>assets/css/bootstrap.css" rel="stylesheet">
    <!--<link href="<?php echo base_url(); ?>assets/css/bootstrap.min.css" rel="stylesheet"> -->
    <!--external css-->
    <link href="<?php echo base_url(); ?>assets/font-awesome/css/font-awesome.css" rel="stylesheet" />
    <link rel="stylesheet" type="text/css" href="<?php echo base_url(); ?>assets/css/zabuto_calendar.css">
    <link rel="stylesheet" type="text/css" href="<?php echo base_url(); ?>assets/js/gritter/css/jquery.gritter.css" />
    <link rel="stylesheet" type="text/css" href="<?php echo base_url(); ?>assets/lineicons/style.css">

    <!-- Custom styles for this template -->
    <link href="<?php echo base_url(); ?>assets/css/style.css" rel="stylesheet">
    <link href="<?php echo base_url(); ?>assets/css/flip_scroll.css" rel="stylesheet">
    <link href="<?php echo base_url(); ?>assets/css/style-responsive.css" rel="stylesheet">

    <link href="<?php echo base_url(); ?>assets/css/facebox.css" media="screen" rel="stylesheet" type="text/css" />

    <link rel="stylesheet" type="text/css" media="all" href="<?php echo base_url(); ?>assets/css/jsDatePick_ltr.min.css" />

    <!-- new add css for calendar-->
    <link href="<?php echo base_url(); ?>assets/datetimepicker/jquery-ui-1.12.1/jquery-ui.min.css" rel="stylesheet">
    <link href="<?php echo base_url(); ?>assets/datetimepicker/general.css" rel="stylesheet">

    <script src="<?php echo base_url(); ?>assets/fullcalendar/fullcalendar.min.css"></script>
    <script src="<?php echo base_url(); ?>assets/fullcalendar/fullcalendar.print.css"></script>

    <!-- <link href="<?php echo base_url(); ?>assets/css/bootstrap-responsive.min.css" rel="stylesheet"> -->

    <script src="<?php echo base_url(); ?>assets/js/chart-master/Chart.js"></script>

    <!-- HTML5 shim and Respond.js IE8 support of HTML5 elements and media queries -->
    <!--[if lt IE 9]>
    <script src="https://oss.maxcdn.com/libs/html5shiv/3.7.0/html5shiv.js"></script>
    <script src="https://oss.maxcdn.com/libs/respond.js/1.4.2/respond.min.js"></script>

    <![endif]-->
</head>
<body>
<?php //if($header) echo $header ;?>


<section id="container" >
    <!-- **********************************************************************************************************************************************************
    TOP BAR CONTENT & NOTIFICATIONS
    *********************************************************************************************************************************************************** -->
    <!--header start-->
    <header class="header black-bg">
        <div class="sidebar-toggle-box">
            <div class="fa fa-bars tooltips" data-placement="right" data-original-title="Toggle Navigation"></div>
        </div>
        <!--logo start-->
        <a href="<?php echo base_url(); ?>" class="logo"><b>WINDY BD</b></a>
        <!--logo end-->
        <div class="nav notify-row" id="top_menu">
            <!--  notification start -->
            <ul class="nav top-menu">
                <!-- settings start -->
                <!--<li class="dropdown">
                    <a data-toggle="dropdown" class="dropdown-toggle" href="index.html#">
                        <i class="fa fa-tasks"></i>
                        <span class="badge bg-theme">4</span>
                    </a>
                    <ul class="dropdown-menu extended tasks-bar">
                        <div class="notify-arrow notify-arrow-green"></div>
                        <li>
                            <p class="green">You have 4 pending tasks</p>
                        </li>
                        <li>
                            <a href="index.html#">
                                <div class="task-info">
                                    <div class="desc">DashGum Admin Panel</div>
                                    <div class="percent">40%</div>
                                </div>
                                <div class="progress progress-striped">
                                    <div class="progress-bar progress-bar-success" role="progressbar" aria-valuenow="40" aria-valuemin="0" aria-valuemax="100" style="width: 40%">
                                        <span class="sr-only">40% Complete (success)</span>
                                    </div>
                                </div>
                            </a>
                        </li>
                        <li>
                            <a href="index.html#">
                                <div class="task-info">
                                    <div class="desc">Database Update</div>
                                    <div class="percent">60%</div>
                                </div>
                                <div class="progress progress-striped">
                                    <div class="progress-bar progress-bar-warning" role="progressbar" aria-valuenow="60" aria-valuemin="0" aria-valuemax="100" style="width: 60%">
                                        <span class="sr-only">60% Complete (warning)</span>
                                    </div>
                                </div>
                            </a>
                        </li>
                        <li>
                            <a href="index.html#">
                                <div class="task-info">
                                    <div class="desc">Product Development</div>
                                    <div class="percent">80%</div>
                                </div>
                                <div class="progress progress-striped">
                                    <div class="progress-bar progress-bar-info" role="progressbar" aria-valuenow="80" aria-valuemin="0" aria-valuemax="100" style="width: 80%">
                                        <span class="sr-only">80% Complete</span>
                                    </div>
                                </div>
                            </a>
                        </li>
                        <li>
                            <a href="index.html#">
                                <div class="task-info">
                                    <div class="desc">Payments Sent</div>
                                    <div class="percent">70%</div>
                                </div>
                                <div class="progress progress-striped">
                                    <div class="progress-bar progress-bar-danger" role="progressbar" aria-valuenow="70" aria-valuemin="0" aria-valuemax="100" style="width: 70%">
                                        <span class="sr-only">70% Complete (Important)</span>
                                    </div>
                                </div>
                            </a>
                        </li>
                        <li class="external">
                            <a href="#">See All Tasks</a>
                        </li>
                    </ul>
                </li>-->
                <!-- settings end -->
                <!-- inbox dropdown start-->
               <!-- <li id="header_inbox_bar" class="dropdown">
                    <a data-toggle="dropdown" class="dropdown-toggle" href="index.html#">
                        <i class="fa fa-envelope-o"></i>
                        <span class="badge bg-theme">5</span>
                    </a>
                    <ul class="dropdown-menu extended inbox">
                        <div class="notify-arrow notify-arrow-green"></div>
                        <li>
                            <p class="green">You have 5 new messages</p>
                        </li>
                        <li>
                            <a href="index.html#">
                                <span class="photo"><img alt="avatar" src="<?php /*echo base_url(); */?>assets/img/ui-zac.jpg"></span>
                                    <span class="subject">
                                    <span class="from">Zac Snider</span>
                                    <span class="time">Just now</span>
                                    </span>
                                    <span class="message">
                                        Hi mate, how is everything?
                                    </span>
                            </a>
                        </li>
                        <li>
                            <a href="index.html#">
                                <span class="photo"><img alt="avatar" src="<?php /*echo base_url(); */?>assets/img/ui-divya.jpg"></span>
                                    <span class="subject">
                                    <span class="from">Divya Manian</span>
                                    <span class="time">40 mins.</span>
                                    </span>
                                    <span class="message">
                                     Hi, I need your help with this.
                                    </span>
                            </a>
                        </li>
                        <li>
                            <a href="index.html#">
                                <span class="photo"><img alt="avatar" src="<?php /*echo base_url(); */?>assets/img/ui-danro.jpg"></span>
                                    <span class="subject">
                                    <span class="from">Dan Rogers</span>
                                    <span class="time">2 hrs.</span>
                                    </span>
                                    <span class="message">
                                        Love your new Dashboard.
                                    </span>
                            </a>
                        </li>
                        <li>
                            <a href="index.html#">
                                <span class="photo"><img alt="avatar" src="<?php /*echo base_url(); */?>assets/img/ui-sherman.jpg"></span>
                                    <span class="subject">
                                    <span class="from">Dj Sherman</span>
                                    <span class="time">4 hrs.</span>
                                    </span>
                                    <span class="message">
                                        Please, answer asap.
                                    </span>
                            </a>
                        </li>
                        <li>
                            <a href="index.html#">See all messages</a>
                        </li>
                    </ul>
                </li>-->
                <!-- inbox dropdown end -->
            </ul>
            <!--  notification end -->
        </div>
        <!--<div class="top-menu">
            <ul class="nav pull-right top-menu">
                <li>
                    <a class="logout" href="<?php /*echo base_url(); */?>hr/login/logout" id="logout">Logout</a>
                </li>
            </ul>
        </div>-->
        <div class="top-menu">
            <ul class="dropdown nav pull-right">
                <!--<li><a class="logout" href="login.html"><i class="fa fa-power-off" aria-hidden="true"></i>  &nbsp;Logout</a></li>-->
                <a href="javascript:;" title="" class="user-ico clearfix" data-toggle="dropdown">
                    <i class="glyphicon glyphicon-user"></i>&nbsp;
                    <!-- <img src="<?php //echo $img; ?>" width="40" height="30" alt="" /> -->
                    <span>Admin: <?php echo $this->session->userdata('username'); ?></span>
                    <i class="glyphicon glyphicon-chevron-down"></i>
                </a>
                <ul class="dropdown-menu float-right">
                    <!--<li>
                            <a id="Inbox" class="ModalTitle_Change_Pass" data-id="" sessionid="<?php //echo $sessionid;?>" data-original-title="Change Password" data-target="#Change_pass" data-toggle="modal" href="javascript:;">
                                <i class="glyphicon glyphicon-lock"></i>
                                Change Password
                            </a>
                        </li>-->
                    <li>
                        <a class="logout" href="<?php echo base_url(); ?>hr/login/logout" id="logout">
                            <i class="glyphicon glyphicon-off"></i>
                            <span class="font-bold">Logout</span>
                        </a>
                    </li>
                    <li class="divider"></li>
                </ul>
            </ul>
        </div>
    </header>
    <!--header end-->


    <?php //if($left) echo $left ;?>
    <!-- Left bar -->
    <?php
        $getCon = $this->uri->segment(2);
        $getMet = $this->uri->segment(3);

    ?>
    <aside>
        <div id="sidebar"  class="nav-collapse ">
            <!-- sidebar menu start-->
            <ul class="sidebar-menu" id="nav-accordion">

                <li class="sub-menu">
                    <a <?php echo($getCon=="hr")?'class="active"': ""?>  href="<?php echo base_url(); ?>hr/hr/index" >
                        <i class="fa fa-dashboard"></i>
                        <span>Home</span>
                    </a>
                </li>

                <?php if ($this->session->userdata("utype") == '1'): ?>
                    <li class="sub-menu">
                        <a <?php echo ($getCon == 'user') ? "class='active'" : ""; ?> href="javascript:;" >
                            <i class="fa fa-user"></i>
                            <span>User</span>
                        </a>
                        <ul class="sub">
                            <li <?php echo ($getMet=="list_user")?'class="active"': ""?>><a  href="<?php echo base_url(); ?>hr/user/list_user">User List</a></li>
<!--                            <li><a  href="--><?php //echo base_url(); ?><!--hr/user/change_password">Change Password</a></li>-->
                        </ul>
                    </li>
                <?php endif; ?>


                <?php if ($this->session->userdata("conf_prev") == '1'): ?>

                    <li class="sub-menu">
                        <a <?php echo (@$mainTab == 'configuration') ? "class='active'" : ""; ?> href="javascript:;" >
                            <i class="fa fa-wrench"></i>
                            <span>Configuration</span>
                        </a>
                        <ul class="sub">
                            <li <?php echo (@$activeTab=="branch")?'class="active"': ""?> ><a  href="<?php echo base_url(); ?>hr/branch/branch_list">View Branch</a></li>
                            <li <?php echo (@$activeTab=="view_department")?'class="active"': ""?> ><a  href="<?php echo base_url(); ?>hr/department/department_list">View Department</a></li>
                            <li <?php echo (@$activeTab=="profession_list")?'class="active"': ""?> ><a  href="<?php echo base_url(); ?>hr/profession/profession_list">View Section</a></li>
                            <li <?php echo (@$activeTab=="emp_type_list")?'class="active"': ""?> ><a  href="<?php echo base_url(); ?>hr/department/emp_type_list">View EMP Type</a></li>
                            <li <?php echo (@$activeTab=="designation_list")?'class="active"': ""?> ><a  href="<?php echo base_url(); ?>hr/department/designation_list">Designation</a></li>
                            <li <?php echo (@$activeTab=="weekend_list")?'class="active"': ""?> ><a  href="<?php echo base_url(); ?>hr/weekend/insert_weekend">View Weekend</a></li>
                            <!--<li <?php /*echo (@$activeTab=="holiday_list")?'class="active"': ""*/?> ><a  href="<?php /*echo base_url(); */?>hr/holiday_package/holiday_list">View Holiday</a></li>-->
                            <li <?php echo (@$activeTab=="holiday_list")?'class="active"': ""?> ><a  href="<?php echo base_url(); ?>hr/holiday_package/holiday_list_calendar">View Holiday</a></li>
                            <li <?php echo (@$activeTab=="provident_fund_rate")?'class="active"': ""?> ><a  href="<?php echo base_url(); ?>hr/provident_fund/add">PF Rate</a></li>
                            <li <?php echo (@$activeTab=="schedule_list")?'class="active"': ""?> ><a  href="<?php echo base_url(); ?>hr/schedule/schedule_list">Duty Schedule</a></li>
                            <!--<li <?php /*echo (@$activeTab=="submit_control")?'class="active"': ""*/?> ><a  href="<?php /*echo base_url(); */?>hr/department/add_control">Submit Control</a></li>-->
                            <li <?php echo (@$activeTab=="bankinfo_list")?'class="active"': ""?> ><a  href="<?php echo base_url(); ?>hr/bank/bankinfo_list">Bank</a></li>
                            <li <?php echo (@$activeTab=="branchinfo_list")?'class="active"': ""?> ><a  href="<?php  echo base_url(); ?>hr/bank/branchinfo_list">Branch</a></li>
                        </ul>
                    </li>
                <?php endif; ?>
                <?php if ($this->session->userdata("leave_prev") == '1'): ?>
                    <li class="sub-menu">
                        <a <?php echo (@$mainTab == 'leave') ? "class='active'" : ""; ?> href="javascript:;" >
                            <i class="fa fa-bell"></i>
                            <span>Leave</span>
                        </a>
                        <ul class="sub">
                            <li <?php echo (@$activeTab=="leave_package_list")?'class="active"': ""?> ><a  href="<?php echo base_url(); ?>hr/leave_package/leave_package_list">View Leave Package</a></li>
                            <li <?php echo (@$activeTab=="leave_report")?'class="active"': ""?> ><a  href="<?php echo base_url(); ?>hr/leave/leave_report">Leave Reprot</a></li>
                            <li <?php echo (@$activeTab=="leave_report_all")?'class="active"': ""?> ><a  href="<?php echo base_url(); ?>hr/leave/leave_all">Leave Reprot(All)</a></li>
                            <li <?php echo (@$activeTab=="dateWiseLeave")?'class="active"': ""?> ><a  href="<?php echo base_url(); ?>hr/leave/dateWiseLeave">Date Wise Leave</a></li>
                            <li <?php echo (@$activeTab=="leave_summary")?'class="active"': ""?> ><a  href="<?php echo base_url(); ?>hr/leave/view_summary">Leave Summary</a></li>
                            <li <?php echo (@$activeTab=="view_application")?'class="active"': ""?> ><a  href="<?php echo base_url();?>hr/leave/view_application">View Leave Application</a></li>
                            <!--<li <?php /*echo (@$activeTab=="dashboard_list")?'class="active"': ""*/?> ><a  href="<?php /*echo base_url();*/?>hr/dashboard/dashboard_list">Dashboard Setting</a></li>-->
                        </ul>
                    </li>
                <?php endif; ?>


                <?php if ($this->session->userdata("salary_prev") == '1'): ?>
                    <li class="sub-menu">
                        <a <?php echo (@$mainTab == 'salary') ? "class='active'" : ""; ?> href="javascript:;" >
                            <i class="fa fa-dollar"></i>
                            <span>Salary</span>
                        </a>
                        <ul class="sub">
                            <li <?php echo (@$activeTab=="arrear_form")?'class="active"': ""?> ><a  href="<?php echo base_url(); ?>hr/arrear/arrear_form">Arrear</a></li>
                            <li <?php echo (@$activeTab=="kpi1")?'class="active"': ""?> ><a  href="<?php echo base_url(); ?>hr/kpi/add">KPI</a></li>
                            <li <?php echo (@$activeTab=="ot")?'class="active"': ""?> ><a  href="<?php echo base_url(); ?>hr/ot/add">OT Sheet</a></li>
                            <li <?php echo (@$activeTab=="loan")?'class="active"': ""?> ><a  href="<?php echo base_url(); ?>hr/loan/add">Add Loan</a></li>
                            <li <?php echo (@$activeTab=="loan_view")?'class="active"': ""?> ><a  href="<?php echo base_url(); ?>hr/loan/view">Loan Report</a></li>
                            <li <?php echo (@$activeTab=="advance")?'class="active"': ""?> ><a  href="<?php echo base_url(); ?>hr/advance/add">Advance</a></li>
                            <li <?php echo (@$activeTab=="kpi")?'class="active"': ""?> ><a  href="<?php echo base_url(); ?>hr/kpi_deduction/add">KPI Deduction</a></li>
                            <li <?php echo (@$activeTab=="create_salary_hourly")?'class="active"': ""?> ><a  href="<?php echo base_url(); ?>hr/monthly_salary/add_hourly_salary">Create Salary</a></li>
                            <li <?php echo (@$activeTab=="view_hourly_salary")?'class="active"': ""?> ><a  href="<?php echo base_url(); ?>hr/monthly_salary/view_hourly_salary">Salary Sheet</a></li>
                            <li <?php echo (@$activeTab=="cash_sheet")?'class="active"': ""?> ><a  href="<?php echo base_url(); ?>hr/monthly_salary/view_cash_sheet">Cash Sheet</a></li>
                            <li <?php echo (@$activeTab=="banksheet")?'class="active"': ""?> ><a  href="<?php echo base_url(); ?>hr/monthly_salary/view_banksheet">Bank Sheet</a></li>
                            <li <?php echo (@$activeTab=="salary_slip")?'class="active"': ""?> ><a  href="<?php echo base_url(); ?>hr/payslip/view_form">Pay Slip</a></li>
                        </ul>
                    </li>
                <?php endif; ?>

                <?php if ($this->session->userdata("hr_prev") == '1'): ?>
                    <li class="sub-menu">
                        <a <?php echo (@$mainTab == 'employee') ? "class='active'" : ""; ?> href="javascript:;" >
                            <i class="glyphicon glyphicon-plus-sign"></i>
                            <span>Employee Info Entry</span>
                        </a>
                        <ul class="sub">
                            <li <?php echo (@$activeTab=="general_info")?'class="active"': ""?> ><a  href="<?php echo base_url(); ?>hr/info/add_employee_general_info">General & Official Info</a></li>
                            <li <?php echo (@$activeTab=="other_info")?'class="active"': ""?> ><a  href="<?php echo base_url(); ?>hr/info/add_other">Other Info</a></li>
                            <li <?php echo (@$activeTab=="employee_doc_info_list")?'class="active"': ""?> ><a  href="<?php echo base_url(); ?>hr/doc/employee_doc_info_list">Employee Document</a></li>
                            <li <?php echo (@$activeTab=="appraisal")?'class="active"': ""?> ><a  href="<?php echo base_url(); ?>hr/appraisal/add_other">Appraisal</a></li>
                        </ul>
                    </li>
                <?php endif; ?>

                <?php if ($this->session->userdata("report_prev") == '1'): ?>
                    <li class="sub-menu">
                        <a <?php echo (@$mainTab == 'report') ? "class='active'" : ""; ?> href="javascript:;" >
                            <i class="glyphicon glyphicon-print"></i>
                            <span>Report</span>
                        </a>
                        <ul class="sub">
                            <li <?php echo (@$activeTab=="all_employee")?'class="active"': ""?> ><a  href="<?php echo base_url(); ?>hr/employee/get_all_employee">All Employee</a></li>
                            <li <?php echo (@$activeTab=="employee_report")?'class="active"': ""?> ><a  href="<?php echo base_url(); ?>hr/employee/emp_report">Employee Report</a></li>
                            <li <?php echo (@$activeTab=="provident_fund")?'class="active"': ""?> ><a  href="<?php echo base_url(); ?>hr/provident_fund/pf_form">Provident Found Report</a></li>
                            <li <?php echo (@$activeTab=="attendance_log")?'class="active"': ""?> ><a  href="<?php echo base_url(); ?>hr/employee/get_all_attendance_statistics">Attendance Log</a></li>
                            <li <?php echo (@$activeTab=="in_out_time")?'class="active"': ""?> ><a  href="<?php echo base_url(); ?>hr/employee/get_in_out_time">In Out Time</a></li>
                        </ul>
                    </li>
                <?php endif; ?>

                <?php if ($this->session->userdata("attendance_prev") == '1'): ?>
                    <li class="sub-menu">
                        <a <?php echo (@$mainTab == 'attendence') ? "class='active'" : ""; ?> href="javascript:;" >
                            <i class="fa fa-file"></i>
                            <span>Attendance</span>
                        </a>
                        <ul class="sub">
                            <li <?php echo (@$activeTab=="attendence_request")?'class="active"': ""?> ><a  href="<?php echo base_url(); ?>hr/attendence/attendence_request">Attendance Request</a></li>
                            <li <?php echo (@$activeTab=="viewSummary")?'class="active"': ""?> ><a  href="<?php echo base_url(); ?>hr/attendence/viewSummary">Attendance Summary</a></li>
                            <li <?php echo (@$activeTab=="viewAttendance")?'class="active"': ""?> ><a  href="<?php echo base_url(); ?>hr/user_attendance/view">View Attendance</a></li>
                        </ul>
                    </li>
                <?php endif; ?>


            </ul>

        </div>
    </aside>
    <!--Left bar end -->

    <section id="main-content">
        <section class="wrapper">
            <?php if($middle) echo $middle ;?>
        </section>
    </section>

    <section>
        <!--footer start-->
        <footer class="site-footer">
            <div class="text-right">
                Copyright &copy; <?php echo date("Y"); ?>. All rights reserved. Design & Developed by &nbsp;<a href="http://windmillbd.net/"><img src="<?php echo base_url(); ?>assets/img/copy_logo.png" height="36px" alt="" style="margin-top:-5px;margin-right: 10px;"></a>
                <?php
                $page_url=current_url();
                $with_out_ext_url = str_replace(".php", "", $page_url);
                $cur_url = str_replace("/index", "", $with_out_ext_url);
                ?>
                <!--<a href="<?php echo $cur_url; ?>" class="go-top">
                <i class="fa fa-angle-up"></i>
            </a>-->
            </div>
        </footer>
        <!--footer end-->
    </section>

</section>
<?php //if($footer) echo $footer ;?>

<!-- js placed at the end of the document so the pages load faster -->
<!--<script src="<?php /*echo base_url(); */?>assets/js/jquery.js"></script>-->

<script src="<?php echo base_url(); ?>assets/js/bootstrap.min.js"></script>
<script class="include" type="text/javascript" src="<?php echo base_url(); ?>assets/js/jquery.dcjqaccordion.2.7.js"></script>
<script src="<?php echo base_url(); ?>assets/js/jquery.scrollTo.min.js"></script>
<script src="<?php echo base_url(); ?>assets/js/jquery.nicescroll.js" type="text/javascript"></script>
<script src="<?php echo base_url(); ?>assets/js/jquery.sparkline.js"></script>


<!--common script for all pages-->
<script src="<?php echo base_url(); ?>assets/js/common-scripts.js"></script>

<script type="text/javascript" src="<?php echo base_url(); ?>assets/js/gritter/js/jquery.gritter.js"></script>
<script type="text/javascript" src="<?php echo base_url(); ?>assets/js/gritter-conf.js"></script>

<!--script for this page-->
<script src="<?php echo base_url(); ?>assets/js/sparkline-chart.js"></script>
<script src="<?php echo base_url(); ?>assets/js/zabuto_calendar.js"></script>

<script src="<?php echo base_url() ; ?>assets/js/facebox.js" type="text/javascript"></script>
<script type="text/javascript" src="<?php echo base_url(); ?>assets/js/jsDatePick.min.1.3.js"></script>
<script type="text/javascript" src="<?php echo base_url(); ?>assets/js/jquery.jqprint.0.3.js"></script>

<script src="<?php echo base_url(); ?>assets/datetimepicker/jquery-ui-1.12.1/jquery-ui.min.js"></script>
<script src="<?php echo base_url(); ?>assets/datetimepicker/jquery-ui-timepicker-addon.js"></script>





<script type="application/javascript">
    $(document).ready(function () {
        $("#date-popover").popover({html: true, trigger: "manual"});
        $("#date-popover").hide();
        $("#date-popover").click(function (e) {
            $(this).hide();
        });

        $("#my-calendar").zabuto_calendar({
            action: function () {
                return myDateFunction(this.id, false);
            },
            action_nav: function () {
                return myNavFunction(this.id);
            },
            ajax: {
                url: "show_data.php?action=1",
                modal: true
            },
            legend: [
                {type: "text", label: "Special event", badge: "00"},
                {type: "block", label: "Regular event", }
            ]
        });
    });


    function myNavFunction(id) {
        $("#date-popover").hide();
        var nav = $("#" + id).data("navigation");
        var to = $("#" + id).data("to");
        console.log('nav ' + nav + ' to: ' + to.month + '/' + to.year);
    }
</script>




<!-- frontend start-->
<script type="text/javascript" charset="utf-8">
    $(document).ready(function() {
        var loadUrl='<?php echo site_url('hr/hr/getProffession'); ?>';
        $("select#ctlJob").change(function(){
            var name=$("#ctlJob").val();
            $("#result").html('Loading....').load(loadUrl+"/"+name);
        })
    })
</script>

<script type="text/javascript" charset="utf-8">
    $(document).ready(function() {
        var loadUrl='<?php echo site_url('hr/hr/getEmp'); ?>';
        $("select#ctlDep").change(function(){
            var name=$("#ctlDep").val();
            $("#result1").html('Loading....').load(loadUrl+"/"+name);
        })
    })



    function call_select_crlbranch(){
        var dept = '<?php echo $this->input->post('dept')?>';
        //alert(dept);
        if(dept == ''){
            <?php if(isset($query)):?>
            <?php foreach ($query as $t):endforeach;?>
            dept = '<?php echo $t->dept?>';
            <?php endif;?>
        }
        var loadUrl23='<?php echo site_url('hr/hr/getdepartment'); ?>';

        if ($('#crlbranch').length > 0) {
            var name=$("#crlbranch").val();
            $("#res").html('Loading....').load(loadUrl23+"/"+name+"/"+dept);
            app(dept);
        }
    }

    $(document).ready(function() {
        $("select#crlbranch").change(function(){
            call_select_crlbranch();
        })
        call_select_crlbranch();
    })

</script>

<script type="text/javascript" charset="utf-8">
    function getvalue($val)
    {
        var loadUrl='<?php echo site_url('hr/hr/get_leave_package'); ?>';
        var name=$val;
        $("#result66").val('Loading....').load(loadUrl+"/"+name);
    }
</script>

<script type="text/javascript" charset="utf-8">
    function app($val)
    {
        var proffession = '<?php echo $this->input->post('proffession')?>';
        if(proffession == '')
        {
            <?php if(isset($query)):?>
            <?php foreach ($query as $t):endforeach;?>
            proffession = '<?php echo $t->proffession?>';
            <?php endif;?>
        }
        var loadUrl='<?php echo site_url('hr/hr/getProffession'); ?>';
        var loadUrl2='<?php echo site_url('hr/hr/getEmp'); ?>';
        var name=$val;
        $("#result").val('Loading....').load(loadUrl+"/"+name+"/"+proffession);
        $("#result1").val('Loading....').load(loadUrl2+"/"+name);
    }
    function display($val)
    {
        var loadUrl='<?php echo site_url('hr/hr/getDesignation'); ?>';
        var name=$val;
        $("#des").val('Loading....').load(loadUrl+"/"+name);
    }
</script>

<script type="text/javascript" charset="utf-8">
    function call_select_getbranch()
    {
        if ($("#bank_payment").length > 0){
            var bank_payment = document.getElementById("bank_payment").checked;
            if(bank_payment==true)
            {
                var bankpayament = document.getElementById("bank_payment").checked;
                if(bankpayament){
                    var bank_branch = '<?php echo $this->input->post('bank_branch')?>';
                    var loadUrl='<?php echo site_url('hr/bank/getbranch'); ?>';
                    var name=$("#bank_name").val();
                    $("#result99").html('Loading....').load(loadUrl+"/"+name+"/"+bank_branch);
                }
            }
        }
    }

    $(document).ready(function() {
        $("select#bank_name").change(function(){
            call_select_getbranch();
        })
        call_select_getbranch();
    })

</script>

<!--new drop down list  -->
<script type="text/javascript" charset="utf-8">
    $(document).ready(function() {
        var loadUrl23='<?php echo site_url('hr/hr/getedepartment'); ?>';
        $("select#ebranch").change(function(){
            var name=$("#ebranch").val();
            //alert(name);
            $("#d").html('Loading....').load(loadUrl23+"/"+name);
        })
    })
</script>

<script type="text/javascript" charset="utf-8">
    function get_section($val)
    {
        var loadUrl='<?php echo site_url('hr/hr/getsection'); ?>';
        var name=$val;
        $("#s").val('Loading....').load(loadUrl+"/"+name);
    }
</script>

<script type="text/javascript" charset="utf-8">
    //for Create new employee
    function call_select_designation()
    {
        var designation = '<?php echo $this->input->post('designation')?>';
        if(designation == '')
        {
            <?php if(isset($query)):?>
            <?php foreach ($query as $t):endforeach;?>
            designation = '<?php echo $t->designation?>';
            <?php endif;?>
        }
        var loadUrl='<?php echo site_url('hr/hr/getDesignation'); ?>';
        var name=$("#emp_type").val();
        $("#ds").html('Loading....').load(loadUrl+"/"+name+"/"+designation);
    }
    $(document).ready(function() {
        $("select#emp_type").change(function(){
            call_select_designation();
        })
        call_select_designation();
    })
</script>

<script type="text/javascript">

    window.onload = function(){
        new JsDatePick({
            useMode:2,
            target:"weekend_date",
            dateFormat:"%Y-%m-%d"
        });
        
        new JsDatePick({
            useMode:2,
            target:"start_date",
            dateFormat:"%Y-%m-%d"
        });
    }
</script>


<script type="text/javascript" charset="utf-8">
    function getEmpName($val)
    {
        var loadUrl='<?php echo site_url('hr/hr/getEmpName'); ?>';
        var name=$val;
        $("#result66").val('Loading....').load(loadUrl+"/"+name);
    }
</script>


<script>

    $(document).ready(function() {
        check_value();
        check();
        check_package();
        check_package_edit()
    })


    function check_package(){
        if ($('#package').length > 0) {
            var myvar = document.getElementById("package").checked;
            if (myvar) {
                $("#package1").show();
            } else {
                $("#package1").hide();
            }
        }else{
            return false;
        }
    }


    function check_package_edit(){
        if ($('#package').length > 0) {
            var myvar = document.getElementById("package").checked;
            if (myvar){
                $("#p1").show();
                $("#p2").show();
                $("#p3").show();
                $("#p4").show();
                $("#p5").show();
                $("#p6").show();
                $("#p7").show();
            } else {
                $("#p1").hide();
                $("#p2").hide();
                $("#p3").hide();
                $("#p4").hide();
                $("#p5").hide();
                $("#p6").hide();
                $("#p7").hide();
            }
        }else{
            return false;
        }
    }

    function calculateAuto($totalAmount) {
        var basic = $totalAmount;
        if (basic != "") {
            var houseRent_amount = parseFloat(Math.round(basic * 60) / 100).toFixed(2);
            var medical_amount = parseFloat(Math.round(basic * 20) / 100).toFixed(2);
            var conveyance_amount = parseFloat(Math.round(basic * 10) / 100).toFixed(2);
            var special_amount = parseFloat(Math.round(basic * 10) / 100).toFixed(2);
            var houseRent = document.getElementById("house_rent");
            var medical = document.getElementById("medical");
            var conveyance = document.getElementById("conveyance");
            var special = document.getElementById("special");
            houseRent.value = houseRent_amount;
            medical.value = medical_amount;
            conveyance.value = conveyance_amount;
            special.value = special_amount;
        }
    }


    function check_value(){
        if ($('#bank_payment').length > 0) {
            var myvar = document.getElementById("cash_payment").checked;
            if (myvar){
                $("#bank_payment").attr("disabled", true);
            } else {
                $("#bank_payment").removeAttr("disabled");
            }
        }
    }

    function check(){
        if ($('#bank_payment').length > 0) {
            var myvar = document.getElementById("bank_payment").checked;
            if (myvar){
                $("#bank_name").show();
                $("#bank_branch").show();
                $("#account_number").show();
                $("#cash_payment").attr("disabled", true);
            }else{
                $("#bank_name").hide();
                $("#bank_branch").hide();
                $("#account_number").hide();
                $("#cash_payment").removeAttr("disabled");
            }
        }else{
            return false;
        }
    }


    function call_select_super_designation(){
        var supervisor = '<?php echo $this->input->post('supervisor')?>';
        var loadUrl = '<?php echo site_url('hr/hr/get_supervisor') ?>';
        var name=$("#empDesignation").val();

        $("#resultSupervisor").html('Loading....').load(loadUrl+"/"+name+"/"+supervisor);
    }

    $(document).ready(function() {
        $("select#empDesignation").change(function(){
            call_select_super_designation();
        })
        call_select_super_designation();
    })
</script>

<script type="text/javascript">
    $(document).ready(function() {
        $('a[rel*=facebox]').facebox({
        })
    })
</script>


<script>
    $(function(){
        $("#print_button").click( function() {
            $("#invoice").jqprint();
        });
    });
</script>

<script>
    function empTypeOnchange(){
        var res = window.location.href.split("?");
        var url = res[0]+"?emp_type="+$("#empTypeSelect").val();
        window.location.replace(url);
    }
</script>

<!--frontend end-->

<script type="text/javascript">
    $('.AppointmentDateTime').timepicker(
        {
            dateFormat: '',
            timeFormat: 'HH:mm'
        }
    );
</script>

<script>
    function branch_wise_weekend(){
        var res = window.location.href.split("?");
        var url = res[0]+"?branch="+$("#branch_Select").val();
        window.location.replace(url);
    }
</script>

<script type="text/javascript">
    $('#start_date_attendance_log').datetimepicker(
        {
            dateFormat: 'yy-mm-dd',
            timeFormat: ''
        }
    );

    $('#end_date_attendance_log').datetimepicker(
        {
            dateFormat: 'yy-mm-dd',
            timeFormat: ''
        }
    );
</script>


</body>
</html>