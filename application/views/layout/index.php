<!DOCTYPE html>
<html class="no-js">
<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta name="description" content="">
    <meta name="author" content="Dashboard">
    <meta name="keyword" content="Dashboard, Bootstrap, Admin, Template, Theme, Responsive, Fluid, Retina">

    <!-- <title>Windybd</title> -->
    <title><?php echo $title;?></title>
    <!-- Favicons -->
    <link rel="shortcut icon" href="<?php echo base_url(); ?>assets/img/w_fav_icon.png">

    <script src="<?php echo base_url(); ?>assets/js/jquery-1.8.3.min.js"></script>

    <!-- Bootstrap core CSS -->
    <link href="<?php echo base_url(); ?>assets/css/bootstrap.css" rel="stylesheet">
    <!--<link href="<?php echo base_url(); ?>assets/css/bootstrap.min.css" rel="stylesheet"> -->
    <!--external css-->
    <link href="<?php echo base_url(); ?>assets/font-awesome/css/font-awesome.css" rel="stylesheet" />
    <link rel="stylesheet" type="text/css" href="<?php echo base_url(); ?>assets/css/zabuto_calendar.css">
    <link rel="stylesheet" type="text/css" href="<?php echo base_url(); ?>assets/js/gritter/css/jquery.gritter.css" />
    <link rel="stylesheet" type="text/css" href="<?php echo base_url(); ?>assets/lineicons/style.css">

    <!-- Custom styles for this template -->
    <link href="<?php echo base_url(); ?>assets/css/style.css" rel="stylesheet">
    <link href="<?php echo base_url(); ?>assets/css/flip_scroll.css" rel="stylesheet">
    <link href="<?php echo base_url(); ?>assets/css/style-responsive.css" rel="stylesheet">
    <link href="<?php echo base_url(); ?>assets/css/custom.css" rel="stylesheet">

    <link rel="stylesheet" type="text/css" media="all" href="<?php echo base_url(); ?>assets/css/jsDatePick_ltr.min.css" />

    <!-- <link href="<?php echo base_url(); ?>assets/css/bootstrap-responsive.min.css" rel="stylesheet"> -->

    <script src="<?php echo base_url(); ?>assets/js/chart-master/Chart.js"></script>




    <!-- HTML5 shim and Respond.js IE8 support of HTML5 elements and media queries -->
    <!--[if lt IE 9]>
    <script src="https://oss.maxcdn.com/libs/html5shiv/3.7.0/html5shiv.js"></script>
    <script src="https://oss.maxcdn.com/libs/respond.js/1.4.2/respond.min.js"></script>
    <![endif]-->
</head>
<body>
<?php //if($header) echo $header ;?>


<section id="container" >
    <!-- **********************************************************************************************************************************************************
    TOP BAR CONTENT & NOTIFICATIONS
    *********************************************************************************************************************************************************** -->
    <!--header start-->
    <header class="header black-bg">
        <div class="sidebar-toggle-box">
            <div class="fa fa-bars tooltips" data-placement="right" data-original-title="Toggle Navigation"></div>
        </div>
        <!--logo start-->
        <a href="<?php echo base_url(); ?>" class="logo"><b>Windybd</b></a>
        <!--logo end-->
        <div class="nav notify-row" id="top_menu">
            <!--  notification start -->
            <ul class="nav top-menu">
                <!-- settings start -->

                <!-- inbox dropdown end -->
            </ul>
            <!--  notification end -->
        </div>
        <div class="top-menu">
            <ul class="dropdown nav pull-right">
                <!--<li><a class="logout" href="login.html"><i class="fa fa-power-off" aria-hidden="true"></i>  &nbsp;Logout</a></li>-->
                <a href="javascript:;" title="" class="user-ico clearfix" data-toggle="dropdown" style="color:#ffffff;">
                    <i class="glyphicon glyphicon-user"></i>&nbsp;
                    <!-- <img src="<?php //echo $img; ?>" width="40" height="30" alt="" /> -->
                    <span><?php echo $this->session->userdata('name');?></span>
                    <i class="glyphicon glyphicon-chevron-down"></i>
                </a>
                <ul class="dropdown-menu float-right">
                    <!--<li>
                            <a id="Inbox" class="ModalTitle_Change_Pass" data-id="" sessionid="<?php //echo $sessionid;?>" data-original-title="Change Password" data-target="#Change_pass" data-toggle="modal" href="javascript:;">
                                <i class="glyphicon glyphicon-lock"></i>
                                Change Password
                            </a>
                        </li>-->
                    <li>
                        <a href="<?php echo base_url(); ?>hr/emp_login/logout">
                            <i class="glyphicon glyphicon-off"></i>
                            <span class="font-bold">Logout</span>
                        </a>
                    </li>
                    <li class="divider"></li>
                </ul>
            </ul>
        </div>
    </header>
    <!--header end-->


    <?php //if($left) echo $left ;?>
    <!-- Left bar -->
    <aside>
        <div id="sidebar"  class="nav-collapse ">
            <!-- sidebar menu start-->
            <ul class="sidebar-menu" id="nav-accordion">
                <?php
                $getCon = $this->uri->segment(2);
                $getMet = $this->uri->segment(3);

                //$getConMet = $getCon.'/'.$getMet;

                ?>
                <li class="sub-menu">
                    <a <?php echo ($getCon == 'home') ? "class='active'" : ""; ?> href="<?php echo base_url(); ?>" >
                        <i class="fa fa-dashboard"></i>
                        <span>Home</span>
                    </a>
                </li>

                <li class="sub-menu">
                    <a <?php echo ($getCon == 'leave') ? "class='active'" : ""; ?> href="javascript:;" >
                        <i class="fa fa-coffee"></i>
                        <span>Leave</span>
                    </a>
                    <ul class="sub">
                        <li <?php echo ($getMet == 'leave_report') ? "class='active'" : "" || ($getMet == 'leave_details') ? "class='active'" : ""; ?> ><a href="<?php echo base_url(); ?>user/leave/leave_report">Leave Summery</a></li>
                        <li <?php echo ($getMet == 'schedule') ? "class='active'" : ""; ?> ><a  href="<?php echo base_url(); ?>user/leave/schedule">Leave Schedule</a></li>
                        <li <?php echo ($getMet == 'apply') ? "class='active'" : ""; ?> ><a  href="<?php echo base_url(); ?>user/leave/apply">Leave Application</a></li>
                        <li <?php echo ($getMet == 'show') ? "class='active'" : ""; ?> ><a  href="<?php echo base_url(); ?>user/leave/show">View Application</a></li>
                        <li <?php echo ($getMet == 'view_application') ? "class='active'" : ""; ?> ><a  href="<?php echo base_url(); ?>user/leave/view_application">View Application Request</a></li>
                        <!--<li <?php /*echo ($getMet == 'holiday') ? "class='active'" : ""; */?> ><a  href="<?php /*echo base_url(); */?>user/leave/holiday">Holiday</a></li>-->
                        <li <?php echo ($getMet == 'holiday_list_calendar') ? "class='active'" : "active"; ?> ><a  href="<?php echo base_url(); ?>user/leave/holiday_list_calendar">Holiday</a></li>
                        <li <?php echo ($getMet == 'weekend') ? "class='active'" : ""; ?> ><a  href="<?php echo base_url(); ?>user/leave/weekend_calendar">Weekend</a></li>
                    </ul>
                </li>
                <li class="sub-menu">
                    <a <?php echo ($getCon == 'attendence') ? "class='active'" : ""; ?> href="javascript:;" >
                        <i class="fa fa-clock-o"></i>
                        <span>Attendance</span>
                    </a>
                    <ul class="sub">
                        <li <?php echo ($getMet == 'view' || $getMet == 'viewed') ? "class='active'" : ""; ?> ><a  href="<?php echo base_url(); ?>user/attendence/view">View Attendance</a></li>
                        <li <?php echo ($getMet == 'attendence_request') ? "class='active'" : ""; ?> ><a  href="<?php echo base_url(); ?>user/attendence/attendence_request">Attendence Request</a></li>
                        <!--<li <?php /*echo ($getMet == 'addLog') ? "class='active'" : ""; */?> ><a  href="<?php /*echo base_url(); */?>user/attendence/addLog">Daily log History</a></li>-->
                    </ul>
                </li>
                <li class="sub-menu">
                    <a  <?php echo ($getCon == 'payslip') ? "class='active'" : ""; ?> href="javascript:;" >
                        <i class="fa fa-dollar"></i>
                        <span>Salary</span>
                    </a>
                    <ul class="sub">
                        <li <?php echo ($getMet == 'view_form' || $getMet == 'viewed') ? "class='active'" : ""; ?> ><a  href="<?php echo base_url(); ?>user/payslip/view_form">Pay Slip</a></li>
                        <li <?php echo ($getMet == 'pf_viewed') ? "class='active'" : ""; ?> ><a  href="<?php echo base_url(); ?>user/payslip/pf_viewed">View Provident Fund Report</a></li>
                    </ul>
                </li>

            </ul>

        </div>
        <?php /* ?>
        <div class="footer_logo">
            <a class="logo_pos" href="<?php echo base_url(); ?>">
                <img src="<?php echo base_url(); ?>assets/img/copy_logo.png" height="40px" alt="" >
            </a>
        </div>
        <?php */ ?>
    </aside>
    <!--Left bar end -->

    <section id="main-content">
        <section class="wrapper">
            <?php

            //echo $values;
            if($middle) echo $middle ;

            ?>
        </section>
    </section>
    <!--main content end-->
    <?php  ?>
    <!--footer start-->
    <footer class="site-footer">
        <div class="text-right">
            Copyright &copy; <?php echo date("Y"); ?>. All rights reserved. Design & Developed by &nbsp;<a href="http://windmillbd.net/"><img src="<?php echo base_url(); ?>assets/img/copy_logo.png" height="36px" alt="" style="margin-top:-5px;margin-right: 10px;"></a>
            <?php
            $page_url=current_url();
            $with_out_ext_url = str_replace(".php", "", $page_url);
            $cur_url = str_replace("/index", "", $with_out_ext_url);
            ?>
            <!--<a href="<?php echo $cur_url; ?>" class="go-top">
                <i class="fa fa-angle-up"></i>
            </a>-->
        </div>
    </footer>
    <!--footer end-->
    <?php  ?>
</section>
<?php //if($footer) echo $footer ;?>


<!-- js placed at the end of the document so the pages load faster -->
<!-- <script src="<?php //echo base_url(); ?>assets/js/jquery.js"></script> -->

<script src="<?php echo base_url(); ?>assets/js/bootstrap.min.js"></script>
<script class="include" type="text/javascript" src="<?php echo base_url(); ?>assets/js/jquery.dcjqaccordion.2.7.js"></script>
<script src="<?php echo base_url(); ?>assets/js/jquery.scrollTo.min.js"></script>
<script src="<?php echo base_url(); ?>assets/js/jquery.nicescroll.js" type="text/javascript"></script>
<script src="<?php echo base_url(); ?>assets/js/jquery.sparkline.js"></script>


<!--common script for all pages-->
<script src="<?php echo base_url(); ?>assets/js/common-scripts.js"></script>

<script type="text/javascript" src="<?php echo base_url(); ?>assets/js/gritter/js/jquery.gritter.js"></script>
<script type="text/javascript" src="<?php echo base_url(); ?>assets/js/gritter-conf.js"></script>

<!--script for this page-->
<script src="<?php echo base_url(); ?>assets/js/sparkline-chart.js"></script>
<script src="<?php echo base_url(); ?>assets/js/zabuto_calendar.js"></script>

<script type="text/javascript" src="<?php echo base_url(); ?>assets/js/jsDatePick.min.1.3.js"></script>

<script type="application/javascript">
    $(document).ready(function () {
        $("#date-popover").popover({html: true, trigger: "manual"});
        $("#date-popover").hide();
        $("#date-popover").click(function (e) {
            $(this).hide();
        });

        $("#my-calendar").zabuto_calendar({
            action: function () {
                return myDateFunction(this.id, false);
            },
            action_nav: function () {
                return myNavFunction(this.id);
            },
            ajax: {
                url: "show_data.php?action=1",
                modal: true
            },
            legend: [
                {type: "text", label: "Special event", badge: "00"},
                {type: "block", label: "Regular event", }
            ]
        });
    });


    function myNavFunction(id) {
        $("#date-popover").hide();
        var nav = $("#" + id).data("navigation");
        var to = $("#" + id).data("to");
        console.log('nav ' + nav + ' to: ' + to.month + '/' + to.year);
    }
</script>



<script type="text/javascript">
    window.onload = function(){
        new JsDatePick({
            useMode:2,
            target:"start_date",
            dateFormat:"%Y-%m-%d"
        });
        new JsDatePick({
            useMode:2,
            target:"end_date",
            dateFormat:"%Y-%m-%d"
        });
    }
</script>


<script type="text/javascript" charset="utf-8">
    $(document).ready(function(){
        //alert();
        $("#leave_type").click(function(){
            var sdate=document.getElementById("start_date").value;
            var edate=document.getElementById("end_date").value;
            if(sdate=="" || edate==""){}
            else{
                var url='<?php echo site_url('user/leave/getDateDifference'); ?>';
                $("#m").html('Loading....').load(url+"/"+sdate+"/"+edate);
            }

        })
    })
    function getSupervisor($val){
        var empDesignation   = $("#empDesignation").val();
        var nominated_person = $("#hidden_nominated_person").val();

        var loadUrl='<?php echo site_url('user/leave/get_supervisor'); ?>';
        var name=$val;
        $("#resultSupervisor").val('Loading....').load(loadUrl+"/"+name, { empDesignation: empDesignation, nominated_person: nominated_person });
    }

</script>

<script type="text/javascript" src="<?php echo base_url(); ?>assets/js/jquery.jqprint.0.3.js"></script>
<script>
    $(function() {
        $("#print_button1").click(function() {
            //alert('ok');
            $("#invoice").jqprint();
        });
    });

</script>


<script language="javascript">
    function confirmSubmit() {
        var agree = confirm("Are you sure to delete this record?");
        if (agree)
            return true;
        else
            return false;
    }


    function setAttDailyLog(date)
    {
        var loder_img = "<img src='<?php echo $this->config->base_url();?>assets/img/ajax-loader.gif'>" ;
        $("#loader__"+date).html('');
        $("#loader__"+date).html(loder_img);

        var comment  = $("#date__"+date+" #comment").val();
        var emp_id   = $("#date__"+date+" #emp_id").val();
        var log_date = $("#date__"+date+" #log_date").val();
        var formParams = "comment="+comment+"&emp_id="+emp_id+"&log_date="+log_date;
        //alert('<?php echo $this->config->base_url();?>index.php/user/attendence/dayLogSave');
        $.ajax({
            type: 'GET',
            url: '<?php echo $this->config->base_url();?>index.php/user/attendence/dayLogSave',
            async: false,
            data: formParams,
            success: function(response) {
                $("#loader__"+date).html(response);
                $("#date__"+date+" #spen_lbl_add").show();
                $("#date__"+date+" #comment").hide();
                $("#date__"+date+" #spen_lbl_comment").show();
                $("#date__"+date+" #spen_lbl_comment").html(comment);
                $("#date__"+date+" .hidden_spen_lbl_comment").val(comment);
            }
        });

    }

    function showDailyLogEdit(date){
        $("#date__"+date+" #comment").show();
        $("#date__"+date+" #spen_lbl_comment").hide();
        $("#date__"+date+" #spen_lbl_add").hide();
    }

    $(document).ready(function(){
        $(".btnshow").show();
        $(".enterkey_prevent").keypress(function(event){
            if (event.keyCode === 10 || event.keyCode === 13)
                event.preventDefault();
        });
    });
</script>
</body>
</html>