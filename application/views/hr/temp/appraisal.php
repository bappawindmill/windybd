<link rel="stylesheet" type="text/css" href="<?php echo base_url(); ?>public/css/t1.css" />
<style type="text/css">
    body{
        font-family:"Lucida Grande", "Lucida Sans Unicode", Verdana, Arial, Helvetica, sans-serif;
        font-size:12px;
    }
    p, h1, form, button{border:0; margin:0; padding:0;}
    .spacer{clear:both; height:1px;}
    /* ----------- My Form ----------- */
    .myform{
        margin:0 auto;
        width:400px;
        padding:14px;
    }

    /* ----------- stylized ----------- */
    #stylized{
        border:solid 0px #b7ddf2;
        background:#ebf4fb;
    }
    #stylized h1 {
        font-size:14px;
        font-weight:bold;
        margin-bottom:8px;
    }
    #stylized p{
        font-size:11px;
        color:#666666;
        margin-bottom:20px;
        border-bottom:solid 1px #b7ddf2;
        padding-bottom:10px;
    }
    #stylized label{
        display:block;
        font-weight:bold;
        text-align:left;
        width:140px;
        padding-top: 5px;
        float:left;
    }
    #stylized .small{
        color:#666666;
        display:block;
        font-size:11px;
        font-weight:normal;
        text-align:right;
        width:140px;
    }
    #stylized input{
        float:left;
        font-size:12px;
        padding:4px 2px;
        border:solid 1px #aacfe4;
        width:200px;
        margin:2px 0 20px 10px;
    }
    #stylized .button{
        clear:both;
        margin-left:150px;
        width:125px;
        height:31px;
        background:#666666 url(img/button.png) no-repeat;
        text-align:center;
        line-height:31px;
        color:#FFFFFF;
        font-size:11px;
        font-weight:bold;
    }
    #footer {
        border-top: none;
        color: #AAAAAA;
        margin-top: 30px;
        padding: 12px 0 15px;
        text-align: center;
    }
</style>
<script type="text/javascript">
    window.onload = function() {
        new JsDatePick({
            useMode: 2,
            target: "start_date",
            dateFormat: "%Y-%m-%d"
        });
        new JsDatePick({
            useMode: 2,
            target: "end_date",
            dateFormat: "%Y-%m-%d"
        });
    }
</script>
<?php echo $this->session->flashdata('message'); ?>

<?php
foreach ($query1 as $info):endforeach;
echo form_open('hr/temp/appraisal_update');
?>

<div id="stylized" class="myform">
    <h1>Edit Appraisal Info</h1>
    <p></p>

    <label>Status

    </label>
    <input type="text"  required  name="status" value="<?php echo $info->status; ?>" id="status"  />

    <label>Effective Date

    </label>
    <input type="text"  required  name="effective_date" value="<?php echo $info->effective_date; ?>" id="effective_date"  />

    <label>Designation

    </label>
    <input type="text"  required  name="designation" value="<?php echo $info->designation; ?>" id="designation"  />
    <label>Pay Scale

    </label>
    <input type="text"  required  name="pay_scale" value="<?php echo $info->pay_scale; ?>" id="pay_scale"  />
    <label>Note

    </label>
    <input type="text"  name="note" id="note" value="<?php echo $info->note; ?>" />
    <input type="hidden" name="cno" value="<?php echo $cno; ?>" />
    <input type="hidden" name="id" value="<?php echo $info->id; ?>" />

    <input type="submit" value="Update" class="button" />
    <div class="spacer"></div>
</form>
</div>
<?php
echo form_close();
?>

