<link rel="stylesheet" type="text/css" href="<?php echo base_url(); ?>public/css/t1.css" />
<style type="text/css">
    <!--
    .style1 {color: #FF0000}
    -->
</style>
<script type="text/javascript">
    window.onload = function(){
        new JsDatePick({
            useMode:2,
            target:"start_date",
            dateFormat:"%Y-%m-%d"
        });
        new JsDatePick({
            useMode:2,
            target:"end_date",
            dateFormat:"%Y-%m-%d"
        });
    }
</script>
<div class="grid_12">
    <div class="module1">
        <h2><span>Edit Academic Info</span></h2>
        <?php echo $this->session->flashdata('message'); ?>
        <div class="module-body">

            <?php
            foreach ($query1 as $info):endforeach;
            echo form_open('hr/temp/update');

            echo '<table>';
            echo '<thead><tr><th colspan="2"><b></b></th></tr></thead>';
            ?>

            <tr><th>Exam Name :</th><td>
                    <input type="text" style="width:130px" required  name="exam_name" value="<?php echo $info->exam_name;?>" id="intime" class="input-short" />
                    <span class="notification-input ni-error"><?php echo ucwords(form_error('exam_name')); ?></span></td></tr>
            <tr><th>Institute Name :</th><td>
                    <input type="text" style="width:130px" required name="institute_name" id="outtime" value="<?php echo $info->institute_name;?>" class="input-short" />
                    <span class="notification-input ni-error"><?php echo ucwords(form_error('institute_name')); ?></span></td>
					
			</tr>
			            <tr><th>Result :</th><td>
                    <input type="text" style="width:130px" required name="result" id="result" value="<?php echo $info->result;?>" class="input-short" />
                    <span class="notification-input ni-error"><?php echo ucwords(form_error('result')); ?></span></td>
					
			</tr>
			</tr>
			 <tr><th>Passing Year :</th><td>
                 <input type="text" style="width:130px" required name="passing_year" id="passing_year" value="<?php echo $info->passing_year;?>" class="input-short" />
                    <span class="notification-input ni-error"><?php echo ucwords(form_error('passing_year')); ?></span></td>
					
			</tr>
			</tr>
			<tr><th>Note :</th><td>
                    <input type="text" style="width:130px" name="note" id="note" value="<?php echo $info->note;?>" class="input-short" />
                    <span class="notification-input ni-error"><?php echo ucwords(form_error('passing_year')); ?></span></td>
					
			</tr>

            <input type="hidden" name="cno" value="<?php echo $cno; ?>" />
            <input type="hidden" name="id" value="<?php echo $info->id; ?>" />
            <?php
            echo '<tr><th></th><td>' . form_submit('mysubmit', 'Update') . '</td></tr>';

            echo '</table>';
            echo form_close();
            ?>
        </div></div></div>
<div style="clear: both;"></div>
