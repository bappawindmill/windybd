<link rel="stylesheet" type="text/css" href="<?php echo base_url(); ?>public/css/t1.css" />
<style type="text/css">
    <!--
    .style1 {color: #FF0000}
    -->
</style>
<script type="text/javascript">
    window.onload = function(){
        new JsDatePick({
            useMode:2,
            target:"start_date",
            dateFormat:"%Y-%m-%d"
        });
        new JsDatePick({
            useMode:2,
            target:"end_date",
            dateFormat:"%Y-%m-%d"
        });
    }
</script>
<div class="grid_12">
    <div class="module1">
        <h2><span>Edit Career History</span></h2>
        <?php echo $this->session->flashdata('message'); ?>
        <div class="module-body">

            <?php
            foreach ($query1 as $info):endforeach;
            echo form_open('hr/temp/hupdate');

            echo '<table>';
            echo '<thead><tr><th colspan="2"><b></b></th></tr></thead>';
            ?>

            <tr style="background-color:none;"><th style="background-color:none;">Company_name :</th><td>
           <input type="text" style="width:130px" required  name="company_name" value="<?php echo $info->company_name;?>" id="company_name" class="input-short" />
                    <span class="notification-input ni-error"><?php echo ucwords(form_error('company_name')); ?></span></td></tr>
            <tr><th>Designation :</th><td>
                 <input type="text" style="width:130px" required name="designation" id="designation" value="<?php echo $info->designation;?>" class="input-short" />
                    <span class="notification-input ni-error"><?php echo ucwords(form_error('designation')); ?></span></td>
					
			</tr>
			<tr><th>Start Date :</th><td>
                    <input type="text" style="width:130px" required name="start_date" id="start_date" value="<?php echo $info->start;?>" class="input-short" />
                    <span class="notification-input ni-error"><?php echo ucwords(form_error('start_date')); ?></span></td>
			</tr>
			</tr>
			<tr><th>End Date :</th><td>
                    <input type="text" style="width:130px" required name="end_date" id="end_date" value="<?php echo $info->end_date;?>" class="input-short" />
                    <span class="notification-input ni-error"><?php echo ucwords(form_error('end_date')); ?></span></td>
			</tr>
			
			</tr>
			<tr><th>Note :</th><td>
                    <input type="text" style="width:130px" name="note" id="note" value="<?php echo $info->note;?>" class="input-short" />
                    <span class="notification-input ni-error"><?php echo ucwords(form_error('note')); ?></span></td>
			</tr>

            <input type="hidden" name="cno" value="<?php echo $cno; ?>" />
            <input type="hidden" name="id" value="<?php echo $info->id; ?>" />
            <?php
            echo '<tr><th></th><td>' . form_submit('mysubmit', 'Update') . '</td></tr>';
            echo '</table>';
            echo form_close();
            ?>
        </div></div></div>
<div style="clear: both;"></div>
