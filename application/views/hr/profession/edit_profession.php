<h3><i class="fa fa-angle-right"></i> Section Edit</h3>

<!-- BASIC FORM ELELEMNTS -->
<div class="row">
    <div class="col-sm-12">
        <div class="content-panel">
            <?php
            echo form_open('hr/profession/profession_edit');?>
            <div class="alert-danger"><?php echo $this->session->flashdata('message'); ?> </div>
            <div class="col-md-12">
                <div class="form-group col-sm-6">
                    <label for="">Section Name:</label>
                    <input value="<?php echo $fproffession['value']; ?>"  type="text" name="name" class="form-control" id="" required>
                    <div class="alert-danger"><?php echo ucwords(form_error('name')); ?></div>
                </div>
                <?php
                $query = $this->db->query("SELECT id,name FROM add_department");
                ?>
                <div class="form-group col-sm-6">
                    <label for="department_id">SBU Name:</label>
                    <select name="department_id" id="department_id" class="form-control" required>
                        <option value="">select</option>
                        <?php foreach ($query->result() as $row) { ?>
                            <option <?php if ($fd['value'] == $row->id): ?> selected="selected" value="<?php echo $fd['value']; ?>" <?php else: ?> value="<?php echo $row->id; ?>"<?php endif; ?>><?php echo $row->name; ?></option>
                            <?php
                        }
                        ?>
                    </select>
                    <div class="alert-danger"><?php echo ucwords(form_error('user_type')); ?></div>
                </div>
            </div>
                <span>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
                    <input type="hidden" name="id" value="<?php echo $fid['value']; ?>" />&nbsp;&nbsp;&nbsp;&nbsp;
                    <input type="submit" name="mysubmit" value="Update"  class="btn btn-primary">
                    <a class="btn btn-theme" href="<?php echo base_url(); ?>hr/profession/profession_list">Cancel</a></span>
            </span>
            </form>
        </div>
    </div><!-- col-lg-12-->
</div><!-- /row -->