<h3><i class="fa fa-angle-right"></i> Create New Section</h3>

<!-- BASIC FORM ELELEMNTS -->
<div class="row">
    <div class="col-sm-12">
        <div class="content-panel">
            <?php
            echo form_open('hr/profession/added_profession');?>
            <?php
            if($this->session->flashdata('message')==NULL){

            }else{ ?>
                <div class="alert-success alert">
                    <?php echo $this->session->flashdata('message') ?>
                </div> <?php
            }
            ?>
            <?php
            $query12 = $this->db->query("SELECT id,branch_name FROM add_branch");
            ?>
            <div class="col-md-12">
                <div class="form-group col-sm-6">
                    <label for="crlbranch">SBU Name:</label>
                    <select name="branch_name" id="crlbranch" class="form-control">
                        <option value="">select</option>
                        <?php foreach ($query12->result() as $row) { ?>
                            <option value="<?php echo $row->id; ?>"><?php echo $row->branch_name; ?></option>
                        <?php
                        }
                        ?>
                    </select>
                    <div class="alert-danger"><?php echo ucwords(form_error('branch_name')); ?></div>
                </div>
                <div class="form-group col-sm-6">
                    <label for="">Department Name:</label>
                    <div id="res">
                        <select name="" id="" class="form-control">
                            <option value="">select</option>
                                <option value=""></option>
                        </select>
                    </div>
                    <div class="alert-danger"><?php echo ucwords(form_error('dept')); ?></div>
                </div>
            </div>
            <div class="col-md-12">
                <div class="form-group col-sm-6">
                    <label for="">Section Name:</label>
                    <input value=""  type="text" name="name" class="form-control" id=" ">
                    <div class="alert-danger"><?php echo ucwords(form_error('name')); ?></div>
                </div>
            </div>
                <span>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
                    <input type="hidden" name="id" value="" />&nbsp;&nbsp;&nbsp;&nbsp;
                    <input type="submit" name="mysubmit" value="Submit"  class="btn btn-primary">
                    <a class="btn btn-theme" href="<?php echo base_url(); ?>hr/profession/profession_list">Cancel</a>
                </span>

            </form>
            <p></p>
        </div>
    </div><!-- col-lg-12-->
</div><!-- /row -->