<script language="javascript">
    function confirmSubmit() {
        var agree = confirm("Are you sure to delete this record?");
        if (agree)
            return true;
        else
            return false;
    }
</script>

<h3><i class="fa fa-angle-right"></i> View Section</h3>
<?php
if($this->session->flashdata('message')==NULL){

}else{ ?>
    <div class="alert-success alert">
        <?php echo $this->session->flashdata('message') ?>
    </div> <?php
}
?>


<div class="row">
    <div class="col-sm-12 col-lg-12">
        <div class="pull-left">
            <i class="fa"></i>
            <a class="btn btn-info" href="<?php echo base_url(); ?>hr/profession/added_profession"   title="">Add  New </a>
        </div>
        <div class=" pull-right">
            <form class="form-inline">
                <div class="form-group has-feedback pull-right">
                    <input type="text" id="searchKey" name="search" class="form-control" placeholder="Search" />
                    <i class="glyphicon glyphicon-search form-control-feedback"></i>
                </div>
            </form>
        </div>
        <div class="pull-right"><a href="<?php echo base_url();?>hr/profession/profession_list" class="btn btn-info" role="button" >Reset</a>&nbsp;</div>
    </div>

    <div class="col-sm-12">
        <div class="content-panel">
            <!--<section id="flip-scroll">-->
            <section id="">
                <table  id="myTable"  class="table table-hover table-bordered">
                    <thead>
                    <tr class="info">
                        <th>SL</th>
                        <th>Section Name</th>
                        <th>Department Name</th>
                        <th colspan="2">Action</th>
                    </tr>
                    </thead>
                    <tbody>
                    <?php
                    $i = 'even';
                    $c = 1;
                    foreach ($query1 as $row) {
                        if ($i == 'even') {
                            $i = 'odd';
                        } else {
                            $i = 'even';
                        }
                        echo "<tr>";
                        echo "<td>" . $c . "</td>";
                        echo "<td>" . $row->pname . "</td>";
                        echo "<td>" . $row->dname . "</td>";
                        ?>
                        <td>
                            <a class="btn btn-warning btn-xs" href="<?php echo base_url(); ?>/hr/profession/profession_edit/<?php echo $row->pid; ?>/<?php echo $row->did; ?> " title="Edit the Content">Edit</a>&nbsp;&nbsp;&nbsp;
                            <a class="btn btn-danger btn-xs" onclick="return confirmSubmit()" href="<?php echo base_url() ?>/hr/profession/profession_delete/<?php echo $row->pid; ?>" title="Delete the Content">Delete</a>
                        </td>
                        <?php
                        echo "</tr>";
                        $c++;
                    }
                    ?>
                    </tbody>
                </table>
                <div class="text-center">
                    <?php echo $this->pagination->create_links(); ?>
                </div>
                <div class="text-center">
                    <?php
                    echo "Total Records: ".$this->pagination_library->total_rows." page ".(($this->pagination->cur_page) ? $this->pagination->cur_page : "1")." of ". ceil(($this->pagination_library->total_rows)/($this->pagination_library->per_page));
                    ?>
                </div>
            </section>
        </div><! --/content-panel -->
    </div><!-- /col-md-12 -->
</div><!-- row -->