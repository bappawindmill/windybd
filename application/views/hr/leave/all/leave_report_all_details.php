<p></p>
<input class="btn btn-primary btn-sm" type="button" id="print_button" value="Print">
<div id="invoice">
    <?php foreach($empInfo as $eInfo){?>

    <?php
    $query1 = $this->db->query("select name from employeeinfo where card_no='" . $eInfo->card_no . "'");
    foreach ($query1->result() as $row3) {
        $employee_name = $row3->name;
    }
    if (empty($employee_name)):$employee_name = "";
    endif;
    ?>


        <div class="row">
            <div class="col-sm-12 col-lg-12">
                <div class="form-inline">
                    <div class="pull-left">
                        <h4>Name : <?php echo $employee_name ?></h4>
                    </div>

                    <div class="pull-right">
                        <div class="form-group has-feedback pull-right">
                            <!--<h4>Year : <?php /*echo $fyear*/?></h4>-->
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <?php
        $query10 = $this->leave_hr_model->getLeaveInfo1($eInfo->card_no, $fyear);
        if (!empty($query10)) {
            ?>
    <h3><i class="fa fa-angle-right"></i> Leave Details:</h3>

    <div class="row">


        <div class="col-sm-12">
            <div class="content-panel">
                <!--<section id="flip-scroll">-->
                <section id="">
                    <div id="invoice">
                    <table  id="myTable"  class="table table-hover">
                        <thead>
                        <tr>
                            <th>SL No</th>
                            <th>Start Date</th>
                            <th>End Date</th>
                            <th>Leave Type</th>
                            <th>Total Duration</th>
                            <th>Extra Days</th>
                            <th>Payment Type</th>
                        </tr>
                        </thead>
                        <tbody>
                        <?php

                        $c1 = 1;
                        $total1 = 0;
                        $total_duration = 0 ;
                        foreach ($query10 as $temp1) { ?>
                            <tr>
                                <?php
                                echo "<td>" . $c1 . "</td>";
                                echo "<td> " . $temp1->start_date . "</td>";
                                echo "<td> " . $temp1->end_date . "</td>";
                                if ($temp1->leave_type == '1') {
                                    $a = "Casual";
                                } elseif ($temp1->leave_type == '2') {
                                    $a = "Sick";
                                } elseif ($temp1->leave_type == '3') {
                                    $a = "Earned";
                                } else {
                                    $a = "";
                                }
                                echo "<td>" . $a . "</td>";
                                echo "<td>" . $temp1->leave_duration . "</td>";
                                echo "<td>" . $temp1->extra_days . "</td>";
                                ?>
                                <td> <?php
                                    if ($temp1->pay_type == "1") {
                                        echo "With  Pay";
                                    } elseif ($temp1->pay_type == "2") {
                                        echo "Without Pay";
                                    } else {
                                        echo "";
                                    }
                                    ?>
                                </td>
                            </tr>
                            <?php
                            $c1 = $c1 + 1;
                            $total_duration = $total_duration +  $temp1->leave_duration;
                        }?>
                        </tbody>
                    </table>
                    </div>
                </section>
            </div><! --/content-panel -->
        </div><!-- /col-md-12 -->
    </div><!-- row -->

                    <?php } ?>



    <h3><i class="fa fa-angle-right"></i> Leave Summary:</h3>

    <?php
    $query1 = $this->db->query("select name from employeeinfo where card_no='" . $eInfo->card_no . "'");
    foreach ($query1->result() as $row3) {
        $employee_name = $row3->name;
    }
    if (empty($employee_name)):$employee_name = "";
    endif;
    ?>

    <div class="row">

        <?php
        $employee_name = "";
        $query1 = $this->db->query("select name from employeeinfo where card_no='" . $eInfo->card_no . "'");
        foreach ($query1->result() as $row3) {
            $employee_name = $row3->name;
        }

        $leave_name = "";
        $res = $this->db->query("SELECT leave_name FROM employeeinfo where card_no='" . $eInfo->card_no . "'");
        foreach ($res->result() as $row3) {
            $leave_name = $row3->leave_name;
        }
        $package_name = "";
        $casual = "";
        $medical = "";
        $earned = "";

        $res1 = $this->db->query("SELECT * FROM add_leave WHERE id='" . $leave_name . "'");
        foreach ($res1->result() as $row1) {
            $package_name = $row1->package_name;
            $casual = $row1->casual;
            $medical = $row1->medical;
            $earned = $row1->earned;
        }
        $query4 = $this->db->query("SELECT * FROM leave_management WHERE card_no='" . $eInfo->card_no . "'  AND year='" . $fyear . "'and hr_status=2");
        $t_casual = 0;
        $t_medical = 0;
        $t_earned = 0;
        $ex_with_pay = 0;
        $ex_without_pay = 0;
        foreach ($query4->result() as $row4) {
            $duration = $row4->leave_duration;
            $extra_days = $row4->extra_days;
            $leave_type = $row4->leave_type;

            if ($leave_type == '1') {
                $t_casual = $t_casual + $duration;
            } else {
                $t_casual = $t_casual;
            }
            if ($leave_type == '2') {
                $t_medical = $t_medical + $duration;
            } else {
                $t_medical = $t_medical;
            }
            if ($leave_type == '3') {
                $t_earned = $t_earned + $duration;
            } else {
                $t_earned = $t_earned;
            }

            if ($row4->pay_type == "1") {
                $ex_with_pay = $ex_with_pay + $extra_days;
            } elseif ($row4->pay_type == "2") {
                $ex_without_pay = $ex_without_pay + $extra_days;
            } else {

            }
        }

        $cas_exists = $casual - $t_casual;
        if ($cas_exists < 0) {
            $cas_exists = 0;
        }
        $medical_exists = $medical - $t_medical;
        if ($medical_exists < 0) {
            $medical_exists = 0;
        }
        $earned_exists = $earned - $t_earned;
        if ($earned_exists < 0) {
            $earned_exists = 0;
        }
        ?>
        <div class="col-sm-12">
            <div class="content-panel">
                <!--<section id="flip-scroll">-->
                <section id="">
                    <table  id="myTable"  class="table table-hover">
                        <thead>
                        <tr>
                            <td ><b>Package Name</b></td>
                            <td ><b><?php echo $package_name; ?></b></td>
                            <td colspan="2"><b>Leave Spent</b></td>
                            <td colspan="2"><b>Leave Balance</b></td>
                        </tr>
                        </thead>
                        <tbody>


                        <tr>
                            <th>Casual Leave:</th>
                            <td><?php echo $casual; ?></td>
                            <th>Casual: </th>
                            <td><?php echo $t_casual; ?></td>
                            <th>Casual: </th>
                            <td><?php echo $cas_exists; ?></td>
                        </tr>
                        <tr>
                            <th>Sick Leave: </th>
                            <td><?php echo $medical; ?></td>
                            <th>Sick: </th>
                            <td><?php echo $t_medical; ?></td>
                            <th>Sick: </th>
                            <td><?php echo $medical_exists; ?></td>
                        </tr>
                        <tr>
                            <th>Earned Leave: </th>
                            <td><?php echo $earned; ?></td>
                            <th>Earned: </th>
                            <td><?php echo $t_earned; ?></td>
                            <th>Earned: </th>
                            <td><?php echo $earned_exists; ?></td>
                        </tr>

                        <tr style="">
                            <td></td>
                            <td ><?php echo $casual + $medical + $earned; ?></td>
                            <td></td>
                            <td><?php echo $t_casual + $t_medical + $t_earned; ?></td>
                            <td></td>
                            <td><?php echo $cas_exists + $medical_exists + $earned_exists; ?></td>
                        </tr>
                        </tbody>
                    </table>

                </section>
            </div><! --/content-panel -->
        </div><!-- /col-md-12 -->
    </div><!-- row -->
    <?php } ?>

</div>
<div class="text-center">
    <?php echo $this->pagination->create_links(); ?>
</div>
<div class="text-center">
    <?php
        echo "Total Records: ".$this->pagination_library->total_rows." page ".(($this->pagination->cur_page) ? $this->pagination->cur_page : "1")." of ". ceil(($this->pagination_library->total_rows)/($this->pagination_library->per_page));
    ?>
</div>