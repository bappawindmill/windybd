<h3><i class="fa fa-angle-right"></i> Leave Report</h3>

<!-- BASIC FORM ELELEMNTS -->
<div class="row">
    <div class="col-sm-12">
        <div class="content-panel">
            <?php
            echo form_open('hr/leave/leave_report_all' );?>
            <?php
            if($this->session->flashdata('message')==NULL){

            }else{ ?>
                <div class="alert-success alert">
                    <?php echo $this->session->flashdata('message') ?>
                </div> <?php
            }
            ?>
            <?php
            $query3 = $this->db->query("select id,branch_name from add_branch");
            $months = array(
                ''=>'Select',
                '01' => 'January',
                '02' => 'February',
                '03' => 'March',
                '04' => 'April',
                '05' => 'May',
                '06' => 'June',
                '07' => 'July',
                '08' => 'August',
                '09' => 'September',
                '10' => 'October',
                '11' => 'November',
                '12' => 'December',
            );
            ?>
            <div class="col-md-12">
                <div class="form-group col-sm-4">
                    <label for="crlbranch">SBU Name:</label>
                    <select name="branch_name" id="crlbranch" class="form-control">
                        <option value="">select</option>
                        <?php foreach ($query3->result() as $row3): ?>
                            <option value="<?php echo $row3->id; ?>"><?php echo $row3->branch_name; ?></option>
                        <?php endforeach; ?>
                    </select>
                    <div class="alert-danger"><?php echo ucwords(form_error('branch_name')); ?></div>
                </div>
                <div class="form-group col-sm-4">
                    <label for="">Department Name:</label>
                    <div id="res">
                        <select name="" id="" class="form-control">
                            <option value="">select</option>
                            <option value=""></option>
                        </select>
                    </div>
                    <div class="alert-danger"><?php echo ucwords(form_error('dept')); ?></div>
                </div>
                <div class="form-group col-sm-4">
                    <label for="year">Year:</label>
                    <select name="year" id="" class="form-control">
                        <option value="">select</option>
                        <?php for ($i = date('Y')-10; $i <= date('Y'); $i++) {
                            if($i==date('Y')){ ?>
                                <option value="<?php echo $i; ?> " selected ><?php echo $i; ?></option>
                                <?php
                            }
                            ?>
                            <option value="<?php echo $i; ?> "><?php echo $i; ?></option>
                        <?php } ?>
                    </select>
                    <div class="alert-danger"><?php echo ucwords(form_error('year')); ?></div>
                </div>
            </div>
                <span>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
                    <input type="hidden" name="id" value="" />&nbsp;&nbsp;&nbsp;&nbsp;
                    <input type="submit" name="mysubmit" value="Submit"  class="btn btn-primary">
                </span>

            </form>
            <p></p>
        </div>
    </div><!-- col-lg-12-->
</div><!-- /row -->