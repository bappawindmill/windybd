<?php

include_once 'BaseController.php';

class Info extends BaseController {

    public function __construct() {
        parent::__construct();
        if ($this->session->userdata('logged_in') != TRUE) {
            redirect('hr/login');
        }
        $this->load->model('hr/info_model');
    }

    function index() {
        $this->load->view('hr/home');
    }

    function print_card() {
        $this->data['mainTab'] = 'report';
        $this->data['activeTab'] = 'print_card';
        $this->data['dynamicView'] = 'hr/card/card_form';
        $this->_commonPageLayout('frontend_viewer');
    }

    function printed_card() {
        $this->data['mainTab'] = 'report';
        $this->data['activeTab'] = 'print_card';
        $this->load->library('form_validation');
        $this->form_validation->set_rules('branch_name', 'Branch Name', 'trim|required');
        $this->form_validation->set_rules('dept', 'Department', 'trim|required');
        $this->form_validation->set_rules('proffession', 'Section', 'trim|required');
        if ($this->form_validation->run() == FALSE) {
            $this->data['dynamicView'] = 'hr/card/card_form';
            $this->_commonPageLayout('frontend_viewer');
        } else {
            $this->data['query1'] = $this->info_model->get_all_employee();
            $this->data['dynamicView'] = 'hr/card/report_details';
            $this->_commonPageLayout('frontend_viewer');
        }
    }

    function add_employee_general_info($start = 0) {
        $this->data['optionDesignation'] = getOptionsDesignation();
        $this->data['optionDepartment'] = getOptionsDepartment();
        $this->data['mainTab'] = 'employee';
        $this->data['activeTab'] = 'general_info';
        $this->load->library('pagination');
        $this->load->library('parser');
        $config['base_url'] = site_url("hr/info/add_employee_general_info");
        $config['total_rows'] = $this->db->count_all('employeeinfo');
        $config['per_page'] = '500';
        $this->pagination->initialize($config);
        $this->load->model('hr/info_model');
        $this->data['query1'] = $this->info_model->view_employee_general_info($start, $config['per_page']);
        $this->data['dynamicView'] = 'hr/empinfo/employee_general_info';
        $this->_commonPageLayout('frontend_viewer');
    }

    function check_card_no($card_no) {

        $result = $this->dx_auth->is_card_no_available($card_no);
        if (!$result) {
            $this->form_validation->set_message('check_card_no', 'Card No exists. ');
        }

        return $result;
    }

    function check_secrete_no($secrete_no) {

        $result = $this->dx_auth->is_secrete_no_available($secrete_no);
        if (!$result) {
            $this->form_validation->set_message('check_secrete_no', 'Secrete No exists.');
        }

        return $result;
    }

    function check_user($username) {

        $result = $this->dx_auth->is_user_available($username);
        if (!$result) {
            $this->form_validation->set_message('check_user', 'Username exists.');
        }

        return $result;
    }

    function added_employee_general_info($start = 0) {
        $this->load->library('form_validation');
        $this->form_validation->set_rules('firstname', 'Firstname', 'trim|required');
       // $this->form_validation->set_rules('lastname', 'Lastname', 'trim|required');
        // $this->form_validation->set_rules('secrete_no', 'Secret No', 'trim|required|callback_check_secrete_no');
        //$this->form_validation->set_rules('card_no', 'ID No', 'trim|required|callback_check_card_no');
      //  $this->form_validation->set_rules('branch_name', 'SBU Name', 'trim|required');
       // $this->form_validation->set_rules('dept', 'Department', 'trim|required');
       // $this->form_validation->set_rules('designation', 'Designation', 'trim|required');
      //  $this->form_validation->set_rules('username', 'Username', 'trim|required|callback_check_user');
      //  $this->form_validation->set_rules('password', 'Password', 'trim|required');
      //  $this->form_validation->set_rules('email', 'Email', 'trim|required|valid_email');
     //   $this->form_validation->set_rules('category', 'Category', 'trim|required');
        if ($this->form_validation->run() == FALSE) {
            $this->data['optionDesignation'] = getOptionsDesignation();
            $this->data['mainTab'] = 'employee';
            $this->data['activeTab'] = 'general_info';
            $this->load->library('pagination');
            $this->load->library('parser');
            $config['base_url'] = site_url("hr/info/add_employee_general_info");
            $config['total_rows'] = $this->db->count_all('employeeinfo');
            $config['per_page'] = '500';
            $this->pagination->initialize($config);
            $this->load->model('hr/info_model');
            $this->data['query1'] = $this->info_model->view_employee_general_info($start, $config['per_page']);
            $this->data['dynamicView'] = 'hr/empinfo/employee_general_info';
            $this->_commonPageLayout('frontend_viewer');
        } else {
            $image_name = "";
            $config['upload_path'] = './uploads/';
            $config['allowed_types'] = 'gif|jpg|png';
            //$config['max_size']	= '10000';
            //$config['max_width']  = '1024';
            //$config['max_height']  = '768';
            $this->load->library('upload');
            $this->upload->initialize($config);
            if ($this->upload->do_upload()) {
                $data = $this->upload->data();
               echo  $image_name = $data['file_name'];
            }
			else{
			  echo $this->upload->display_errors();
			}
			
			die();
            $this->load->model('hr/info_model');
            //$this->load->view('upload_success', $data);
            if ($this->input->post('cash_payment') == 1) {
                $data = array('name' => $this->input->post('firstname') . " " . $this->input->post('lastname'),
                    'card_no' => $this->input->post('card_no'),
                    'dept' => $this->input->post('dept'),
                    'branch_name' => $this->input->post('branch_name'),
                    'leave_name' => $this->input->post('leave_name'),
                    'dob' => $this->input->post('year') . '-' . $this->input->post('month') . '-' . $this->input->post('day'),
                    'gender' => $this->input->post('gender'),
                    'photo' => $image_name,
                    'marital_status' => $this->input->post('marital_status'),
                    'nationality' => $this->input->post('nationality'),
                    'blood' => $this->input->post('blood'),
                    'telephone' => $this->input->post('telephone'),
                    'contact_person' => $this->input->post('contact_person'),
                    'national_id' => $this->input->post('national_id'),
                    'email' => $this->input->post('email'),
                    'emergency_no' => $this->input->post('emergency_no'),
                    'relation' => $this->input->post('relation'),
                    'join_date' => $this->input->post('join_date'),
                    'notice_period' => $this->input->post('notice_period'),
                    'attendence_rules' => $this->input->post('attendence_rules'),
                    'provident_fund' => $this->input->post('provident_fund'),
                    'payment_mode' => $this->input->post('cash_payment'),
                    'note' => $this->input->post('note'),
                    'secrete_no' => $this->input->post('secrete_no'),
                    'emp_type' => $this->input->post('emp_type'),
                    'designation' => $this->input->post('designation'),
                    'category' => $this->input->post("category"),
                    'username' => $this->input->post("username"),
                    'password' => $this->input->post("password"),
					'sup_designation'=>$this->input->post("sup_designation"),
                    'supervisor' => $this->input->post("supervisor"),
                    'confirmation_date' => $this->input->post("confirmation_date"),
                    'reference_name' => $this->input->post("reference_name"),
                    'referee_contact_no' => $this->input->post("referee_contact_no"),
                    'sapackage' => $this->input->post('sapackage'),
                    'basic' => $this->input->post('basic'),
                    'house_rent' => $this->input->post('house_rent'),
                    'medical' => $this->input->post('medical'),
                    'conveyance' => $this->input->post('conveyance')
                );
            } elseif ($this->input->post('bank_payment') == 2) {

                $data = array(
                    'name' => $this->input->post('firstname') . " " . $this->input->post('lastname'),
                    'card_no' => $this->input->post('card_no'),
                    'dept' => $this->input->post('dept'),
                    'branch_name' => $this->input->post('branch_name'),
                    'proffession' => $this->input->post('proffession'),
                    'leave_name' => $this->input->post('leave_name'),
                    'dob' => $this->input->post('year') . '-' . $this->input->post('month') . '-' . $this->input->post('day'),
                    'gender' => $this->input->post('gender'),
                    'photo' => $image_name,
                    'marital_status' => $this->input->post('marital_status'),
                    'nationality' => $this->input->post('nationality'),
                    'blood' => $this->input->post('blood'),
                    'telephone' => $this->input->post('telephone'),
                    'contact_person' => $this->input->post('contact_person'),
                    'national_id' => $this->input->post('national_id'),
                    'email' => $this->input->post('email'),
                    'emergency_no' => $this->input->post('emergency_no'),
                    'relation' => $this->input->post('relation'),
                    'join_date' => $this->input->post('join_date'),
                    'notice_period' => $this->input->post('notice_period'),
                    'attendence_rules' => $this->input->post('attendence_rules'),
                    'provident_fund' => $this->input->post('provident_fund'),
                    'payment_mode' => $this->input->post('bank_payment'),
                    'bank_name' => $this->input->post('bank_name'),
                    'bank_branch' => $this->input->post('bank_branch'),
                    'account_number' => $this->input->post('account_number'),
                    'note' => $this->input->post('note'),
                    'secrete_no' => $this->input->post('secrete_no'),
                    'emp_type' => $this->input->post('emp_type'),
                    'designation' => $this->input->post('designation'),
                    'category' => $this->input->post("category"),
                    'username' => $this->input->post("username"),
                    'password' => $this->input->post("password"),
					'sup_designation'=>$this->input->post("sup_designation"),
                    'supervisor' => $this->input->post("supervisor"),
                    'confirmation_date' => $this->input->post("confirmation_date"),
                    'reference_name' => $this->input->post("reference_name"),
                    'referee_contact_no' => $this->input->post("referee_contact_no"),
                    'sapackage' => $this->input->post('sapackage'),
                    'basic' => $this->input->post('basic'),
                    'house_rent' => $this->input->post('house_rent'),
                    'medical' => $this->input->post('medical'),
                    'conveyance' => $this->input->post('conveyance')
                );
            } else {

                $data = array(
                    'name' => $this->input->post('firstname') . " " . $this->input->post('lastname'),
                    'card_no' => $this->input->post('card_no'),
                    'dept' => $this->input->post('dept'),
                    'branch_name' => $this->input->post('branch_name'),
                    'proffession' => $this->input->post('proffession'),
                    'leave_name' => $this->input->post('leave_name'),
                    'dob' => $this->input->post('year') . '-' . $this->input->post('month') . '-' . $this->input->post('day'),
                    'gender' => $this->input->post('gender'),
                    'photo' => $image_name,
                    'marital_status' => $this->input->post('marital_status'),
                    'nationality' => $this->input->post('nationality'),
                    'blood' => $this->input->post('blood'),
                    'telephone' => $this->input->post('telephone'),
                    'contact_person' => $this->input->post('contact_person'),
                    'national_id' => $this->input->post('national_id'),
                    'email' => $this->input->post('email'),
                    'emergency_no' => $this->input->post('emergency_no'),
                    'relation' => $this->input->post('relation'),
                    'join_date' => $this->input->post('join_date'),                   
                    'notice_period' => $this->input->post('notice_period'),
                    'attendence_rules' => $this->input->post('attendence_rules'),
                    'provident_fund' => $this->input->post('provident_fund'),
                    'note' => $this->input->post('note'),
                    'secrete_no' => $this->input->post('secrete_no'),
                    'emp_type' => $this->input->post('emp_type'),
                    'designation' => $this->input->post('designation'),
                    'category' => $this->input->post("category"),
                    'username' => $this->input->post("username"),
                    'password' => $this->input->post("password"),
					'sup_designation'=>$this->input->post("sup_designation"),
                    'supervisor' => $this->input->post("supervisor"),
                    'confirmation_date' => $this->input->post("confirmation_date"),
                    'reference_name' => $this->input->post("reference_name"),
                    'referee_contact_no' => $this->input->post("referee_contact_no"),
                    'sapackage' => $this->input->post('sapackage'),
                    'basic' => $this->input->post('basic'),
                    'house_rent' => $this->input->post('house_rent'),
                    'medical' => $this->input->post('medical'),
                    'conveyance' => $this->input->post('conveyance')
                );
            }
            $this->info_model->employee_info_insert($data);
            $e_id = $this->db->insert_id();
            $data = array(
                'emp_id' => $e_id,
                'month' => date('j'),
                'year' => date('Y'),
                'status' => $this->input->post('provident_fund'),
            );
            $this->info_model->pf_insert($data);
            $this->session->set_flashdata('message', '<div id="message">Employee Info saved Successfully.</div>');
            redirect('hr/info/add_employee_general_info', 'location', 301);
        }
    }

    function employee_general_info_edit($id = NULL) {
        $this->data['mainTab'] = 'employee';
        $this->data['activeTab'] = 'general_info';
        $this->load->library('form_validation');
        $this->form_validation->set_rules('name', 'name', 'trim|required');
          
        if ($this->input->post('mysubmit')) {
            $this->load->model('hr/info_model');
            $image_name = "";
            /*$config['upload_path'] = './uploads/';
            $config['allowed_types'] = 'gif|jpg|png';
            //$config['max_size']	= '10000';
            //$config['max_width']  = '1024';
            //$config['max_height']  = '768';
            $this->load->library('upload');
            $this->upload->initialize($config);
            if ($this->upload->do_upload()) {
                $data = $this->upload->data();
                $image_name = $data['file_name'];
            }*/
			
			
			
              $data = array(
                    'name' => $this->input->post('name') ,
                    'card_no' => $this->input->post('card_no'),
                    'dept' => $this->input->post('dept'),
                    'branch_name' => $this->input->post('branch_name'),
                    'proffession' => $this->input->post('proffession'),
                    'leave_name' => $this->input->post('leave_name'),
                    'dob' => $this->input->post('date_of_birth') ,
                    'gender' => $this->input->post('gender'),
                    'photo' => $image_name,
                    'marital_status' => $this->input->post('marital_status'),
                    'nationality' => $this->input->post('nationality'),
                    'blood' => $this->input->post('blood'),
                    'telephone' => $this->input->post('telephone'),
                    'contact_person' => $this->input->post('contact_person'),
                    'national_id' => $this->input->post('national_id'),
                    'email' => $this->input->post('email'),
                    'emergency_no' => $this->input->post('emergency_no'),
                    'relation' => $this->input->post('relation'),
                    'join_date' => $this->input->post('join_date'),
                    'notice_period' => $this->input->post('notice_period'),
                    'attendence_rules' => $this->input->post('attendence_rules'),
                    'provident_fund' => $this->input->post('provident_fund'),
                    'note' => $this->input->post('note'),
                    'secrete_no' => $this->input->post('secrete_no'),
                    'emp_type' => $this->input->post('emp_type'),
                    'designation' => $this->input->post('designation'),
                    'category' => $this->input->post("category"),
                    'username' => $this->input->post("username"),
                    'password' => $this->input->post("password"),
					'sup_designation'=>$this->input->post("sup_designation"),
                    'supervisor' => $this->input->post("supervisor"),
                    'confirmation_date' => $this->input->post("confirmation_date"),
                    'reference_name' => $this->input->post("reference_name"),
                    'referee_contact_no' => $this->input->post("referee_contact_no"),
                    'sapackage' => $this->input->post('sapackage'),
                    'basic' => $this->input->post('basic'),
                    'house_rent' => $this->input->post('house_rent'),
                    'medical' => $this->input->post('medical'),
                    'conveyance' => $this->input->post('conveyance')
                );
				
            $this->info_model->employee_general_info_update($data);
             if (($this->input->post('cash_payment') == 1) && $this->input->post('bank_payment') == 2) {
                
            } elseif ($this->input->post('cash_payment') == 1 && $this->input->post('bank_payment') != 2) {
                $data = array(
                    'payment_mode' => $this->input->post('cash_payment')
                );
                $this->info_model->employee_general_info_update($data);
            } elseif ($this->input->post('bank_payment') == 2 && $this->input->post('cash_payment') != 1) {

                $data = array(
                    'payment_mode' => $this->input->post('bank_payment'),
                    'bank_name' => $this->input->post('bank_name'),
                    'bank_branch' => $this->input->post('bank_branch'),
                    'account_number' => $this->input->post('account_number')
                );
                $this->info_model->employee_general_info_update($data);
            } else {
                
            }
            
            redirect('hr/employee/get_all_employee', 'location', 301);
        }


        if ($id != NULL) {
            $this->load->model('hr/info_model');
            $this->data['query'] = $this->info_model->get_employee_general_info($id);
            $this->data['dynamicView'] = 'hr/empinfo/employee_general_info_edit';
            $this->_commonPageLayout('frontend_viewer');
        }
    }

    /* End Edit employee_info */

    /* Start Delete employee_info */

    function employee_general_info_delete($id = NULL) {
        $this->load->model('hr/info_model');
        $this->info_model->delete_employee_general_info($id);
        redirect('hr/employee/get_all_employee', 'location', 301);
    }

    function update_secrete_no() {
        $query = $this->db->query("select * from employeeinfo");
        foreach ($query->result() as $info):
            $id = $info->id;
            $p_sl = $info->secrete_no;
            $aIdlen = strlen($p_sl);
            if ($aIdlen == 1): $asIdlen = "000000000";
            elseif ($aIdlen == 2): $asIdlen = "00000000";
            elseif ($aIdlen == 3): $asIdlen = "0000000";
            elseif ($aIdlen == 4): $asIdlen = "000000";
            elseif ($aIdlen == 5): $asIdlen = "00000";
            elseif ($aIdlen == 6): $asIdlen = "0000";
            elseif ($aIdlen == 7): $asIdlen = "000";
            elseif ($aIdlen == 8): $asIdlen = "00";
            elseif ($aIdlen == 9): $asIdlen = "0";
            elseif ($aIdlen == 10): $asIdlen = "";
            endif;
            $en_id = $asIdlen . $p_sl;
            $data = array(
                'secrete_no' => $en_id
            );
            $this->info_model->update_secrete_no($data, $id);
        endforeach;
    }

    function view_summery() {
        $this->data['mainTab'] = 'report';
        $this->data['activeTab'] = 'view_summery';
        $this->data['dynamicView'] = 'hr/summery/summery_form';
        $this->_commonPageLayout('frontend_viewer');
    }

    function viewed_summery() {
        $this->data['mainTab'] = 'report';
        $this->data['activeTab'] = 'view_summery';
        $this->load->library('form_validation');
        $this->form_validation->set_rules('branch_name', 'Branch Name', 'trim|required');
        if ($this->form_validation->run() == FALSE) {
            $this->data['dynamicView'] = 'hr/summery/summery_form';
            $this->_commonPageLayout('frontend_viewer');
        } else {
            if ($this->input->post("dept") != "" && $this->input->post("proffession") == "") {
                $dept = $this->input->post("dept");
                $this->data['query1'] = $this->info_model->get_section_dept($dept);
            } else if ($this->input->post("dept") != "" && $this->input->post("proffession") != "") {
                $section = $this->input->post("proffession");
                $this->data['query1'] = $this->info_model->get_section($section);
            } else {
                $this->data['query1'] = $this->info_model->get_all_section();
            }

            $this->data['dynamicView'] = 'hr/summery/details';
            $this->_commonPageLayout('frontend_viewer');
        }
    }

    protected

    function _commonPageLayout($viewName, $cacheIt = FALSE) {
        $view = $this->layout->view($viewName, $this->data, TRUE);
        $replaces = array(
            '{SITE_TOP_LOGO}' => '', //$this->load->view('frontend/site_top_logo', $this->data, TRUE),
            '{SITE_TOP_MENU}' => $this->load->view('frontend/site_top_menu', NULL, TRUE),
            '{SITE_MIDDLE_CONTENT}' => $this->load->view($this->data['dynamicView'], NULL, TRUE),
            '{SITE_FOOTER}' => $this->load->view('frontend/site_footer', NULL, TRUE)
        );

        $this->load->view('view', array('view' => $view, 'replaces' => $replaces));
    }

    /* End Delete employee_info */
    /* start employee report */
}
