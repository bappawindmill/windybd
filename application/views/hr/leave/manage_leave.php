<link rel="stylesheet" type="text/css" href="<?php echo base_url(); ?>public/css/t1.css" />
<style type="text/css">
    <!--
    .style1 {color: #FF0000}
    -->
</style>
<script type="text/javascript">
    window.onload = function(){
        new JsDatePick({
            useMode:2,
            target:"start_date",
            dateFormat:"%Y-%m-%d"
        });
        new JsDatePick({
            useMode:2,
            target:"end_date",
            dateFormat:"%Y-%m-%d"
        });
    }
</script>
<script>
    function h_check()
    {
       
        var myvar = document.getElementById("hourly").checked;
        if(myvar)
        {
            $("#daily").attr("disabled",true);
            $("#h").show();
        }
        else
        {
            $("#daily").removeAttr("disabled");
            $("#h").hide();
        }
    }
    function d_check()
    {
        var myvar = document.getElementById("daily").checked;
        if(myvar)
        {
            $("#d").show();          
            $("#hourly").attr("disabled",true);;
						
        }
        else
        {
            $("#d").hide();           
            $("#hourly").removeAttr("disabled");
							
        }
    }
</script>
<div class="grid_12">
    <div class="module1">
        <h2><span>Leave Management</span></h2>
        <?php echo $this->session->flashdata('message'); ?>
        <div class="module-body">
            <?php
            $query3 = $this->db->query("select id,branch_name from add_branch");
            ?>
            <?php
            $leaves = array(
                '' => 'Select',
                '1' => 'Causal ',
                '2' => 'Medical',
                '3' => 'Earned',
                '4' => 'Compensation',
                '5' => 'Other'
            );
            echo form_open('hr/leave/leave_managed');

            echo '<table>';
            echo '<thead><tr><th colspan="2"><b></b></th></tr></thead>';
            $months = array('01' => 'January',
                '02' => 'February',
                '03' => 'March',
                '04' => 'April',
                '05' => 'May',
                '06' => 'June',
                '07' => 'July',
                '08' => 'August',
                '09' => 'September',
                '10' => 'October',
                '11' => 'November',
                '12' => 'December',
            );
            ?>
            <tr><th>Branch Name :</th><td>
                    <select name="branch_name" id="crlbranch" class="input-short" style="width:130px;">
                        <option   value="">Select</option>									
                        <?php foreach ($query3->result() as $row3): ?>
                            <option value="<?php echo $row3->id; ?>"><?php echo $row3->branch_name; ?></option>
                        <?php endforeach; ?>
                    </select>
                    <span class="notification-input ni-error"> <?php echo ucwords(form_error('branch_name')); ?></span>
                </td></tr> 
            <tr><th>Card No :</th><td>
                    <input type="text" name="card_no" />
                    <span class="notification-input ni-error"> <?php echo ucwords(form_error('card_no')); ?></span>
                </td></tr>

            <tr><th> Date :</th><td>
                    <input type="text" name="start_date" id="start_date" />
                    <span class="notification-input ni-error"> <?php echo ucwords(form_error('start_date')); ?></span>
                </td></tr>  
            <tr><th>Reason :</th><td>
                    <input type="text" name="note" id="note" />
                    <span class="notification-input ni-error"> <?php echo ucwords(form_error('note')); ?></span>
                </td></tr>       
            <tr><th>Leave Category :</th><td>
                    Hourly<input type="checkbox" name="category" value="1" id="hourly" onClick="return h_check();">                    
                    Daily<input type="checkbox" value="2" name="category" id="daily" onclick="return d_check()">
                    <span class="notification-input ni-error"> <?php echo ucwords(form_error('category')); ?></span>
                </td></tr>       
            <tr id="h" style="display:none"><th>Leave Hour :</th><td>
                    <input type="text" name="leave_hour" id="note" />(00:00:00)
                    <span class="notification-input ni-error"> <?php echo ucwords(form_error('leave_hour')); ?></span>
                </td></tr> 
            <tr id="d" style="display:none"><th>Leave Type :</th><td>
                    <?php echo form_dropdown('leave_type', $leaves, 'leave_type') ?>
                </td></tr>

            <?php
            echo '<tr><th></th><td>' . form_submit('mysubmit', 'Submit') . '</td></tr>';

            echo '</table>';
            echo form_close();
            ?>
        </div></div></div>
<div style="clear: both;"></div>
