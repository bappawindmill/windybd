<?php $this->load->view('hr/layout');?>
                            <div class="art-Post-body">
                        <div class="art-Post-inner">
                                        <div class="art-PostMetadataHeader">
                                            <h2 class="art-PostHeader">
                                                <img src="<?php echo base_url();?>images/PostHeaderIcon.png" width="29" height="29" alt="" />
                                                Leave Management Edit
                                            </h2>
                                        </div>
                                        <div class="art-PostContent">
                                              <br />
		<?php 
		$leaves = array('1' => 'Causal Leave',
						'2' => 'Emergency Leave',
						'3' => 'Sick Leave',
						'4' => 'Absence'
						);
		$months = array('1' => 'January',
						'2' => 'February',
						'3' => 'March',
						'4' => 'April',
						'5' => 'May',
						'6' => 'June',
						'7' => 'July',
						'8' => 'August',
						'9' => 'September',
						'10' => 'October',
						'11' => 'November',
						'12' => 'December',
						);
		echo form_open('hr/leave/leave_manage_edit');
		echo form_hidden('id',$fid['value']);
		echo '<table>';		
		echo '<thead><tr><th colspan="2"><b>Leave Management</b></th></tr></thead>';
		?>
		{EMPLOYEE}
		<?php
		echo '<tr><th>Leave Type :</th><td>'.form_dropdown('leave_type',$leaves,$fleave_type['value']).'</td></tr>';
		echo '<tr><th>Leave Duration :</th><td>'.form_input('duration',$fduration['value'],'duration').'<span style="color:red;">*</span></td></tr>';	
		echo '<tr><th>Year :</th><td>'.form_input('year',$fdeparture['value'],'year').'<span style="color:red;">*</span></td></tr>';
		echo '<tr><th>Month :</th><td>'.form_dropdown('month',$months,$freturn['value']).'</td></tr>';
		
		//echo '<tr><th>Date of Departure :</th><td>'.form_input('departure','','departure').'</td></tr>';
		//echo '<tr><th>Date of Return :</th><td>'.form_input('return','','return').'</td></tr>';
		echo '<tr><th>Purpose of Leave :</th><td>'.form_input('purpose',$fpurpose['value'],'purpose').'<span style="color:red;">*</span></td></tr>';
		echo '<tr><th>Note :</th><td>'.form_input('note',$fnote['value'],'note').'</td></tr>';
		echo '<tr><th></th><td>'.form_submit('mysubmit','Add Leave').'</td></tr>';
		echo '<tr><th></th><td>'.validation_errors('<p class="error">').'</td></tr>';
		echo '</table>';
echo form_close(); 
?>

 <br /> <br />
                                             
                                            	
                                                
                                        </div>
                                        <div class="cleared"></div>
                        </div>
                        
                        		<div class="cleared"></div>
                            </div>
                        </div>
                    </div>
                <div class="cleared"></div>
				<?php $this->load->view('hr/footer');?>
