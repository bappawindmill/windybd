<h3><i class="fa fa-angle-right"></i> Datewise Leave Report</h3>

<!-- BASIC FORM ELELEMNTS -->
<div class="row">
    <div class="col-sm-12">
        <div class="content-panel">
            <?php
            echo form_open('hr/leave/dateWiseView');?>
            <?php
            if($this->session->flashdata('message')==NULL){

            }else{ ?>
                <div class="alert-success alert">
                    <?php echo $this->session->flashdata('message') ?>
                </div> <?php
            }
            ?>
            <div class="col-md-12">
                <div class="form-group col-sm-6">
                    <label for="start_date">Start Date:</label>
                    <input value=""  type="text" name="start_date" id="start_date" class="form-control">
                    <div class="alert-danger"><?php echo ucwords(form_error('start_date')); ?></div>
                </div>

                <div class="form-group col-sm-6">
                    <label for="end_date">End Date:</label>
                    <input value=""  type="text" name="end_date" id="weekend_date" class="form-control">
                    <div class="alert-danger"><?php echo ucwords(form_error('end_date')); ?></div>
                </div>
            </div>
                <span>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
                    <input type="hidden" name="id" value="" />&nbsp;&nbsp;&nbsp;&nbsp;
                    <input type="submit" name="mysubmit" value="Submit"  class="btn btn-primary">
                </span>
            </form>
            <p></p>
        </div>
    </div><!-- col-lg-12-->
</div><!-- /row -->