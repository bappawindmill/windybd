<!--<h3><i class="fa fa-angle-right"></i> View User</h3>-->
<p></p>
<div class="row">

    <div class="col-sm-12 col-lg-12">
        <div class="pull-left">
            <p style="font-size: large"><i class="fa fa-angle-right"></i> Datewise Leave Report</p>
        </div>
        <div class="pull-right">
            <form class="form-inline">
                <div class="form-group has-feedback pull-right">
                    <input type="text" id="searchKey" name="search" class="form-control" placeholder="Search by start date" />
                    <i class="glyphicon glyphicon-search form-control-feedback"></i>
                    <input type="hidden" name="start_date" value="<?php echo $start_date;?>" />
                    <input type="hidden" name="end_date" value="<?php echo $end_date;?>" />
                </div>
            </form>
        </div>
        <div class="pull-right"><a href="<?php echo base_url();?>hr/leave/dateWiseView" class="btn btn-info" role="button" >Reset</a>&nbsp;</div>
    </div>
</div>

<div class="row">
    <div class="col-sm-12">
        <div class="content-panel">
            <!--<hr>-->
            <div class="table-responsive">
                <table class="table table-bordered table-hover">
                    <thead>
                    <tr class="info">
                        <th>SL No</th>
                        <th>SBU</th>
                        <th>Dept</th>
                        <th>Name</th>
                        <th>Start Date</th>
                        <th>End Date</th>
                        <th>Leave Type</th>
                        <th>Total Duration</th>
                        <th>Extra Days</th>
                        <th>Payment Type</th>
                    </tr>
                    </thead>
                    <tbody>
                    <?php

                    $start  = ($this->input->get('start')) ? $this->input->get('start') : 0;

                    if($start){
                        $i = $start+1;
                    }else{
                        $i = '1';
                    }

                    foreach ($empInfo as $temp) {

                        //echo $temp->branch_name;exit;
                        $dept_name = "";
                        $query = $this->db->query("select name from add_department where id='" . $temp->dept . "'");
                        foreach ($query->result() as $row3) {
                            $dept_name = $row3->name;
                        }
                        $branch_name = "";
                        $query3 = $this->db->query("select branch_name from add_branch where id='" . $temp->bra_name . "'");
                        foreach ($query3->result() as $row33) {
                            $branch_name = $row33->branch_name;
                        }

                        ?>
                        <tr>
                            <td><?php echo $i++;?></td>
                            <td><?php echo $branch_name;?></td>
                            <td><?php echo $dept_name;?></td>
                            <td><?php echo $temp->name;?></td>
                            <td><?php echo $temp->start_date;?></td>
                            <td><?php echo $temp->end_date;?></td>
                            <td>
                                <?php
                                if ($temp->leave_type == '1') {
                                    $a = "Casual";
                                } elseif ($temp->leave_type == '2') {
                                    $a = "Sick";
                                } elseif ($temp->leave_type == '3') {
                                    $a = "Earned";
                                } else {
                                    $a = "";
                                }
                                echo $a;
                                ?>
                            </td>
                            <td><?php echo $temp->leave_duration;?></td>
                            <td><?php echo $temp->extra_days;?></td>
                            <td>
                                <?php
                                if ($temp->pay_type == "1") {
                                    echo "With  Pay";
                                } elseif ($temp->pay_type == "2") {
                                    echo "Without Pay";
                                } else {
                                    echo "";
                                }
                                ?>
                            </td>
                        </tr>
                        <?php
                    }
                    ?>
                    </tbody>
                </table>
                <div class="text-center">
                    <?php
                    echo $this->pagination->create_links();
                    ?>
                </div>

                <div class="text-center">
                    <?php
                    echo "Total Records: ".$this->pagination_library->total_rows." page ".(($this->pagination->cur_page) ? $this->pagination->cur_page : "1")." of ". ceil(($this->pagination_library->total_rows)/($this->pagination_library->per_page));
                    ?>
                </div>

            </div>
        </div><! --/content-panel -->
    </div><!-- /col-md-12 -->
</div><!-- row -->