<script language="javascript">
    function confirmSubmit() {
        var agree = confirm("Are you sure to delete this record?");
        if (agree)
            return true;
        else
            return false;
    }
</script>

<h3><i class="fa fa-angle-right"></i> Namewise Report Details</h3>
<?php
if($this->session->flashdata('message')==NULL){

}else{ ?>
    <div class="alert-success alert">
        <?php echo $this->session->flashdata('message') ?>
    </div> <?php
}
?>


<div class="row">

    <div class="col-sm-12">
        <div class="content-panel">
            <!--<section id="flip-scroll">-->
            <section id="">
                <table  id="myTable"  class="table table-hover">
                    <thead>
                    <tr>
                        <th>#</th>
                        <th>SBU</th>
                        <th>Dept</th>
                        <th>Name</th>
                        <th>Start Date</th>
                        <th>End Date</th>
                        <th>Leave Type</th>
                        <th>Total Duration</th>
                        <th>Extra Days</th>
                        <th>Payment Type</th>
                    </tr>
                    </thead>
                    <tbody>
                    <?php

                    $this->load->model('hr/leave_model');

                    $c1 = 1;

                    foreach ($empInfo as $eInfo):

                        $dataExplode = explode("-", $sdate);

                        $fyear = $dataExplode[0];

                        ?>



                        <?php

                        $employee_name = "";

                        $query1 = $this->db->query("select name from employeeinfo where card_no='" . $eInfo->card_no . "'");

                        foreach ($query1->result() as $row3) {

                            $employee_name = $row3->name;

                        }



                        $dept_name = "";

                        $query = $this->db->query("select name from add_department where id='" . $eInfo->dept . "'");

                        foreach ($query->result() as $row3) {

                            $dept_name = $row3->name;

                        }

                        $branch_name = "";

                        $query3 = $this->db->query("select branch_name from add_branch where id='" . $eInfo->branch_name . "'");

                        foreach ($query3->result() as $row33) {

                            $branch_name = $row33->branch_name;

                        }



                        $query10 = $this->leave_hr_model->getLeaveInfoForReport($eInfo->id, $sdate, $edate);

                        if (!empty($query10)) {





                            $total1 = 0;

                            ?>







                            <?php

                            $k=0;

                            foreach ($query10 as $temp1) {?>

                                <tr >

                                    <?php if ($k == 0): ?>

                                        <td style='text-align:center'><?php echo $c1; ?></td>

                                    <?php else: ?>

                                        <td style='text-align:center'></td>

                                    <?php endif; ?>

                                    <td style='text-align:center'> <a href="<?php echo base_url(); ?>/hr/leave/branchWiseView/<?php echo $eInfo->branch_name;?>/<?php echo $sdate;?>/<?php echo $edate;?>"><?php echo $branch_name ?></a></td>

                                    <td style='text-align:center'><?php echo $dept_name ?></td>

                                    <td style='text-align:center'> <a href="<?php echo base_url(); ?>/hr/leave/nameWiseView/<?php echo $eInfo->card_no;?>/<?php echo $sdate;?>/<?php echo $edate;?>"><?php echo $employee_name ?></a></td>

                                    <td style='text-align:center'> <?php echo $temp1->start_date?></td>

                                    <td style='text-align:center'> <?php echo $temp1->end_date ?></td>

                                    <?php

                                    if ($temp1->leave_type == '1') {



                                        $a = "Casual";

                                    } elseif ($temp1->leave_type == '2') {



                                        $a = "Sick";

                                    } elseif ($temp1->leave_type == '3') {



                                        $a = "Earned";

                                    } else {

                                        $a = "";

                                    }?>

                                    <td style='text-align:center'> <a href="<?php echo base_url(); ?>/hr/leave/leaveTypeWiseView/<?php echo $temp1->leave_type;?>/<?php echo $sdate;?>/<?php echo $edate;?>"><?php echo $a ?></a></td>



                                    <td style='text-align:center'><?php echo $temp1->leave_duration ?></td>

                                    <td style='text-align:center'><?php echo $temp1->extra_days ?></td>

                                    <td> <?php

                                        if ($temp1->pay_type == "1") {

                                            echo "With  Pay";

                                        } elseif ($temp1->pay_type == "2") {

                                            echo "Without Pay";

                                        } else {

                                            echo "";

                                        }

                                        ?>

                                    </td></tr>

                                <?php

                                $k=$k+1;

                            }









                            $leave_name = "";

                            $res = $this->db->query("SELECT leave_name FROM employeeinfo where card_no='" . $eInfo->card_no . "'");

                            foreach ($res->result() as $row3) {

                                $leave_name = $row3->leave_name;

                            }

                            $package_name = "";

                            $casual = "";

                            $medical = "";

                            $earned = "";



                            $res1 = $this->db->query("SELECT * FROM add_leave WHERE id='" . $leave_name . "'");

                            foreach ($res1->result() as $row1) {

                                $package_name = $row1->package_name;

                                $casual = $row1->casual;

                                $medical = $row1->medical;

                                $earned = $row1->earned;

                            }



                            $query4 = $this->db->query("SELECT * FROM leave_management WHERE card_no='" . $eInfo->card_no . "'  AND year='" . $fyear . "'and hr_status=2");



                            $t_casual = 0;

                            $t_medical = 0;

                            $t_earned = 0;

                            $ex_with_pay = 0;

                            $ex_without_pay = 0;

                            foreach ($query4->result() as $row4) {

                                $duration = $row4->leave_duration;

                                $extra_days = $row4->extra_days;

                                $leave_type = $row4->leave_type;



                                if ($leave_type == '1') {

                                    $t_casual = $t_casual + $duration;

                                } else {

                                    $t_casual = $t_casual;

                                }

                                if ($leave_type == '2') {

                                    $t_medical = $t_medical + $duration;

                                } else {

                                    $t_medical = $t_medical;

                                }

                                if ($leave_type == '3') {

                                    $t_earned = $t_earned + $duration;

                                } else {

                                    $t_earned = $t_earned;

                                }



                                if ($row4->pay_type == "1") {

                                    $ex_with_pay = $ex_with_pay + $extra_days;

                                } elseif ($row4->pay_type == "2") {

                                    $ex_without_pay = $ex_without_pay + $extra_days;

                                } else {



                                }

                            }



                            $cas_exists = $casual - $t_casual;

                            if ($cas_exists < 0) {

                                $cas_exists = 0;

                            }

                            $medical_exists = $medical - $t_medical;

                            if ($medical_exists < 0) {

                                $medical_exists = 0;

                            }

                            $earned_exists = $earned - $t_earned;

                            if ($earned_exists < 0) {

                                $earned_exists = 0;

                            }

                            $c1 = $c1 + 1;

                        }

                    endforeach; ?>
                    </tbody>
                </table>
                <div class="text-center">
                    <?php echo $this->pagination->create_links(); ?>
                </div>
            </section>
        </div><! --/content-panel -->
    </div><!-- /col-md-12 -->
</div><!-- row -->