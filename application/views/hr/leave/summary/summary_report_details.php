<p></p>
<input class="btn btn-primary btn-sm" type="button" id="print_button" value="Print">
<script language="javascript">
    function confirmSubmit() {
        var agree = confirm("Are you sure to delete this record?");
        if (agree)
            return true;
        else
            return false;
    }
</script>

<h3><i class="fa fa-angle-right"></i> Leave Summary Report Details</h3>
<?php
if($this->session->flashdata('message')==NULL){

}else{ ?>
    <div class="alert-success alert">
        <?php echo $this->session->flashdata('message') ?>
    </div> <?php
}
?>


<div class="row">

    <div class="col-sm-12">
        <div class="content-panel">
            <!--<section id="flip-scroll">-->
            <section id="">
                <div id="invoice">
                <table  id="myTable"  class="table table-hover table-bordered">
                    <thead>
                        <tr class="info">
                            <th></th>
                            <th></th>
                            <th class=" " colspan="3">Sick Leave</th>
                            <th colspan="3">Casual Leave</th>
                            <th colspan="3">Annual Leave</th>
                        </tr>
                        <tr>

                            <th rowspan=""  valign="middle">#</th>
                            <th rowspan="">Employee Name </th>
                            <th>Total</th>
                            <th>Spend</th>
                            <th>Balence</th>
                            <th>Total</th>
                            <th>Spend</th>
                            <th>Balence</th>
                            <th>Total</th>
                            <th>Spend</th>
                            <th>Balence</th>
                        </tr>
                    </thead>
                    <tbody>
                    <?php

                    $this->load->model('hr/leave_model');

                    $c1 = 1;

                    foreach ($empInfo as $eInfo):

                        $leave_name = "";

                        $res = $this->db->query("SELECT leave_name FROM employeeinfo where card_no='" . $eInfo->card_no . "'");

                        foreach ($res->result() as $row3) {

                            $leave_name = $row3->leave_name;

                        }

                        $package_name = "";

                        $casual = "";

                        $medical = "";

                        $earned = "";



                        $res1 = $this->db->query("SELECT * FROM add_leave WHERE id='" . $leave_name . "'");

                        foreach ($res1->result() as $row1) {

                            $package_name = $row1->package_name;

                            $casual = $row1->casual;

                            $medical = $row1->medical;

                            $earned = $row1->earned;

                        }



                        $query4 = $this->db->query("SELECT * FROM leave_management WHERE card_no='" . $eInfo->card_no . "'  AND year='" . $syear . "'and hr_status=2");



                        $t_casual = 0;

                        $t_medical = 0;

                        $t_earned = 0;



                        foreach ($query4->result() as $row4) {

                            $duration = $row4->leave_duration;

                            $leave_type = $row4->leave_type;

                            if ($leave_type == '1') {

                                $t_casual = $t_casual + $duration;

                            } else {

                                $t_casual = $t_casual;

                            }

                            if ($leave_type == '2') {

                                $t_medical = $t_medical + $duration;

                            } else {

                                $t_medical = $t_medical;

                            }

                            if ($leave_type == '3') {

                                $t_earned = $t_earned + $duration;

                            } else {

                                $t_earned = $t_earned;

                            }

                        }



                        $cas_exists = $casual - $t_casual;

                        if ($cas_exists < 0) {

                            $cas_exists = 0;

                        }

                        $medical_exists = $medical - $t_medical;

                        if ($medical_exists < 0) {

                            $medical_exists = 0;

                        }

                        $earned_exists = $earned - $t_earned;

                        if ($earned_exists < 0) {

                            $earned_exists = 0;

                        }

                        ?>

                        <tr>

                            <!-- Start sick leave-->

                            <th><?php echo $c1;?></th>
                            <th><?php echo $eInfo->name;?></th>
                            <th><?php echo $medical;?></th>
                            <th><?php echo $t_medical;?></th>
                            <th><?php echo $medical_exists;?></th>
                            <th><?php echo $casual;?></th>
                            <th><?php echo $t_casual;?></th>
                            <th><?php echo $cas_exists;?></th>
                            <th><?php echo $earned;?></th>
                            <th><?php echo $t_earned;?></th>
                            <th><?php echo $earned_exists;?></th>
                        </tr>
                        <?php

                        $c1 = $c1 + 1;
                    endforeach;

                    ?>
                    </tbody>
                </table>
                </div>
                <div class="text-center">
                    <?php echo $this->pagination->create_links(); ?>
                </div>
                <div class="text-center">
                    <?php
                    echo "Total Records: ".$this->pagination_library->total_rows." page ".(($this->pagination->cur_page) ? $this->pagination->cur_page : "1")." of ". ceil(($this->pagination_library->total_rows)/($this->pagination_library->per_page));
                    ?>
                </div>
            </section>
        </div><! --/content-panel -->
    </div><!-- /col-md-12 -->
</div><!-- row -->