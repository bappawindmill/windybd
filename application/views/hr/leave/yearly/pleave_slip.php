<link rel="stylesheet" type="text/css" href="<?php echo base_url(); ?>public/css/t1.css" />
<style type="text/css">
    <!--
    .style1 {color: #FF0000}
    -->
</style>
<script type="text/javascript" src="<?php echo base_url(); ?>/public/js/jquery.jqprint.0.3.js"></script>
<script>
    $(function(){
        $("#print_button").click( function() {
            $("#invoice").jqprint();
        });
    });
</script>
<!--<input type="button" id="print_button" value="Print" style="margin-top:5px; ;">
--><div id="invoice" style="float:left; width:900px">
    <?php
    $query1 = $this->db->query("select name from employeeinfo where card_no='" . $c . "'");
    foreach ($query1->result() as $row3) {
        $employee_name = $row3->name;
    }
    if (empty($employee_name)):$employee_name = "";
    endif;
    echo '<div align="center" style=" margin-right: 50px; margin-left: 50px; border-bottom: 1px solid #000000; padding-bottom: 5px;"><h4>Windmill Group</h4>Leave Report of <b>' . $employee_name . '</b><br />For the Year of  ' . $fyear . '</div><br />';
    echo '<div style=" padding-left: 100px; height: 30px;">';
    echo '<div style=" width:70%; float:left;"><b>Employee Name: </b>' . $employee_name . '</div>';
    echo '<div style=" width:30%; float: right;">Year: ' . $fyear . '</div></div>';

    if (!empty($query10)) {

        $c1 = 1;
        $total1 = 0;
        ?>
        <p style=" font-weight: bold; padding-left: 40px; margin: 20px">Leave Details:</p>
        <table width="90%">
            <tr>
                <th style="text-align:center">SL No</th>
                <th style="text-align:center">Start Date</th>
                <th style="text-align:center">End Date</th>
                <th style="text-align:center">Leave Type</th>
                <th style="text-align:center">Total Duration</th>
                <th style="text-align:center">Extra Days</th>
                <th style="text-align:center">Payment Type</th>
            </tr>

            <?php
            foreach ($query10 as $temp1) {
                echo "<tr >
                <td style='text-align:center'>" . $c1 . "</td>";
                echo "<td style='text-align:center'> " . $temp1->start_date . "</td>";
                echo "<td style='text-align:center'> " . $temp1->end_date . "</td>";

                if ($temp1->leave_type == '1') {

                    $a = "Casual";
                } elseif ($temp1->leave_type == '2') {

                    $a = "Sick";
                } elseif ($temp1->leave_type == '3') {

                    $a = "Earned";
                } else {
                    $a = "";
                }
                echo "<td style='text-align:center'>" . $a . "</td>";
                echo "<td style='text-align:center'>" . $temp1->leave_duration . "</td>";
                echo "<td style='text-align:center'>" . $temp1->extra_days . "</td>";
                ?>
                <td> <?php
        if ($temp1->pay_type == "1") {
            echo "With  Pay";
        } elseif ($temp1->pay_type == "2") {
            echo "Without Pay";
        } else {
            echo "";
        }
                ?></td></tr>
                    <?php
                    $c1 = $c1 + 1;
                }
                echo "</table>";
            }




            $employee_name = "";
            $query1 = $this->db->query("select name from employeeinfo where card_no='" . $c . "'");
            foreach ($query1->result() as $row3) {
                $employee_name = $row3->name;
            }

            $leave_name = "";
            $res = $this->db->query("SELECT leave_name FROM employeeinfo where card_no='" . $c . "'");
            foreach ($res->result() as $row3) {
                $leave_name = $row3->leave_name;
            }
            $package_name = "";
            $casual = "";
            $medical = "";
            $earned = "";

            $res1 = $this->db->query("SELECT * FROM add_leave WHERE id='" . $leave_name . "'");
            foreach ($res1->result() as $row1) {
                $package_name = $row1->package_name;
                $casual = $row1->casual;
                $medical = $row1->medical;
                $earned = $row1->earned;
            }

            $query4 = $this->db->query("SELECT * FROM leave_management WHERE card_no='" . $c . "'  AND year='" . $fyear . "'and hr_status=2");

            $t_casual = 0;
            $t_medical = 0;
            $t_earned = 0;
            $ex_with_pay = 0;
            $ex_without_pay = 0;
            foreach ($query4->result() as $row4) {
                $duration = $row4->leave_duration;
                $extra_days = $row4->extra_days;
                $leave_type = $row4->leave_type;

                if ($leave_type == '1') {
                    $t_casual = $t_casual + $duration;
                } else {
                    $t_casual = $t_casual;
                }
                if ($leave_type == '2') {
                    $t_medical = $t_medical + $duration;
                } else {
                    $t_medical = $t_medical;
                }
                if ($leave_type == '3') {
                    $t_earned = $t_earned + $duration;
                } else {
                    $t_earned = $t_earned;
                }

                if ($row4->pay_type == "1") {
                    $ex_with_pay = $ex_with_pay + $extra_days;
                } elseif ($row4->pay_type == "2") {
                    $ex_without_pay = $ex_without_pay + $extra_days;
                } else {
                    
                }
            }

            $cas_exists = $casual - $t_casual;
            if ($cas_exists < 0) {
                $cas_exists = 0;
            }
            $medical_exists = $medical - $t_medical;
            if ($medical_exists < 0) {
                $medical_exists = 0;
            }
            $earned_exists = $earned - $t_earned;
            if ($earned_exists < 0) {
                $earned_exists = 0;
            }
            ?>
        <p style=" font-weight: bold; padding-left: 40px; margin: 20px">Leave Summary:</p>
        <table width="90%">


            <tr><td colspan="3">
                    <table width="100%">
                        <thead>

                            <tr>
                                <td colspan="2" style="text-align:center"><b>Leave Package</b></td>
                                <td colspan="2" style="text-align:center"><b>Leave Spent</b></td>
                                <td colspan="2" style="text-align:center"><b>Leave Balance</b></td>
                            </tr></thead>
                        <tr>
                            <th width="133">Package Name: </th>
                            <td width="133"><?php echo $package_name; ?></td>
                            <th width="133"> </th>
                            <td width="133"></td>
                            <th width="133"> </th>
                            <td width="133"></td>
                        </tr>

                        <tr>
                            <th>Casual Leave: </th>
                            <td><?php echo $casual; ?></td>
                            <th width="133">Casual: </th>
                            <td width="133"><?php echo $t_casual; ?></td>
                            <th width="133">Casual: </th>
                            <td width="133"><?php echo $cas_exists; ?></td>
                        </tr>
                        <tr>
                            <th>Sick Leave: </th>
                            <td><?php echo $medical; ?></td>
                            <th width="133">Sick: </th>
                            <td width="133"><?php echo $t_medical; ?></td>
                            <th width="133">Sick: </th>
                            <td width="133"><?php echo $medical_exists; ?></td>
                        </tr>
                        <tr>
                            <th>Earned Leave: </th>
                            <td><?php echo $earned; ?></td>
                            <th width="133">Earned: </th>
                            <td width="133"><?php echo $t_earned; ?></td>
                            <th width="133">Earned: </th>
                            <td width="133"><?php echo $earned_exists; ?></td>
                        </tr>

                        <tr style="">
                            <td colspan="2" style="text-align:center" >Total:<?php echo $casual + $medical + $earned; ?></td>
                            <td colspan="2" style="text-align:center" >Total:<?php echo $t_casual + $t_medical + $t_earned; ?></td>
                            <td colspan="2" style="text-align:center" >Total:<?php echo $cas_exists + $medical_exists + $earned_exists; ?></td>
                        </tr>
                    </table></td>
            </tr>
        </table>
        <table width="90%" style="text-align: left; margin-top: 10px">
            <tr>
                <th>Total With Pay : </th>
                <td><?php echo $ex_with_pay; ?> Days</td>

            </tr>
            <tr>
                <th>Total without  Pay : </th>
                <td><?php echo $ex_without_pay; ?> Days</td>

            </tr>

        </table>
</div>
<div style="clear: both;
     "></div>
