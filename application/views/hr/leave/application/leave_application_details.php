<h3><i class="fa fa-angle-right"></i> Leave Application Details:</h3>



<div class="row">

    <div class="col-sm-12">
        <div class="content-panel">
            <!--<section id="flip-scroll">-->
            <section id=""><?php
                foreach ($query10 as $t):endforeach;
                $query = $this->db->query("select * from employeeinfo where id='" . $t->emp_id . "'");
                foreach ($query->result() as $temp):endforeach;
                $package_name = "";
                $casual = "";
                $medical = "";
                $earned = "";
                $res1 = $this->db->query("SELECT * FROM add_leave WHERE id='" . $temp->leave_name . "'");
                foreach ($res1->result() as $row1) {
                $package_name = $row1->package_name;
                $casual = $row1->casual;
                $medical = $row1->medical;
                $earned = $row1->earned;
                }

                $query4 = $this->db->query("SELECT * FROM leave_management WHERE emp_id='" . $temp->id . "'  AND year='" . $t->year . "'and hr_status=2");
                $t_casual = 0;
                $t_medical = 0;
                $t_earned = 0;
                $ex_with_pay = 0;
                $ex_without_pay = 0;

                foreach ($query4->result() as $row4) {
                $duration = $row4->leave_duration;
                $leave_type = $row4->leave_type;
                $extra_days = $row4->extra_days;

                if ($leave_type == '1') {
                $t_casual = $t_casual + $duration;
                } else {
                $t_casual = $t_casual;
                }
                if ($leave_type == '2') {
                $t_medical = $t_medical + $duration;
                } else {
                $t_medical = $t_medical;
                }
                if ($leave_type == '3') {
                $t_earned = $t_earned + $duration;
                } else {
                $t_earned = $t_earned;
                }

                if ($row4->pay_type == "1") {
                $ex_with_pay = $ex_with_pay + $extra_days;
                } elseif ($row4->pay_type == "2") {
                $ex_without_pay = $ex_without_pay + $extra_days;
                } else {

                }
                }

                $cas_exists = $casual - $t_casual;
                $medical_exists = $medical - $t_medical;
                $earned_exists = $earned - $t_earned;
                if ($cas_exists < 0) {
                $cas_exists = 0;
                }

                if ($medical_exists < 0) {
                $medical_exists = 0;
                }

                if ($earned_exists < 0) {
                $earned_exists = 0;
                }
                ?>
                <?php
                $dept_name = "";
                $query8 = $this->db->query("select name from add_department where id='" . $temp->dept . "'");
                foreach ($query8->result() as $row3) {
                    $dept_name = $row3->name;
                }
                $emp_deg = "";
                $ed = $this->db->query("select designation from designation where id='" . $temp->designation . "'");
                foreach ($ed->result() as $d3) {
                    $emp_deg = $d3->designation;
                }
                $npersonName = "";
                $npersonDeg = "";
                $aInfo = $this->db->query("select * from employeeinfo where id='" . $t->nominated_person . "'");
                foreach ($aInfo->result() as $a1Info) {
                    $npersonName = $a1Info->name;
                    $npersonDeg = $a1Info->designation;
                }
                $npersonDegName = "";
                $ndegInfo = $this->db->query("select designation from designation where id='" . $npersonDeg . "'");
                foreach ($ndegInfo->result() as $d9) {
                    $npersonDegName = $d9->designation;
                }
                ?>
                <table class="table table-hover">
                    <tr>
                        <td><strong>EmployeeID No</strong></td>
                        <td><?php echo $temp->card_no; ?></td>
                        <td><strong>Application Submission Date</strong></td>
                        <td><?php echo $t->app_date; ?></td>
                    </tr>
                    <tr>
                        <td><strong>Name</strong></td>
                        <td><?php echo $temp->name; ?></td>

                        <td><strong>Designation</strong></td>
                        <td><?php echo $emp_deg; ?></td>
                    </tr>
                    <tr>
                        <td><strong>Department</strong></td>
                        <td><?php echo $dept_name; ?></td>
                        <td><strong>Leave Type</strong></td>
                        <td>
                            <?php
                            if ($t->leave_type == '1') {

                                $a = "Casual";
                            } elseif ($t->leave_type == '2') {

                                $a = "Sick";
                            } elseif ($t->leave_type == '3') {

                                $a = "Earned";
                            } else {
                                $a = "";
                            }
                            echo $a;
                            ?>
                        </td>
                    </tr>
                    <tr>
                        <td><strong>Leave Period</strong></td>
                        <td><strong>From</strong>: <?php echo $t->start_date; ?></td>
                        <td><strong>To</strong>: <?php echo $t->end_date; ?></td>
                        <td><strong>Total Duration</strong>: <?php echo $t->leave_duration; ?></td>
                    </tr>
                    <tr>
                        <td colspan="2"><strong>Authorized Person Name : <?php echo $npersonName; ?></strong></td>
                        <td colspan="2"><strong>Authorized Person Designation : <?php echo $npersonDegName; ?></strong></td>

                    </tr>
                    <?php if ($t->extra_days > 0): ?>
                        <tr>
                            <td><strong>Extra Days</strong>: <?php echo $t->extra_days; ?></td>
                            <td><strong>Payment Type</strong>: <?php
                                if ($t->pay_type == "1"):echo "With  Pay";
                                else:echo "Without Pay";
                                endif;
                                ?></td>
                        </tr>
                    <?php endif; ?>

                </table>
                <div style="height:20px"></div>
                <?php
                if ($temp->supervisor != 0 || $temp->supervisor != ""):
                    $this->load->model('hr/leave_model');
                    $this->data['sInfo'] = $this->leave_hr_model->get_supervisor_info($temp->supervisor);
                    if (count($this->data['sInfo'])):
                        foreach ($this->data['sInfo'] as $s_info):

                        endforeach;
                        $dname = "";
                        $d7 = $this->db->query("select name from add_department where id='" . $s_info->dept . "'");
                        foreach ($d7->result() as $r7) {
                            $dname = $r7->name;
                        }
                        $sdeg = "";
                        $d12 = $this->db->query("select designation from designation where id='" . $s_info->designation . "'");
                        foreach ($d12->result() as $d33) {
                            $sdeg = $d33->designation;
                        }
                        ?>
                        <table width="90%" border="0">
                            <tr>
                                <td><strong>Supervisor Name</strong></td>
                                <td><?php echo $s_info->name; ?></td>
                                <td><strong>Card No</strong></td>
                                <td><?php echo $s_info->card_no; ?></td>
                            </tr>
                            <tr>
                                <td><strong>Designation</strong></td>
                                <td><?php echo $sdeg; ?></td>
                                <td><strong>Department</strong></td>
                                <td><?php echo $dname; ?></td>
                            </tr>
                            <tr>
                                <td><strong>Approve Status</strong></td>
                                <td><?php
                                    if ($t->supervisor_status == '2'):
                                        echo "Approved";
                                    else:
                                        echo "Not Approved";
                                    endif;
                                    ?></td>

                            </tr>


                        </table>

                        <?php
                    endif;
                endif;
                ?>
                <div class="text-center">
                    <?php echo $this->pagination->create_links(); ?>
                </div>
            </section>
        </div><! --/content-panel -->
    </div><!-- /col-md-12 -->
</div><!-- row -->





<h3><i class="fa fa-angle-right"></i> Leave Summary:</h3>
    <div class="col-sm-12">
        <div class="content-panel">
            <!--<section id="flip-scroll">-->
            <section id="">
                <table  id="myTable"  class="table table-hover">
                    <thead>
                    <tr>
                        <td ><b>Package Name</b></td>
                        <td ><b><?php echo $package_name; ?></b></td>
                        <td colspan="2"><b>Leave Spent</b></td>
                        <td colspan="2"><b>Leave Balance</b></td>
                    </tr>
                    </thead>
                    <tbody>


                    <tr>
                        <th>Casual Leave:</th>
                        <td><?php echo $casual; ?></td>
                        <th>Casual: </th>
                        <td><?php echo $t_casual; ?></td>
                        <th>Casual: </th>
                        <td><?php echo $cas_exists; ?></td>
                    </tr>
                    <tr>
                        <th>Sick Leave: </th>
                        <td><?php echo $medical; ?></td>
                        <th>Sick: </th>
                        <td><?php echo $t_medical; ?></td>
                        <th>Sick: </th>
                        <td><?php echo $medical_exists; ?></td>
                    </tr>
                    <tr>
                        <th>Earned Leave: </th>
                        <td><?php echo $earned; ?></td>
                        <th>Earned: </th>
                        <td><?php echo $t_earned; ?></td>
                        <th>Earned: </th>
                        <td><?php echo $earned_exists; ?></td>
                    </tr>

                    <tr style="">
                        <td></td>
                        <td ><?php echo $casual + $medical + $earned; ?></td>
                        <td></td>
                        <td><?php echo $t_casual + $t_medical + $t_earned; ?></td>
                        <td></td>
                        <td><?php echo $cas_exists + $medical_exists + $earned_exists; ?></td>
                    </tr>
                    </tbody>
                </table>
                <a class="btn btn-primary" href="<?php echo base_url(); ?>/hr/leave/approve/<?php echo $t->id; ?>">Approve</a>
                <a class="btn btn-danger" href="<?php echo base_url(); ?>/hr/leave/deny/<?php echo $t->id; ?>/<?php echo $temp->id; ?>">Deny</a>
                <div class="text-center">
                    <?php echo $this->pagination->create_links(); ?>
                </div>
            </section>
        </div><! --/content-panel -->
    </div><!-- /col-md-12 -->
</div><!-- row -->