<script language="javascript">
    function confirmSubmit() {
        var agree = confirm("Are you sure to delete this record?");
        if (agree)
            return true;
        else
            return false;
    }
</script>

<h3><i class="fa fa-angle-right"></i> Leave Application</h3>
<?php
if($this->session->flashdata('message')==NULL){

}else{ ?>
    <div class="alert-success alert">
        <?php echo $this->session->flashdata('message') ?>
    </div> <?php
}
?>


<div class="row">

    <div class="col-sm-12">
        <div class="content-panel">
            <!--<section id="flip-scroll">-->
            <section id="">
                <table  id="myTable"  class="table table-hover table-bordered">
                    <thead>
                    <tr class="info">
                        <th rowspan="" >SL</th>
                        <th rowspan="">Name </th>
                        <th>Employee ID No</th>
                        <th>Leave Type</th>
                        <th>Start Date</th>
                        <th>End Date</th>
                        <th>Duration</th>
                        <th>Action</th>
                    </tr>
                    </thead>
                    <tbody>
                    <?php
                    $c1 = 1;

                    $total1 = 0;
                    //$query = $this->db->query("select * from leave_management where  hr_status=1 and supervisor_status=2 ");

                    if (count($query)):

                        foreach ($query->result() as $temp1) {

                            $name = "";

                            $card_no = "";

                            $q1 = $this->db->query("select * from employeeinfo where id='" . $temp1->emp_id . "'");

                            foreach ($q1->result() as $q12):

                                $name = $q12->name;

                                $card_no = $q12->card_no;

                            endforeach;

                            echo "<tr >

                        <td style='text-align:center'>" . $c1 . "</td>";
                            echo "<td> " . $name . "</td>";
                            echo "<td>" . $card_no . "</td>";
                            if ($temp1->leave_type == '1') {
                                $a = "Casual";
                            } elseif ($temp1->leave_type == '2') {
                                $a = "Sick";
                            } elseif ($temp1->leave_type == '3') {
                                $a = "Earned";
                            }else {
                                $a = "";
                            }
                            echo "<td>" . $a . "</td>";
                            echo "<td>" . $temp1->start_date . "</td>";
                            echo "<td>" . $temp1->end_date . "</td>";
                            $datetime1 = new DateTime($temp1->start_date);
                            $datetime2 = new DateTime($temp1->end_date);
                            $interval = $datetime1->diff($datetime2)->d;
                            ?>
                            <td> <?php echo $temp1->leave_duration==0? $interval+1:  $temp1->leave_duration; ?> </td>
                            <td>
                                <a class="btn btn-warning btn-xs" href="<?php echo base_url(); ?>hr/leave/details/<?php echo $temp1->id; ?>/<?php echo $temp1->emp_id; ?> " title="Details">Action</a>
                            </td>
                            <?php
                            $c1 = $c1 + 1;
                        }
                    endif;
                    ?>
                    </tbody>
                </table>
                <div class="text-center">
                    <?php echo $this->pagination->create_links(); ?>
                </div>
                <div class="text-center">
                    <?php
                    echo "Total Records: ".$this->pagination_library->total_rows." page ".(($this->pagination->cur_page) ? $this->pagination->cur_page : "1")." of ". ceil(($this->pagination_library->total_rows)/($this->pagination_library->per_page));
                    ?>
                </div>
            </section>
        </div><! --/content-panel -->
    </div><!-- /col-md-12 -->
</div><!-- row -->