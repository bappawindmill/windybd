<script language="javascript">
    function confirmSubmit() {
        var agree = confirm("Are you sure to delete this record?");
        if (agree)
            return true;
        else
            return false;
    }
</script>

<h3><i class="fa fa-angle-right"></i> View All Employee</h3>
<?php
if($this->session->flashdata('message')==NULL){

}else{ ?>
    <div class="alert-success alert">
        <?php echo $this->session->flashdata('message') ?>
    </div> <?php
}
?>


<div class="row">
    <div class="col-sm-12 col-lg-12">
        <div class="pull-left">
            <i class="fa"></i>
            <!--<a class="btn btn-info" href="<?php /*echo base_url(); */?>hr/leave_package/added_leave_package"   title="">Add  New </a>-->
        </div>
        <div class=" pull-right">
            <form class="form-inline">
                <div class="form-group has-feedback pull-right">
                    <input type="text" id="searchKey" name="search" class="form-control" placeholder="Search" />
                    <i class="glyphicon glyphicon-search form-control-feedback"></i>
                </div>
            </form>
        </div>
        <div class="pull-right"><a href="<?php echo base_url();?>hr/employee/get_all_employee" class="btn btn-info" role="button" >Reset</a>&nbsp;</div>
    </div>

    <div class="col-sm-12">
        <div class="content-panel">
            <!--<section id="flip-scroll">-->
            <section id="">
                <table  id="myTable"  class="table table-hover table-responsive table-bordered">
                    <thead>
                    <tr class="info">
                        <th>SL</th>
                        <th>SBU</th>
                        <th>Department</th>
                        <th>Emp. Name</th>
                        <th>Employee ID NO</th>
                        <th>Designation</th>
                        <th>Username</th>
                        <th>Password</th>
                        <th>Supervisor name</th>
                        <th>Supervisor Deg</th>
                        <th>Join Date</th>
                        <th colspan="3">Action</th>

                    </tr>
                    </thead>
                    <tbody>
                    <?php
                    $i = 'even';
                    $c = 1;
                    foreach ($query1 as $row) {
                        if ($i == 'even') {
                            $i = 'odd';
                        } else {
                            $i = 'even';
                        }
                        $branch_name = "";
                        $branchData = $this->db->query("select branch_name from add_branch where id='" . $row['branch_name'] . "'");
                        foreach ($branchData->result() as $row23) {
                            $branch_name = $row23->branch_name;
                        }
                        $dept_name = "";
                        $query = $this->db->query("select name from add_department where id='" . $row['dept'] . "'");
                        foreach ($query->result() as $row3) {
                            $dept_name = $row3->name;
                        }
                        $proffession_name = "";
                        $query5 = $this->db->query("select proffession from add_proffession where id='" . $row['proffession'] . "'");
                        foreach ($query5->result() as $row6) {
                            $proffession_name = $row6->proffession;
                        }
                        $sup_name = "";
                        $sup = $this->db->query("select name from employeeinfo where id='" . $row['supervisor'] . "'");
                        foreach ($sup->result() as $sup1) {
                            $sup_name = $sup1->name;
                        }

                        $sup_deg = "";
                        $sd = $this->db->query("select designation from designation where id='" . $row['sup_designation'] . "'");
                        foreach ($sd->result() as $d1) {
                            $sup_deg = $d1->designation;
                        }
                        $emp_deg = "";
                        $ed = $this->db->query("select designation from designation where id='" . $row['designation'] . "'");
                        foreach ($ed->result() as $d3) {
                            $emp_deg = $d3->designation;
                        }

                        echo "<tr>";
                        echo "<td>" . $c . "</td>";
                        echo "<td>" . $branch_name . "</td>";
                        echo "<td>" . $dept_name . "</td>";
                        if ($row['status'] == '1'):
                            echo "<td style='color:red'>" . $row['name'] . "</td>";
                        else:
                            echo "<td>" . $row['name'] . "</td>";
                        endif;
                        if ($row['status'] == '1'):
                            echo "<td style='color:red'>" . $row['card_no'] . "</td>";
                        else:
                            echo "<td>" . $row['card_no'] . "</td>";
                        endif;


                        echo "<td>" . $emp_deg . "</td>";

                        echo "<td>" . $row['username'] . "</td>";
                        echo "<td>" . $row['password'] . "</td>";
                        echo "<td>" . $sup_name . "</td>";
                        echo "<td>" . $sup_deg . "</td>";
                        echo "<td>" . $row['join_date'] . "</td>";
                        ?>

                        <td>
                            <a class="btn btn-warning btn-xs" href="<?php echo base_url(); ?>/hr/info/employee_general_info_edit/<?php echo $row['id']; ?> " title="Edit the Content">
                                edit</a>&nbsp;&nbsp;&nbsp;
                            <?php if ($this->session->userdata("utype") == '1'){ ?>
                                <a class="btn btn-primary btn-xs" href="<?php echo base_url(); ?>/hr/info/change_status/<?php echo $row['id']; ?> " title="Change Status">
                                    Status</a>

                            <?php } ?>
                        </td>
                        <td>
                            <?php if ($this->session->userdata("utype") == '1'){ ?>
                                <a class="btn btn-danger btn-xs" onclick="return confirmSubmit()" href="<?php echo base_url() ?>/hr/info/employee_general_info_delete/<?php echo $row['id']; ?>" title="Delete the Content">
                                    Delete</a>
                            <?php } ?>
                        </td>

                        <?php
                        $c++;
                    }
                    ?>
                    </tbody>
                </table>
                <div class="text-center">
                    <?php echo $this->pagination->create_links(); ?>
                </div>
                <div class="text-center">
                    <?php
                    echo "Total Records: ".$this->pagination_library->total_rows." page ".(($this->pagination->cur_page) ? $this->pagination->cur_page : "1")." of ". ceil(($this->pagination_library->total_rows)/($this->pagination_library->per_page));
                    ?>
                </div>
            </section>
        </div><! --/content-panel -->
    </div><!-- /col-md-12 -->
</div><!-- row -->