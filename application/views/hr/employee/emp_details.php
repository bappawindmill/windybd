<script language="javascript">
    function confirmSubmit() {
        var agree = confirm("Are you sure to delete this record?");
        if (agree)
            return true;
        else
            return false;
    }
</script>

<h3><i class="fa fa-angle-right"></i> Employee Report View</h3>
<input type="button" id="print_button" value="Print" style="margin-top:5px;">
<?php
if($this->session->flashdata('message')==NULL){

}else{ ?>
    <div class="alert-success alert">
        <?php echo $this->session->flashdata('message') ?>
    </div> <?php
}
?>


<div class="row">

    <div class="col-sm-12">
        <div class="content-panel">
            <!--<section id="flip-scroll">-->
            <section id="invoice">
                <?php
                //Second step
                foreach ($query as $row):
                    $name = $row->name;
                    ?>
                    <table class="table table-bordered">
                        <tr>
                            <td>
                                <img src=" <?php echo base_url().'uploads/'.$row->photo?> "  width="100" height="100" />
                            </td>
                        </tr>
                    </table>


                    <table class="table table-bordered">
                        <tr>
                            <td colspan="2" style="background-color:#DADCE1"><h3 style="color:#268209"> General  Info</h3></td>
                        </tr>

                        <tr>
                            <td width="220" style="text-align:right; background-color:#E9ECE9">Name :</td>
                            <td width="539" style="background-color:#E9ECE9"> <?php echo ucfirst($name); ?></td>
                        </tr>
                        <tr>
                            <td width="220" style="text-align:right; background-color:#E9ECE9">Employee ID No:</td>
                            <td width="539" style="background-color:#E9ECE9"> <?php echo $row->card_no; ?></td>
                        </tr>
                        <tr>
                            <td width="220" style="text-align:right; background-color:#E9ECE9">Secret No:</td>
                            <td width="539" style="background-color:#E9ECE9"> <?php echo $row->secrete_no; ?></td>
                        </tr>

                        <tr>
                            <td width="220" style="text-align:right; background-color:#E9ECE9">Date of Birth :</td>
                            <td width="539" style="background-color:#E9ECE9"> <?php echo $row->dob; ?></td>
                        </tr>
                        <tr>
                            <td width="220" style="text-align:right; background-color:#E9ECE9">Gender :</td>
                            <td width="539" style="background-color:#E9ECE9"> <?php echo $row->gender; ?></td>
                        </tr>
                        <tr>
                            <td width="220" style="text-align:right; background-color:#E9ECE9">Marital Status :</td>
                            <td width="539" style="background-color:#E9ECE9"> <?php echo $row->marital_status; ?></td>
                        </tr>
                        <tr>
                            <td width="220" style="text-align:right; background-color:#E9ECE9">Nationality :</td>
                            <td width="539" style="background-color:#E9ECE9"> <?php echo $row->nationality; ?></td>
                        </tr>
                        <tr>
                            <td width="220" style="text-align:right; background-color:#E9ECE9">Blood Group :</td>
                            <td width="539" style="background-color:#E9ECE9"> <?php echo $row->blood; ?></td>
                        </tr>
                        <tr>
                            <td width="220" style="text-align:right; background-color:#E9ECE9">Mobile No :</td>
                            <td width="539" style="background-color:#E9ECE9"> <?php echo $row->telephone; ?></td>
                        </tr><tr>
                            <td width="210" style="text-align:right; background-color:#E9ECE9">Email :</td>
                            <td width="539" style="background-color:#E9ECE9"> <?php echo $row->email; ?></td>
                        </tr>
                        <tr>
                            <td width="220" style="text-align:right; background-color:#E9ECE9">National ID :</td>
                            <td width="539" style="background-color:#E9ECE9"> <?php echo $row->national_id; ?></td>
                        </tr>
                        <tr>
                            <td width="220" style="text-align:right; background-color:#E9ECE9">Emergency No :</td>
                            <td width="539" style="background-color:#E9ECE9"> <?php echo $row->emergency_no; ?></td>
                        </tr>

                        <tr>
                            <td width="220" style="text-align:right; background-color:#E9ECE9">Join Date :</td>
                            <td width="539" style="background-color:#E9ECE9"> <?php echo $row->join_date; ?></td>
                        </tr>
                        <tr>
                            <td width="220" style="text-align:right; background-color:#E9ECE9">Present Address :</td>
                            <td width="539" style="background-color:#E9ECE9"> <?php echo $row->present_address; ?></td>
                        </tr>
                        <tr>
                            <td width="220" style="text-align:right; background-color:#E9ECE9">Permanent Address :</td>
                            <td width="539" style="background-color:#E9ECE9"> <?php echo $row->permanent_address; ?></td>
                        </tr>
                        <tr>
                            <td width="220" style="text-align:right; background-color:#E9ECE9">Notice Period :</td>
                            <td width="539" style="background-color:#E9ECE9"> <?php echo $row->notice_period; ?></td>
                        </tr>

                        <tr>
                            <td width="220" style="text-align:right; background-color:#E9ECE9">Note :</td>
                            <td width="539" style="background-color:#E9ECE9"> <?php echo $row->note; ?></td>
                        </tr>

                        <tr>
                            <td width="220" style="text-align:right; background-color:#E9ECE9"> Contact Person Name:</td>
                            <td width="539" style="background-color:#E9ECE9"> <?php echo ucfirst($row->contact_person); ?></td>
                        </tr>

                        <tr>
                            <td width="220" style="text-align:right; background-color:#E9ECE9">Relation :</td>
                            <td width="539" style="background-color:#E9ECE9"> <?php echo $row->relation; ?></td>
                        </tr>
                    </table>



                    <?php
                    $dept_name = "";
                    $deptData = $this->db->query("SELECT name FROM add_department WHERE id='" . $row->dept . "'");
                    foreach ($deptData->result() as $deptInfo) {
                        $dept_name = $deptInfo->name;
                    }
                    $companyInfo = $this->db->query("SELECT branch_name FROM add_branch WHERE id='" . $row->branch_name . "'");
                    foreach ($companyInfo->result() as $cInfo) {
                        $branch = $cInfo->branch_name;
                    }
                    $etype = "";
                    $deg = "";
                    $lpackage = "";
                    $res61 = $this->db->query("SELECT emp_type FROM emp_type WHERE id='" . $row->emp_type . "'");
                    foreach ($res61->result() as $row61) {
                        $etype = $row61->emp_type;
                    }
                    $res614 = $this->db->query("SELECT designation FROM designation WHERE id='" . $row->designation . "'");
                    foreach ($res614->result() as $row614) {
                        $deg = $row614->designation;
                    }
                    $res4 = $this->db->query("SELECT package_name FROM add_leave WHERE id='" . $row->leave_name . "'");
                    foreach ($res4->result() as $row4) {
                        $lpackage = $row4->package_name;
                    }
                    ?>
                    <table class="table table-bordered">
                        <tr>
                            <td colspan="2" style="background-color:#DADCE1"><h3 style="color:#268209"> Official Info</h3></td>
                        </tr>
                        <tr>
                            <td width="220" style="text-align:right; background-color:#E9ECE9">SBU Name :</td>
                            <td width="539" style="background-color:#E9ECE9"> <?php echo ucfirst($branch); ?></td>
                        </tr>
                        <tr>
                            <td width="220" style="text-align:right; background-color:#E9ECE9">Department :</td>
                            <td width="539" style="background-color:#E9ECE9"> <?php echo ucfirst($dept_name); ?></td>
                        </tr>

                        <tr>
                            <td width="220" style="text-align:right; background-color:#E9ECE9">Employee Type :</td>
                            <td width="539" style="background-color:#E9ECE9"> <?php echo ucfirst($etype); ?></td>
                        </tr>
                        <tr>
                            <td width="220" style="text-align:right; background-color:#E9ECE9">Designation :</td>
                            <td width="539" style="background-color:#E9ECE9"> <?php echo ucfirst($deg); ?></td>
                        </tr>
                        <tr>
                            <td width="220" style="text-align:right; background-color:#E9ECE9">Leave Package :</td>
                            <td width="539" style="background-color:#E9ECE9"> <?php echo ucfirst($lpackage); ?></td>
                        </tr>
                        <?php
                        $basic = $row->basic;
                        $house_rent = $row->house_rent;
                        $medical = $row->medical;
                        $conveyance = $row->conveyance;
                        $special = $row->special;
                        $special = $row->special;
                        $salary_step = $row->salary_step;
                        $salary_band = $row->salary_band;
                        $total = $basic + $house_rent + $medical + $conveyance + $special;
                        ?>

                        <tr>
                            <td width="220" style="text-align:right; background-color:#E9ECE9">Basic :</td>
                            <td width="539" style="background-color:#E9ECE9"> <?php echo $basic; ?></td>
                        </tr>
                        <tr>
                            <td width="220" style="text-align:right; background-color:#E9ECE9">House Rent :</td>
                            <td width="539" style="background-color:#E9ECE9"> <?php echo $house_rent; ?></td>
                        </tr>

                        <tr>
                            <td width="220" style="text-align:right; background-color:#E9ECE9">Medical :</td>
                            <td width="539" style="background-color:#E9ECE9"> <?php echo $medical; ?></td>
                        </tr>

                        <tr>
                            <td width="220" style="text-align:right; background-color:#E9ECE9">Conveyance :</td>
                            <td width="539" style="background-color:#E9ECE9"> <?php echo $conveyance; ?></td>
                        </tr>

                        <tr>
                            <td width="220" style="text-align:right; background-color:#E9ECE9">Special :</td>
                            <td width="539" style="background-color:#E9ECE9"> <?php echo $special; ?></td>
                        </tr>
                        <tr>
                            <td width="220" style="text-align:right; background-color:#E9ECE9">Salary Step :</td>
                            <td width="539" style="background-color:#E9ECE9"> <?php echo $salary_step; ?></td>
                        </tr>
                        <tr>
                            <td width="220" style="text-align:right; background-color:#E9ECE9">Salary Band :</td>
                            <td width="539" style="background-color:#E9ECE9"> <?php echo $salary_band; ?></td>
                        </tr>
                        <tr>
                            <td width="220" style="text-align:right; background-color:#E9ECE9">Gross Salary :</td>
                            <td width="539" style="background-color:#E9ECE9"> <?php echo $total; ?></td>
                        </tr>

                        <tr>
                            <td width="220" style="text-align:right; background-color:#E9ECE9">Probation Period :</td>
                            <td width="539" style="background-color:#E9ECE9"> <?php echo $row->probation_period; ?></td>
                        </tr>


                    </table>

                <?php endforeach; ?>
                <table class="table table-bordered">
                    <tr>
                        <td id="academitinfoPosition" colspan="6" style="background-color:#DADCE1; "><h3 style="color:#268209"> Academic Info:</h3></td>
                    </tr>
                    <?php $res2 = $this->db->query("SELECT * FROM academic WHERE emp_id='" . $row->id . "' order by passing_year desc"); ?>


                    <tr>
                        <td style="text-align:center; color:#268209; background-color:#DADCE1;">Title of Exam</td>
                        <td style="text-align:center; color:#268209; background-color:#DADCE1;">Institute Name</td>
                        <td style="text-align:center; color:#268209; background-color:#DADCE1;">Result</td>
                        <td style="text-align:center; color:#268209; background-color:#DADCE1;">Passing Year</td>
                        <td style="text-align:center; color:#268209; background-color:#DADCE1;"></td>
                    </tr>
                    <?php foreach ($res2->result() as $row2): ?>
                        <tr>
                            <td style="background-color:#E9ECE9;text-align:center;"> <?php echo $row2->exam_name; ?></td>
                            <td style="background-color:#E9ECE9;text-align:center;"> <?php echo ucfirst($row2->institute_name); ?></td>
                            <td style="background-color:#E9ECE9;text-align:center;"> <?php echo $row2->result; ?></td>
                            <td style="background-color:#E9ECE9;text-align:center;"> <?php echo $row2->passing_year; ?></td>
                            <td style="background-color:#E9ECE9;text-align:center;">
                                <a class="btn btn-primary btn-xs" rel="facebox" href="<?php echo base_url(); ?>hr/temp/edit/<?php echo $row2->id; ?>/<?php echo $cno; ?>">
                                    edit</a>
                                <a class="btn btn-danger btn-xs" onclick="return confirmSubmit()" href="<?php echo base_url() ?>/hr/temp/acDelete/<?php echo $row2->id; ?>/<?php echo $cno; ?>" title="Delete the Content">delete</a>
                            </td>
                        </tr>
                    <?php endforeach; ?>
                </table>


                <table class="table table-bordered">
                    <tr>
                        <td colspan="6" style="background-color:#DADCE1; "><h3 style="color:#268209">Career History:</h3></td>
                    </tr>
                    <?php $query7 = $this->db->query("select * from career_history where emp_id='" . $row->id . "'order by start desc"); ?>
                    <tr>
                        <td style="text-align:center; color:#268209; background-color:#DADCE1;">Company Name</td>
                        <td style="text-align:center; color:#268209; background-color:#DADCE1;">Designation</td>
                        <td style="text-align:center; color:#268209; background-color:#DADCE1;">Start</td>
                        <td style="text-align:center; color:#268209; background-color:#DADCE1;">End</td>
                        <td style="text-align:center; color:#268209; background-color:#DADCE1;"></td>
                    </tr>
                    <?php foreach ($query7->result() as $temp1): ?>
                        <tr>
                            <td style="background-color:#E9ECE9;text-align:center;"> <?php echo ucfirst($temp1->company_name); ?></td>
                            <td style="background-color:#E9ECE9;text-align:center;"> <?php echo ucfirst($temp1->designation); ?></td>
                            <td style="background-color:#E9ECE9;text-align:center;"> <?php echo $temp1->start; ?></td>
                            <td style="background-color:#E9ECE9;text-align:center;"> <?php echo $temp1->end_date; ?></td>
                            <td style="background-color:#E9ECE9;text-align:center;">
                                <a class="btn btn-danger btn-xs" rel="facebox" href="<?php echo base_url(); ?>/hr/temp/hedit/<?php echo $temp1->id; ?>/<?php echo $cno; ?>">
                                    edit</a>
                                <a class="btn btn-danger btn-xs" onclick="return confirmSubmit()" href="<?php echo base_url() ?>/hr/temp/hsDelete/<?php echo $temp1->id; ?>/<?php echo $cno; ?>" title="Delete the Content">delete</a>
                            </td>
                        </tr>
                    <?php endforeach; ?>
                </table>


                <table class="table table-bordered">
                    <tr>
                        <td colspan="6" style="background-color:#DADCE1; "><h3 style="color:#268209">Extra qualification:</h3></td>
                    </tr>
                    <?php $res3 = $this->db->query("SELECT * FROM qualification WHERE emp_id='" . $row->id . "'order by year desc"); ?>

                    <tr>
                        <td style="text-align:center; color:#268209; background-color:#DADCE1;">Qualification Name</td>
                        <td style="text-align:center; color:#268209; background-color:#DADCE1;">Institute Name</td>
                        <td style="text-align:center; color:#268209; background-color:#DADCE1;">Result</td>
                        <td style="text-align:center; color:#268209; background-color:#DADCE1;">Passing Year</td>
                        <td style="text-align:center; color:#268209; background-color:#DADCE1;"></td>

                    </tr>
                    <?php foreach ($res3->result() as $row5): ?>

                        <tr>
                            <td style="background-color:#E9ECE9;text-align:center;"> <?php echo $row5->qualification_name; ?></td>
                            <td style="background-color:#E9ECE9;text-align:center;"> <?php echo ucfirst($row5->institute_name); ?></td>
                            <td style="background-color:#E9ECE9;text-align:center;"> <?php echo $row5->result; ?></td>
                            <td style="background-color:#E9ECE9;text-align:center;"> <?php echo $row5->year; ?></td>
                            <td style="background-color:#E9ECE9;text-align:center;">
                                <a class="btn btn-primary btn-xs" rel="facebox" href="<?php echo base_url(); ?>/hr/temp/qedit/<?php echo $row5->id; ?>/<?php echo $cno; ?>">
                                    edit</a>
                                <a class="btn btn-danger btn-xs" onclick="return confirmSubmit()" href="<?php echo base_url() ?>/hr/temp/quDelete/<?php echo $row5->id; ?>/<?php echo $cno; ?>" title="Delete the Content">delete</a>
                            </td>
                        </tr>
                    <?php endforeach; ?>
                </table>
                <table class="table table-bordered">
                    <tr>
                        <td colspan="6" style="background-color:#DADCE1; "><h3 style="color:#268209">Appraisal:</h3></td>
                    </tr>
                    <?php $res3 = $this->db->query("SELECT * FROM appraisal WHERE emp_id='" . $row->id . "'order by effective_date desc"); ?>

                    <tr>
                        <td style="text-align:center; color:#268209; background-color:#DADCE1;">Status</td>
                        <td style="text-align:center; color:#268209; background-color:#DADCE1;">Effective Date</td>
                        <td style="text-align:center; color:#268209; background-color:#DADCE1;">Designation</td>
                        <td style="text-align:center; color:#268209; background-color:#DADCE1;">Pay Scale</td>
                        <td style="text-align:center; color:#268209; background-color:#DADCE1;">Note</td>
                        <td style="text-align:center; color:#268209; background-color:#DADCE1;"></td>

                    </tr>
                    <?php foreach ($res3->result() as $row51): ?>

                        <tr>
                            <td style="background-color:#E9ECE9;text-align:center;"> <?php echo $row51->status; ?></td>
                            <td style="background-color:#E9ECE9;text-align:center;"> <?php echo $row51->effective_date; ?></td>
                            <td style="background-color:#E9ECE9;text-align:center;"> <?php echo $row51->designation; ?></td>
                            <td style="background-color:#E9ECE9;text-align:center;"> <?php echo $row51->pay_scale; ?></td>
                            <td style="background-color:#E9ECE9;text-align:center;"> <?php echo $row51->note; ?></td>
                            <td style="background-color:#E9ECE9;text-align:center;">
                                <a class="btn btn-primary btn-xs" rel="facebox" href="<?php echo base_url(); ?>/hr/temp/appraisal_edit/<?php echo $row51->id; ?>/<?php echo $cno; ?>">
                                    edit</a>
                                <a class="btn btn-danger btn-xs" onclick="return confirmSubmit()" href="<?php echo base_url() ?>/hr/temp/appraisal_delete/<?php echo $row51->id; ?>/<?php echo $cno; ?>" title="Delete the Content">delete</a>
                            </td>
                        </tr>
                    <?php endforeach; ?>
                </table>



                <table class="table table-bordered">
                    <tr>
                        <td colspan="6" style="background-color:#DADCE1; "><h3 style="color:#268209">Document:</h3></td>
                    </tr>
                    <?php $doc = $this->db->query("SELECT * FROM doc WHERE emp_id='" . $row->id . "'"); ?>

                    <tr>
                        <td style="text-align:left; color:#268209; background-color:#DADCE1;">Title</td>
                        <td style="text-align:left; color:#268209; background-color:#DADCE1;">Name of Document</td>
                    </tr>
                    <?php foreach ($doc->result() as $temp): ?>

                        <tr>
                            <td style="background-color:#E9ECE9;"> <?php echo ucfirst($temp->doc_title); ?></td>
                            <td style="background-color:#E9ECE9;"><a target="_blank" href="<?php echo base_url(); ?>doc/<?php echo $temp->doc_name; ?>"><?php echo $temp->doc_title; ?></a></td>
                        </tr>
                    <?php endforeach; ?>
                </table>

                <div class="text-center">
                    <?php echo $this->pagination->create_links(); ?>
                </div>
                <!--<div class="text-center">
                    <?php
/*                    echo "Total Records: ".$this->pagination_library->total_rows." page ".(($this->pagination->cur_page) ? $this->pagination->cur_page : "1")." of ". ceil(($this->pagination_library->total_rows)/($this->pagination_library->per_page));
                    */?>
                </div>-->
            </section>
        </div><! --/content-panel -->
    </div><!-- /col-md-12 -->
</div><!-- row -->