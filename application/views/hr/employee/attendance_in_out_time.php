<script language="javascript">
    function confirmSubmit() {
        var agree = confirm("Are you sure to delete this record?");
        if (agree)
            return true;
        else
            return false;
    }
</script>

<h3><i class="fa fa-angle-right"></i> Attendance In Out Time</h3>
<?php
if($this->session->flashdata('message')==NULL){

}else{ ?>
    <div class="alert-success alert">
        <?php echo $this->session->flashdata('message') ?>
    </div> <?php
}
?>



<div class="row">
    <div class="col-sm-12 col-lg-12">
        <div class="pull-left">
            <i class="fa"></i>
            <!--<a class="btn btn-info" href="<?php /*echo base_url(); */?>hr/leave_package/added_leave_package"   title="">Add  New </a>-->
        </div>
        <div class=" ">

            <form class="form-inline">
                <div class="form-group">
                    <input type="text" id="searchKey" name="search" value="<?php echo $this->input->get_post('search') ;?>" class="form-control" placeholder="Name/Emp ID/Card No" />
                </div>
                <div class="form-group">
                    <input type="text" class="form-control " value="<?php echo $this->input->get_post('start_date_attendance_log') ;?>" name="start_date_attendance_log" id="start_date_attendance_log" placeholder="Date from">
                </div>
                <div class="form-group">
                    <input type="text" class="form-control" value="<?php echo $this->input->get_post('end_date_attendance_log') ;?>" name="end_date_attendance_log" id="end_date_attendance_log" placeholder="Date to">
                </div>
                <?php
                $query66 = $this->db->query("SELECT id,branch_name FROM add_branch");
                $dataStr12 = "<div class='form-group'>
                        <select name='branch_name' id='crlbranch' class='form-control'>";
                $dataStr12.="<option value=''>Select Branch</option>";
                foreach ($query66->result() as $row33) {
                    $dataStr12.="<option value='" . $row33->id . "'";
                    if($this->input->get_post('branch_name')==$row33->id){
                        $dataStr12.=" Selected";
                    }
                    $dataStr12.=">" . $row33->branch_name . "</option>";
                }
                $dataStr12.='</select>'
                ?>

                <?php

                echo $dataStr12
                ?>
        </div>
        <div class='form-group' id='res'>

        </div>

        <!-- {DEPARTMENT}-->

        <button type="submit" class="btn btn-info">Submit</button>
        <a href="<?php echo base_url();?>hr/employee/get_in_out_time" class="btn btn-danger" role="button" >Reset</a>
        </form>


        <form class="form-inline">
            <div class="form-group form-inline has-feedback ">

            </div>
            <div class="form-inline">&nbsp;</div>
        </form>
    </div>
</div>

<div class="col-sm-12">
    <div class="content-panel">
        <!--<section id="flip-scroll">-->
        <section id="">
            <table  id="myTable"  class="table table-hover table-responsive table-bordered">
                <thead>
                <tr class="info">
                    <th>SL</th>
                    <!--<th>SBU</th>
                    <th>Department</th>-->
                    <th>Emp. Name</th>
                    <th>Employee ID NO</th>
                    <th>Card No</th>
                    <th>Attendance Date</th>
                    <th>Enter</th>
                    <th>Exit</th>
                </tr>
                </thead>
                <tbody>
                <?php
                $i = 'even';
                $c = 1;
                foreach ($query1 as $row) {
                    if ($i == 'even') {
                        $i = 'odd';
                    } else {
                        $i = 'even';
                    }

                    echo "<tr>";
                    echo "<td>" . $c . "</td>";
                    /*echo "<td>" . $branch_name . "</td>";
                    echo "<td>" . $dept_name . "</td>";*/
                    if ($row['status'] == '1'):
                        echo "<td style='color:red'>" . $row['name'] . "</td>";
                    else:
                        echo "<td>" . $row['name'] . "</td>";
                    endif;
                    if ($row['status'] == '1'):
                        echo "<td style='color:red'>" . $row['card_no'] . "</td>";
                    else:
                        echo "<td>" . $row['card_no'] . "</td>";
                    endif;
                    ?>
                    <?php
                    echo "<td>" . $row['secrete_no'] . "</td>";
                    echo "<td>" . $row['att_date'] . "</td>";
                    echo "<td>" . $row['timein'] . "</td>";
                    echo "<td>" . $row['timeout'] . "</td>";
                    $c++;
                }
                ?>
                </tbody>
            </table>
            <div class="text-center">
                <?php echo $this->pagination->create_links(); ?>
            </div>
            <div class="text-center">
                <?php
                echo "Total Records: ".$this->pagination_library->total_rows." page ".(($this->pagination->cur_page) ? $this->pagination->cur_page : "1")." of ". ceil(($this->pagination_library->total_rows)/($this->pagination_library->per_page));
                ?>
            </div>
        </section>
    </div><! --/content-panel -->
</div><!-- /col-md-12 -->
</div><!-- row -->

