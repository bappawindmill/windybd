<?php $this->load->view('hr/layout');?>			   
	<script>
	$(function() {
		$( "#datepicker" ).datepicker();
	});
	</script>



                   
                            <div class="art-Post-body">
                        <div class="art-Post-inner">
                                        <div class="art-PostMetadataHeader">
                                            <h2 class="art-PostHeader">
                                                <img src="<?php echo base_url();?>images/PostHeaderIcon.png" width="29" height="29" alt="" />
                                                Add Employee Information
                                            </h2>
                                        </div>
                                        <div class="art-PostContent">
                                              <br />
				<script>
                    function check()
                    {
                       var myvar = document.getElementById("upload").checked;
                       if(myvar)
                       {
                            $("#basic").show();
							$("#house_allowance").show();
							$("#food_allowance").show();
							$("#conveyance_allowance").show();
                       }
                       else
                       {
                            $("#basic").hide();
							$("#house_allowance").hide();
							$("#food_allowance").hide();
							$("#conveyance_allowance").hide();
                       }
                    }
                </script>
		<?php
		$row = $query1;
		$gender1 = array('Male' => 'Male',
					   'Female' => 'Female'
					   );
					   
		$msatatus = array('Yes' => 'Yes',
					   'No' => 'No'
					   );
		$bg = array('A+' => 'A+',
				   'A' => 'A',
				   'A-' => 'A-',
				   'B+' => 'B+',
				   'B-' => 'B-',
				   'O+' => 'O+',
				   'O-' => 'O-',
				   'AB+' => 'AB+',
				   );
	/*if ($query1->num_rows() > 0)
	{*/
/*$year = array('1960' => '1960','1961' => '1961','1962' => '1962','1963' => '1963','1964' => '1964','1965' => '1965','1966' => '1966','1967' => '1967','1968' => '1968','1969' => '1969','1970' => '1970','1971' => '1971','1972' => '1972','1973' => '1973','1974' => '1974','1975' => '1975','1976' => '1976','1977' => '1977','1978' => '1978','1979' => '1979','1980' => '1980','1981' => '1981','1982' => '1982','1983' => '1983','1984' => '1984','1985' => '1985','1986' => '1986','1987' => '1987','1988' => '1988','1989' => '1989','1990' => '1990','1991' => '1991','1992' => '1992','1993' => '1993','1994' => '1994','1995' => '1995','1996' => '1996','1997' => '1997','1998' => '1998','1999' => '1999','2000' => '2000','2001' => '2001','2002' => '2002','2003' => '2003','2004' => '2004','2005' => '2005','2006' => '2006','2007' => '2007','2008' => '2009','2010' => '2010','2011' => '2011');*/

   //echo $row['title'];
   //echo $row['name'];
   //echo $row['body'];
				   
		echo form_open_multipart('hr/employee/employee_info_edit/'.$row['id'].'/'.$row['proffession'].'/'.$row['dept'].'/'.$row['salary_package'].'/'.$row['leave_name']);
		echo '<div style="width: 100%; height:450px;"><div style="width: 50%; float: left;">';
		echo '<table width="400">';
		echo '<thead><tr><th colspan="2"><b>General Information</b></th></tr></thead>';
		echo '<tr><th>Name :</th><td>'.form_input('name',$row['name'],'name').'<span style="color:red;">*</span></td></tr>';
//		echo '<tr><th>Date Of Birth :</th><td>'.form_dropdown('year',$row['name'],'name').'</td></tr>';

		$query3 = $this->db->get_where('experience', array('emp_id'=>$row['id']));
  		$row3 = $query3->row_array();
		echo '<tr><th>gender :</th><td>'.form_dropdown('gender',$gender1,$row['gender']).'</td></tr>';
		echo '<tr><th>marital_status :</th><td>'.form_dropdown('marital_status',$msatatus,$row['marital_status']).'</td></tr>';
		echo '<tr><th>nationality :</th><td>'.form_input('nationality',$row['nationality'],'nationality').'<span style="color:red;">*</span></td></tr>';
		echo '<tr><th>Blood Group :</th><td>'.form_dropdown('blood',$bg,$row['blood']).'<span style="color:red;">*</span></td></tr>';
		echo '<tr><th>telephone :</th><td>'.form_input('telephone',$row['telephone'],'telephone').'</td></tr>';

		echo '</table><br />';
		echo '<table width="400">';
		echo '<thead><tr><th colspan="2"><b>Experience</b></th></tr></thead>';
		echo '<tr><th>Company :</th><td>'.form_input('company',$row3['company'],'company').'</td></tr>';
		echo '<tr><th>proffession :</th><td>'.form_input('proffession1',$row3['proffession'],'proffession1').'</td></tr>';
		echo form_hidden('ex_id',$row3['id'],'ex_id');
		?>
		<tr><th>From :</th><td><input type="text" name="from" value="<?php echo $row3['exp_from']; ?>" onClick="displayDatePicker('from');" class="text" /><a href="javascript:void(0);" onClick="displayDatePicker('from');"><img src="<?php echo base_url(); ?>images/calendar.png" alt="calendar" border="0"></a></td></tr>
		<tr><th>To :</th><td><input type="text" name="to" value="<?php echo $row3['to']; ?>" onClick="displayDatePicker('to');" class="text" /><a href="javascript:void(0);" onClick="displayDatePicker('to');"><img src="<?php echo base_url(); ?>images/calendar.png" alt="calendar" border="0"></a></td></tr>
		<?php
		
		echo '<tr><th></th><td>'.form_submit('mysubmit','Edit Employee Info').'</td></tr>';
		
		echo '</table>';
		echo '</div>';
		
		
		echo '<div style="width: 50%; float: right;">';
		echo '<table width="400">';
		echo '<thead><tr><th colspan="2"><b>Employee Profile</b></th></tr></thead>';
		?>
		{DEPARTMENT}
		<?php
  		$query2 = $this->db->get_where('salary_package', array('id'=>$row['salary_package']));
  		$row1 = $query2->row_array();
		?>
		<tr><th>Proffession :</th><td><div id='result'></div></td></tr>
		{SPACKAGE}
		<!--<tr><th>Custom Salary Package: </th><td> <div style="float:left; width:230px;">
                        <input type="checkbox" name="upload" value=1 id="upload" onClick="return check();"></td></tr>
 <tr><th>Basic: </th><td>  <input type="text" name="basic" id="basic" value="<? echo $row1['basic']; ?>" style="display: none;"></td></tr>
 <tr><th>house_allowance: </th><td>  <input type="text" name="house_allowance" id="house_allowance" value="<? echo $row1['house_allowance']; ?>" style="display: none;"></td></tr>
 <tr><th>food_allowance: </th><td>  <input type="text" name="food_allowance" id="food_allowance" value="<? echo $row1['food_allowance']; ?>" style="display: none;"></td></tr>
 <tr><th>conveyance_allowance: </th><td>  <input type="text" name="conveyance_allowance" id="conveyance_allowance" value="<? echo $row1['conveyance_allowance']; ?>" style="display: none;"></td></tr>
                    </div><div style="clear: both;"></div>-->
		{LPACKAGE}
		<tr><th>join date :</th><td><input type="text" name="join_date" value="<?php echo $row['join_date']; ?>" onClick="displayDatePicker('join_date');" class="text" /><a href="javascript:void(0);" onClick="displayDatePicker('join_date');"><img src="<?php echo base_url(); ?>images/calendar.png" alt="calendar" border="0"></a><span style="color:red;">*</span></td></tr>
		<?php
		echo '<tr><th>contract_duration :</th><td>'.form_input('contract_duration',$row['contract_duration'],'contract_duration').'</td></tr>';
		echo '<tr><th>contact person :</th><td>'.form_input('contact_person',$row['contract_duration'],'contact_person').'</td></tr>';
		echo '<tr><th>relation :</th><td>'.form_input('relation',$row['relation'],'relation').'</td></tr>';
		echo '<tr><th>emergency_no :</th><td>'.form_input('emergency_no',$row['emergency_no'],'emergency_no').'<span style="color:red;">*</span></td></tr>';
		echo '<tr><th>email :</th><td>'.form_input('email',$row['email'],'email').'<span style="color:red;">*</span></td></tr>';
		echo '<tr><th>education :</th><td>'.form_input('education',$row['education'],'education').'<span style="color:red;">*</span></td></tr>';
		///echo '<tr><th>qualification :</th><td>'.form_input('qualification','','qualification').'<span style="color:red;">*</span></td></tr>';
		echo '<tr><th>note :</th><td>'.form_input('note',$row['note'],'note').'</td></tr>';
		echo '</table></div></div>';
		
		echo validation_errors('<p class="error">');

echo form_close(); 
//}
?>


<br />                                              
                                            	
                                                
                                        </div>
                                        <div class="cleared"></div>
                        </div>
                        
                        		<div class="cleared"></div>
                            </div>
                        </div>
                    </div>
                <div class="cleared"></div>
				<?php $this->load->view('hr/footer');?>
