<h3><i class="fa fa-angle-right"></i> Holiday Edit</h3>

<!-- BASIC FORM ELELEMNTS -->
<div class="row">
    <div class="col-sm-12">
        <div class="content-panel">
            <?php
            echo form_open('hr/holiday_package/update');?>
                <?php
                if($this->session->flashdata('message')==NULL){

                }else{ ?>
                    <div class="alert-success alert">
                        <?php echo $this->session->flashdata('message') ?>
                    </div> <?php
                }
                ?>
                <?php foreach ($query as $temp):endforeach; ?>
                <div class="col-md-12">
                    <div class="form-group col-sm-6">
                        <label for="name">Title Of Holiday :</label>
                        <input value="<?php echo $temp->title;?>"  type="text" name="title" id="title" class="form-control" required>
                        <div class="alert-danger"><?php echo ucwords(form_error('title')); ?></div>
                    </div>

                    <div class="form-group col-sm-6">
                        <label for="name">Holiday Date:</label>
                        <input value="<?php echo $temp->start_date;?>"  type="text" name="start_date" id="weekend_date" class="form-control" required>
                        <div class="alert-danger"><?php echo ucwords(form_error('start_date')); ?></div>
                    </div>
                </div>
                <span>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
                    <input type="hidden" name="id" value="<?php echo $temp->id; ?>" />&nbsp;&nbsp;&nbsp;&nbsp;
                    <input type="submit" name="mysubmit" value="Update"  class="btn btn-primary">
                    <a class="btn btn-theme" href="<?php echo base_url(); ?>hr/holiday_package/holiday_list">Cancel</a>
                </span>
            </form>
        </div>
    </div><!-- col-lg-12-->
</div><!-- /row -->