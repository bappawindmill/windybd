<link href='<?php echo base_url(); ?>assets/fullcalendar/fullcalendar.min.css' rel='stylesheet' />
<link href='<?php echo base_url(); ?>assets/fullcalendar/fullcalendar.print.min.css' rel='stylesheet' media='print' />


<script src='<?php echo base_url(); ?>assets/fullcalendar/lib/moment.min.js'></script>
<script src='<?php echo base_url(); ?>assets/fullcalendar/fullcalendar.min.js'></script>

<style>

    #calendar {
        max-width: 900px;
        margin: 0 auto;
        color:black;
    }
    .event_delete
    {
       /* border: 1px solid #ff184b;
        background: #ff1016;*/


    }
</style>
<h3><i class="fa fa-angle-right"></i> View Holiday Calendar</h3>
<div class="row">
    <div class="col-sm-12">
        <div class="content-panel">
            <!--<section id="flip-scroll">-->
            <section id="">
                <div id='calendar'></div>
            </section>
        </div><! --/content-panel -->
    </div><!-- /col-md-12 -->
</div>
<?php
foreach($query1 as $row){
    ucfirst($row->title) ;
    $row->start_date ;
}
?>

<script>
    $(document).ready(function() {
        $('#calendar').fullCalendar({
            header: {
                left: 'prev,next today',
                center: 'title',
                right: 'month,agendaWeek,agendaDay'
            },
            defaultDate: '<?php echo date("Y-m-d") ?>',
            navLinks: true, // can click day/week names to navigate views
            selectable: true,
            selectHelper: true,
            select: function(start, end) {
                var title = prompt('Event Title:');
                start=moment(start).format('YYYY-MM-DD');
                var eventData;
                if (title) {
                    eventData = {
                        title: title,
                        start: start,
                        end: end,
                    };
                    $('#calendar').fullCalendar('renderEvent', eventData, true); // stick? = true
                    $.ajax({
                        type: 'POST',
                        url: "<?php echo base_url(); ?>" + "index.php/hr/holiday_package/change_holiday",
                        data: {
                            startDate: start,
                            endDate: 2,
                            eventTitle: title
                        },
                        dateType: 'json',
                        success: function (resp) {
                            //calendar.fullCalendar('refetchEvents');
                        }
                    });
                }
                $('#calendar').fullCalendar('unselect');
            },
            eventClick: function(calEvent, jsEvent, view) {
                var title = prompt('Event Title:', calEvent.title, { buttons: { Ok: true, Cancel: false} });

                if (title){
                    $.ajax({
                        type: 'POST',
                        url: "<?php echo base_url(); ?>" + "index.php/hr/holiday_package/change_holiday",
                        data: {
                            eventId: calEvent.id,
                            eventTitle: title
                        },
                        dateType: 'json',
                        success: function (resp) {
                            location.reload();
                        }
                    });
                }

            },

            eventRender: function(event, element) {

                element.append( "<span class='closeon event_delete'><a href='#' onclick='return confirm_submit("+ event.id +");'><img height='15px' src='<?php echo base_url(); ?>assets/img/ic_delete_white_24dp_1x.png' /></a></span>");
                element.find(".closeon").click(function() {
                    $('#calendar').fullCalendar('removeEvents',event._id);
                });
            },

            editable: true,
            eventLimit: true, // allow "more" link when too many events
            events: [

                <?php foreach($query1 as $key => $row):?>
                {
                    id: "<?php echo $row->id; ?>",
                    title: "<?php echo ucfirst($row->title); ?>",
                    start: "<?php echo ucfirst($row->start_date); ?>",
                    editable:true

                }<?php if(key_exists(($key+1),$query1)):?>,<?php endif;?>
                <?php endforeach;?>
            ]
        });

    });

    function confirm_submit(id)
    {
        if(confirm('Do you really want to delete?!!'))
        {
            $.ajax({
                type: 'POST',
                url: "<?php echo base_url(); ?>" + "index.php/hr/holiday_package/deleteAjex",
                data: {
                    id: id
                },
                dateType: 'json',
                success: function (resp) {
                    location.reload();
                }
            });

        }
        else
        {
            location.reload();
        }


    }

</script>