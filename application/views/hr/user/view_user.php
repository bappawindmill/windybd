    <script language="javascript">
        function confirmSubmit(){
            var agree = confirm("Are you sure to delete this record?");
            if (agree)
                return true;
            else
                return false;
        }
    </script>

        <h3><i class="fa fa-angle-right"></i> View User</h3>
        <div class="alert-danger">
            <?php echo $this->session->flashdata('message') ?>
        </div>

        <div class="row">

            <div class="col-sm-12 col-lg-12">
                <div class="pull-left">
                    <i class="fa"></i>
                    <a class="btn btn-info" href="<?php echo base_url(); ?>hr/user/added_user "   title="">Add  New </a>
                </div>
                <div class="pull-right">
                    <form class="form-inline">
                        <div class="form-group has-feedback pull-right">
                            <input type="text" id="searchKey" name="search" class="form-control" placeholder="Search" />
                            <i class="glyphicon glyphicon-search form-control-feedback"></i>
                        </div>
                    </form>
                </div>
                <div class="pull-right"><a href="<?php echo base_url();?>hr/user/list_user" class="btn btn-info" role="button" >Reset</a>&nbsp;</div>
            </div>
            

            <div class="col-sm-12">
                <div class="content-panel">
                    <section id="flip-scroll">
                        <table  id="myTable"  class="table table-bordered table-hover">
                            <thead>
                            <tr class="info">
                                <th>SL</th>
                                <th>Name</th>
                                <th>User Name</th>
                                <th>Email</th>
                                <th>User Type</th>
                                <th>Configuration</th>
                                <th>Leave</th>
                                <th>Salary</th>
                                <th>Hr</th>
                                <th>Finance</th>
                                <th>Reports</th>
                                <th>Attendance</th>
                                <th colspan="2" class="">Action</th>
                            </tr>
                            </thead>
                            <tbody>
                            <tr>
                                <?php
                                $i = 'even';
                                $c = 1;
                                foreach ($query as $row) {
                                    if ($i == 'even') {
                                        $i = 'odd';
                                    } else {
                                        $i = 'even';
                                    }
                                    echo "<tr class = " . $i . ">";
                                    echo "<td>" . $c . "</td>";
                                    echo "<td>" . $row->name . "</td>";
                                    echo "<td>" . $row->username . "</td>";
                                    echo "<td>" . $row->email . "</td>";
                                    ?>
                                    <td> <?php
                                        if ($row->user_type == '1') {
                                            echo "Super Admin";
                                        } elseif ($row->user_type == '2') {
                                            echo "Admin";
                                        } else {
                                            echo "";
                                        }
                                        ?>
                                    </td>
                                    <td> <?php if ($row->conf_prev == '1'):echo "Yes";
                                        else:echo "No";
                                        endif; ?> </td>
                                    <td> <?php if ($row->leave_prev == '1'):echo "Yes";
                                        else:echo "No";
                                        endif; ?> </td>
                                    <td> <?php if ($row->salary_prev == '1'):echo "Yes";
                                        else:echo "No";
                                        endif; ?> </td>
                                    <td> <?php if ($row->hr_prev == '1'):echo "Yes";
                                        else:echo "No";
                                        endif; ?> </td>
                                    <td> <?php if ($row->finance_prev == '1'):echo "Yes";
                                        else:echo "No";
                                        endif; ?> </td>
                                    <td> <?php if ($row->report_prev == '1'):echo "Yes";
                                        else:echo "No";
                                        endif; ?> </td>
                                    <td> <?php if ($row->attendance_prev == '1'):echo "Yes";
                                        else:echo "No";
                                        endif; ?> </td>
                                    <td>
                                        <a class="btn btn-warning btn-xs" href="<?php echo base_url(); ?>hr/user/user_edit/<?php echo $row->id; ?> "   title="Edit the Content">Edit</a>
                                        <a class="btn btn-danger btn-xs" onclick="return confirmSubmit()" href="<?php echo base_url() ?>hr/user/user_delete/<?php echo $row->id; ?>" title="Delete the Content">Delete</a><p></p>
                                        <a class="btn btn-primary btn-xs" href="<?php echo base_url(); ?>hr/user/change_password/<?php echo $row->username; ?>" title="Edit the Content">Change Password</a>
                                    </td>
                                    <?php
                                    $c++;
                                }
                                ?>
                            </tr>
                            </tbody>
                        </table>
                        <div class="text-center">
                        <?php echo $this->pagination->create_links(); ?>
                        </div>
                        <div class="text-center">
                            <?php
                            echo "Total Records: ".$this->pagination_library->total_rows." page ".(($this->pagination->cur_page) ? $this->pagination->cur_page : "1")." of ". ceil(($this->pagination_library->total_rows)/($this->pagination_library->per_page));
                            ?>
                        </div>
                    </section>
                </div><! --/content-panel -->
            </div><!-- /col-md-12 -->
        </div><!-- row -->

