 <h3><i class="fa fa-angle-right"></i> Create New User</h3>
        <!-- BASIC FORM ELELEMNTS -->
        <div class="row">
            <div class="col-sm-12">
                <div class="content-panel">
                    <?php
                    echo form_open('../hr/user/added_user'); ?>
                    <div class="alert-danger"><?php echo $this->session->flashdata('message'); ?> </div>
                    <div class="col-md-12">
                    <!--<form action="" method="">-->
                        <div class="form-group col-sm-6">
                            <label for="">Name</label>
                            <input type="text" value="<?php echo $this->input->post('name')?>" name="name" class="form-control" id="">
                            <div class="alert-danger"><?php echo ucwords(form_error('name')); ?></div>
                        </div>
                        <div class="form-group col-sm-6">
                            <label for="">Username</label>
                            <input value="<?php echo $this->input->post('username')?>"  type="text" name="username" class="form-control" id="">
                            <div class="alert-danger"><?php echo ucwords(form_error('username')); ?></div>
                        </div>
                    </div>
                    <div class="col-md-12">
                        <div class="form-group col-sm-6">
                            <label for="">Password</label>
                            <input type="password" name="password" class="form-control" id="">
                            <div class="alert-danger"><?php echo ucwords(form_error('password')); ?></div>
                        </div>
                        <div class="form-group col-sm-6">
                            <label for="">Retype Password</label>
                            <input type="password" name="passconf" class="form-control" id="">
                            <div class="alert-danger"><?php echo ucwords(form_error('passconf')); ?></div>
                        </div>
                    </div>
                    <div class="col-md-12">
                        <div class="form-group col-sm-6">
                            <label for="">Email:</label>
                            <input value="<?php echo $this->input->post('email')?>"  type="email" name="email" class="form-control" id="">
                            <div class="alert-danger"><?php echo ucwords(form_error('email')); ?></div>
                        </div>
                        <div class="form-group col-sm-6">
                            <label for="">User Type:</label>
                            <select name="user_type" class="form-control">
                                <option value="">Select</option>
                                <option value="1" <?php if($this->input->post('user_type')==1){echo 'selected' ;} ?> >SuperAdmin</option>
                                <option value="2" <?php if($this->input->post('user_type')==2){echo 'selected' ;} ?> >Admin</option>
                            </select>
                            <div class="alert-danger"><?php echo ucwords(form_error('user_type')); ?></div>
                        </div>
                    </div>

                        <div class="form-group col-sm-12">
                            <div class="col-md-12">
                            <label for="">User Privilege Option:</label>
                                <p  class="">
                                    <label class="checkbox-inline"><input type="checkbox" name="conf_prev" value="1" <?php echo $this->input->post('conf_prev')==1?  'checked':'';?> />&nbsp;&nbsp;Configuration </label>
                                    <label class="checkbox-inline"><input type="checkbox" name="leave_prev" value="1" <?php echo $this->input->post('leave_prev')==1?  'checked':'';?>  />&nbsp;&nbsp;Leave</label>
                                    <label class="checkbox-inline"><input type="checkbox" name="salary_prev" value="1" <?php echo $this->input->post('salary_prev')==1?  'checked':'';?> />&nbsp;&nbsp;Salary</label>
                                    <label class="checkbox-inline"><input type="checkbox" name="hr_prev" value="1" <?php echo $this->input->post('hr_prev')==1?  'checked':'';?> />&nbsp;&nbsp;Hr</label>
                                    <label class="checkbox-inline"><input type="checkbox" name="finance_prev" value="1"  <?php echo $this->input->post('finance_prev')==1?  'checked':'';?> />&nbsp;&nbsp;Finance</label>
                                    <label class="checkbox-inline"><input type="checkbox" name="report_prev" value="1" <?php echo $this->input->post('report_prev')==1?  'checked':'';?>  />&nbsp;&nbsp;Reports</label>
                                    <label class="checkbox-inline"><input type="checkbox" name="attendance_prev" value="1"  <?php echo $this->input->post('attendance_prev')==1?  'checked':'';?> />&nbsp;&nbsp;Attendance</label>
                                </p>
                            </div>


                        </div>
                            <span>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<button type="submit" name="mysubmit" class="btn btn-default">Submit</button></span><span>&nbsp;&nbsp;<a class="btn btn-theme" href="<?php echo base_url(); ?>hr/user/list_user">Cancel</a></span>
                        

                    </form>

                </div>
            </div><!-- col-lg-12-->
        </div><!-- /row -->