<h3><i class="fa fa-angle-right"></i>Create New User</h3>

        <!-- BASIC FORM ELELEMNTS -->
        <div class="row">
            <div class="col-sm-12">
                <div class="content-panel">

                    <?php
                    echo form_open('hr/user/added_user'); ?>
                    <div class="alert-danger">    <?php echo $this->session->flashdata('message'); ?> </div>                     <!--<form action="" method="">-->
                        <div class="form-group col-sm-6">
                            <label for="">Name</label>
                            <input type="text" name="name" class="form-control" id="">
                            <div class="alert-danger"><?php echo ucwords(form_error('name')); ?></div>
                        </div>
                        <div class="form-group col-sm-6">
                            <label for="">Username</label>
                            <input type="text" name="username" class="form-control" id="">
                            <div class="alert-danger"><?php echo ucwords(form_error('username')); ?></div>
                        </div>
                        <div class="form-group col-sm-6">
                            <label for="">Password</label>
                            <input type="password" name="password" class="form-control" id="">
                            <div class="alert-danger"><?php echo ucwords(form_error('password')); ?></div>
                        </div>
                        <div class="form-group col-sm-6">
                            <label for="">Retype Password</label>
                            <input type="password" name="passconf" class="form-control" id="">
                            <div class="alert-danger"><?php echo ucwords(form_error('passconf')); ?></div>
                        </div>

                        <div class="form-group col-sm-6">
                            <label for="">Email:</label>
                            <input type="email" name="email" class="form-control" id="">
                            <div class="alert-danger"><?php echo ucwords(form_error('email')); ?></div>
                        </div>
                        <div class="form-group col-sm-6">
                            <label for="">User Type:</label>
                            <select name="user_type" class="form-control">
                                <option value="">Select</option>
                                <option value="1">SuperAdmin</option>
                                <option value="2">Admin</option>
                            </select>
                            <div class="alert-danger"><?php echo ucwords(form_error('user_type')); ?></div>
                        </div>
                        <div class="form-group col-sm-12">
                            <label for="">User Privilege Option:</label>
                            <p  class="form-control">
                                <label class="checkbox-inline"><input type="checkbox" name="conf_prev" value="1"  />&nbsp;&nbsp;Configuration </label>
                                <label class="checkbox-inline"><input type="checkbox" name="leave_prev" value="1" />&nbsp;&nbsp;Leave</label>
                                <label class="checkbox-inline"><input type="checkbox" name="salary_prev" value="1" />&nbsp;&nbsp;Salary</label>
                                <label class="checkbox-inline"><input type="checkbox" name="hr_prev" value="1" />&nbsp;&nbsp;Hr</label>
                                <label class="checkbox-inline"><label class="checkbox-inline"><input type="checkbox" name="finance_prev" value="1" />&nbsp;&nbsp;Finance</label>
                                <label class="checkbox-inline"><input type="checkbox" name="report_prev" value="1" />&nbsp;&nbsp;Reports</label>
                                <label class="checkbox-inline"><input type="checkbox" name="attendance_prev" value="1" />&nbsp;&nbsp;Attendance</label>
                            </p>
                        </div>
                        <span>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<button type="submit" name="mysubmit" class="btn btn-default">Submit</button></span><span>&nbsp;&nbsp;<button type="submit" class="btn btn-theme">Cansel</button></span>
                    </form>

                </div>
            </div><!-- col-lg-12-->
        </div><!-- /row -->

<script language="javascript">
    function confirmSubmit() {
        var agree = confirm("Are you sure to delete this record?");
        if (agree)
            return true;
        else
            return false;
    }
</script>


        <h3><i class="fa fa-angle-right"></i> View User</h3>
        <div class="row">
            <div class="col-sm-12">
                <div class="content-panel">
                    <section id="flip-scroll">
                        <table  id="myTable"  class="table">
                            <thead>
                            <tr>
                                <th>#</th>
                                <th>Name</th>
                                <th>UserName</th>
                                <th>Email</th>
                                <th>UserType</th>
                                <th>Configuration</th>
                                <th>Leave</th>
                                <th>Salary</th>
                                <th>Hr</th>
                                <th>Finance</th>
                                <th>Reports</th>
                                <th>Attendance</th>
                                <th colspan="2" class="">Action</th>
                            </tr>
                            </thead>
                            <tbody>
                            <tr>
                                <?php
                                $i = 'even';
                                $c = 1;
                                foreach ($query as $row) {
                                    if ($i == 'even') {
                                        $i = 'odd';
                                    } else {
                                        $i = 'even';
                                    }
                                    echo "<tr class = " . $i . ">";
                                    echo "<td>" . $c . "</td>";
                                    echo "<td>" . $row->name . "</td>";
                                    echo "<td>" . $row->username . "</td>";
                                    echo "<td>" . $row->email . "</td>";
                                    ?>
                                    <td> <?php
                                        if ($row->user_type == '1') {
                                            echo "Super Admin";
                                        } elseif ($row->user_type == '2') {
                                            echo "Admin";
                                        } else {
                                            echo "";
                                        }
                                        ?>
                                    </td>
                                    <td> <?php if ($row->conf_prev == '1'):echo "Yes";
                                        else:echo "No";
                                        endif; ?> </td>
                                    <td> <?php if ($row->leave_prev == '1'):echo "Yes";
                                        else:echo "No";
                                        endif; ?> </td>
                                    <td> <?php if ($row->salary_prev == '1'):echo "Yes";
                                        else:echo "No";
                                        endif; ?> </td>
                                    <td> <?php if ($row->hr_prev == '1'):echo "Yes";
                                        else:echo "No";
                                        endif; ?> </td>
                                    <td> <?php if ($row->finance_prev == '1'):echo "Yes";
                                        else:echo "No";
                                        endif; ?> </td>
                                    <td> <?php if ($row->report_prev == '1'):echo "Yes";
                                        else:echo "No";
                                        endif; ?> </td>
                                    <td> <?php if ($row->attendance_prev == '1'):echo "Yes";
                                        else:echo "No";
                                        endif; ?> </td>
                                    <td> <a class="btn btn-warning" href="<?php echo base_url(); ?>hr/user/user_edit/<?php echo $row->id; ?> "   title="Edit the Content">Edit</a>
                                       <a class="btn btn-warning" onclick="return confirmSubmit()" href="<?php echo base_url() ?>hr/user/user_delete/<?php echo $row->id; ?>" title="Delete the Content">Delete</a></td>
                                    <?php
                                    $c++;
                                }
                                ?>
                            </tr>
                            </tbody>
                        </table>
                    </section>
                </div><! --/content-panel -->
            </div><!-- /col-md-12 -->

        </div><!-- row -->