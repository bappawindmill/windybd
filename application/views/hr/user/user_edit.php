<h3><i class="fa fa-angle-right"></i> User Edit</h3>

<!-- BASIC FORM ELELEMNTS -->
<div class="row">
    <div class="col-sm-12">
        <div class="content-panel">
            <?php
            $utype = array(
                '' => 'Select',
                '1' => 'SuperAdmin',
                '2' => 'Admin'
            );
            foreach ($query as $temp):endforeach;
            echo form_open('hr/user/user_edit'); ?>

            <div class="alert-danger"><?php echo $this->session->flashdata('message'); ?> </div>
            <div class="col-md-12">
                <!--<form action="" method="">-->
                <div class="form-group col-sm-6">
                    <label for="">Name</label>
                    <input type="text"  value="<?php echo $temp->name; ?>" name="name" class="form-control" id="" required>
                    <div class="alert-danger"><?php echo ucwords(form_error('name')); ?></div>
                </div>
                <div class="form-group col-sm-6">
                    <label for="">Username</label>
                    <input value="<?php echo $temp->username; ?>"  type="text" name="username" class="form-control" id="" required>
                    <div class="alert-danger"><?php echo ucwords(form_error('username')); ?></div>
                </div>
            </div>

            <div class="col-md-12">
                <div class="form-group col-sm-6">
                    <label for="">Email:</label>
                    <input value="<?php echo $temp->email; ?>"  type="email" name="email" class="form-control" id="" required>
                    <div class="alert-danger"><?php echo ucwords(form_error('email')); ?></div>
                </div>
                <div class="form-group col-sm-6">
                    <label for="">User Type:</label>
                    <?php echo form_dropdown('user_type', $utype, $temp->user_type,'class="form-control" required'); ?>
                    <div class="alert-danger"><?php echo ucwords(form_error('user_type')); ?></div>
                </div>
            </div>

            <div class="form-group col-sm-12">
                <div class="col-md-12">
                    <label for="">User Privilege Option:</label>
                    <p  class="">
                        <label class="checkbox-inline"><input type="checkbox" name="conf_prev" value="1" <?php if ($temp->conf_prev == "1"): ?> checked="checked"<?php endif; ?> />&nbsp;&nbsp;Configuration </label>
                        <label class="checkbox-inline"><input type="checkbox" name="leave_prev" value="1"<?php if ($temp->leave_prev == "1"): ?> checked="checked"<?php endif; ?>/>&nbsp;&nbsp;Leave</label>
                        <label class="checkbox-inline"><input type="checkbox" name="salary_prev" value="1" <?php if ($temp->salary_prev == "1"): ?> checked="checked"<?php endif; ?>/>&nbsp;&nbsp;Salary</label>
                        <label class="checkbox-inline"><input type="checkbox" name="hr_prev" value="1" <?php if ($temp->hr_prev == "1"): ?> checked="checked"<?php endif; ?> />&nbsp;&nbsp;Hr</label>
                        <label class="checkbox-inline"><input type="checkbox" name="finance_prev" value="1" <?php if ($temp->finance_prev == "1"): ?> checked="checked"<?php endif; ?> />&nbsp;&nbsp;Finance</label>
                        <label class="checkbox-inline"><input type="checkbox" name="report_prev" value="1" <?php if ($temp->report_prev == "1"): ?> checked="checked"<?php endif; ?>/>&nbsp;&nbsp;Reports</label>
                        <label class="checkbox-inline"><input type="checkbox" name="attendance_prev" value="1" <?php if ($temp->attendance_prev == "1"): ?> checked="checked"<?php endif; ?> />&nbsp;&nbsp;Attendance</label>
                    </p>
                </div>


            </div>
            <span>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
                <input type="hidden" name="id" value="<?php echo $temp->id; ?>" />
                <input type="submit" name="mysubmit" value="Update"  class="btn btn-primary">
                <a class="btn btn-theme" href="<?php echo base_url(); ?>hr/user/list_user">Cancel</a></span>
            </span>


            </form>

        </div>
    </div><!-- col-lg-12-->
</div><!-- /row -->