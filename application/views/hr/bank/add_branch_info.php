<h3><i class="fa fa-angle-right"></i> Create Branch</h3>

<!-- BASIC FORM ELELEMNTS -->
<div class="row">
    <div class="col-sm-12">
        <div class="content-panel">
            <?php
            echo form_open('hr/bank/added_branchinfo');?>
            <?php
            if($this->session->flashdata('message')==NULL){

            }else{ ?>
                <div class="alert-success alert">
                    <?php echo $this->session->flashdata('message') ?>
                </div> <?php
            }
            ?>
            <?php
            $query00=$this->db->query("select id,bank_name from bank");
            ?>
            <div class="col-md-12">
                <div class="form-group col-sm-4">
                    <label for="branch_name">Branch Name:</label>
                    <input value=""  type="text" name="branch_name" class="form-control" id="branch_name">
                    <div class="alert-danger"><?php echo ucwords(form_error('branch_name')); ?></div>
                </div>
                <div class="form-group col-sm-4">
                    <label for="">Bank Name:</label>
                    <div id="res">
                        <select name="bank_name" id="bank_name" class="form-control">
                            <option value="">select</option>
                            <?php foreach($query00->result() as $info2):?>
                                <option value="<?php echo $info2->id;?>"><?php echo $info2->bank_name; ?></option>
                            <?php endforeach;?>
                        </select>
                    </div>
                    <div class="alert-danger"><?php echo ucwords(form_error('bank_name')); ?></div>
                </div>
                <div class="form-group col-sm-4">
                    <label for="address">Address:</label>
                    <input value=""  type="text" name="address" class="form-control" id="">
                    <div class="alert-danger"><?php echo ucwords(form_error('address')); ?></div>
                </div>
            </div>
                <span>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
                    <input type="hidden" name="id" value="" />&nbsp;&nbsp;&nbsp;&nbsp;
                    <input type="submit" name="mysubmit" value="Submit"  class="btn btn-primary">
                    <a class="btn btn-theme" href="<?php echo base_url(); ?>hr/bank/branchinfo_list">Cancel</a>
                </span>

            </form>
            <p></p>
        </div>
    </div><!-- col-lg-12-->
</div><!-- /row -->