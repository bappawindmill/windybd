<script language="javascript">
    function confirmSubmit() {
        var agree = confirm("Are you sure to delete this record?");
        if (agree)
            return true;
        else
            return false;
    }
</script>

<h3><i class="fa fa-angle-right"></i> View Branch Information</h3>
<?php
if($this->session->flashdata('message')==NULL){

}else{ ?>
    <div class="alert-success alert">
        <?php echo $this->session->flashdata('message') ?>
    </div> <?php
}
?>


<div class="row">
    <div class="col-sm-12 col-lg-12">
        <div class="pull-left">
            <i class="fa"></i>
            <a class="btn btn-info" href="<?php echo base_url(); ?>hr/bank/added_branchinfo"   title="">Add  New </a>
        </div>
        <div class=" pull-right">
            <form class="form-inline">
                <div class="form-group has-feedback pull-right">
                    <input type="text" id="searchKey" name="search" class="form-control" placeholder="Search" />
                    <i class="glyphicon glyphicon-search form-control-feedback"></i>
                </div>
            </form>
        </div>
        <div class="pull-right"><a href="<?php echo base_url();?>hr/bank/branchinfo_list" class="btn btn-info" role="button" >Reset</a>&nbsp;</div>
    </div>

    <div class="col-sm-12">
        <div class="content-panel">
            <!--<section id="flip-scroll">-->
            <section id="">
                <table  id="myTable"  class="table table-hover table-bordered">
                    <thead>
                    <tr class="info">
                        <th>SL</th>
                        <th>Branch Name</th>
                        <th>Bank Name</th>
                        <th>Address</th>
                        <th colspan="2">Action</th>
                    </tr>
                    </thead>
                    <tbody>
                    <?php
                    $i = 'even';
                    $c = 1;
                    foreach ($query as $row) {
                        if ($i == 'even') {
                            $i = 'odd';
                        } else {
                            $i = 'even';
                        }
                        echo "<tr class = " . $i . ">";
                        echo "<td>" . $c . "</td>";
                        echo "<td>" . $row->branch_name . "</td>";
                        $bankName="";
                        $query =$this->db->query("select bank_name from bank where id='".$row->bank_name."'");
                        foreach($query->result() as $temp):
                            $bankName=$temp->bank_name;
                        endforeach;
                        echo "<td>" . $bankName. "</td>";
                        echo "<td>" . $row->address. "</td>";
                        ?>
                        <td>
                            <a class="btn btn-danger btn-xs" onclick="return confirmSubmit()" href="<?php echo base_url() ?>hr/bank/branchinfo_delete/<?php echo $row->id; ?>" title="Delete the Content">Delete</a>
                        </td>
                        <?php
                        $c++;
                    }
                    ?>
                    </tbody>
                </table>
                <div class="text-center">
                    <?php echo $this->pagination->create_links(); ?>
                </div>
                <div class="text-center">
                    <?php
                    echo "Total Records: ".$this->pagination_library->total_rows." page ".(($this->pagination->cur_page) ? $this->pagination->cur_page : "1")." of ". ceil(($this->pagination_library->total_rows)/($this->pagination_library->per_page));
                    ?>
                </div>
            </section>
        </div><! --/content-panel -->
    </div><!-- /col-md-12 -->
</div><!-- row -->