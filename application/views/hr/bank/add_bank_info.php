<h3><i class="fa fa-angle-right"></i> Add Bank Info</h3>

<!-- BASIC FORM ELELEMNTS -->
<div class="row">
    <div class="col-sm-12">
        <div class="content-panel">
            <?php
            echo form_open('hr/bank/added_bankinfo');?>
            <?php
            if($this->session->flashdata('message')==NULL){

            }else{ ?>
                <div class="alert-success alert">
                    <?php echo $this->session->flashdata('message') ?>
                </div> <?php
            }
            ?>
            <div class="col-md-12">
                <div class="form-group col-sm-6">
                    <label for="name">Bank Name :</label>
                    <input value="<?php echo $this->input->post('name'); ?>"  type="text" name="name" id="name" class="form-control">
                    <div class="alert-danger"><?php echo ucwords(form_error('name')); ?></div>
                </div>

                <div class="form-group col-sm-6">
                    <label for="name">Address:</label>
                    <input value="<?php echo $this->input->post('address'); ?>"  type="text" name="address" id="address" class="form-control">
                    <div class="alert-danger"><?php echo ucwords(form_error('address')); ?></div>
                </div>
            </div>
                <span>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
                    <input type="hidden" name="id" value="" />&nbsp;&nbsp;&nbsp;&nbsp;
                    <input type="submit" name="mysubmit" value="Submit"  class="btn btn-primary">
                    <a class="btn btn-theme" href="<?php echo base_url(); ?>hr/bank/bankinfo_list">Cancel</a>
                </span>
            </form>
            <p></p>
        </div>
    </div><!-- col-lg-12-->
</div><!-- /row -->