<h3><i class="fa fa-angle-right"></i> Duty Schedule Edit</h3>

<!-- BASIC FORM ELELEMNTS -->
<div class="row">
    <div class="col-sm-12">
        <div class="content-panel">
            <?php
            echo form_open('hr/schedule/schedule_edit');?>
                <?php
                if($this->session->flashdata('message')==NULL){

                }else{ ?>
                    <div class="alert-success alert">
                        <?php echo $this->session->flashdata('message') ?>
                    </div> <?php
                }
                ?>
                <?php foreach ($record as $sInfo):endforeach; ?>
                <div class="col-md-12">
                    <div class="form-group col-sm-6">
                        <label for="schedule_title">Schedule Title :</label>
                        <input value="<?php echo $sInfo->schedule_title; ?>"  type="text" name="schedule_title" id="schedule_title" class="form-control" required>
                        <div class="alert-danger"><?php echo ucwords(form_error('schedule_title')); ?></div>
                    </div>

                    <div class="form-group col-sm-6">
                        <label for="name">Start Time:</label>
                        <input value="<?php echo $sInfo->start_time; ?>"  type="text" name="start_time" id="start_time" class="form-control" placeholder="hh:mm" required>
                        <div class="alert-danger"><?php echo ucwords(form_error('start_time')); ?></div>
                    </div>
                </div>
                <div class="col-md-12">
                    <div class="form-group col-sm-6">
                        <label for="late_time">Late Time :</label>
                        <input value="<?php echo $sInfo->late_time; ?>"  type="text" name="late_time" id="late_time" class="form-control" required>
                        <div class="alert-danger"><?php echo ucwords(form_error('late_time')); ?></div>
                    </div>

                    <div class="form-group col-sm-6">
                        <label for="end_time">End Time:</label>
                        <input value="<?php echo $sInfo->end_time; ?>"  type="text" name="end_time" id="end_time" class="form-control" placeholder="hh:mm"  required>
                        <div class="alert-danger"><?php echo ucwords(form_error('end_time')); ?></div>
                    </div>
                </div>
                <span>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
                    <input type="hidden" name="id" value="<?php echo $sInfo->id; ?>" />&nbsp;&nbsp;&nbsp;&nbsp;
                    <input type="submit" name="mysubmit" value="Update"  class="btn btn-primary">
                    <a class="btn btn-theme" href="<?php echo base_url(); ?>hr/schedule/schedule_list">Cancel</a>
                </span>
            </form>
        </div>
    </div><!-- col-lg-12-->
</div><!-- /row -->