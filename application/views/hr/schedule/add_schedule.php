<h3><i class="fa fa-angle-right"></i> Create Duty Schedule</h3>

<!-- BASIC FORM ELELEMNTS -->
<div class="row">
    <div class="col-sm-12">
        <div class="content-panel">
            <?php
            echo form_open("hr/schedule/added_schedule");?>
            <div class="alert-danger"><?php echo $this->session->flashdata('message'); ?> </div>
            <div class="col-md-12">
                <div class="form-group col-sm-6">
                    <label for="schedule_title">Schedule Title :</label>
                    <input value=""  type="text" name="schedule_title" id="schedule_title" class="form-control">
                    <div class="alert-danger"><?php echo ucwords(form_error('schedule_title')); ?></div>
                </div>

                <div class="form-group col-sm-6">
                    <label for="name">Start Time:</label>
                    <input value=""  type="text" name="start_time" id="start_time" class="form-control" placeholder="hh:mm">
                    <div class="alert-danger"><?php echo ucwords(form_error('start_time')); ?></div>
                </div>
            </div>
            <div class="col-md-12">
                <div class="form-group col-sm-6">
                    <label for="late_time">Late Time :</label>
                    <input value=""  type="text" name="late_time" id="late_time" class="form-control">
                    <div class="alert-danger"><?php echo ucwords(form_error('late_time')); ?></div>
                </div>

                <div class="form-group col-sm-6">
                    <label for="end_time">End Time:</label>
                    <input value=""  type="text" name="end_time" id="end_time" class="form-control" placeholder="hh:mm">
                    <div class="alert-danger"><?php echo ucwords(form_error('end_time')); ?></div>
                </div>
            </div>
                <span>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
                    <input type="hidden" name="id" value="" />&nbsp;&nbsp;&nbsp;&nbsp;
                    <input type="submit" name="mysubmit" value="Submit"  class="btn btn-primary">
                    <a class="btn btn-theme" href="<?php echo base_url(); ?>hr/schedule/schedule_list">Cancel</a>
                </span>
            </form>
            <p></p>
        </div>
    </div><!-- col-lg-12-->
</div><!-- /row -->