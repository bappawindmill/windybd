<script language="javascript">
    function confirmSubmit() {
        var agree = confirm("Are you sure to delete this record?");
        if (agree)
            return true;
        else
            return false;
    }
</script>

<h3><i class="fa fa-angle-right"></i> View Employee OT Info</h3>
<?php
if($this->session->flashdata('message')==NULL){

} else { ?>
    <div class="alert-success alert">
        <?php echo $this->session->flashdata('message') ?>
    </div> <?php
}
?>
<?php

 foreach ($query9 as $row):endforeach;
			            if ($month == '01') {
                            $total_month_day = 31;
                            $monthName="January";
                        } elseif ($month == '02') {
                            $total_month_day = 28;
                            $monthName="February";
                        } elseif ($month == '03') {
                            $total_month_day = 31;
                            $monthName="March";
                        } elseif ($month == '04') {
                            $total_month_day = 30;
                            $monthName="April";
                        } elseif ($month == "05") {
                            $total_month_day = 31;
                            $monthName="May";
                        } elseif ($month == "06") {
                            $total_month_day = 30;
                            $monthName="June";
                        } elseif ($month == "07") {
                            $total_month_day = 31;
                            $monthName="July";
                        } elseif ($month == "08") {
                            $monthName="August";
                            $total_month_day = 31;
                        } elseif ($month == "09") {
                            $total_month_day = 30;
                            $monthName="September";
                        } elseif ($month == "10") {
                            $total_month_day = 31;
                            $monthName="October";
                        } elseif ($month == "11") {
                            $total_month_day = 30;
                            $monthName="November";
                        } elseif ($month == "12") {
                            $total_month_day = 31;
                            $monthName="December";
                        } else {
                            $total_month_day = 30;
                            $monthName="";
                        }
?>
<p style="text-align:center"> <?php echo $monthName;?>,<?php echo $year;?></p>



<p style="text-align:center;font-weight:bold"> Employee Name : <?php echo $row->name ;?></p>

<p style="text-align:center;font-weight:bold">Employee ID No : <?php echo $row->card_no ;?></p>

<div class="row">
    <div class="col-sm-12">
        <div class="content-panel">
            <!--<section id="flip-scroll">-->
            <section id="">
                <table  id=""  class="table table-hover">
                    <thead>
                    <tr>
                        <th>Date</th>
                        <th>In Time </th>
                        <th>Out Time</th>
                        <th>Total Hour</th>
                        <th>Hour Rate</th>
                        <th>Total</th>
                    </tr>
                    </thead>
                    <tbody>
                    <?php
                    echo form_open('hr/ot/changed');
                    for ($i = 1; $i <= $total_month_day; $i++) {
                        if ($i <= 9) {
                            $day = "0" . $i;
                        } else {
                            $day = $i;
                        }
                        $sdate = $year . "-" . $month . "-" . $day . "";
                        $query = $this->shows->getThisValue("emp_id = '$row->id' and ot_date = '$sdate'","ot_sheet");
                        if(count($query)){
                            foreach($query as $tempData){
                                $intime_data = $tempData->intime;
                                $outtime_data = $tempData->outtime;
                                $total_hour_data = $tempData->total_hour;
                                $hour_rate_data = $tempData->hour_rate;
                                $total_data = $tempData->total_amount;
                            }
                        } else {
                            $intime_data = "";
                            $outtime_data = "";
                            $total_hour_data = "";
                            $hour_rate_data = "";
                            $total_data = "";
                        }
                        ?>
<tr><td>
                        <input type="hidden" name="month" value="<?php echo $monthName;?>"  />
                        <input type="hidden" name="card_no_d" value="<?php echo $row->card_no ;?>"  />
                        <input type="hidden" name="year" value="<?php echo $year ;?>"  />
    </td></tr>
                        <tr class = "">
                            <td><?php echo  $sdate ;?><input type="hidden" name="sdate[]" value="<?php echo $sdate;?>" /></td>
                            <td><input type="text" name="intime[]" value="<?php echo $intime_data;?>" /></td>
                            <td><input type="text" name="outtime[]" value="<?php echo $outtime_data;?>" /></td>
                            <td><!--<input type="text" name="total_hour[]" value="<?php /*echo $total_hour_data;*/?>" />-->

                                <?php
                                $mystring = $total_hour_data;
                                $findme   = '.';
                                $pos = strpos($mystring, $findme);
                                if ($pos !=false) {
                                    $total_hourl = explode(".", $total_hour_data);
                                    $start_hour = $total_hourl[0];
                                    $start_minute =round( $total_hourl[1]/100*60);
                                    if(strlen($start_minute)==1){
                                        $start_minute=$start_minute."0" ;
                                    }
                                    $adjust_time = $start_hour.':'.$start_minute;// show($start_minute);
                                } else {
                                    $adjust_time=$mystring; //show($final_total_hour);
                                }
                                ?>

                                <!--<input id="AppointmentDateTime_<?php /*echo $sdate;*/?>" name="total_hour[]" value="<?php /*echo $total_hour_data;*/?>" class="form-control AppointmentDateTime" type="text" readonly>-->
                                <input id="AppointmentDateTime_<?php echo $sdate;?>" name="total_hour[]" value="<?php echo @$adjust_time;?>" class="form-control AppointmentDateTime" type="text" readonly>
                                <a href="javascript:void(0)" onclick="document.getElementById('AppointmentDateTime_<?php echo $sdate;?>').value = ''">Clear</a>
                            </td>
                            <td><input type="text" name="hour_rate[]" value="<?php echo $hour_rate_data;?>" /></td>
                            <td><input type="text" readonly="readonly"  value="<?php echo $total_data;?>" /></td>
                        </tr>
                        <?php
                        $adjust_time='';
                    }
                    ?>
                    </tbody>
                </table>
                <table>
                    <thead>
                        <tr>
                            <td>
                                <input type="hidden" name="id" value="<?php echo $row->id;?>"/>
                                <input type="hidden" name="year" value="<?php echo $year;?>"/>
                                <input type="hidden" name="month" value="<?php echo $month;?>"/>
                            </td>
                        </tr>
                    <thead>
                </table>
                <input class="btn btn-primary" type="submit" name="change" value="Save" />
                <?php echo form_close(); ?>

                <div class="text-center">
                    <?php echo $this->pagination->create_links(); ?>
                </div>
            </section>
        </div><! --/content-panel -->
    </div><!-- /col-md-12 -->
</div><!-- row -->