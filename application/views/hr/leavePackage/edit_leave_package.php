<h3><i class="fa fa-angle-right"></i> Leave Package Edit</h3>

<!-- BASIC FORM ELELEMNTS -->
<div class="row">
    <div class="col-sm-12">
        <div class="content-panel">
            <?php
            echo form_open('hr/leave_package/leave_package_edit');?>
            <div class="alert-danger"><?php echo $this->session->flashdata('message'); ?> </div>
            <div class="col-md-12">
                <div class="form-group col-sm-6">
                    <label for="package_name">Leave Package Name:</label>
                    <input value="<?php echo $package_name['value'];?>"  type="text" name="package_name" class="form-control" id="package_name" required>
                    <div class="alert-danger"><?php echo ucwords(form_error('package_name')); ?></div>
                </div>
                <div class="form-group col-sm-6">
                    <label for="casual">Csual leave:</label>
                    <input value="<?php echo $casual['value'];?>"  type="text" name="casual" class="form-control" id="casual" required>
                    <div class="alert-danger"><?php echo ucwords(form_error('casual')); ?></div>
                </div>
            </div>
            <div class="col-md-12">
                <div class="form-group col-sm-6">
                    <label for="medical">Sick:</label>
                    <input value="<?php echo $medical['value'];?>"  type="text" name="medical" class="form-control" id="medical" required>
                    <div class="alert-danger"><?php echo ucwords(form_error('medical')); ?></div>
                </div>
                <div class="form-group col-sm-6">
                    <label for="earned">Earned:</label>
                    <input value="<?php echo $earned['value'];?>"  type="text" name="earned" class="form-control" id="earned" required>
                    <div class="alert-danger"><?php echo ucwords(form_error('earned')); ?></div>
                </div>
            </div>
            <span>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
                <input type="hidden" name="id" value="<?php echo $fid['value']; ?>" />&nbsp;&nbsp;&nbsp;&nbsp;
                <input type="submit" name="mysubmit" value="Update"  class="btn btn-primary">
                <a class="btn btn-theme" href="<?php echo base_url(); ?>hr/leave_package/leave_package_list">Cancel</a>
            </span>
            </form>
        </div>
    </div><!-- col-lg-12-->
</div><!-- /row -->