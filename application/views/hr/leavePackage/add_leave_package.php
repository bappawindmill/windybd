<h3><i class="fa fa-angle-right"></i> Create Leave Package</h3>

<!-- BASIC FORM ELELEMNTS -->
<div class="row">
    <div class="col-sm-12">
        <div class="content-panel">
            <?php
            echo form_open('hr/leave_package/added_leave_package');?>
            <?php
            if($this->session->flashdata('message')==NULL){

            }else{ ?>
                <div class="alert-success alert">
                    <?php echo $this->session->flashdata('message') ?>
                </div> <?php
            }
            ?>
            <?php
            $query12 = $this->db->query("SELECT id,branch_name FROM add_branch");
            ?>
                <div class="col-md-12">
                    <div class="form-group col-sm-6">
                        <label for="package_name">Leave Package Name:</label>
                        <input value="<?php echo $this->input->post('package_name');?>"  type="text" name="package_name" class="form-control" id="package_name">
                        <div class="alert-danger"><?php echo ucwords(form_error('package_name')); ?></div>
                    </div>
                    <div class="form-group col-sm-6">
                        <label for="casual">Csual leave:</label>
                        <input value="<?php echo $this->input->post('casual');?>"  type="text" name="casual" class="form-control" id="casual">
                        <div class="alert-danger"><?php echo ucwords(form_error('casual')); ?></div>
                    </div>
                </div>
                <div class="col-md-12">
                    <div class="form-group col-sm-6">
                        <label for="medical">Sick:</label>
                        <input value="<?php echo $this->input->post('medical')?>"  type="text" name="medical" class="form-control" id="medical">
                        <div class="alert-danger"><?php echo ucwords(form_error('medical')); ?></div>
                    </div>
                    <div class="form-group col-sm-6">
                        <label for="earned">Earned:</label>
                        <input value="<?php echo $this->input->post('earned');?>"  type="text" name="earned" class="form-control" id="earned">
                        <div class="alert-danger"><?php echo ucwords(form_error('earned')); ?></div>
                    </div>
                </div>
                <span>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
                    <input type="hidden" name="id" value="" />&nbsp;&nbsp;&nbsp;&nbsp;
                    <input type="submit" name="mysubmit" value="Submit"  class="btn btn-primary">
                    <a class="btn btn-theme" href="<?php echo base_url(); ?>hr/leave_package/leave_package_list">Cancel</a>
                </span>

            </form>
            <p></p>
        </div>
    </div><!-- col-lg-12-->
</div><!-- /row -->