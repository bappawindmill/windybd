<script language="javascript">
    function confirmSubmit() {
        var agree = confirm("Are you sure to delete this record?");
        if (agree)
            return true;
        else
            return false;
    }
</script>
<?php
if ($month == '01') {
    $total_month_day = 31;
    $mname="January";
} elseif ($month == '02') {
    $total_month_day = 28;
    $mname="February";
} elseif ($month == '03') {
    $total_month_day = 31;
    $mname="March";
} elseif ($month == '04') {
    $total_month_day = 30;
    $mname="April";
} elseif ($month == "05") {
    $total_month_day = 31;
    $mname="May";
} elseif ($month == "06") {
    $total_month_day = 30;
    $mname="June";
} elseif ($month == "07") {
    $total_month_day = 31;
    $mname="July";
} elseif ($month == "08") {
    $total_month_day = 31;
    $mname="August";
} elseif ($month == "09") {
    $total_month_day = 30;
    $mname="September";
} elseif ($month == "10") {
    $total_month_day = 31;
    $mname="October";
} elseif ($month == "11") {
    $total_month_day = 30;
    $mname="November";
} elseif ($month == "12") {
    $total_month_day = 31;
    $mname="December";
} else {
    $total_month_day = 30;
    $mname="";
}
?>
<h3><i class="fa fa-angle-right"></i> Attendance Summary of <?php echo $mname;?>,<?php echo $year; ?></h3>
<?php
if($this->session->flashdata('message')==NULL){

}else{ ?>
    <div class="alert-success alert">
        <?php echo $this->session->flashdata('message') ?>
    </div> <?php
}
?>


<div class="row">

    <div class="col-sm-12">
        <div class="content-panel">
            <!--<section id="flip-scroll">-->
            <section id="">
                <table class="table table-bordered table-hover">
                    <thead>
                    <tr>
                        <th style="text-align:center">Date</th>
                        <th style="text-align:center">Entry time</th>
                        <th style="text-align:center">Exit time</th>
                        <th style="text-align:center">Status</th>
                        <!--<th>Late Time</th>-->
                        <th style="text-align:center">Remarks</th>
                    </tr>
                    </thead>
                    <tbody>
                    <?php
                    // echo form_open("hr/user_attendance/viewLog");
                    $c = 1;
                    if ($month == '01') {
                        $total_month_day = 31;
                    } elseif ($month == '02') {
                        $total_month_day = 28;
                    } elseif ($month == '03') {
                        $total_month_day = 31;
                    } elseif ($month == '04') {
                        $total_month_day = 30;
                    } elseif ($month == "05") {
                        $total_month_day = 31;
                    } elseif ($month == "06") {
                        $total_month_day = 30;
                    } elseif ($month == "07") {
                        $total_month_day = 31;
                    } elseif ($month == "08") {
                        $total_month_day = 31;
                    } elseif ($month == "09") {
                        $total_month_day = 30;
                    } elseif ($month == "10") {
                        $total_month_day = 31;
                    } elseif ($month == "11") {
                        $total_month_day = 30;
                    } elseif ($month == "12") {
                        $total_month_day = 31;
                    } else {
                        $total_month_day = 30;
                    }
                    $total_start_time = "";
                    $total_end_time = "";
                    $total_late_time = 0;
                    $duty_schedule = 0;
                    $empSchedule = $this->attendence_model->getEmpInfo($empId);
                    foreach ($empSchedule as $eInfo):
                        $duty_schedule = $eInfo->duty_schedule;
                    endforeach;
                    $scheduleInfo = $this->attendence_model->getSchedule($duty_schedule);
                    if (count($scheduleInfo)):
                        foreach ($scheduleInfo as $sInfo):
                            $start_time = $sInfo->start_time;
                            $end_time = $sInfo->end_time;
                            $late_time = $sInfo->late_time;
                        endforeach;
                        //convert start time to minute
                        $explode_start_time = explode(":", $start_time);
                        $start_hour = $explode_start_time[0];
                        $start_hour_to_minute = $start_hour * 60;
                        $start_minute = $explode_start_time[1];
                        $total_start_time = $start_hour_to_minute + $start_minute;
                        //convert end time to minute
                        $explode_end_time = explode(":", $end_time);
                        $end_hour = $explode_end_time[0];
                        $end_hour_to_minute = $end_hour * 60;
                        $end_minute = $explode_end_time[1];
                        $total_end_time = $end_hour_to_minute + $end_minute;

                        //convert Late time to minute
                        $explode_late_time = explode(":", $late_time);
                        $late_hour = $explode_late_time[0];
                        $late_hour_to_minute = $late_hour * 60;
                        $late_minute = $explode_late_time[1];
                        $total_late_time = $late_hour_to_minute + $late_minute;

                    endif;

                    $leave = 0;
                    $holiday = 0;
                    $weekend = 0;
                    $absent = 0;
                    $present = 0;
                    $late = 0;
                    //start for loop
                    for ($i = 1; $i <= $total_month_day; $i++) {
                        if ($i <= 9) {
                            $day = "0" . $i;
                        } else {
                            $day = $i;
                        }
                        $sdate = $year . "-" . $month . "-" . $day . "";
                        $cardNo = $secrectNo;
                        $branch = $branch_name;

                        //Retrive In time data
                        $this->load->model("user/attendence_model");
                        $timeInInfo = $this->attendence_model->getTimeInLogHr($cardNo, $sdate);

                        /* $timeInInfo = $this->db->query("select top(1) * from attendence where card_no='" . $this->session->userdata("secreteNo") . "'and att_date='" . $sdate . "'order by id asc"); */
                        $att_time_in = "";
                        $att_time_out = "";
                        $totalInTime = "";
                        foreach ($timeInInfo as $row3) {
                            $att_time_in = $row3->att_time;
                        }

                        if ($att_time_in != "") {
                            $explodeTimeIn = explode(":", $att_time_in);
                            $InHour = $explodeTimeIn[0];
                            $InHourToMinute = $InHour * 60;
                            $InHourminute = $explodeTimeIn[1];
                            $totalInTime = $InHourToMinute + $InHourminute;
                        }

                        //Retrive Out time data
                        $timeOutInfo = $this->attendence_model->getTimeOutLogHr($cardNo, $sdate);
                        /* $timeOutInfo = $this->db->query("select top(1) * from attendence where card_no='" . $this->session->userdata("secreteNo") . "'and att_date='" . $sdate . "'order by id DESC"); */

                        foreach ($timeOutInfo as $row31) {
                            $att_time_out = $row31->att_time;
                        }
                        if ($att_time_in == $att_time_out) {
                            $att_time_out = "";
                        }

                        //check weekend,holiday and leave

                        $holidayInfo = $this->attendence_model->getHoliday($sdate);

                        // $holidayInfo = $this->db->query("select * from add_holiday where  branch_name='" . $temp->branch_name . "'and '" . $sdate . "' between start_date and end_date ");
                        //echo "<pre>";print_r($holiday->result());
                        if (count($holidayInfo)) {
                            $holiday = $holiday + 1;
                            $status = "<font style='color:#006633'>Holiday</font>";
                            $status1 = "holiday";
                        } else {

                            $weekendInfo = $this->attendence_model->getWeekend($sdate, $branch);
                            //  $weekendInfo = $this->db->query("select * from add_weekend where dept='" . $temp->dept . "'and branch_name='" . $temp->branch_name . "'and weekend='" . $weekDay . "'");
                            if (count($weekendInfo)) {
                                $weekend = $weekend + 1;
                                $status = "<font style='color:#006633'>Weekend</font>";
                                $status1 = "weekend";
                            } else {
                                $leaveInfo = $this->attendence_model->getLeaveInfo($sdate, $empId);
                                // $leave1 = $this->db->query("select * from leave_management where emp_id='" . $temp->emp_id . "'and '" . $sdate . "' between start_date and end_date ");
                                if (count($leaveInfo)) {
                                    $leave = $leave + 1;
                                    $status = "<font style='color:green'>leave</font>";
                                    $status1 = "leave";
                                } else {
                                    if ($att_time_in == "" && $att_time_out == "") {
                                        $absent = $absent + 1;
                                        $status = "<font style='color:red'>Absent</font>";
                                        $status1 = "absent";
                                    } else {
                                        $present = $present + 1;
                                        $status = "<font style='color:#990033'>Present</font>";
                                        $status1 = "Present";
                                        if ($total_late_time != 0 && $totalInTime != ""){
                                            if ($totalInTime > $total_late_time) {
                                                $late = $late + 1;
                                                $late_minute_count = $totalInTime - $total_late_time ;
                                                if($late_minute_count >59){
                                                    $minutes = $late_minute_count;
                                                    $zero    = new DateTime('@0');
                                                    $offset  = new DateTime('@' . $minutes * 60);
                                                    $diff    = $zero->diff($offset);
                                                    $late_minute_count= $diff->format('%h Hours, %i Minutes');
                                                } else {
                                                    $minutes = $late_minute_count;
                                                    $zero    = new DateTime('@0');
                                                    $offset  = new DateTime('@' . $minutes * 60);
                                                    $diff    = $zero->diff($offset);
                                                    $late_minute_count= $diff->format('%i Minutes');
                                                }
                                                // $late_duration = $timein - $total_late_time;
                                                //$total_late_minute = $total_late_minute + $late_duration;

                                                $status = "<font style='color:#990033'>late</font>";
                                                $status1 = "late";
                                            }
                                        }
                                    }
                                }
                            }
                        }
                        //end weelend,holiday,leave check
                        $dateComment = "";
                        $loginfo = $this->db->query("select comment from DailyLog where emp_id ='" . $empId . "'and log_date ='" . $sdate . "'");
                        if (count($loginfo)):
                            foreach ($loginfo->result() as $c1Info):
                                $dateComment = $c1Info->comment;
                            endforeach;
                        endif;
                        ?>
                        <tr class=" <?php echo $i ?>">
                            <td><?php echo $sdate; ?><input type="hidden" name="att_date[]" value="<?php echo $sdate; ?>" /></td>
                            <td><?php echo $att_time_in; ?><input type="hidden" name="timein[]" value="<?php echo $att_time_in; ?>" /></td>
                            <td><?php echo $att_time_out; ?><input type="hidden" name="timeout[]" value="<?php echo $att_time_out; ?>" /></td>
                            <td><?php echo $status; ?><!--<input type="hidden" name="status[]" value="<?php //echo $status1;    ?>" />--></td>
                            <!--<td><?php /*echo @$late_minute_count; */?></td>-->
                            <td style="width:250px">
                                <input type="text" name="comment[]" style="width:180px" value="<?php echo $dateComment; ?>" />
                                <input type="hidden" name="year" value="<?php echo $year; ?>" style="width:180px" />
                                <input type="hidden" name="emp_id" value="<?php echo $empId; ?>" style="width:180px" />
                                <input type="hidden" name="month" value="<?php echo $month; ?>" style="width:180px" />
                            </td>
                        </tr>
                        <?php
                        $late_minute_count="";
                    }
                    //end forloop
                    ?>
                    <tr>
                        <td style="width:250px">
                            <?php
                            $s_status = "";
                            $query = $this->db->query("select * from submit_control");
                            foreach ($query->result() as $t):
                                $s_status = $t->status;
                            endforeach;
                            ?>
                            <!-- <input type="submit"  value="Submit"  <?php //if ($s_status == '2'):   ?> disabled="disabled" <?php // endif;   ?>>-->
                            <!-- </form>-->
                        </td>
                    </tr>
                    </tbody>
                </table>
                <div class="text-center">
                    <?php echo $this->pagination->create_links(); ?>
                </div>
                <div class="text-center">
                    <?php
                    echo "Total Records: ".$this->pagination_library->total_rows." page ".(($this->pagination->cur_page) ? $this->pagination->cur_page : "1")." of ". ceil(($this->pagination_library->total_rows)/($this->pagination_library->per_page));
                    ?>
                </div>
            </section>
        </div><! --/content-panel -->
    </div><!-- /col-md-12 -->
</div><!-- row -->