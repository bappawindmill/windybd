<h3><i class="fa fa-angle-right"></i> Windmill Group:</h3>
<?php
$sup = array(
    '' => 'Select',
    '1' => 'Approve',
    '2' => 'Deny'
);
$hr = array(
    '' => 'Select',
    '1' => 'Approve',
    '2' => 'Deny',
    '3' => 'Not Employee'
);
?>
<div class="row">
    <div class="col-sm-12">
        <div class="content-panel">
            <!--<hr>-->
            <div class="table-responsive">
                <table class="table table-hover table-bordered">
                    <thead>
                    <tr>
                        <th>Date</th>
                        <th>Entry time</th>
                        <th>Exit time</th>
                        <th>Status</th>
                        <!--<th>Late Time</th>-->
                        <th>EmpRemarks</th>
                        <th>SupRemarks</th>
                        <th>SupStatus</th>
                        <th>Hr Remarks</th>
                        <th>Hr Status</th>
                    </tr>
                    </thead>
                    <tbody>
                    <?php
                    $c = 1;
                    if ($month == '01') {
                        $total_month_day = 31;
                    } elseif ($month == '02') {
                        $total_month_day = 28;
                    } elseif ($month == '03') {
                        $total_month_day = 31;
                    } elseif ($month == '04') {
                        $total_month_day = 30;
                    } elseif ($month == "05") {
                        $total_month_day = 31;
                    } elseif ($month == "06") {
                        $total_month_day = 30;
                    } elseif ($month == "07") {
                        $total_month_day = 31;
                    } elseif ($month == "08") {
                        $total_month_day = 31;
                    } elseif ($month == "09") {
                        $total_month_day = 30;
                    } elseif ($month == "10") {
                        $total_month_day = 31;
                    } elseif ($month == "11") {
                        $total_month_day = 30;
                    } elseif ($month == "12") {
                        $total_month_day = 31;
                    } else {
                        $total_month_day = 30;
                    }
                    $total_start_time = "";
                    $total_end_time = "";
                    $total_late_time = 0;
                    $duty_schedule = 0;
                    $empSchedule = $this->attendence_model->getEmpInfo($empId);
                    foreach ($empSchedule as $eInfo):
                        $duty_schedule = $eInfo->duty_schedule;
                    endforeach;
                    $scheduleInfo = $this->attendence_model->getSchedule($duty_schedule);
                    if (count($scheduleInfo)):
                        foreach ($scheduleInfo as $sInfo):
                            $start_time = $sInfo->start_time;
                            $end_time = $sInfo->end_time;
                            $late_time = $sInfo->late_time;
                        endforeach;
                        //convert start time to minute
                        $explode_start_time = explode(":", $start_time);
                        $start_hour = $explode_start_time[0];
                        $start_hour_to_minute = $start_hour * 60;
                        $start_minute = $explode_start_time[1];
                        $total_start_time = $start_hour_to_minute + $start_minute;
                        //convert end time to minute
                        $explode_end_time = explode(":", $end_time);
                        $end_hour = $explode_end_time[0];
                        $end_hour_to_minute = $end_hour * 60;
                        $end_minute = $explode_end_time[1];
                        $total_end_time = $end_hour_to_minute + $end_minute;

                        //convert Late time to minute
                        $explode_late_time = explode(":", $late_time);
                        $late_hour = $explode_late_time[0];
                        $late_hour_to_minute = $late_hour * 60;
                        $late_minute = $explode_late_time[1];
                        $total_late_time = $late_hour_to_minute + $late_minute;

                    endif;

                    $leave = 0;
                    $holiday = 0;
                    $weekend = 0;
                    $absent = 0;
                    $present = 0;
                    $late = 0;
                    //start for loop
                    for ($i = 1; $i <= $total_month_day; $i++) {
                        if ($i <= 9) {
                            $day = "0" . $i;
                        } else {
                            $day = $i;
                        }
                        $sdate = $year . "-" . $month . "-" . $day . "";
                        $cardNo = $secrectNo;
                        $branch = $branch_name;

                        //Retrive In time data
                        $this->load->model("user/attendence_model");
                        $timeInInfo = $this->attendence_model->getTimeLog($empId, $sdate);
                        //echo '<pre>';print_r($timeInInfo);exit;
                        $att_time_in = "";
                        $att_time_out = "";
                        $totalInTime = "";
                        $comment = "";
                        $sup_comment = "";
                        $status = "";
                        $hr_comment = "";
                        foreach ($timeInInfo as $row3) {
                            $att_time_in = $row3->timein;
                            $att_time_out = $row3->timeout;
                            $comment = $row3->emp_comment;
                            $sup_comment = $row3->sup_comment;
                            $hr_comment = $row3->hr_comment;
                        }

                        if ($att_time_in != "") {
                            $explodeTimeIn = explode(":", $att_time_in);
                            $InHour = $explodeTimeIn[0];
                            $InHourToMinute = $InHour * 60;
                            $InHourminute = $explodeTimeIn[1];
                            $totalInTime = $InHourToMinute + $InHourminute;
                        }
                        //check weekend,holiday and leave

                        $holidayInfo = $this->attendence_model->getHoliday($sdate);
                        if (count($holidayInfo)) {
                            $holiday = $holiday + 1;
                            $status = "<font style='color:#006633'>Holiday</font>";
                            $status1 = "holiday";
                        } else {
                            $weekendInfo = $this->attendence_model->getWeekend($sdate, $branch);
                            if (count($weekendInfo)) {
                                $weekend = $weekend + 1;
                                $status = "<font style='color:#006633'>Weekend</font>";
                                $status1 = "weekend";
                            } else {
                                $leaveInfo = $this->attendence_model->getLeaveInfo($sdate, $empId);
                                if (count($leaveInfo)) {
                                    $leave = $leave + 1;
                                    $status = "<font style='color:green'>leave</font>";
                                    $status1 = "leave";
                                } else {
                                    if ($att_time_in == "" && $att_time_out == "") {
                                        $absent = $absent + 1;
                                        $status = "<font style='color:red'>Absent</font>";
                                        $status1 = "absent";
                                    } else {
                                        $present = $present + 1;
                                        $status = "<font style='color:#990033'>Present</font>";
                                        $status1 = "Present";
                                        if ($total_late_time != 0 && $totalInTime != ""){
                                            if ($totalInTime > $total_late_time) {
                                                $late = $late + 1;
                                                $late_minute_count = $totalInTime - $total_late_time ;
                                                if($late_minute_count >59){
                                                    $minutes = $late_minute_count;
                                                    $zero    = new DateTime('@0');
                                                    $offset  = new DateTime('@' . $minutes * 60);
                                                    $diff    = $zero->diff($offset);
                                                    $late_minute_count= $diff->format('%h Hours, %i Minutes');
                                                }else{
                                                    $minutes = $late_minute_count;
                                                    $zero    = new DateTime('@0');
                                                    $offset  = new DateTime('@' . $minutes * 60);
                                                    $diff    = $zero->diff($offset);
                                                    $late_minute_count= $diff->format('%i Minutes');
                                                }
                                                // $late_duration = $timein - $total_late_time;
                                                //$total_late_minute = $total_late_minute + $late_duration;

                                                $status = "<font style='color:#990033'>Late</font>";
                                                $status1 = "late";
                                            }
                                        }
                                    }
                                }
                            }
                        }
                        //end weelend,holiday,leave check
                        ?>
                        <tr class=" <?php echo $i ?>">
                            <td><?php echo $sdate; ?></td>
                            <td><?php echo $att_time_in; ?></td>
                            <td><?php echo $att_time_out; ?></td>
                            <td><?php echo $status; ?></td>
                            <!--<td><?php /*echo @$late_minute_count; */?></td>-->
                            <td><?php echo $comment; ?></td>
                            <td><?php echo $sup_comment; ?></td>

                            <td><?php echo form_dropdown('sup_status', $sup, $row3->sup_approve, 'class="form-control" disabled="disabled"'); ?></td>

                            <td style="width:250px"><?php echo $hr_comment; ?></td>
                            <td><?php echo form_dropdown('hr_status', $hr, $row3->hr_approve, 'class="form-control" disabled="disabled"'); ?></td>

                        </tr>
                        <?php
                        $late_minute_count="";
                    }
                    //end forloop
                    ?>
                    <td>

                    </td>
                    </tbody>
                </table>
            </div>
        </div><! --/content-panel -->
    </div><!-- /col-md-12 -->
</div><!-- row -->