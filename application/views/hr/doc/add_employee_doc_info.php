<h3><i class="fa fa-angle-right"></i> Create New Section</h3>

<!-- BASIC FORM ELELEMNTS -->
<div class="row">
    <div class="col-sm-12">
        <div class="content-panel">
            <?php
                echo form_open_multipart('hr/doc/added_employee_doc_info');?>
            <?php
            if($this->session->flashdata('message')==NULL){

            }else{ ?>
                <div class="alert-success alert">
                    <?php echo $this->session->flashdata('message') ?>
                </div> <?php
            }
            ?>
            <?php
            $query12 = $this->db->query("SELECT id,branch_name FROM add_branch");
            ?>
            <div class="col-md-12">
                <div class="form-group col-sm-4">
                    <label for="crlbranch">Employee ID No :</label>
                    <input class="form-control" name="card_no" />
                    <div class="alert-danger"><?php echo ucwords(form_error('card_no')); ?></div>
                </div>
                <div class="form-group col-sm-4">
                    <label for="">Document Title :</label>
                        <input class="form-control" name="doc_title" />
                    <div class="alert-danger"><?php echo ucwords(form_error('doc_title')); ?></div>
                </div>
                <div class="form-group col-sm-4">
                    <label for="">Attach Document :</label>
                    <input type="file" name="userfile"  />
                    <div class="alert-danger"><?php echo ucwords(form_error('userfile')); ?></div>
                </div>
            </div>
            <span>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
                <input type="hidden" name="id" value="" />&nbsp;&nbsp;&nbsp;&nbsp;
                <input type="submit" name="mysubmit" value="Submit"  class="btn btn-primary">
                <a class="btn btn-theme" href="<?php echo base_url(); ?>hr/doc/employee_doc_info_list">Cancel</a>
            </span>

            </form>
            <p></p>
        </div>
    </div><!-- col-lg-12-->
</div><!-- /row -->