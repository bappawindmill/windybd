<script language="javascript">
    function confirmSubmit() {
        var agree = confirm("Are you sure to delete this record?");
        if (agree)
            return true;
        else
            return false;
    }
</script>

<h3><i class="fa fa-angle-right"></i> Employee Document Information</h3>
<?php
if($this->session->flashdata('message')==NULL){

}else{ ?>
    <div class="alert-success alert">
        <?php echo $this->session->flashdata('message') ?>
    </div> <?php
}
?>


<div class="row">
    <div class="col-sm-12 col-lg-12">
        <div class="pull-left">
            <i class="fa"></i>
            <a class="btn btn-info" href="<?php echo base_url(); ?>hr/doc/add_employee_doc_info"   title="">Add  New </a>
        </div>
        <div class=" pull-right">
            <form class="form-inline">
                <div class="form-group has-feedback pull-right">
                    <input type="text" id="searchKey" name="search" class="form-control" placeholder="Search" />
                    <i class="glyphicon glyphicon-search form-control-feedback"></i>
                </div>
            </form>
        </div>
        <div class="pull-right"><a href="<?php echo base_url();?>hr/doc/employee_doc_info_list" class="btn btn-info" role="button" >Reset</a>&nbsp;</div>
    </div>

    <div class="col-sm-12">
        <div class="content-panel">
            <!--<section id="flip-scroll">-->
            <section id="">
                <table  id="myTable"  class="table table-hover table-bordered">
                    <thead>
                    <tr class="info">
                        <th>SL</th>
                        <th>Emp.Name</th>
                        <th>ID No</th>
                        <th>Document </th>
                        <th colspan="2">Action</th>
                    </tr>
                    </thead>
                    <tbody>
                    <?php
                    $i = 'even';
                    $c = 1;
                    //show($query1);
                    foreach ($query1 as $row) {
                        if ($i == 'even') {
                            $i = 'odd';
                        } else {
                            $i = 'even';
                        }
                        $emp_name = "";
                        $card_no="";
                        $query1 = $this->db->query("select * from employeeinfo where id='" . $row->emp_id . "'");
                        foreach ($query1->result() as $row1) {
                            $emp_name = $row1->name;
                            $card_no = $row1->card_no;
                        }
                        echo "<tr>";
                        echo "<td>" . $c . "</td>";
                        echo "<td>" . $emp_name . "</td>";
                        echo "<td>" . $card_no . "</td>";
                        ?>
                        <td><a href="<?php echo base_url() . 'doc/' . $row->doc_name; ?>" ><?php echo $row->doc_title; ?></a>

                        <td>
                            <a class="btn btn-danger btn-xs" onclick="return confirmSubmit()" href="<?php echo base_url() ?>hr/doc/delete/<?php echo $row->doc_id; ?>" title="Delete the Content">Delete</a>
                        </td>
                        </tr>
                        <?php
                        $c++;
                    }
                        ?>
                    </tbody>
                </table>
                <div class="text-center">
                    <?php echo $this->pagination->create_links(); ?>
                </div>
                <div class="text-center">
                    <?php
                    echo "Total Records: ".$this->pagination_library->total_rows." page ".(($this->pagination->cur_page) ? $this->pagination->cur_page : "1")." of ". ceil(($this->pagination_library->total_rows)/($this->pagination_library->per_page));
                    ?>
                </div>
            </section>
        </div><! --/content-panel -->
    </div><!-- /col-md-12 -->
</div><!-- row -->