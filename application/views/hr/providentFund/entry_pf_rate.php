<h3><i class="fa fa-angle-right"></i> Entry Provident Fund Rate</h3>

<!-- BASIC FORM ELELEMNTS -->
<div class="row">
    <div class="col-sm-12">
        <div class="content-panel">
            <?php
            echo form_open('hr/provident_fund/entry');?>
            <div class="alert-danger"><?php echo $this->session->flashdata('message'); ?> </div>
            <div class="col-md-12">
                <div class="form-group col-sm-4">
                    <label for="employee_rate">Employee Rate :</label>
                    <input value=""  type="text" name="employee_rate" id="employee_rate" class="form-control" placeholder="%">
                    <div class="alert-danger"><?php echo ucwords(form_error('employee_rate')); ?></div>
                </div>

                <div class="form-group col-sm-4">
                    <label for="name">Employeer Rate:</label>
                    <input value=""  type="text" name="employeer_rate" id="employeer_rate" class="form-control" placeholder="%">
                    <div class="alert-danger"><?php echo ucwords(form_error('employeer_rate')); ?></div>
                </div>
                <div class="form-group col-sm-4">
                    <label for="name">Maturity:</label>
                    <input value=""  type="text" name="maturity" id="maturity" class="form-control" placeholder="days">
                    <div class="alert-danger"><?php echo ucwords(form_error('maturity')); ?></div>
                </div>
            </div>
                <span>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
                    <input type="hidden" name="id" value="" />&nbsp;&nbsp;&nbsp;&nbsp;
                    <input type="submit" name="mysubmit" value="Submit"  class="btn btn-primary">
                </span>
            </form>
            <p></p>
        </div>
    </div><!-- col-lg-12-->
</div><!-- /row -->