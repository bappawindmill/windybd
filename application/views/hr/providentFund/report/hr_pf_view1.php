<link rel="stylesheet" type="text/css" href="<?php echo base_url(); ?>public/css/t1.css" />
<style type="text/css">
    <!--
    .style1 {color: #FF0000}
    -->
</style>

<script type="text/javascript" src="<?php echo base_url(); ?>/public/js/jquery.jqprint.0.3.js"></script>
<script>
    $(function() {
        $("#print_button1").click(function() {

            $("#invoice").jqprint();
        });
    });

</script>

<div class="grid_12">
    <p><input type="button" id="print_button1" value="Print" style="margin-top:5px; ;"><br /><br /></p>
    <div class="module1">
        <h2><span>View Provident Fund Report</span></h2>
        <?php echo $this->session->flashdata('message'); ?>
        <div class="module-body" id="invoice">
            <style>
                #view-pf{ height:auto; width:100%; margin:15px 0; text-align:left; float:left;}
            </style>
            <?php
            if (count($query) > 0):
                $b_name = "";
                $dept_nm = "";
                foreach ($query as $row):
                    $branch = $this->db->query("select id,branch_name from add_branch where id ='" . $row->branch_name . "'");
                    foreach ($branch->result() as $row11):
                        $b_name = ucwords($row11->branch_name);
                    endforeach;
                    $dep = $this->db->query("select id,name from add_department where id ='" . $row->dept . "'");
                    foreach ($dep->result() as $row22):
                        $dept_nm = ucwords($row22->name);
                    endforeach;
                    ?>
                <?php endforeach; ?>


                <div id="view-pf">
                    <table width="60%" style="float:left;" border="1">
                        <tr>
                            <th scope="row" width="30%">Name</th>
                            <td><?php echo ucwords($row->name); ?></td>
                        </tr>
                        <tr>
                            <th scope="row">Branch</th>
                            <td><?php echo $b_name; ?></td>
                        </tr>
                        <tr>
                            <th scope="row">Department</th>
                            <td><?php echo $dept_nm; ?></td>
                        </tr>
                        <tr>
                            <th scope="row">Previous Balance</th>
                            <td><?php echo $row->employee_pf_balance; ?></td>
                        </tr>


                    </table>
                </div>
                <div id="view-pf">
                    <table width="100%" border="1">
                        <tr>
                            <th scope="col">SL</th>
                            <th scope="col">Year</th>
                            <th scope="col">Month</th>
                            <th scope="col">Pf Amount</th>

                        </tr>
                        <?php
                        $total_em = 0;
                        $total_emr = 0;
                        $total_pf = 0;
                        $c = 1;
                        /* echo '<pre>';
                        var_dump($allData); */
                        foreach ($allData as $rows):
                            if ($rows->employee_amount > 0) {
                                ?>
                                <tr>
                                    <td align="right"><?php echo $c; ?></td>
                                    <td align="right"><?php echo $rows->year; ?></td>
                                    <td align="right"><?php
                                        $months = array('01' => 'January',
                                            '02' => 'February',
                                            '03' => 'March',
                                            '04' => 'April',
                                            '05' => 'May',
                                            '06' => 'June',
                                            '07' => 'July',
                                            '08' => 'August',
                                            '09' => 'September',
                                            '10' => 'October',
                                            '11' => 'November',
                                            '12' => 'December',
                                        );
                                        $monTh = "";
                                        foreach ($months as $key => $val) {
                                            if ($key == $rows->month) {
                                                $monTh = $val;
                                            }
                                        }
                                        ?>
                                        <?php echo $monTh; ?></td>
                                    <td align="right"> <?php echo round($rows->employee_amount); ?></td>


                                </tr>
                                <?php
                                $total_em = round($rows->employee_amount) + $total_em;
                                $c++;
                            }
                        endforeach;
                        ?>

                        <tr>
                            <td align="right" colspan="3" style="font-weight:bold; text-align: center">Total</td>
                            <td align="right"><?php echo $total_em; ?></td>

                        </tr>
                        <tr>
                            <td align="right" colspan="3" style="font-weight:bold; text-align: center">Grand Total</td>
                            <td align="right"><?php echo $total_em + round($row->employee_pf_balance); ?></td>

                        </tr>
                    </table>
                </div>

            <?php endif; ?>

        </div>
    </div></div>
<div style="clear: both;"></div>