
<h3>View Provident Fund Report</h3>
<h5><input type="button" id="print_button1" value="Print" /></h5>
<?php
if (count($emp_profile_info) > 0):
    $b_name = "";
    $dept_nm = "";
    foreach ($emp_profile_info as $row):
        //$branch = $this->db->query("select id,branch_name from add_branch where id ='" . $row->branch_name . "'");
        $branch_info = $this->provident_fund_model->getEmpBranchInfo($row->branch_name);
        foreach ($branch_info as $row11):
            $b_name = ucwords($row11->branch_name);
        endforeach;
        //$dep = $this->db->query("select id,name from add_department where id ='" . $row->dept . "'");
        $dept_info = $this->provident_fund_model->getEmpDeptInfo($row->dept);
        /*echo '<pre>';
        var_dump($dept_info);
        echo '</pre>';
        exit;*/
        foreach ($dept_info as $row22):
            $dept_nm = ucwords($row22->name);
        endforeach;
        ?>
    <?php endforeach; ?>
<div class="module-body" id="invoice">
    <!-- <table width="100%" border="1">
        <tr>
            <th scope="col">SL</th>
            <th scope="col">Year</th>
            <th scope="col">Month</th>
            <th scope="col">Pf Amount</th>

        </tr>
        </table> -->
    <div class="row">
        <div class="col-sm-12">
            <div class="content-panel">
                <!--<hr>-->
                <div class="table-responsive">
                    <table class="table table-bordered">
                        <thead>
                        <tr class="tbl_header_bg_color">
                            <th>SL</th>
                            <th>Year</th>
                            <th>Month</th>
                            <th>Pf Amount</th>
                        </tr>
                        </thead>
                        <tbody>
                        <?php
                        $total_em = 0;
                        $total_emr = 0;
                        $total_pf = 0;
                        $c = 1;
                        /* echo '<pre>';
                        var_dump($allData); */
                        foreach ($allData as $rows):
                            if ($rows->employee_amount > 0) {

                                $months = array(
                                    '01' => 'January',
                                    '02' => 'February',
                                    '03' => 'March',
                                    '04' => 'April',
                                    '05' => 'May',
                                    '06' => 'June',
                                    '07' => 'July',
                                    '08' => 'August',
                                    '09' => 'September',
                                    '10' => 'October',
                                    '11' => 'November',
                                    '12' => 'December',
                                );
                                $monTh = "";
                                foreach ($months as $key => $val) {
                                    if ($key == $rows->month) {
                                        $monTh = $val;
                                    }
                                }
                                ?>
                                <tr>
                                    <td><?php echo $c;?></td>
                                    <td><?php echo $rows->year;?></td>
                                    <td><?php echo $monTh;?></td>
                                    <td><?php echo round($rows->employee_amount);?></td>
                                </tr>
                                <?php
                                $total_em = round($rows->employee_amount) + $total_em;
                                $c++;
                            }
                        endforeach;
                        ?>
                        <tr>
                            <th colspan="3" class="text-center">Total</th>
                            <td colspan="3"><?php echo $total_em; ?></td>
                        </tr>
                        <tr>
                            <th colspan="3" class="text-center">Grand Total</th>
                            <td colspan="3"><?php echo $total_em + round($row->employee_pf_balance); ?></td>
                        </tr>
                        </tbody>
                    </table>

                </div>
            </div><! --/content-panel -->
        </div><!-- /col-md-12 -->
    </div><!-- row -->
</div>
<?php endif; ?>

