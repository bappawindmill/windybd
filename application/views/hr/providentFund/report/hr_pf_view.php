
        <h2><span>View Provident Fund Report</span></h2>
        <input type="button" id="print_button" value="Print" style="margin-top:5px;">
        <?php echo $this->session->flashdata('message'); ?>

            <style>
                #view-pf{ height:auto; width:100%; margin:15px 0; text-align:left; float:left;}
            </style>
        <div id="invoice">
            <?php
            if (count($query) > 0):
                $b_name = "";
                $dept_nm = "";
                foreach ($query as $row):

                    $branch = $this->db->query("select id,branch_name from add_branch where id ='" . $row->branch_name . "'");
                    foreach ($branch->result() as $row11):
                        $b_name = ucwords($row11->branch_name);
                    endforeach;
                    $dep = $this->db->query("select id,name from add_department where id ='" . $row->dept . "'");
                    foreach ($dep->result() as $row22):
                        $dept_nm = ucwords($row22->name);
                    endforeach;
                    ?>
                <?php endforeach; ?>


                <div id="view-pf">
                    <table class="table table-bordered">
                        <tr>
                            <th scope="row" width="30%">Name</th>
                            <td><?php echo ucwords($row->name); ?></td>
                        </tr>
                        <tr>
                            <th scope="row">Branch</th>
                            <td><?php echo $b_name; ?></td>
                        </tr>
                        <tr>
                            <th scope="row">Depertment</th>
                            <td><?php echo $dept_nm; ?></td>
                        </tr>
                        <tr>
                            <th scope="row">Employee Previous Balance</th>
                            <td><?php echo $row->employee_pf_balance ?></td>
                        </tr>
                        <tr>
                            <th scope="row">Employeer Previous Balance</th>
                            <td><?php echo $row->employeer_pf_balance; ?></td>
                        </tr>
                        <tr>



                        </tr>
                    </table>
                </div>
                <div id="view-pf">
                    <table class="table table-bordered">
                        <tr>
                            <th>SL</th>
                            <th>Year</th>
                            <th>Month</th>
                            <th>Employee Amount</th>
                            <th>Employeer Amount</th>
                            <th>Total Provident Fund Amount</th>

                        </tr>
                        <?php
                        $total_em = 0;
                        $total_emr = 0;
                        $total_pf = 0;
                        $c = 1;
                        foreach ($allData as $rows):
                            if ($rows->employee_amount > 0 && $rows->employeer_amount > 0) {
                                ?>
                                <tr>
                                    <td><?php echo $c; ?></td>
                                    <td><?php echo $rows->year; ?></td>
                                    <td><?php
                                        $months = array('01' => 'January',
                                            '02' => 'February',
                                            '03' => 'March',
                                            '04' => 'April',
                                            '05' => 'May',
                                            '06' => 'June',
                                            '07' => 'July',
                                            '08' => 'August',
                                            '09' => 'September',
                                            '10' => 'October',
                                            '11' => 'November',
                                            '12' => 'December',
                                        );
                                        $monTh = "";
                                        foreach ($months as $key => $val) {
                                            if ($key == $rows->month) {
                                                $monTh = $val;
                                            }
                                        }
                                        ?>
                                        <?php echo $monTh; ?></td>
                                    <td> <?php echo round($rows->employee_amount); ?></td>
                                    <td> <?php echo round($rows->employeer_amount); ?></td>
                                    <td> <?php echo round($rows->total_pf_amount); ?></td>

                                </tr>
                                <?php
                                $total_em = round($rows->employee_amount) + $total_em;
                                $total_emr = round($rows->employeer_amount) + $total_emr;
                                $total_pf = round($rows->total_pf_amount) + $total_pf;
                                $c++;
                            }
                        endforeach;
                        ?>

                        <tr>
                            <td colspan="3">Total</td>
                                

                            <td  style="font-weight:bold;"> <?php echo $total_em; ?></td>
                            <td style="font-weight:bold;"> <?php echo $total_emr; ?></td>
                            <td style="font-weight:bold;"> <?php echo $total_pf; ?></td>

                        </tr>
                        <tr>
                            <td colspan="3">Grand Total</td>

                            <td> <?php echo $total_em + round($row->employee_pf_balance); ?></td>
                            <td> <?php echo $total_emr + round($row->employeer_pf_balance); ?></td>
                            <td> <?php echo $total_pf + round($row->employee_pf_balance) + round($row->employeer_pf_balance); ?></td>

                        </tr>
                    </table>

                </div>
            <?php endif; ?>
        </div>
