<style>
    table tr{ border:1px solid #EEEEEE;}
    table tr th{font-size: 11px; font-weight: bold; text-align: center}
    table tr td{font-size: 11px;text-align: center }
</style>
<script type="text/javascript" src="<?php echo base_url(); ?>/public/js/jquery.jqprint.0.3.js"></script>
<script>
    $(function(){
        $("#print_button").click( function() {
            $("#invoice").jqprint();
        });
    });
</script>
<input type="button" id="print_button" value="Print" style="margin-top:5px; ;">
<div class="module">

    <div style="width:800px;margin-left:50px" id="invoice">

        <div style="width:800px; height:60px">
            <div style="width:800px; text-align: center; font-size: 18px; font-weight: bold; color:blue">  Golden Son Limited</div>
            <div style="width:800px;text-align: center;font-size: 10px; color:blue">Khoagnagor, Azimpara, Karnnofuly, Chittagong</div>
            <div style="width:800px;font-weight: bold;text-align: left;font-size: 14px; color:black;margin-top: 15px">Daily Absent Report For <?php echo $sdate; ?>
                <span style=" font-size: 12px; font-weight: normal; margin-left: 270px">  Reporting Date: &nbsp; <?php echo $sdate; ?></span>
            </div>

        </div>
        <div style="width:800px;">
            <?php
            $g_total = 0;
            foreach ($query1 as $section_info) {
                $section = $section_info->id;
                //get all employee
                $this->load->model('hr/attendence_model');
                $this->data['employee_info'] = $this->attendence_model->get_all_employee($section, $shift);
                $absent = 0;
                foreach ($this->data['employee_info'] as $e_info) {
                    $r = $this->db->query("select * from attendence where card_no='" . $e_info->card_no . "'and att_date='" . $sdate . "'");
                    if ($r->num_rows() == 0) {
                        $absent = $absent + 1;
                    }
                }
                //start absent checking
                if ($absent > 0) {
                    ?>
                    <table border="1" style="margin-bottom:10px">
                        <tr>
                            <?php
                            $department = "";
                            $d = $this->db->query("select name from add_department where id='" . $section_info->department . "'");
                            foreach ($d->result() as $d_name):
                                $department = $d_name->name;
                            endforeach;
                            ?>  
                            <th colspan="6">
                                Department & Section Name:&nbsp;&nbsp;  <?php echo $department; ?> &nbsp;&nbsp;&nbsp; & &nbsp; <?php echo $section_info->proffession; ?>  
                            </th>
                        </tr>
                        <tr>
                            <th style="text-align: center">SL No</th> 
                            <th style="text-align: center">Card No</th> 
                            <th style="text-align: center">Name</th> 
                            <th style="text-align: center">Designation</th> 
                            <th style="text-align: center">Joining Date</th> 
                            <th style="text-align: center">Remarks</th> 
                        </tr>
                        <?php
                        $n = 1;
                        foreach ($this->data['employee_info'] as $emp_info) {
                            $r = $this->db->query("select * from attendence where card_no='" . $e_info->card_no . "'and att_date='" . $sdate . "'");
                            if ($r->num_rows() == 0) {
                                $deg = "";
                                $t = $this->db->query("select designation from designation where id='" . $emp_info->designation . "'");
                                foreach ($t->result() as $d2):
                                    $deg = $d2->designation;
                                endforeach;
                                echo "<tr>";
                                echo "<td>" . $n . "</td>";
                                echo "<td>" . $emp_info->card_no . "</td>";
                                echo "<td>" . $emp_info->name . "</td>";
                                echo "<td>" . $deg . "</td>";
                                echo "<td>" . $emp_info->join_date . "</td>";
                                echo "<td></td>";

                                echo "</tr>";
                                $n = $n + 1;
                            }
                        }
                        ?>
                    </table>
                    <p style="margin-top:10px; padding-left: 50px; font-weight: bold; margin-bottom: 10px"> Total No. of Absent :<?php echo $n - 1; ?></p>
                    <?php
                    $g_total = $g_total + ($n - 1);
                }
                //end checking
                $absent = 0;
            }
            ?>  
            <p style="margin-top:10px; padding-left: 50px;  margin-bottom: 10px">Grand Total No. of Absent :<?php echo $g_total; ?></p>
        </div>
    </div>
    <div style="width:800px; margin-top:50px; margin-bottom: 20px">
        <div style="width:250px; float: left; text-align: center; position:relative">
            <span style="border-top:1px solid black">Prepared By</span>
        </div> 
        <div style="width:250px; float: left;text-align: center; position:relative">
            <span style="border-top:1px solid black"> Checked By</span>
        </div> 
        <div style="width:250px; float: left;text-align: center; position:relative">
            <span style="border-top:1px solid black">   Approved By</span>
        </div> 
    </div>
    <div style="width:800px; clear: both; margin-top: 10px; height: 30px"></div>


</div>
<div style="clear: both;"></div>

