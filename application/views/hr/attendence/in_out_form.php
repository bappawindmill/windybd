<link rel="stylesheet" type="text/css" href="<?php echo base_url(); ?>public/css/t1.css" />
<style type="text/css">
    <!--
    .style1 {color: #FF0000}
    -->
</style>
<script type="text/javascript">
    window.onload = function(){
        new JsDatePick({
            useMode:2,
            target:"start_date",
            dateFormat:"%Y-%m-%d"
        });
        new JsDatePick({
            useMode:2,
            target:"end_date",
            dateFormat:"%Y-%m-%d"
        });
    }
</script>
<div class="grid_12">
    <div class="module1">
        <h2><span>Daily Missing In/Out Report</span></h2>
        <?php echo $this->session->flashdata('message'); ?>
        <div class="module-body">
            <?php
            $query3 = $this->db->query("select id,branch_name from add_branch");
            ?>
            <?php
            echo form_open('hr/attendence/added_in_out');

            echo '<table>';
            echo '<thead><tr><th colspan="2"><b></b></th></tr></thead>';
            ?>


            <tr><th>Branch Name :</th><td>
                    <select name="branch_name" id="crlbranch" class="input-short" style="width:130px;">
                        <option   value="">Select</option>									
                        <?php foreach ($query3->result() as $row3): ?>
                            <option value="<?php echo $row3->id; ?>"><?php echo $row3->branch_name; ?></option>
                        <?php endforeach; ?>
                    </select>
                    <span class="notification-input ni-error"> <?php echo ucwords(form_error('branch_name')); ?></span>
                </td></tr>   
            <tr><th>department :</th><td><div id='res'>
                        <select style="width:130px;">
                            <option  value="">Select</option>
                        </select>

                    </div><span class="notification-input ni-error"><?php echo ucwords(form_error('dept')); ?></span></td></tr>
            <tr><th>section :</th><td><div id='result'>
                        <select style="width:130px;">
                            <option  value="">Select</option>
                        </select>

                    </div><span class="notification-input ni-error"><?php echo ucwords(form_error('proffession')); ?></span></td></tr>
            <?php $query55 = $this->db->query("SELECT id,shift_name FROM shift"); ?>
            <tr><th>Shift Name :</th><td>
                    <select name="shift_name">
                        <option value="">Select</option>
                        <?php foreach ($query55->result() as $q5): ?>
                            <option value="<?php echo $q5->id; ?>"><?php echo $q5->shift_name; ?></option>
                        <?php endforeach; ?>

                    </select>
                    <span class="notification-input ni-error"><?php echo ucwords(form_error('shift_name')); ?></span>
                </td></tr>

            <tr><th>Date :</th><td>
                    <input type="text" style="width:130px" name="start_date" id="start_date" class="input-short" />
                    <span class="notification-input ni-error"><?php echo ucwords(form_error('start_date')); ?></span></td></tr>
            <?php
            echo '<tr><th></th><td>' . form_submit('mysubmit', 'Submit') . '</td></tr>';

            echo '</table>';
            echo form_close();
            ?>
        </div></div></div>
<div style="clear: both;"></div>
