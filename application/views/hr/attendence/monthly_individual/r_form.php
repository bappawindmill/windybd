<link rel="stylesheet" type="text/css" href="<?php echo base_url(); ?>public/css/t1.css" />
<style type="text/css">
    <!--
    .style1 {color: #FF0000}
    -->
</style>
<script type="text/javascript">
    window.onload = function(){
        new JsDatePick({
            useMode:2,
            target:"start_date",
            dateFormat:"%Y-%m-%d"
        });
        new JsDatePick({
            useMode:2,
            target:"end_date",
            dateFormat:"%Y-%m-%d"
        });
    }
</script>
<div class="grid_12">
    <div class="module1">
        <h2><span>View Attendance</span></h2>
        <?php echo $this->session->flashdata('message'); ?>
        <div class="module-body">
            <?php
            echo form_open('hr/attendence/viewed_attendence1');
            $months = array(
                '' => 'Select',
                '01' => 'January',
                '02' => 'February',
                '03' => 'March',
                '04' => 'April',
                '05' => 'May',
                '06' => 'June',
                '07' => 'July',
                '08' => 'August',
                '09' => 'September',
                '10' => 'October',
                '11' => 'November',
                '12' => 'December',
            );
            // echo $dataStr120;
            ?> 
            <table>
			
			       <tr><th>EMPLOYEE ID No :</th> <td>
                       <input type="text" name="card_no" />
                        <span class="notification-input ni-error"> <?php echo ucwords(form_error('card_no')); ?></span>       
                    </td></tr>
                   <tr><th>Year :</th><td>
                        <select name="year" style="width:150px;">
                            <?php for ($i = 2012; $i <= date('Y'); $i++) { ?>                
                                <option value="<?php echo $i; ?> "><?php echo $i; ?></option>
                            <?php } ?>                
                        </select>
                        <span class="notification-input ni-error"> <?php echo ucwords(form_error('year')); ?></span>
                    </td></tr> 

                <tr><th>Month :</th> <td>
                        <?php echo form_dropdown('month', $months, date('m')); ?>
                        <span class="notification-input ni-error"> <?php echo ucwords(form_error('month')); ?></span>       
                    </td></tr>
					
                <?php
                echo '<tr><th></th><td>' . form_submit('mysubmit', 'Submit') . '</td></tr>';

                echo '</table>';
                echo form_close();
                ?>
        </div></div></div>
<div style="clear: both;"></div>