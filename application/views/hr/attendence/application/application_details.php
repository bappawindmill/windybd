<h3><i class="fa fa-angle-right"></i> Windmill Group:</h3>
<?php
$sup = array(
    '' => 'Select',
    '1' => 'Approve',
    '2' => 'Deny'
);
$hr = array(
    '' => 'Select',
    '1' => 'Approve',
    '2' => 'Deny',
    '3' => 'Not Employee'
);
?>
<div class="row">
    <div class="col-sm-12">
        <div class="content-panel">
            <!--<hr>-->
            <div class="table-responsive">
                <table class="table table-bordered table-hover">
                    <thead>
                    <tr class="info">
                        <th >Date</th>
                        <th >Entry time</th>
                        <th >Exit time</th>
                        <th >Status</th>
                        <th >Emp Remarks</th>
                        <th >Sup Remarks</th>
                        <th >Sup Status</th>
                        <th >Hr Remarks</th>
                        <th >Hr Status</th>
                    </tr>
                    </thead>
                    <tbody>
                    <?php
                    $this->load->model("hr/attendencehr_model");
                    echo form_open("hr/attendence/update");
                    $c = 1;
                    if ($month == '01') {
                        $total_month_day = 31;
                    } elseif ($month == '02') {
                        $total_month_day = 28;
                    } elseif ($month == '03') {
                        $total_month_day = 31;
                    } elseif ($month == '04') {
                        $total_month_day = 30;
                    } elseif ($month == "05") {
                        $total_month_day = 31;
                    } elseif ($month == "06") {
                        $total_month_day = 30;
                    } elseif ($month == "07") {
                        $total_month_day = 31;
                    } elseif ($month == "08") {
                        $total_month_day = 31;
                    } elseif ($month == "09") {
                        $total_month_day = 30;
                    } elseif ($month == "10") {
                        $total_month_day = 31;
                    } elseif ($month == "11") {
                        $total_month_day = 30;
                    } elseif ($month == "12") {
                        $total_month_day = 31;
                    } else {
                        $total_month_day = 30;
                    }
                    $branch = "";
                    $duty_schedule = 0;
                    $query9 = $this->attendence_model->getEmployeeInfo($empId);
                    foreach ($query9 as $row):
                        $branch = $row->branch_name;
                        $duty_schedule = $row->duty_schedule;
                    endforeach;
                    $total_start_time = "";
                    $total_end_time = "";
                    $total_late_time = 0;
                    $scheduleInfo = $this->attendencehr_model->getScheduleInfo($duty_schedule);
                    if (count($scheduleInfo)):
                        foreach ($scheduleInfo as $sInfo):
                            $start_time = $sInfo->start_time;
                            $late_time = $sInfo->late_time;
                            $end_time = $sInfo->end_time;
                        endforeach;
                        //convert start time to minute
                        $explode_start_time = explode(":", $start_time);
                        $start_hour = $explode_start_time[0];
                        $start_hour_to_minute = $start_hour * 60;
                        $start_minute = $explode_start_time[1];
                        $total_start_time = $start_hour_to_minute + $start_minute;
                        //convert end time to minute
                        $explode_end_time = explode(":", $end_time);
                        $end_hour = $explode_end_time[0];
                        $end_hour_to_minute = $end_hour * 60;
                        $end_minute = $explode_end_time[1];
                        $total_end_time = $end_hour_to_minute + $end_minute;
                        //convert Late time to minute
                        $explode_late_time = explode(":", $late_time);
                        $late_hour = $explode_late_time[0];
                        $late_hour_to_minute = $late_hour * 60;
                        $late_minute = $explode_late_time[1];
                        $total_late_time = $late_hour_to_minute + $late_minute;

                    endif;

                    $leave = 0;
                    $holiday = 0;
                    $weekend = 0;
                    $absent = 0;
                    $present = 0;
                    $late = 0;
                    //start for loop
                    for ($i = 1; $i <= $total_month_day; $i++) {
                        if ($i <= 9) {
                            $day = "0" . $i;
                        } else {
                            $day = $i;
                        }
                        $sdate = $year . "-" . $month . "-" . $day . "";
                        $empId = $empId;
                        $timeInInfo = $this->attendence_model->getTimeLogInfo($empId, $sdate);
                        $att_time_in = "";
                        $att_time_out = "";
                        $totalInTime = "";
                        $emp_comment = "";
                        $sup_comment = "";
                        $status = "";
                        $hr_comment = "";
                        $a = "";
                        foreach ($timeInInfo as $row3) {
                            $att_time_in = $row3->timein;
                            $att_time_out = $row3->timeout;
                            $emp_comment = $row3->emp_comment;
                            $sup_comment = $row3->sup_comment;
                            $hr_comment = $row3->hr_comment;
                        }

                        if ($att_time_in != "") {
                            $explodeTimeIn = explode(":", $att_time_in);
                            $InHour = $explodeTimeIn[0];
                            $InHourToMinute = $InHour * 60;
                            $InHourminute = $explodeTimeIn[1];
                            $totalInTime = $InHourToMinute + $InHourminute;
                        }
                        //check weekend,holiday and leave

                        $holidayInfo = $this->attendence_model->getHoliday($sdate);
                        if (count($holidayInfo)) {
                            $holiday = $holiday + 1;
                            $status = "<font style='color:#006633'>Holiday</font>";
                            $status1 = "holiday";
                            $a = 1;
                        } else {
                            $weekendInfo = $this->attendence_model->getWeekend($sdate, $branch);
                            if (count($weekendInfo)) {
                                $weekend = $weekend + 1;
                                $status = "<font style='color:#006633'>Weekend</font>";
                                $status1 = "weekend";
                                $a = 1;
                            } else {
                                //$leaveInfo = $this->attendence_model->getLeaveInfo($sdate, $empId);
                                $leave1 = $this->db->query("select * from leave_management where hr_status='2' and emp_id='" .$empId . "'and '" . $sdate . "' between start_date and end_date ");
                                //if (count($leaveInfo)) {
                                if ($leave1->num_rows()) {
                                    $leave = $leave + 1;
                                    $status = "<font style='color:green'>leave</font>";
                                    $status1 = "leave";
                                    $a = 1;
                                } else {
                                    if ($att_time_in == "" && $att_time_out == "") {
                                        $absent = $absent + 1;
                                        $status = "<font style='color:red'>Absent</font>";
                                        $status1 = "absent";
                                        $a = "";
                                    } else {
                                        $present = $present + 1;
                                        $status = "present";
                                        $status1 = "present";
                                        $a = 1;
                                        if ($total_late_time != 0 && $totalInTime != ""):
                                            if ($totalInTime > $total_late_time) {
                                                $late = $late + 1;
                                                // $late_duration = $timein - $total_late_time;
                                                //$total_late_minute = $total_late_minute + $late_duration;

                                                $status = "<font style='color:#990033'>late</font>";
                                                $status1 = "late";
                                                $a = "";
                                            }
                                        endif;
                                    }
                                }
                            }
                        }
                        //end weelend,holiday,leave check
                        ?>

                        <tr >
                            <td><input class="form-control" type="text" readonly="readonly" name="att_date[]" value="<?php echo $sdate; ?>" /></td>
                            <td><input class="form-control" type="text" name="timein[]"  value="<?php echo $att_time_in; ?>" /></td>
                            <td><input class="form-control" type="text" name="timeout[]" value="<?php echo $att_time_out; ?>" /></td>
                            <td>
                                <input type="hidden" readonly="readonly" name="status[]" value="<?php echo $status1; ?>" />
                                <?php echo $status; ?></td>


                            </td>
                            <td >
                                <input class="form-control" type="text" readonly="readonly" name="emp_comment[]" value="<?php echo $emp_comment; ?>"  />
                            </td>
                            <td >
                                <input class="form-control" type="text" readonly="readonly"  name="sup_comment[]" value="<?php echo $sup_comment; ?>"  />
                            </td>
                            <td><?php echo form_dropdown('sup_status[]', $sup, $row3->sup_approve, 'class="form-control" disabled="disabled"'); ?></td>
                            <td >
                                <input class="form-control" type="text"  name="hr_comment[]" value="<?php echo $hr_comment; ?>"  />
                            </td>
                            <td>
                                <?php
                                // edit start by joy
                                $hr1 = array(
                                    '1' => '',
                                    '2' => 'Deny'
                                );
                                // edit end

                                if ($a == "1"):
                                    echo form_dropdown('hr_status[]', $hr1, $a, "class='form-control'");
                                else:
                                    echo form_dropdown('hr_status[]', $hr, $a, "class='form-control' style='color:red'");
                                endif;
                                ?>
                                <?php //echo form_dropdown('hr_status[]',$hr,$row3->hr_approve); ?>


                            </td>

                        </tr>
                        <?php
                    }
                    //end forloop
                    ?>
                    <tr>
                        <td>
                            <input type="hidden" name="emp_id" value="<?php echo $empId; ?>" />
                            <input type="hidden" name="year" value="<?php echo $year; ?>"  />
                            <input type="hidden" name="month" value="<?php echo $month; ?>"  />
                            <input class="btn btn-primary" type="submit" value="Submit">
                            </form>
                        </td>
                    </tr>
                    </tbody>
                </table>

            </div>
        </div><! --/content-panel -->
    </div><!-- /col-md-12 -->
</div><!-- row -->


