<script language="javascript">
    function confirmSubmit() {
        var agree = confirm("Are you sure to delete this record?");
        if (agree)
            return true;
        else
            return false;
    }
</script>

<h3><i class="fa fa-angle-right"></i> Attendance Request</h3>
<?php
if($this->session->flashdata('message')==NULL){

}else{ ?>
    <div class="alert-success alert">
        <?php echo $this->session->flashdata('message') ?>
    </div> <?php
}
?>


<div class="row">
    <div class="col-sm-12 col-lg-12">
        <div class=" pull-right">
            <form class="form-inline">
                <div class="form-group has-feedback pull-right">
                    <input type="text" id="searchKey" name="search" class="form-control" placeholder="Search" />
                    <i class="glyphicon glyphicon-search form-control-feedback"></i>
                </div>
            </form>
        </div>
        <div class="pull-right"><a href="<?php echo base_url();?>hr/attendence/attendence_request" class="btn btn-info" role="button" >Reset</a>&nbsp;</div>
    </div>

    <div class="col-sm-12">
        <div class="content-panel">
            <!--<section id="flip-scroll">-->
            <section id="">
                <?php
                    $c1 = 1;
                    $total1 = 0;
                ?>
                <table class="table table-hover">
                    <tr>

                        <th>SL No</th>
                        <th>Name</th>
                        <th>Employee Id No</th>
                        <th>Year</th>
                        <th>Month</th>
                        <th>Action</th>
                    </tr>
                    <?php
                    foreach ($query10 as $temp) {
                        $id = $temp->id;
                        $query = $this->db->query("select * from employeeinfo where id='" . $temp->emp_id . "'");
                        foreach ($query->result() as $temp1):endforeach;
                        if(!isset($temp1->card_no)) continue; ?>
                        <tr>
                            <td><?php echo $c1 ?></td>
                            <td><?php echo $temp1->name ?></td>
                            <td><?php echo $temp1->card_no ?></td>
                            <td> <?php echo $temp->year ?></td>
                            <td><?php echo  $temp->month ?></td>
                            <td> <a class="btn btn-primary btn-xs" href="<?php echo base_url(); ?>hr/attendence/details/<?php echo $temp->emp_id; ?>/<?php echo $temp->year; ?>/<?php echo urlencode($temp->month); ?>" title="Details">Action</a></td>
                        </tr>
                        <?php
                        $c1 = $c1 + 1;
                    } ?>

                </table>
                <div class="text-center">
                    <?php echo $this->pagination->create_links(); ?>
                </div>
                <div class="text-center">
                    <?php
                    echo "Total Records: ".$this->pagination_library->total_rows." page ".(($this->pagination->cur_page) ? $this->pagination->cur_page : "1")." of ". ceil(($this->pagination_library->total_rows)/($this->pagination_library->per_page));
                    ?>
                </div>
            </section>
        </div><! --/content-panel -->
    </div><!-- /col-md-12 -->
</div><!-- row -->