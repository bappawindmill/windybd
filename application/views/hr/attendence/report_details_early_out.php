<style>
    table tr{ border:1px solid #EEEEEE;}
    table tr th{font-size: 11px; font-weight: bold; text-align: center}
    table tr td{font-size: 11px;text-align: center }
</style>
<script type="text/javascript" src="<?php echo base_url(); ?>/public/js/jquery.jqprint.0.3.js"></script>
<script>
    $(function(){
        $("#print_button").click( function() {
            $("#invoice").jqprint();
        });
    });
</script>
<input type="button" id="print_button" value="Print" style="margin-top:5px; ;">
<div class="module">

    <div style="width:800px;margin-left:50px" id="invoice">

        <div style="width:800px; height:60px">
            <div style="width:900px; text-align: center; font-size: 18px; font-weight: bold; color:blue">  Golden Son Limited</div>
            <div style="width:900px;text-align: center;font-size: 10px; color:blue">Khoagnagor, Azimpara, Karnnofuly, Chittagong</div>
            <div style="width:900px;font-weight: bold;text-align: left;font-size: 14px; color:black;margin-top: 15px">Daily Early Out Report For <?php echo $sdate; ?>
                <span style=" font-size: 12px; font-weight: normal; margin-left: 270px">  Reporting Date: &nbsp; <?php echo $sdate; ?></span>
            </div>

        </div>
        <div style="width:800px;">
            <?php
            $g_total = 0;
            foreach ($query1 as $section_info) {
                $section = $section_info->id;
                //retrive duty schedule
                $query66 = $this->db->query("select * from duty_schedule where section='" . $section_info->id . "'and shift_name='" . $shift . "'");
                $shift_end_time = "";
                $shift_start_time = "";
                $card_punch_time = "";
                $late_time = "";
                foreach ($query66->result() as $s_info):
                    $card_punch_time = $s_info->start_time;
                    $shift_start_time = $s_info->duty_start_time;
                    $shift_end_time = $s_info->end_time;
                    $late_time = $s_info->late_time;
                    $ot_time = $s_info->ot;
                    $lunch_start_time = $s_info->lunch_in_time;
                    $lunch_end_time = $s_info->lunch_out_time;
                endforeach;
                //get all employee
                $this->load->model('hr/attendence_model');
                $this->data['employee_info'] = $this->attendence_model->get_all_employee($section, $shift);
                $late = 0;
                if ($shift_end_time < $shift_start_time) {
                    foreach ($this->data['employee_info'] as $e_info) {
                        $att_time_in = "";
                        $query = $this->db->query("select * from attendence where card_no='" . $e_info->card_no . "'and att_date='" . $sdate . "'and att_time>'" . $card_punch_time . "'order by id asc limit 1");
                        foreach ($query->result() as $row3) {
                            $att_time_in = $row3->att_time;
                        }
                        //add one day with current Day
                        $next_day = explode("-", $sdate);
                        $y = $next_day[0];
                        $m = $next_day[1];
                        $d = $next_day[2];
                        $d1 = $d + 1;
                        $day = $y . "-" . $m . "-" . $d1;
                        $att_time_out = "";
                        $query1 = $this->db->query("select * from attendence where card_no='" . $e_info->card_no . "'and att_date='" . $day . "'and att_time<'" . $card_punch_time . "'order by id desc limit 1");
                        foreach ($query1->result() as $row31) {
                            $att_time_out = $row31->att_time;
                        }
                        if ($att_time_in != "" && $att_time_out != "") {
                            if ($att_time_out < $shift_end_time) {
                                $late = $late + 1;
                            }
                        }
                    }
                } else {

                    foreach ($this->data['employee_info'] as $e_info) {
                        $query5858 = $this->db->query("select * from attendence where card_no='" . $e_info->card_no . "'and att_date='" . $sdate . "'and att_time>'" . $card_punch_time . "'order by id asc limit 1");
                        //echo "<pre>";print_r($query->result());
                        $time_in = "";
                        foreach ($query5858->result() as $row3999) {
                            $time_in = $row3999->att_time;
                        }
                        //detect outtime
                        $query167 = $this->db->query("select * from attendence where card_no='" . $e_info->card_no . "'and att_date='" . $sdate . "'order by id desc limit 1");
                        // echo "<pre>";print_r($query1->result());
                        $time_out = "";
                        foreach ($query167->result() as $row317) {
                            $time_out = $row317->att_time;
                        }
                        if ($time_in == $time_out):
                            $time_out = "";
                        endif;
                        if ($time_in != "" && $time_out != ""):
                            if ($time_out < $shift_end_time):
                                $late = $late + 1;
                            endif;
                        endif;
                    }
                }


                //start absent checking
                $n = 1;
                if ($late > 0) {
                    ?>
                    <table border="1" style="margin-bottom:10px">
                        <tr>
                            <?php
                            $department = "";
                            $d = $this->db->query("select name from add_department where id='" . $section_info->department . "'");
                            foreach ($d->result() as $d_name):
                                $department = $d_name->name;
                            endforeach;
                            ?>  
                            <th colspan="7">
                                Department & Section Name:&nbsp;&nbsp;  <?php echo $department; ?> &nbsp;&nbsp;&nbsp; & &nbsp; <?php echo $section_info->proffession; ?>  
                            </th>
                        </tr>
                        <tr>
                            <th style="text-align: center">SL No</th> 
                            <th style="text-align: center">Card No</th> 
                            <th style="text-align: center">Name</th> 
                            <th style="text-align: center">InTime</th> 
                            <th style="text-align: center">OutTime</th> 
                            <th style="text-align: center">Duration</th> 
                            <th style="text-align: center">Status</th> 

                        </tr>
                        <?php
                        //start foreach loop
                        foreach ($this->data['employee_info'] as $emp_info) {
                            if ($shift_end_time < $shift_start_time) {
                                $att_time_in = "";
                                $query = $this->db->query("select * from attendence where card_no='" . $emp_info->card_no . "'and att_date='" . $sdate . "'and att_time>'" . $card_punch_time . "'order by id asc limit 1");
                                foreach ($query->result() as $row3) {
                                    $att_time_in = $row3->att_time;
                                }
                                //add one day with current Day
                                $next_day = explode("-", $sdate);
                                $y = $next_day[0];
                                $m = $next_day[1];
                                $d = $next_day[2];
                                $d1 = $d + 1;
                                $day = $y . "-" . $m . "-" . $d1;
                                $att_time_out = "";
                                $query1 = $this->db->query("select * from attendence where card_no='" . $emp_info->card_no . "'and att_date='" . $day . "'and att_time<'" . $card_punch_time . "'order by id desc limit 1");
                                foreach ($query1->result() as $row31) {
                                    $att_time_out = $row31->att_time;
                                }
                                if ($att_time_in != "" && $att_time_out != "") {
                                    if ($att_time_out < $shift_end_time) {
                                        $explode_time_in = explode(":", $att_time_out);
                                        $explode_time_in_hour = $explode_time_in[0];
                                        $explode_time_in_hour_minute = $explode_time_in_hour * 60;
                                        $explode_time_in_minute = $explode_time_in[1];
                                        $total_time_in_minute = $explode_time_in_hour_minute + $explode_time_in_minute;

                                        //convert attendendence timeout to minute
                                        $explode_att_time_out = explode(":", $shift_end_time);
                                        $explode_att_time_out_hour = $explode_att_time_out[0];
                                        $explode_att_time_out_hour_minute = $explode_att_time_out_hour * 60;
                                        $explode_att_time_out_minute = $explode_att_time_out[1];
                                        $total_timeout_minute = $explode_att_time_out_hour_minute + $explode_att_time_out_minute;
                                        $total_count_time = $total_timeout_minute - $total_time_in_minute;


                                        echo "<tr>";
                                        echo "<td>" . $n . "</td>";
                                        echo "<td>" . $emp_info->card_no . "</td>";
                                        echo "<td>" . $emp_info->name . "</td>";
                                        echo "<td>" . $att_time_in . "</td>";
                                        echo "<td>" . $att_time_out . "</td>";
                                        echo "<td>" . $total_count_time . "</td>";
                                        echo "<td>Early Out</td>";
                                        echo "</tr>";
                                        $n = $n + 1;
                                    }
                                }
                            } else {
                                $query14 = $this->db->query("select * from attendence where card_no='" . $emp_info->card_no . "'and att_date='" . $sdate . "'and att_time>'" . $card_punch_time . "'order by id asc limit 1");
                                // $query = $this->db->query("select * from attendence where card_no='" . $card_no . "'and att_date='" . $start_date . "'order by id asc limit 1");
                                $att_time_in = "";
                                foreach ($query14->result() as $row3) {
                                    $att_time_in = $row3->att_time;
                                }
                                //detect outtime
                                $att_time_out = "";
                                $query1 = $this->db->query("select * from attendence where card_no='" . $emp_info->card_no . "'and att_date='" . $sdate . "'order by id desc limit 1");
                                foreach ($query1->result() as $row31) {
                                    $att_time_out = $row31->att_time;
                                }
                                if ($att_time_in == $att_time_out):
                                    $att_time_out = "";
                                endif;
                                if ($att_time_in != "" && $att_time_out != "") {
                                    if ($att_time_out < $shift_end_time) {
                                        $explode_time_in = explode(":", $att_time_out);
                                        $explode_time_in_hour = $explode_time_in[0];
                                        $explode_time_in_hour_minute = $explode_time_in_hour * 60;
                                        $explode_time_in_minute = $explode_time_in[1];
                                        $total_time_in_minute = $explode_time_in_hour_minute + $explode_time_in_minute;

                                        //convert attendendence timeout to minute
                                        $explode_att_time_out = explode(":", $shift_end_time);
                                        $explode_att_time_out_hour = $explode_att_time_out[0];
                                        $explode_att_time_out_hour_minute = $explode_att_time_out_hour * 60;
                                        $explode_att_time_out_minute = $explode_att_time_out[1];
                                        $total_timeout_minute = $explode_att_time_out_hour_minute + $explode_att_time_out_minute;
                                        $total_count_time = $total_timeout_minute - $total_time_in_minute;

                                        echo "<tr>";
                                        echo "<td>" . $n . "</td>";
                                        echo "<td>" . $emp_info->card_no . "</td>";
                                        echo "<td>" . $emp_info->name . "</td>";
                                        echo "<td>" . $att_time_in . "</td>";
                                        echo "<td>" . $att_time_out . "</td>";
                                        echo "<td>" . $total_count_time . "</td>";
                                        echo "<td>Early Out</td>";
                                        echo "</tr>";
                                        $n = $n + 1;
                                    }
                                }
                            }
                            //endforeach
                        }
                        ?>
                    </table>
                    <p style="margin-top:10px; padding-left: 50px; font-weight: bold; margin-bottom: 10px"> Total No. of Early Out :<?php echo $n - 1; ?></p>
                    <?php
                    $g_total = $g_total + ($n - 1);
                }
                ?>
                <?php
                //end checking
            }
            ?>    
            <p style="margin-top:10px; padding-left: 50px;  margin-bottom: 10px">Grand Total No. of Early Out :<?php echo $g_total; ?></p>
        </div>

        <div style="width:800px; margin-top:50px; margin-bottom: 20px">
            <div style="width:250px; float: left; text-align: center; position:relative">
                <span style="border-top:1px solid black">Prepared By</span>
            </div> 
            <div style="width:250px; float: left;text-align: center; position:relative">
                <span style="border-top:1px solid black"> Checked By</span>
            </div> 
            <div style="width:250px; float: left;text-align: center; position:relative">
                <span style="border-top:1px solid black">   Approved By</span>
            </div> 
        </div>
        <div style="width:800px; clear: both; margin-top: 10px; height: 30px"></div>

    </div>

</div>
<div style="clear: both;"></div>

