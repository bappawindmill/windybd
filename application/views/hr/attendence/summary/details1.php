<link rel="stylesheet" type="text/css" href="<?php echo base_url(); ?>public/css/t1.css" />
<style type="text/css">
    <!--
    .style1 {color: #FF0000}
    #invoice a{ text-decoration: none}
    -->
</style>
<script type="text/javascript" src="<?php echo base_url(); ?>/public/js/jquery.jqprint.0.3.js"></script>
<script>
    $(function() {
        $("#print_button").click(function() {
            $("#invoice").jqprint();
        });
    });
</script>

<p><input type="button" id="print_button" value="Print" style="margin-top:5px;"/>

    <?php
    /*  echo form_open('hr/excel/exportExcel');
      echo form_hidden('start_date', $sdate);
      echo form_hidden('end_date', $edate);
      echo form_submit('mysubmit', 'Export Excel');
      echo form_close(); */
    ?>
</p>
<?php
if ($month == '01') {
    $total_month_day = 31;
    $mname="January";
} elseif ($month == '02') {
    $total_month_day = 28;
     $mname="February";
} elseif ($month == '03') {
    $total_month_day = 31;
     $mname="March";
} elseif ($month == '04') {
    $total_month_day = 30;
     $mname="April";
} elseif ($month == "05") {
    $total_month_day = 31;
     $mname="May";
} elseif ($month == "06") {
    $total_month_day = 30;
     $mname="June";
} elseif ($month == "07") {
    $total_month_day = 31;
     $mname="July";
} elseif ($month == "08") {
    $total_month_day = 31;
     $mname="August";
} elseif ($month == "09") {
    $total_month_day = 30;
     $mname="September";
} elseif ($month == "10") {
    $total_month_day = 31;
     $mname="October";
} elseif ($month == "11") {
    $total_month_day = 30;
     $mname="November";
} elseif ($month == "12") {
    $total_month_day = 31;
     $mname="December";
} else {
    $total_month_day = 30;
     $mname="";
}
?>

<div id="invoice" style="float:left; width:900px">
    <p style="font-weight:bold; font-style: italic; height:35px; text-align: center;">Attendance Summary of <?php echo $mname;?>,<?php echo $year; ?></p>
    <table width="100%">
        <tr>
            <th style="text-align:center">SL No</th>
            <th style="text-align:center">SBU</th>
            <th style="text-align:center">Name</th>
            <th style="text-align:center">Total Days</th>
            <th style="text-align:center">On time</th>
            <th style="text-align:center">Approve Late</th>
            <th style="text-align:center">Unapproved Late</th>
            <th style="text-align:center">Leave </th>
            <th style="text-align:center">Absent</th>
            <th style="text-align:center">Deduction Days</th>
        </tr>

        <?php
        $this->load->model('hr/attendence_model');

        $c1 = 1;
        foreach ($empInfo as $eInfo) {
            $branch_name = "";
            $query3 = $this->db->query("select branch_name from add_branch where id='" . $eInfo->branch_name . "'");
            foreach ($query3->result() as $row33) {
                $branch_name = $row33->branch_name;
            }

            $checkAttendence = $this->attendence_model->checkSubmission($year, $month, $eInfo->id);
            if (count($checkAttendence)) {
                $ontime = "";
                $approve_late = "";
                $unapprove_late = "";
                $leave = "";
                $absent = "";
                $deduction_days=0;

                for ($i = 1; $i <= $total_month_day; $i++) {
                    if ($i <= 9) {
                        $day = "0" . $i;
                    } else {
                        $day = $i;
                    }
                    $sdate = $year . "-" . $month . "-" . $day . "";
                    $attendenceInfo = $this->attendence_model->getDailyAttendance($sdate, $eInfo->id);
                    if (count($attendenceInfo)) {
                        $astatus = "";
                        $hr_approve = "";
                        foreach ($attendenceInfo as $aInfo) {
                            $astatus = $aInfo->status;
                            $hr_approve = $aInfo->hr_approve;
                        }
                        //
                        if ($astatus == "present") {
                            $ontime = $ontime + 1;
                        } elseif ($astatus == "weekend") {
                            $ontime = $ontime + 1;
                        } elseif ($astatus == "holiday") {
                            $ontime = $ontime + 1;
                        } elseif ($astatus == "leave") {
                            $leave = $leave + 1;
                        } elseif ($astatus == "absent") {
                            if ($hr_approve == '1') {
                                $ontime = $ontime + 1;
                            } else {
                                $absent = $absent + 1;
                                
                            }
                        } elseif ($astatus == "late") {
                            if ($hr_approve == '1') {
                                $approve_late = $approve_late + 1;
                            } else {
                                $unapprove_late = $unapprove_late + 1;
                            }
                        } else {
                            
                        }
                    } else {
                        
                    }
                }
                $countLate=intval($unapprove_late/3);
                $deduction_days=$countLate + $absent;
                ?>
                <tr>
                    <td style='text-align:center'><?php echo $c1; ?></td>
                    <td style='text-align:center'><?php echo $branch_name; ?></td>
                    <td style='text-align:center'><?php echo $eInfo->name; ?></td>
                    <td style='text-align:center'><?php echo $total_month_day; ?></td>
                    <td style='text-align:center'><?php echo $ontime; ?></td>
                    <td style='text-align:center'><?php echo $approve_late; ?></td>
                    <td style='text-align:center'><?php echo $unapprove_late; ?></td>
                    <td style='text-align:center'><?php echo $leave; ?></td>
                    <td style='text-align:center'><?php echo $absent; ?></td>
                    <td style='text-align:center'><?php echo $deduction_days; ?></td>

                </tr>

                <?php
            } else {
                ?>

                <tr>
                    <td style='text-align:center'><?php echo $c1; ?></td>
                    <td style='text-align:center'><?php echo $branch_name; ?></td>
                    <td style='text-align:center'><?php echo $eInfo->name; ?></td>
                    <td style='text-align:center'><?php echo $total_month_day; ?></td>
                    <td style='text-align:center'></td>
                    <td style='text-align:center'></td>
                    <td style='text-align:center'></td>
                    <td style='text-align:center'></td>
                    <td style='text-align:center'></td>
                    <td style='text-align:center'></td>
                </tr>
                <?php
            }
            $c1=$c1+1;
        }
        ?>


    </table>

</div>
<div style="clear: both;"></div>

