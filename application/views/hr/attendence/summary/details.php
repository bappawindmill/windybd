<script language="javascript">
    function confirmSubmit() {
        var agree = confirm("Are you sure to delete this record?");
        if (agree)
            return true;
        else
            return false;
    }
</script>
<input type="button" id="print_button" value="Print" style="margin-top:5px;">
<?php
$month=$this->input->get_post('month');

if ($month == '01') {
    $total_month_day = 31;
    $mname="January";
} elseif ($month == '02') {
    $total_month_day = 28;
    $mname="February";
} elseif ($month == '03') {
    $total_month_day = 31;
    $mname="March";
} elseif ($month == '04') {
    $total_month_day = 30;
    $mname="April";
} elseif ($month == "05") {
    $total_month_day = 31;
    $mname="May";
} elseif ($month == "06") {
    $total_month_day = 30;
    $mname="June";
} elseif ($month == "07") {
    $total_month_day = 31;
    $mname="July";
} elseif ($month == "08") {
    $total_month_day = 31;
    $mname="August";
} elseif ($month == "09") {
    $total_month_day = 30;
    $mname="September";
} elseif ($month == "10") {
    $total_month_day = 31;
    $mname="October";
} elseif ($month == "11") {
    $total_month_day = 30;
    $mname="November";
} elseif ($month == "12") {
    $total_month_day = 31;
    $mname="December";
} else {
    $total_month_day = 30;
    $mname="";
}
?>
<h3><i class="fa fa-angle-right"></i> Attendance Summary of <?php echo $mname;?>,<?php echo $this->input->get_post('year'); ?></h3>
<?php
if($this->session->flashdata('message')==NULL){

} else { ?>
    <div class="alert-success alert">
        <?php echo $this->session->flashdata('message') ?>
    </div> <?php
}
?>

<div class="row">

    <div class="col-sm-12">
        <div class="content-panel">
            <!--<section id="flip-scroll">-->
            <section id="">
                <table id="invoice" class="table table-hover">
                    <tr>
                        <th>SL No</th>
                        <th>SBU</th>
                        <th>Name</th>
                        <th>Total Days</th>
                        <th>On time</th>
                        <th>Approve Late</th>
                        <th>Unapproved Late</th>
                        <th>Leave </th>
                        <th>Absent</th>
                        <th>Deduction Days</th>
                    </tr>

                    <?php
                    $this->load->model('hr/attendencehr_model');

                    $c1 = 1;

                    foreach ($empInfo as $eInfo) {
                        $branch_name = "";
                        $query3 = $this->db->query("select branch_name from add_branch where id='" . $eInfo->branch_name . "'");
                        foreach ($query3->result() as $row33) {
                            $branch_name = $row33->branch_name;
                        }

                        $checkAttendence = $this->attendencehr_model->checkSubmission($year, $month, $eInfo->id);
                        if (count($checkAttendence)) {
                            $ontime = "";
                            $approve_late = "";
                            $unapprove_late = "";
                            $leave = "";
                            $absent = "";
                            $deduction_days=0;

                            for ($i = 1; $i <= $total_month_day; $i++) {
                                if ($i <= 9) {
                                    $day = "0" . $i;
                                } else {
                                    $day = $i;
                                }
                                $sdate = $year . "-" . $month . "-" . $day . "";
                                $attendenceInfo = $this->attendencehr_model->getDailyAttendance($sdate, $eInfo->id);
                                if (count($attendenceInfo)) {
                                    $astatus = "";
                                    $hr_approve = "";
                                    foreach ($attendenceInfo as $aInfo) {
                                        $astatus = $aInfo->status;
                                        $hr_approve = $aInfo->hr_approve;
                                    }

                                    if ($astatus == "present") {
                                        $ontime = $ontime + 1;
                                    } elseif ($astatus == "weekend") {
                                        $ontime = $ontime + 1;
                                    } elseif ($astatus == "holiday") {
                                        $ontime = $ontime + 1;
                                    } elseif ($astatus == "leave") {
                                        $leave = $leave + 1;
                                    } elseif ($astatus == "absent") {
                                        if ($hr_approve == '1') {
                                            $ontime = $ontime + 1;
                                        } else {
                                            $absent = $absent + 1;
                                        }
                                    } elseif ($astatus == "late") {
                                        if ($hr_approve == '1') {
                                            $approve_late = $approve_late + 1;
                                        } elseif($hr_approve == '2') {
                                            $unapprove_late = $unapprove_late + 1;
                                        }
                                        else{}
                                    } else {

                                    }
                                } else {

                                }
                            }
                            $countLate=intval($unapprove_late/3);
                            $deduction_days=$countLate + $absent;
                            ?>
                            <tr>
                                <td><?php echo $c1; ?></td>
                                <td><?php echo $branch_name; ?></td>
                                <td><?php echo $eInfo->name; ?></td>
                                <td><?php echo $total_month_day; ?></td>
                                <td><?php echo $ontime; ?></td>
                                <td><?php echo $approve_late; ?></td>
                                <td><?php echo $unapprove_late; ?></td>
                                <td><?php echo $leave; ?></td>
                                <td><?php echo $absent; ?></td>
                                <td><?php echo $deduction_days; ?></td>
                            </tr>
                            <?php
                        } else {
                            ?>
                            <tr>
                                <td><?php echo $c1; ?></td>
                                <td><?php echo $branch_name; ?></td>
                                <td><?php echo $eInfo->name; ?></td>
                                <td><?php echo $total_month_day; ?></td>
                                <td></td>
                                <td></td>
                                <td></td>
                                <td></td>
                                <td></td>
                                <td></td>
                            </tr>
                            <?php
                        }
                        $c1=$c1+1;
                    }
                    ?>
                </table>
                <div class="text-center">
                    <?php echo $this->pagination->create_links(); ?>
                </div>
                <div class="text-center">
                    <?php
                    echo "Total Records: ".$this->pagination_library->total_rows." page ".(($this->pagination->cur_page) ? $this->pagination->cur_page : "1")." of ". ceil(($this->pagination_library->total_rows)/($this->pagination_library->per_page));
                    ?>
                </div>
            </section>
        </div><! --/content-panel -->
    </div><!-- /col-md-12 -->
</div><!-- row -->