<h3><i class="fa fa-angle-right"></i> Submit Control</h3>

<!-- BASIC FORM ELELEMNTS -->
<div class="row">
    <div class="col-sm-12">
        <div class="content-panel">
            <?php
            $s = array(
                '' => 'Select',
                '1' => 'Enable',
                '2' => 'Disable'
            );
            echo form_open('hr/department/added_control');
            $query=$this->db->query("select * from submit_control");
            ?>
            <?php
            if($this->session->flashdata('message')==NULL){

            }else{ ?>
                <div class="alert-success alert">
                    <?php echo $this->session->flashdata('message') ?>
                </div> <?php
            }
            ?>
            <div class="col-md-12">
                <?php
                    if($query->num_rows()>0){
                        foreach($query->result() as $t):
                            $s_status=$t->status;
                            $id=$t->id;
                        endforeach;
                ?>
                <div class="form-group col-sm-12">
                    <label for="emp_type">Submission Status:</label>
                    <?php echo form_dropdown('status', $s,$s_status,'class="form-control"'); ?>
                    <div class="alert-danger"><?php echo ucwords(form_error('status')); ?></div>
                    <input type="hidden" name="id" value="<?php echo $id;?>" />
                </div>
                <?php } else{ ?>
                    <div class="form-group col-sm-12">
                        <label for="emp_type">Submission Status:</label>
                        <?php echo form_dropdown('status', $s,'class="form-control"'); ?>
                        <div class="alert-danger"><?php echo ucwords(form_error('status')); ?></div>
                        <input type="hidden" name="id" value="<?php echo $id;?>" />
                    </div>
                <?php } ?>
            </div>
                <span>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
                    <input type="submit" name="mysubmit" value="Submit"  class="btn btn-primary">
                </span>
            </form>
            <p></p>
        </div>
    </div><!-- col-lg-12-->
</div><!-- /row -->