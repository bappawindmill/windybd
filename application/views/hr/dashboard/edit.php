<link rel="stylesheet" type="text/css" href="<?php echo base_url(); ?>public/css/t1.css" />
<style type="text/css">
    <!--
    .style1  {color: #FF0000}
    .error {color:red}
    -->
	.input-short{width:150px}
}
</style>
<div class="grid_12">
    <div class="module1">
        <h2><span>Modify Leave Package</span></h2>
          <?php echo $this->session->flashdata("message");?>
        <div class="module-body">
		<?php
		echo form_open('hr/leave_package/leave_edit');
		echo "<input type = 'hidden' name = 'id' value='".$fid['value']."' >";
		echo '<table>';
		echo '<tr><th>Leave Package Name :</th><td>'.form_input('package_name',$package_name['value'],'package_name').'<span style="color:red;">*</span></td></tr>';
		echo '<tr><th>Casual Leave :</th><td>'.form_input('casual',$casual['value'],'casual').'<span style="color:red;">*</span></td></tr>';
		echo '<tr><th>Medical :</th><td>'.form_input('medical',$medical['value'],'medical').'<span style="color:red;">*</span></td></tr>';
		echo '<tr><th>Earned :</th><td>'.form_input('earned',$earned['value'],'earned').'<span style="color:red;">*</span></td></tr>';
		echo '<tr><th>Compensation:</th><td>'.form_input('compensation',$compensation['value'],'compensation').'<span style="color:red;">*</span></td></tr>';
		echo '<tr><th>Other:</th><td>'.form_input('other',$other['value'],'other').'<span style="color:red;">*</span></td></tr>';
		echo '<tr><th></th><td>'.form_submit('mysubmit','Update Leave').'</td></tr>';
		echo '<tr><th></th><td>'.validation_errors('<p class="error">').'</td></tr>';
		echo '</table>';
        echo form_close(); 
?>
</div></div></div>
    <div style="clear: both;"></div>
 
