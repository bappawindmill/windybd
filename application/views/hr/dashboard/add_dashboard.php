<h3><i class="fa fa-angle-right"></i> Create New Dashboard Message</h3>

<!-- BASIC FORM ELELEMNTS -->
<div class="row">
    <div class="col-sm-12">
        <div class="content-panel">
            <?php
            echo form_open('hr/dashboard/added_dashboard');?>
            <div class="alert-danger"><?php echo $this->session->flashdata('message'); ?> </div>
            <div class="col-md-12">
                <div class="form-group col-sm-12">
                    <label for="">Title:</label>
                    <input value=""  type="text" name="title" class="form-control" id=" ">
                    <div class="alert-danger"><?php echo ucwords(form_error('title')); ?></div>
                </div>
                <div class="form-group col-sm-12">
                    <label for="">Message:</label>
                    <textarea class="form-control" rows="" cols="" name="message"></textarea>
                    <div class="alert-danger"><?php echo ucwords(form_error('message')); ?></div>
                </div>
            </div>
                <span>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
                    <input type="hidden" name="id" value="" />&nbsp;&nbsp;&nbsp;&nbsp;
                    <input type="submit" name="mysubmit" value="Submit"  class="btn btn-primary">
                    <a class="btn btn-theme" href="<?php echo base_url(); ?>hr/dashboard/dashboard_list">Cancel</a></span>
            </span>
            </form>
            <p></p>
        </div>
    </div><!-- col-lg-12-->
</div><!-- /row -->