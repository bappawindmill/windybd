<h3><i class="fa fa-angle-right"></i> Create New Department</h3>

<!-- BASIC FORM ELELEMNTS -->
<div class="row">
    <div class="col-sm-12">
        <div class="content-panel">
            <?php
            echo form_open('hr/department/added_department');?>
                <div class="alert-danger"><?php echo $this->session->flashdata('message'); ?> </div>
                <div class="col-md-12">
                    <div class="form-group col-sm-6">
                        <label for="name">Branch Name:</label>
                        <input value=""  type="text" name="name" class="form-control" id=" ">
                        <div class="alert-danger"><?php echo ucwords(form_error('name')); ?></div>
                    </div>
                    <?php
                    $query1 = $this->db->query("SELECT id,branch_name FROM add_branch");
                    ?>
                    <div class="form-group col-sm-6">
                        <label for="branch_name">SBU Name:</label>
                        <select name="branch_name" id="branch_name" class="form-control">
                            <option value="">select</option>
                            <?php foreach ($query1->result() as $row) { ?>
                                <option value="<?php echo $row->id; ?>"><?php echo $row->branch_name; ?></option>
                                <?php
                            }
                            ?>
                        </select>
                        <div class="alert-danger"><?php echo ucwords(form_error('user_type')); ?></div>
                    </div>
                </div>
                <span>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
                    <input type="hidden" name="id" value="" />&nbsp;&nbsp;&nbsp;&nbsp;
                    <input type="submit" name="mysubmit" value="Submit"  class="btn btn-primary">
                    <a class="btn btn-theme" href="<?php echo base_url(); ?>hr/department/department_list">Cancel</a>
                </span>

            </form>
            <p></p>
        </div>
    </div><!-- col-lg-12-->
</div><!-- /row -->