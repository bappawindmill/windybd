<script language="javascript">
    function confirmSubmit() {
        var agree = confirm("Are you sure to delete this record?");
        if (agree)
            return true;
        else
            return false;
    }
</script>
<!--4096-->
<script type="text/javascript" src="<?php echo base_url(); ?>/public/js/jquery.jqprint.0.3.js"></script>

<input type="button" id="print_button" value="Print" style="margin-top:5px; ;">

<div class="module">
    <h2><span>View Salary</span></h2>
    <div class="module-table-body" id="invoice" style="">
        <style>
            table .tablesorter {font-size:7px}
            div.module table td {
                background-color: #FFFFFF;
                border: 1px solid black;
                padding: 5px;
            }
        </style>
        <?php
        $this->load->model('hr/salary_model');
        $branch = "";
        $query10 = $this->db->query("select branch_name from add_branch where id='" . $sbranch . "'");
        foreach ($query10->result() as $ab) {
            $branch = $ab->branch_name;
        }
        $dept_name = "";
        $deptInfo = $this->db->query("select name from add_department where id='" . $sdept . "'");
        foreach ($deptInfo->result() as $temp) {
            $dept_name = $temp->name;
        }
        if ($month == '01') {
            $total_month_day = 31;
            $mName = "January";
        } elseif ($month == '02') {
            $total_month_day = 28;
            $mName = "February";
        } elseif ($month == '03') {
            $total_month_day = 31;
            $mName = "March";
        } elseif ($month == '04') {
            $total_month_day = 30;
            $mName = "April";
        } elseif ($month == "05") {
            $total_month_day = 31;
            $mName = "May";
        } elseif ($month == "06") {
            $total_month_day = 30;
            $mName = "June";
        } elseif ($month == "07") {
            $total_month_day = 31;
            $mName = "July";
        } elseif ($month == "08") {
            $total_month_day = 31;
            $mName = "August";
        } elseif ($month == "09") {
            $total_month_day = 30;
            $mName = "September";
        } elseif ($month == "10") {
            $total_month_day = 31;
            $mName = "October";
        } elseif ($month == "11") {
            $total_month_day = 30;
            $mName = "November";
        } elseif ($month == "12") {
            $mName = "December";
            $total_month_day = 31;
        } else {
            $mName = "";
            $total_month_day = 30;
        }
        ?>
        <p style="text-align:center; font-weight:bold; font-size:12px"> <?php echo ucfirst($branch); ?></p>
        <p style="text-align:center; font-size:11px">Salary Sheet of <?php echo ucfirst($mName); ?> ,<?php echo $syear; ?> </p>
        <p style="text-align:left; margin-bottom:10px;font-weight:bold; font-size:12px"></p>
        <table width=""  border="1" class="tablesorter" style="border-collapse:collapse;">
            <thead>
                <tr>
                    <td  width="1%" style="font-size:10px;font-weight:bold; border: 1px solid black;text-align:center;vertical-align:middle" >SL No</td>
                    <td  width="4%" style="font-size:10px;font-weight:bold; border: 1px solid black;text-align:center;vertical-align:middle" class="">Employee ID No</td>					
                    <td  width="4%" style="font-size:10px;font-weight:bold; border: 1px solid black;text-align:left;vertical-align:middle" class="">Name</td>	
                    <td  width="3%" style="font-size:10px;font-weight:bold; border: 1px solid black;text-align:center;vertical-align:middle" class="">Designation</td>                                           
                        <td  width="5%" style="font-size:10px;font-weight:bold; border: 1px solid black;text-align:center;vertical-align:middle" class="">Account No</td>
                    <td  width="5%" style="font-size:10px;font-weight:bold; border: 1px solid black;text-align:center;vertical-align:middle" class="">Net Payable</td>


                </tr>

            </thead>
            <tbody>
                <?php
                $i = 'even';
                $c = 1;
                $total = 0;
                $total_gross_salary = 0;
                $total_basic = 0;
                $total_house_rent = 0;
                $total_medical = 0;
                $total_conveyance = 0;
                $total_special = 0;
                $total_arrear = 0;
                $total_mobile_bill = 0;
                $total_bonus = 0;
                $total_gross_payable = 0;
                $total_absent_deduction = 0;
                $total_loan = 0;
                $total_tax = 0;
                $total_kpi = 0;
                $total_deduction_amount = 0;
                $total_advance = 0;

                foreach ($query as $row) {
                    $empInfo = $this->salary_model->getEmpInfo($row->emp_id);
                    $deg = "";
                    $payment_mode = "";
                    $bank_name = "";
                    $bank_branch = "";
                    $account_number = "";
					$empStatus = "";
                    foreach ($empInfo as $temp):
                        $deg = $temp->designation;
                        $payment_mode = $temp->payment_mode;
                        $bank_name = $temp->bank_name;
                        $bank_branch = $temp->bank_branch;
                        $account_number = $temp->account_number;
						$empStatus = $temp->status;
                    endforeach;

                    $bankName = "";
                    $query = $this->db->query("select bank_name from bank where id='" . $bank_name . "'");
                    foreach ($query->result() as $temp2):
                        $bankName = $temp2->bank_name;
                    endforeach;
                    $branchName = "";
                    $query99 = $this->db->query("select branch_name from branch_info where id='" . $bank_branch . "'");
                    foreach ($query99->result() as $temp23):
                        $branchName = $temp23->branch_name;
                    endforeach;


                    if ($payment_mode == '2') {
                        $paymentMsg = "Bank";
                    } else {
                        $paymentMsg = "Cash";
                    }
                    $degName = "";
                    $degInfo = $this->salary_model->getDesignation($deg);
                    foreach ($degInfo as $tempDeg):$degName = $tempDeg->designation;
                    endforeach;
                    if ($temp->category == '1') {
                        $status = "T";
                    } elseif ($temp->category == '2') {
                        $status = "P";
                    }
                    if ($temp->category == '3') {
                        $status = "P";
                    } else {
                        $status = "";
                    }
                    if ($row->present >= 0 and $payment_mode == '2'):
					 if ($empStatus == 0):
                        echo "<tr>";
                        echo "<td style='font-size:10px; border: 1px solid black;text-align:center;vertical-align:middle'>" . $c . "</td>";
                        echo "<td style='font-size:10px; border: 1px solid black;text-align:center;vertical-align:middle'>" . $temp->card_no . "</td>";
                        echo "<td style='font-size:10px; border: 1px solid black;text-align:left;vertical-align:middle'>" . ucfirst($temp->name) . "</td>";
                        echo "<td style='font-size:10px; border: 1px solid black;text-align:center;vertical-align:middle'>" . $degName . "</td>";
                      
                        echo "<td style='font-size:10px; border: 1px solid black;text-align:center;vertical-align:middle'>" . $account_number . "</td>";

                        echo "<td style='font-size:10px; border: 1px solid black;text-align:center;vertical-align:middle'>" . $row->net_salary . "</td>";


                        $total = $total + $row->net_salary;
                        $total_gross_salary = $total_gross_salary + $row->gross_salary;
                        $total_basic = $total_basic + $row->basic;
                        $total_house_rent = $total_house_rent + $row->house_rent;
                        $total_medical = $total_medical + $row->medical;
                        $total_conveyance = $total_conveyance + $row->conveyance;
                        $total_special = $total_special + $row->special;
                        $total_arrear = $total_arrear + $row->arrear;
                        $total_mobile_bill = $total_mobile_bill + $row->mobile_bill;
                        $total_bonus = $total_bonus + $row->bonus;
                        $total_gross_payable = $total_gross_payable + $row->gross_payable;
                        $total_absent_deduction = $total_absent_deduction + $row->absent_deduction;
                        $total_loan = $total_loan + $row->loan;
                        $total_advance = $total_loan + $row->advance;
                        $total_tax = $total_tax + $row->tax;
                        $total_kpi = $total_kpi + $row->kpi;
                        $total_deduction_amount = $total_deduction_amount + $row->total_deduction_amount;
                        $c++;
                    endif;
					 endif;
                }
                ?>
                <tr >
                    <td style="font-size:10px; border: 1px solid black;;vertical-align:middle"></td>
                    <td colspan="4"  style="font-size:10px; border: 1px solid black;text-align:center;vertical-align:middle">Total</td>
                                    
                    <td style="font-size:10px; border: 1px solid black;text-align:center;vertical-align:middle"><?php echo $total; ?></td>                 

                </tr>

            </tbody>
        </table>
        <div class="pager" id="pager" style="width:1000px; height:40px"></div>

        <div style="width:1000px; margin-top:50px; margin-bottom: 20px">
            <div style="width:250px; float: left; text-align: center; position:relative">
                <span style="border-top:1px solid black"></span>
            </div> 
            <div style="width:250px; float: left;text-align: center; position:relative">
                <span style="border-top:1px solid black"></span>
            </div> 
            <div style="width:250px; float: left;text-align: center; position:relative">
                <span style="border-top:1px solid black"></span>
            </div> 
            <div style="width:250px; float: left;text-align: center; position:relative">
                <span style="border-top:1px solid black"></span>
            </div> 
        </div>
        <div style="width:800px; clear: both; margin-top: 10px; height: 30px"></div>
        <div class="table-apply">
        </div>
    </div></div>
<div style="clear: both;"></div>