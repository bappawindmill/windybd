<script language="javascript">
    function confirmSubmit() {
        var agree = confirm("Are you sure to delete this record?");
        if (agree)
            return true;
        else
            return false;
    }
</script>
<!--4096-->
<script type="text/javascript" src="<?php echo base_url(); ?>/public/js/jquery.jqprint.0.3.js"></script>
<script>
    $(function() {
        $("#print_button1").click(function() {
            $("#invoice").jqprint();
        });
    });

</script>
<input type="button" id="print_button1" value="Print" style="margin-top:5px; ;">
<?php
/*
  echo form_open('hr/monthly_salary/salary_pdf');
  echo form_hidden('pyear', $syear);
  echo form_hidden('pmonth', $month);
  echo form_hidden('pbranch', $sbranch);
  echo form_submit('mysubmit', 'Export PDF');
  echo form_close();
  ?>
  <?php
  echo form_open('hr/excel/branchExcel');
  echo form_hidden('pyear', $syear);
  echo form_hidden('pmonth', $month);
  echo form_hidden('pbranch', $sbranch);
  echo form_submit('mysubmit', 'Export Excel');
  echo form_close();

 */
?>
<div class="module">
    <h2><span>View Salary</span></h2>
    <div class="module-table-body" id="invoice">
        <style>
            table .tablesorter {font-size:10px}
            div.module table td {
                background-color: #FFFFFF;
                border: 1px solid black;
                padding: 5px;
            }
        </style>
        <?php
        $this->load->model('hr/salary_model');
        $branch = "";
        $query10 = $this->db->query("select branch_name from add_branch where id='" . $sbranch . "'");
        foreach ($query10->result() as $ab) {
            $branch = $ab->branch_name;
        }
        if ($month == '01') {
            $total_month_day = 31;
            $mName = "January";
        } elseif ($month == '02') {
            $total_month_day = 28;
            $mName = "February";
        } elseif ($month == '03') {
            $total_month_day = 31;
            $mName = "March";
        } elseif ($month == '04') {
            $total_month_day = 30;
            $mName = "April";
        } elseif ($month == "05") {
            $total_month_day = 31;
            $mName = "May";
        } elseif ($month == "06") {
            $total_month_day = 30;
            $mName = "June";
        } elseif ($month == "07") {
            $total_month_day = 31;
            $mName = "July";
        } elseif ($month == "08") {
            $total_month_day = 31;
            $mName = "August";
        } elseif ($month == "09") {
            $total_month_day = 30;
            $mName = "September";
        } elseif ($month == "10") {
            $total_month_day = 31;
            $mName = "October";
        } elseif ($month == "11") {
            $total_month_day = 30;
            $mName = "November";
        } elseif ($month == "12") {
            $mName = "December";
            $total_month_day = 31;
        } else {
            $mName = "";
            $total_month_day = 30;
        }
        ?>
        <p style="text-align:center; font-weight:bold; font-size:12px"> <?php echo ucfirst($branch); ?></p>
        <p style="text-align:center; font-size:11px">Salary Sheet of <?php echo ucfirst($mName); ?> ,<?php echo $syear; ?> </p>
        <?php
        foreach ($DeptInfo as $dInfo):
            $deptID = $dInfo->id;
            $query = $this->salary_model->getDeptWiseSalary($deptID, $syear, $month);
            $dept_name = $dInfo->name;
            ?>
            <p style="text-align:left; margin-bottom:10px;font-weight:bold; font-size:12px">Department : <?php echo ucfirst($dept_name); ?>  </p>
            <table width=""  border="1" class="tablesorter" style="border-collapse:collapse;">
                <thead>
                    <tr>
                        <td rowspan="2" width="1%" style="font-size:10px;font-weight:bold; border: 1px solid black;text-align:center;vertical-align:middle" >SL No</td>
                        <td rowspan="2" width="4%" style="font-size:10px;font-weight:bold; border: 1px solid black;text-align:center;vertical-align:middle" class="">ID No</td>					
                        <td rowspan="2" width="4%" style="font-size:10px;font-weight:bold; border: 1px solid black;text-align:left;vertical-align:middle" class="">Name</td>	
                        <td rowspan="2" width="3%" style="font-size:10px;font-weight:bold; border: 1px solid black;text-align:center;vertical-align:middle" class="">Deg</td>
                        <td rowspan="2" width="3%" style="font-size:10px;font-weight:bold; border: 1px solid black;text-align:center;vertical-align:middle" class="">Status</td>                   
                        <td  colspan="12" width="2%" style="font-size:10px;font-weight:bold; border: 1px solid black;text-align:center;vertical-align:middle" class="">Gross Salary</td>                                       
                        <td rowspan="2" width="3%" style="font-size:10px;font-weight:bold; border: 1px solid black;text-align:center;vertical-align:middle" class="">Present</td>                     
                        <td rowspan="2" width="1%" style="font-size:10px;font-weight:bold; border: 1px solid black;text-align:center;vertical-align:middle" class="">Late</td>                     
                        <td  rowspan="2" width="2%" style="font-size:10px;font-weight:bold; border: 1px solid black;text-align:center;vertical-align:middle" class="">Absent</td>

                        <td  colspan="7" width="3%" style="font-size:10px;font-weight:bold; border: 1px solid black;text-align:center;vertical-align:middle" class="">Deduction</td>                     
                        <td rowspan="2" width="5%" style="font-size:10px;font-weight:bold; border: 1px solid black;text-align:center;vertical-align:middle" class="">Ot</td>
                        <td rowspan="2" width="5%" style="font-size:10px;font-weight:bold; border: 1px solid black;text-align:center;vertical-align:middle" class="">Net Payable</td>
                        <td rowspan="2" width="2%" style="font-size:10px;font-weight:bold; border: 1px solid black;text-align:center;vertical-align:middle" class="">P.M</td>

                    </tr>
                    <tr>

                        <td width="2%" style="font-size:10px;font-weight:bold; border: 1px solid black;text-align:center;vertical-align:middle" class="">Gross</td> 
                        <td width="2%" style="font-size:10px;font-weight:bold; border: 1px solid black;text-align:center;vertical-align:middle" class="">Basic</td>                     
                        <td width="2%" style="font-size:10px;font-weight:bold; border: 1px solid black;text-align:center;vertical-align:middle" class="">H.Rent</td>
                        <td width="2%" style="font-size:10px;font-weight:bold; border: 1px solid black;text-align:center;vertical-align:middle" class="">Med</td>
                        <td width="2%" style="font-size:10px;font-weight:bold; border: 1px solid black;text-align:center;vertical-align:middle" class="">Transport</td>
                        <td width="2%" style="font-size:10px;font-weight:bold; border: 1px solid black;text-align:center;vertical-align:middle" class="">Special</td>                    
                        <td width="2%" style="font-size:10px;font-weight:bold; border: 1px solid black;text-align:center;vertical-align:middle" class="">Arrear</td>
                        <td width="2%" style="font-size:10px;font-weight:bold; border: 1px solid black;text-align:center;vertical-align:middle" class="">Kpi</td>

                        <td width="2%" style="font-size:10px;font-weight:bold; border: 1px solid black;text-align:center;vertical-align:middle" class="">M.Bill</td>
                        <td width="2%" style="font-size:10px;font-weight:bold; border: 1px solid black;text-align:center;vertical-align:middle" class="">PF</td>                   
                        <td width="2%" style="font-size:10px;font-weight:bold; border: 1px solid black;text-align:center;vertical-align:middle" class="">Yearly Bonus</td>                   
                        <td width="2%" style="font-size:10px;font-weight:bold; border: 1px solid black;text-align:center;vertical-align:middle" class="">G.Payable</td>

                        <td width="3%" style="font-size:10px;font-weight:bold; border: 1px solid black;text-align:center;vertical-align:middle" class="">PF<br/>(WAL+Emp)</td>                     
                        <td width="3%" style="font-size:10px;font-weight:bold; border: 1px solid black;text-align:center;vertical-align:middle" class="">Absent</td>                     
                        <td width="3%" style="font-size:10px;font-weight:bold; border: 1px solid black;text-align:center;vertical-align:middle" class="">Loan</td>                     
                        <td width="3%" style="font-size:10px;font-weight:bold; border: 1px solid black;text-align:center;vertical-align:middle" class="">Advance</td>
                        <td width="2%" style="font-size:10px;font-weight:bold; border: 1px solid black;text-align:center;vertical-align:middle" class="">TDS</td>
                        <td width="2%" style="font-size:10px;font-weight:bold; border: 1px solid black;text-align:center;vertical-align:middle" class="">KPI</td>
                        <td width="2%" style="font-size:10px;font-weight:bold; border: 1px solid black;text-align:center;vertical-align:middle" class="">T.Deduction</td>


                    </tr>
                </thead>
                <tbody>
                    <?php
                    $i = 'even';
                    $c = 1;
                    $total = 0;
                    $total_gross_salary = 0;
                    $total_basic = 0;
                    $total_house_rent = 0;
                    $total_medical = 0;
                    $total_conveyance = 0;
                    $total_special = 0;
                    $total_arrear = 0;
                    $total_mobile_bill = 0;
                    $total_bonus = 0;
                    $total_gross_payable = 0;
                    $total_absent_deduction = 0;
                    $total_loan = 0;
                    $total_tax = 0;
                    $total_kpi = 0;
                    $total_deduction_amount = 0;
                    $total_advance = 0;

                    foreach ($query as $row) {
                        $empInfo = $this->salary_model->getEmpInfo($row->emp_id);
                        $deg = "";
                        $payment_mode = "";
                        foreach ($empInfo as $temp):
                            $deg = $temp->designation;
                            $payment_mode = $temp->payment_mode;
                        endforeach;
                        if ($payment_mode == '2') {
                            $paymentMsg = "Bank";
                        } else {
                            $paymentMsg = "Cash";
                        }
                        $degName = "";
                        $degInfo = $this->salary_model->getDesignation($deg);
                        foreach ($degInfo as $tempDeg):$degName = $tempDeg->designation;
                        endforeach;
                        if ($temp->category == '1') {
                            $status = "T";
                        } elseif ($temp->category == '2') {
                            $status = "P";
                        }
                        if ($temp->category == '3') {
                            $status = "P";
                        } else {
                            $status = "";
                        }
                        if ($row->present >= 0):
                            echo "<tr>";
                            echo "<td style='font-size:10px; border: 1px solid black;text-align:center;vertical-align:middle'>" . $c . "</td>";
                            echo "<td style='font-size:10px; border: 1px solid black;text-align:center;vertical-align:middle'>" . $temp->card_no . "</td>";
                            echo "<td style='font-size:10px; border: 1px solid black;text-align:left;vertical-align:middle'>" . ucfirst($temp->name) . "</td>";
                            echo "<td style='font-size:10px; border: 1px solid black;text-align:center;vertical-align:middle'>" . $degName . "</td>";
                            echo "<td style='font-size:10px; border: 1px solid black;text-align:center;vertical-align:middle'>" . $status . "</td>";
                            echo "<td style='font-size:10px; border: 1px solid black;text-align:center;vertical-align:middle'>" . $row->gross_salary . "</td>";
                            echo "<td style='font-size:10px; border: 1px solid black;text-align:center;vertical-align:middle'>" . $row->basic . "</td>";
                            echo "<td style='font-size:10px; border: 1px solid black;text-align:center;vertical-align:middle'>" . $row->house_rent . "</td>";
                            echo "<td style='font-size:10px; border: 1px solid black;text-align:center;vertical-align:middle'>" . $row->medical . "</td>";
                            echo "<td style='font-size:10px; border: 1px solid black;text-align:center;vertical-align:middle'>" . $row->conveyance . "</td>";
                            echo "<td style='font-size:10px; border: 1px solid black;text-align:center;vertical-align:middle'>" . $row->special . "</td>";
                            echo "<td style='font-size:10px; border: 1px solid black;text-align:center;vertical-align:middle'>" . $row->arrear . "</td>";
                            echo "<td style='font-size:10px; border: 1px solid black;text-align:center;vertical-align:middle'>" . $row->kpi_add . "</td>";

                            echo "<td style='font-size:10px; border: 1px solid black;text-align:center;vertical-align:middle'>" . $row->mobile_bill . "</td>";
                            echo "<td style='font-size:10px; border: 1px solid black;text-align:center;vertical-align:middle'>" . $row->total_pf . "</td>";
                            echo "<td style='font-size:10px; border: 1px solid black;text-align:center;vertical-align:middle'>" . $row->bonus . "</td>";
                            echo "<td style='font-size:10px; border: 1px solid black;text-align:center;vertical-align:middle'>" . $row->gross_payable . "</td>";
                            echo "<td style='font-size:10px; border: 1px solid black;text-align:center;vertical-align:middle'>" . $row->present . "</td>";
                            echo "<td style='font-size:10px; border: 1px solid black;text-align:center;vertical-align:middle'>" . $row->late . "</td>";
                            echo "<td style='font-size:10px; border: 1px solid black;text-align:center;vertical-align:middle'>" . $row->absent . "</td>";
                            echo "<td style='font-size:10px; border: 1px solid black;text-align:center;vertical-align:middle'>" . $row->total_pf . "</td>";
                            echo "<td style='font-size:10px; border: 1px solid black;text-align:center;vertical-align:middle'>" . $row->absent_deduction . "</td>";
                            echo "<td style='font-size:10px; border: 1px solid black;text-align:center;vertical-align:middle'>" . $row->loan . "</td>";
                            echo "<td style='font-size:10px; border: 1px solid black;text-align:center;vertical-align:middle'>" . $row->advance . "</td>";
                            echo "<td style='font-size:10px; border: 1px solid black;text-align:center;vertical-align:middle'>" . $row->tax . "</td>";
                            echo "<td style='font-size:10px; border: 1px solid black;text-align:center;vertical-align:middle'>" . $row->kpi . "</td>";
                            echo "<td style='font-size:10px; border: 1px solid black;text-align:center;vertical-align:middle'>" . $row->total_deduction_amount . "</td>";
                            echo "<td style='font-size:10px; border: 1px solid black;text-align:center;vertical-align:middle'>" . $row->ot . "</td>";
                            echo "<td style='font-size:10px; border: 1px solid black;text-align:center;vertical-align:middle'>" . $row->net_salary . "</td>";
                            echo "<td style='font-size:10px; border: 1px solid black;text-align:center;vertical-align:middle'>" . $paymentMsg . "</td>";

                            $total = $total + $row->net_salary;
                            $total_gross_salary = $total_gross_salary + $row->gross_salary;
                            $total_basic = $total_basic + $row->basic;
                            $total_house_rent = $total_house_rent + $row->house_rent;
                            $total_medical = $total_medical + $row->medical;
                            $total_conveyance = $total_conveyance + $row->conveyance;
                            $total_special = $total_special + $row->special;
                            $total_arrear = $total_arrear + $row->arrear;
                            $total_mobile_bill = $total_mobile_bill + $row->mobile_bill;
                            $total_bonus = $total_bonus + $row->bonus;
                            $total_gross_payable = $total_gross_payable + $row->gross_payable;
                            $total_absent_deduction = $total_absent_deduction + $row->absent_deduction;
                            $total_loan = $total_loan + $row->loan;
                            $total_advance = $total_loan + $row->advance;
                            $total_tax = $total_tax + $row->tax;
                            $total_kpi = $total_kpi + $row->kpi;
                            $total_deduction_amount = $total_deduction_amount + $row->total_deduction_amount;
                            $c++;
                        endif;
                    }
                    ?>
                    <tr >
                        <td style="font-size:10px; border: 1px solid black;;vertical-align:middle"></td>
                        <td colspan="4"  style="font-size:10px; border: 1px solid black;text-align:center;vertical-align:middle">Total</td>
                        <td style="font-size:10px; border: 1px solid black;text-align:center;vertical-align:middle"><?php echo $total_gross_salary; ?></td>
                        <td style="font-size:10px; border: 1px solid black;text-align:center;vertical-align:middle"><?php echo $total_basic; ?></td>
                        <td style="font-size:10px; border: 1px solid black;text-align:center;vertical-align:middle"><?php echo $total_house_rent; ?></td>
                        <td style="font-size:10px; border: 1px solid black;text-align:center;vertical-align:middle"><?php echo $total_medical; ?></td>
                        <td style="font-size:10px; border: 1px solid black;text-align:center;vertical-align:middle"><?php echo $total_conveyance; ?></td>                  
                        <td style="font-size:10px; border: 1px solid black;text-align:center;vertical-align:middle"><?php echo $total_special; ?></td>                  
                        <td style="font-size:10px; border: 1px solid black;text-align:center;vertical-align:middle"><?php echo $total_arrear; ?></td>
                        <td style="font-size:10px; border: 1px solid black;text-align:center;vertical-align:middle"><?php //echo $total_arrear;  ?></td>

                        <td style="font-size:10px; border: 1px solid black;text-align:center;vertical-align:middle"><?php echo $total_mobile_bill; ?></td>                  
                        <td style="font-size:10px; border: 1px solid black;text-align:center;vertical-align:middle"><?php //echo $total_conveyance;     ?></td>                  
                        <td style="font-size:10px; border: 1px solid black;text-align:center;vertical-align:middle"><?php echo $total_bonus; ?></td>                  
                        <td style="font-size:10px; border: 1px solid black;text-align:center;vertical-align:middle"><?php echo $total_gross_payable; ?></td>                  
                        <td style="font-size:10px; border: 1px solid black;text-align:center;vertical-align:middle"><?php //echo $total_conveyance;     ?></td>                  
                        <td style="font-size:10px; border: 1px solid black;text-align:center;vertical-align:middle"><?php //echo $total_conveyance;     ?></td>                  
                        <td style="font-size:10px; border: 1px solid black;text-align:center;vertical-align:middle"><?php //echo $total_conveyance;     ?></td>                  
                        <td style="font-size:10px; border: 1px solid black;text-align:center;vertical-align:middle"><?php //echo $total_conveyance;     ?></td>                  
                        <td style="font-size:10px; border: 1px solid black;text-align:center;vertical-align:middle"><?php echo $total_absent_deduction; ?></td>                  
                        <td style="font-size:10px; border: 1px solid black;text-align:center;vertical-align:middle"><?php echo $total_loan; ?></td>                  
                        <td style="font-size:10px; border: 1px solid black;text-align:center;vertical-align:middle"><?php echo $total_advance; ?></td>                  
                        <td style="font-size:10px; border: 1px solid black;text-align:center;vertical-align:middle"><?php echo $total_tax; ?></td>                  
                        <td style="font-size:10px; border: 1px solid black;text-align:center;vertical-align:middle"><?php echo $total_kpi; ?></td>                  
                        <td style="font-size:10px; border: 1px solid black;text-align:center;vertical-align:middle"><?php echo $total_deduction_amount; ?></td>                
                        <td style="font-size:10px; border: 1px solid black;text-align:center;vertical-align:middle"></td>                  
                        <td style="font-size:10px; border: 1px solid black;text-align:center;vertical-align:middle"><?php echo $total; ?></td> 
                        <td style="font-size:10px; border: 1px solid black;text-align:center;vertical-align:middle"></td>                 

                    </tr>

                </tbody>
            </table>
        <?php endforeach; ?>


        <div class="pager" id="pager" style="width:1000px; height:40px"></div>
        <div style="width:1000px; margin-top:50px; margin-bottom: 20px">
            <div style="width:250px; float: left; text-align: center; position:relative">
                <span style="border-top:1px solid black">Prepared By</span>
            </div> 
            <div style="width:250px; float: left;text-align: center; position:relative">
                <span style="border-top:1px solid black"> Checked By</span>
            </div> 
            <div style="width:250px; float: left;text-align: center; position:relative">
                <span style="border-top:1px solid black"> Recommended By </span>
            </div> 
            <div style="width:250px; float: left;text-align: center; position:relative">
                <span style="border-top:1px solid black">  Approved By</span>
            </div> 
        </div>
        <div style="width:800px; clear: both; margin-top: 10px; height: 30px"></div>
        <div class="table-apply">
        </div>
    </div></div>
<div style="clear: both;"></div>

