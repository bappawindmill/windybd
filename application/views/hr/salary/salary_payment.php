<link rel="stylesheet" type="text/css" href="<?php echo base_url(); ?>public/css/t1.css" />
<style type="text/css">
<!--
.style1 {color: #FF0000}
-->
</style>
<script type="text/javascript">
    window.onload = function(){
        new JsDatePick({
            useMode:2,
            target:"pay_date",
            dateFormat:"%Y-%m-%d"
        });
     
    }
</script>

<div class="grid_12">
    <div class="module1">
        <h2><span>Pay Salary</span></h2>

        <div class="module-body">		<?php
		$months = array('1' => 'January',
						'2' => 'February',
						'3' => 'March',
						'4' => 'April',
						'5' => 'May',
						'6' => 'June',
						'7' => 'July',
						'8' => 'August',
						'9' => 'September',
						'10' => 'October',
						'11' => 'November',
						'12' => 'December',
						);
		$type = array('Cash' => 'Cash',
						'Cheque' => 'Cheque',
						);
	   foreach($query as $row):endforeach;		
		echo form_open('hr/salary/added_salary_payment');
		//echo form_hidden('id',$row['id']);
		echo '<table>';
		echo '<tr><th>Year :</th><td>'.form_input('year',$row->year,'readonly').'<span style="color:red;">*</span></td></tr>';
		echo '<tr><th>Month :</th><td>'.form_input('month',$row->month,'readonly').'</td></tr>';
		echo '<tr><th>Card No :</th><td>'.form_input('card_no',$row->card_no,'readonly').'</td></tr>';
		?>
		<tr><th>pay date :</th><td><input type="text" name="pay_date" id="pay_date" value=""  /></td></tr>
		<?php
		echo '<tr><th>pay type :</th><td>'.form_dropdown('pay_type',$type,'pay_type').'</td></tr>';
		echo '<tr><th>pay by :</th><td>'.form_input('pay_by','','pay_by').'<span style="color:red;">*</span></td></tr>';
         $a=round($row->net_payable);
		echo '<tr><th>pay amount :</th><td>'.form_input('pay_amount',$a,'readonly').'<span style="color:red;">*</span></td></tr>';
		//echo '<tr><th>pay type :</th><td>'.form_input('pay_type','','pay_type').'<span style="color:red;">Hour</span></td></tr>';
		
		//echo '<tr><th>Amount :</th><td>'.form_input('amount','','amount').'</td></tr>';
		echo '<tr><th></th><td>'.form_submit('mysubmit','Pay Salary').'</td></tr>';
		echo '<tr><th></th><td>'.validation_errors('<p class="error">').'</td></tr>';
		echo '</table>';
echo form_close(); 
?>
</div></div></div>
<div style="clear: both;"></div>
