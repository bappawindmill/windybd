<?php $this->load->view('hr/layout');?>
                            <div class="art-Post-body">
                        <div class="art-Post-inner">
                                        <div class="art-PostMetadataHeader">
                                            <h2 class="art-PostHeader">
                                                <img src="<?php echo base_url();?>images/PostHeaderIcon.png" width="29" height="29" alt="" />
                                                Pay Salary Slip
                                            </h2>
                                        </div>
                                        <div class="art-PostContent">
                                              <br />
		<?php
		$months = array('1' => 'January',
						'2' => 'February',
						'3' => 'March',
						'4' => 'April',
						'5' => 'May',
						'6' => 'June',
						'7' => 'July',
						'8' => 'August',
						'9' => 'September',
						'10' => 'October',
						'11' => 'November',
						'12' => 'December',
						);				
		echo form_open('hr/salary/salary_slip');
		echo '<table>';?>
         <tr><th>Year:</th><td>
         <select name="year" style="width:60px;">
				<?php for($i=1920;$i<= date('Y');$i++){?>                
                <option value="<?php echo $i; ?> "><?php  echo $i;?></option>
                <?php }?>                
				 </select><span style="color:red;">*</span>
           </td></tr>
		<?php //echo '<tr><th>Year :</th><td>'.form_input('year','','year').'</td></tr>';
		echo '<tr><th>Month :</th><td>'.form_dropdown('month',$months,'month').'</td></tr>';
		?>
		{EMPLOYEE}
		<tr><th>Employee :</th><td><div id='result1'></div></td></tr>
		<?php
		echo '<tr><th></th><td>'.form_submit('mysubmit','Salary Details').'</td></tr>';
		echo '<tr><th></th><td>'.validation_errors('<p class="error">').'</td></tr>';
		echo '</table>';
echo form_close(); 
?>

 <br /> <br />
                                            	
                                                
                                        </div>
                                        <div class="cleared"></div>
                        </div>
                        
                        		<div class="cleared"></div>
                            </div>
                        </div>
                    </div>
                <div class="cleared"></div>
				<?php $this->load->view('hr/footer');?>
