<link rel="stylesheet" type="text/css" href="<?php echo base_url(); ?>public/css/t1.css" />
<style type="text/css">
<!--
.style1 {color: #FF0000}
-->
</style>
<div class="grid_12">
    <div class="module1">
        <h2><span>Monthly Salary</span></h2>

        <div class="module-body">	
<?php
		echo form_open('hr/salary/salary_pdf');

		echo form_hidden('year',$fyear['value']);
		echo form_hidden('month',$fmonth['value']);
		echo form_hidden('dept',$fdept['value']);
		echo form_submit('mysubmit','Export PDF');
		echo form_close();
		echo '<br />';
?>		
<table border="1" cellpadding="1" cellspacing="1" align="center">
<thead>
  <tr>
    <th>SL No</th>
	<th>Emp. Name</th>
    <th>Working Day</th>
	<th>Earned Salary</th>
	<th>Total OT</th>
	<th>OT Money</th>
	<th>Total OD</th>
    <th>OD Money</th>
	<th>Absence Money</th>
<!--	<th>HA</th>
	<th>FA</th>  
	<th>Conv</th>-->
	<th>Bonus</th>
	<th>Deduction</th>  
	<th>Total Salary</th>   
  </tr>
</thead>
<tbody>
<?php 
$i = 'even';
$c=1;
$total = 0;
foreach($query as $row){
if($i=='even')
{
	$i = 'odd';
}
else
{
	$i = 'even';
}

    echo "<tr class = ".$i.">";
    echo "<td>". $c ."</td>";
	$res=$this->db->query("SELECT name, salary_package FROM employeeinfo WHERE id=".$row->emp_id);
	foreach ($res->result() as $row3)
	{
		echo "<td>". $row3->name ."</td>";
	}
	/*
	$res1=$this->db->query("SELECT * FROM salary_package WHERE id=".$salary_package);
	foreach ($res1->result() as $row1)
	{
		$basic = $row1->basic;
		$house_allowance = $row1->house_allowance;
		$food_allowance = $row1->food_allowance;
		$conveyance_allowance = $row1->conveyance_allowance;
	}
	
	$query4 = $this->db->query("SELECT * FROM leave_management WHERE emp_id='".$row->emp_id."' AND month='".$fmonth['value']."' AND year=".$fyear['value']);
			$t_abs = 0;
			foreach ($query4->result() as $row4)
			{
				$duration = $row4->duration;
				$leave_type = $row4->leave_type;
				if($leave_type = 4)
				{
					$t_abs = $t_abs + $duration;
				}
			}

	$salary = $basic+$house_allowance+$food_allowance+$conveyance_allowance;
	$day = $salary/30;
	$earned = $salary - ($day*$t_abs);
	$hour = $day/8;*/
    echo "<td>". $row->total_attend ."</td>";
	echo "<td>". $row->earned ."</td>";
	echo "<td>". $row->total_ot ."</td>";
	echo "<td>". $row->ot_amount ."</td>";
	echo "<td>". $row->total_od ."</td>";
    echo "<td>". $row->od_amount ."</td>";
	echo "<td>". $row->absence ."</td>";
/*	echo "<td>". $house_allowance ."</td>";
    echo "<td>". $food_allowance ."</td>";
	echo "<td>". $conveyance_allowance ."</td>";*/
    echo "<td>". $row->bonus ."</td>";
	echo "<td>". $row->deduction ."</td>";  
	echo "<td>". $row->total ."</td>";  
	//echo "<td>". anchor('hr/hr/employee_info_edit/'.$row->id,'Edit') ."</td>";
    //echo "<td>". anchor('hr/hr/employee_info_delete/'.$row->id,'Delete',array('onclick'=>"return confirm('Are You Sure Want To Delete?')")) ."</td></tr>"; 
	$total = $total + $row->total;
	$c++;
}
?>
<tr><th colspan="11">Total: </th><td><?php echo $total;?></td></tr>
</tbody>
</table>
</div></div></div>
<div style="clear: both;"></div>
