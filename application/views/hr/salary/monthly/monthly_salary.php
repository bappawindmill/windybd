<link rel="stylesheet" type="text/css" href="<?php echo base_url(); ?>public/css/t1.css" />
<style type="text/css">
<!--
.style1 {color: #FF0000}
-->
</style>
<div class="grid_12">
    <div class="module1">
        <h2><span>Monthly Salary</span></h2>

        <div class="module-body">		<?php
		$months = array('1' => 'January',
						'2' => 'February',
						'3' => 'March',
						'4' => 'April',
						'5' => 'May',
						'6' => 'June',
						'7' => 'July',
						'8' => 'August',
						'9' => 'September',
						'10' => 'October',
						'11' => 'November',
						'12' => 'December',
						);				
		echo form_open('hr/salary/monthly_report');
		echo '<table>';?>
         <tr><th>Year:</th><td>
         <select name="year" style="width:60px;">
				<?php for($i=1920;$i<= date('Y');$i++){?>                
                <option value="<?php echo $i; ?> "><?php  echo $i;?></option>
                <?php }?>                
				 </select><span style="color:red;">*</span>
           </td></tr>
	<?php	echo '<tr><th>Month :</th><td>'.form_dropdown('month',$months,'month').'</td></tr>';
		$query66 = $this->db->query("SELECT id,branch_name FROM add_branch");

        $dataStr12 = "<tr><th>Branch Name :</th><td><select name='branch_name' id='crlbranch' style='width:130px;'>";
        $dataStr12.="<option value='select'>select</option>";
        foreach ($query66->result() as $row33) {
            $dataStr12.="<option value='" . $row33->id . "'";
            $dataStr12.=">" . $row33->branch_name . "</option>";
        }

        $dataStr12.='</select></td></tr>';
		echo $dataStr12;
		?>
		
		<tr><th>Department :</th><td><div id='res'></div></td></tr>
		<?php
		echo '<tr><th></th><td>'.form_submit('mysubmit','Salary Details').'</td></tr>';
		echo '<tr><th></th><td>'.validation_errors('<p class="error">').'</td></tr>';
		echo '</table>';
echo form_close(); 
?>
</div></div></div>
<div style="clear: both;"></div>
