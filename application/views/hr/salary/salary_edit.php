<link rel="stylesheet" type="text/css" href="<?php echo base_url(); ?>public/css/t1.css" />
<style type="text/css">
<!--
.style1 {color: #FF0000}
-->
</style>
<div class="grid_12">
    <div class="module1">
        <h2><span>Edit Salary</span></h2>

        <div class="module-body">
		<script type="text/javascript" charset="utf-8"> 
		$(document).ready(function() {
		
			var loadUrl='<?php echo site_url('hr/hr/get_package');?>';
			
			$("select#ctlPackage").change(function(){
				var name=$("#ctlPackage").val();

				$("#result2").html('Loading....').load(loadUrl+"/"+name);
		
		
			})			
		})
		</script> 											  
		<?php
		$row = $query;
		$months = array('1' => 'January',
						'2' => 'February',
						'3' => 'March',
						'4' => 'April',
						'5' => 'May',
						'6' => 'June',
						'7' => 'July',
						'8' => 'August',
						'9' => 'September',
						'10' => 'October',
						'11' => 'November',
						'12' => 'December',
						);
		$type = array('Bonus' => 'Bonus',
						'Deduction' => 'Deduction',
						);
						
		echo form_open('hr/salary/edit_salary/'.$row['id']);
		echo '<table>';
		echo '<tr><th>Year :</th><td>'.form_input('year',$row['year'],'year').'<span style="color:red;">*</span></td></tr>';
		echo '<tr><th>Month :</th><td>'.form_dropdown('month',$months,$row['month']).'</td></tr>';
		$query3 = $this->db->get_where('employeeinfo', array('id'=>$row['emp_id']));
  		$row3 = $query3->row_array();
		echo form_hidden('emp_id',$row3['id']);
		echo '<tr><th>Employee :</th><td>'.form_input('name',$row3['name'],'readonly').'</td></tr>';
		echo '<tr><th>Total Working Day :</th><td>'.form_input('total_attend',$row['total_attend'],'total_attend').'<span style="color:red;">*</span></td></tr>';
		echo '<tr><th>Total OT :</th><td>'.form_input('total_ot',$row['total_ot'],'total_ot').'<span style="color:red;">Hour</span></td></tr>';
		echo '<tr><th>Total OD :</th><td>'.form_input('total_od',$row['total_od'],'total_od').'<span style="color:red;">Hour</span></td></tr>';
		
		if($row['bonus'] != 0)
		{
			echo '<tr><th>Choose Bonus or Deduction :</th><td>'.form_dropdown('choose',$type,'Bonus').'</td></tr>';
			echo '<tr><th>Amount :</th><td>'.form_input('amount',$row['bonus'],'amount').'</td></tr>';
		}
		else
		{
			echo '<tr><th>Choose Bonus or Deduction :</th><td>'.form_dropdown('choose',$type,'Deduction').'</td></tr>';
			echo '<tr><th>Amount :</th><td>'.form_input('amount',$row['deduction'],'amount').'</td></tr>';
		}
		echo '<tr><th></th><td>'.form_submit('mysubmit','Add Emp Salary').'</td></tr>';
		echo '<tr><th></th><td>'.validation_errors('<p class="error">').'</td></tr>';
		echo '</table>';
		?>
		<div id="result2"></div>
		<?php
echo form_close(); 
?>
</div></div></div>
<div style="clear: both;"></div>
         
