<link rel="stylesheet" type="text/css" href="<?php echo base_url(); ?>public/css/t1.css" />
<style type="text/css">
    <!--
    .style1 {color: #FF0000}
    -->
</style>
<script type="text/javascript">
                window.onload = function(){
                    new JsDatePick({
                        useMode:2,
                        target:"start_date",
                        dateFormat:"%Y-%m-%d"
                    });
                    new JsDatePick({
                        useMode:2,
                        target:"end_date",
                        dateFormat:"%Y-%m-%d"
                    });
                }
</script>
<div class="grid_12">
    <div class="module1">
        <h2><span>Pay Slip</span></h2>
 <?php echo $this->session->flashdata('message'); ?>
        <div class="module-body">
            <?php
            $query3 = $this->db->query("select id,branch_name from add_branch");
            ?>
            <?php
            echo form_open('hr/salary/get_slip');

            echo '<table>';
            echo '<thead><tr><th colspan="2"><b></b></th></tr></thead>';
					$months = array('01' => 'January',
						'02' => 'February',
						'03' => 'March',
						'04' => 'April',
						'05' => 'May',
						'06' => 'June',
						'07' => 'July',
						'08' => 'August',
						'09' => 'September',
						'10' => 'October',
						'11' => 'November',
						'12' => 'December',
						);
            ?>
            <tr><th>Branch Name :</th><td>
                    <select name="branch_name" id="crlbranch" class="input-short" style="width:130px;">
                        <option   value="">Select</option>									
                        <?php foreach ($query3->result() as $row3): ?>
                            <option value="<?php echo $row3->id; ?>"><?php echo $row3->branch_name; ?></option>
                        <?php endforeach; ?>
                    </select>
                    <span class="notification-input ni-error"> <?php echo ucwords(form_error('branch_name')); ?></span>
                </td></tr>   
            <tr><th>department :</th><td><div id='res'>
                        <select style="width:130px;">
                            <option  value="">Select</option>
                        </select>

                    </div><span class="notification-input ni-error"><?php echo ucwords(form_error('dept')); ?></span></td></tr>
            <tr><th>section :</th><td><div id='result'>
                        <select style="width:130px;">
                            <option  value="">Select</option>
                        </select>

                    </div><span class="notification-input ni-error"><?php echo ucwords(form_error('proffession')); ?></span></td></tr>
                                <tr><th>Card No :</th><td>
                       <input type="text" name="card_no" />

                    </div><span class="notification-input ni-error"><?php echo ucwords(form_error('card_no')); ?></span></td></tr>

                     <tr><th>Year:</th><td>
         <select name="year" style="width:130px;">
				<?php for($i=2011;$i<= date('Y');$i++){?>                
                <option  value="<?php echo $i; ?> " ><?php  echo $i;?></option>
                <?php }?>                
				 </select><span style="color:red;"></span>
           </td></tr>
           
     <?php echo '<tr><th>Month :</th><td >'.form_dropdown('month',$months,'month','style="width:130px"').'</td></tr>';
            echo '<tr><th></th><td>' . form_submit('mysubmit', 'Submit') . '</td></tr>';

            echo '</table>';
            echo form_close();
            ?>
        </div></div></div>
<div style="clear: both;"></div>
