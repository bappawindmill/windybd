<link rel="stylesheet" type="text/css" href="<?php echo base_url(); ?>public/css/t1.css" />
<style type="text/css">
<!--
.style1 {color: #FF0000}
-->
</style>
<div class="grid_12">
    <div class="module1">
        <h2><span>Create Salary</span></h2>

        <div class="module-body">
		<script type="text/javascript" charset="utf-8"> 
		$(document).ready(function() {
			var loadUrl='<?php echo site_url('hr/hr/get_package');?>';
			$("select#ctlPackage").change(function(){
				var name=$("#ctlPackage").val();
				$("#result2").html('Loading....').load(loadUrl+"/"+name);
			})			
		})
		</script> 											  
		<?php
		$months = array('1' => 'January',
						'2' => 'February',
						'3' => 'March',
						'4' => 'April',
						'5' => 'May',
						'6' => 'June',
						'7' => 'July',
						'8' => 'August',
						'9' => 'September',
						'10' => 'October',
						'11' => 'November',
						'12' => 'December',
						);
		$type = array('Bonus' => 'Bonus',
						'Deduction' => 'Deduction',
						);
						
		echo form_open('hr/salary/added_salary');
		echo '<table>';?>
        <tr><th>Year:</th><td>
         <select name="year" style="width:130px;">
				<?php for($i=2000;$i<= date('Y');$i++){?>                
                <option value="<?php echo $i; ?> "><?php  echo $i;?></option>
                <?php }?>                
				 </select><span style="color:red;">*</span>
           </td></tr>
     <?php echo '<tr><th>Month :</th><td >'.form_dropdown('month',$months,'month','style="width:130px"').'</td></tr>';
		$query66 = $this->db->query("SELECT id,branch_name FROM add_branch");

        $dataStr12 = "<tr><th>Branch Name :</th><td><select name='branch_name' id='crlbranch' style='width:130px;'>";
        $dataStr12.="<option value='select'>select</option>";
        foreach ($query66->result() as $row33) {
            $dataStr12.="<option value='" . $row33->id . "'";
            $dataStr12.=">" . $row33->branch_name . "</option>";
        }
        $dataStr12.='</select></td></tr>';
		?>
		<?php echo $dataStr12;?>
       	<tr><th>Department :</th><td><div id='res'></div></td></tr>
		<tr><th>Employee :</th><td><div id='result1'></div></td></tr>
		<?php
		echo '<tr><th>Total Working Day :</th><td>'.form_input('total_attend','','total_attend').'<span style="color:red;">*</span></td></tr>';
		echo '<tr><th>Total OT :</th><td>'.form_input('total_ot','','total_ot').'<span style="color:red;">Hour</span></td></tr>';
		echo '<tr><th>Choose Bonus or Deduction :</th><td>'.form_dropdown('choose',$type,'choose').'</td></tr>';
		echo '<tr><th>Amount :</th><td>'.form_input('amount','','amount').'</td></tr>';
		echo '<tr><th></th><td>'.form_submit('mysubmit','Add Emp Salary').'</td></tr>';
		echo '<tr><th></th><td>'.validation_errors('<p class="error">').'</td></tr>';
		echo '</table>';
		?>
		<div id="result2"></div>
		<?php
    echo form_close(); 
?>

 <br /> <br />
<table width="100%" border="1" cellpadding="0" cellspacing="0" align="left">
<thead>
  <tr>
    <th style="width:5%">SL No</th>
    <th style="width:5%">Month / Year</th>
	<th style="width:5%">Employee</th>
	<th style="width:5%">Total Working Day</th>
    <th style="width:5%">Total OT</th>
	
    <th style="width:5%">Provident Fund</th>
	<th style="width:5%">Earned</th>
	<th style="width:5%">Bonus Amount</th>  
	<th style="width:5%">Deduction Amount</th> 
	<th style="width:5%">Total Amount</th>  
    <th colspan="3" style="width:5%">Action</th>  
  </tr>
</thead>
<tbody>
<?php 

$i = 'even';
$c=1;
foreach($query as $row){
if($i=='even')
{
	$i = 'odd';
}
else
{
	$i = 'even';
}

    echo "<tr class = ".$i.">";
    echo "<td>". $c ."</td>";
    echo "<td>". $row->month ." / ".$row->year."</td>";
	echo "<td>". $row->ename ."</td>";
	echo "<td>". $row->total_attend ."</td>";
	echo "<td>". $row->ot_amount ."</td>";
	echo "<td>". $row->pfamount ."</td>";
	echo "<td>". $row->earned ."</td>";
	echo "<td>". $row->bonus ."</td>";
	echo "<td>". $row->deduction ."</td>";
	echo "<td>". $row->total ."</td>";
    
	echo "<td>". anchor('hr/salary/edit_salary/'.$row->id,'Edit') ."</td>";
    //echo "<td>". anchor('hr/salary/salary_delete/'.$row->id.'/'.$row->year.'/'.$row->month,'Delete',array('onclick'=>"return confirm('Are You Sure Want To Delete?')")) ."</td>"; 
	 echo "<td>". anchor('hr/salary/salary_delete/'.$row->id,'Delete',array('onclick'=>"return confirm('Are You Sure Want To Delete?')")) ."</td>"; 

//	$query2 = $this->db->get_where('salary', array('id'=>$row->id,'year'=>$row->year,'id'=>$row->year,));
	//$row2 = $query2->row_array();
	$query3 = $this->db->get_where('salary_payment', array('emp_id'=>$row->emp_id,'year'=>$row->year,'month'=>$row->month));
	$row3 = $query3->row_array();
	if($query3->num_rows()>0)
	{
		echo "<td>Paid</td></tr>";	
	}
	else
	{
		echo "<td>". anchor('hr/salary/added_salary_payment/'.$row->id,'Pay Salary') ."</td></tr>";
	}

	$c++;
}

?>
</tbody>
</table>



    <?php
            echo $this->pagination->create_links();
			
			?>
</div></div></div>
<div style="clear: both;"></div>
         