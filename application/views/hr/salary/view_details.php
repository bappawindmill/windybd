<script language="javascript">
    function confirmSubmit() {
        var agree=confirm("Are you sure to delete this record?");
        if (agree)
            return true;
        else
            return false;
    }
</script>
<style>
    .table thead tr th{ font-size:7px}
</style>
<script type="text/javascript" src="<?php echo base_url(); ?>/public/js/jquery.jqprint.0.3.js"></script>
<script>
    $(function(){
        $("#print_button1").click( function() {
            $("#invoice").jqprint();
        });
    });

</script>
<input type="button" id="print_button1" value="Print" style="margin-top:5px; ;">
<div class="module">
    <h2><span>View Salary</span></h2>
    <div class="module-table-body" id="invoice">
        <table width="900" border="1" class="tablesorter" id="myTable" >
            <thead>
                <tr>
                    <th width="5%" >SL No</th>
                    <th width="5%" class="">Card No</th> 
                    <th width="5%" class="">Name</th>
                    <th width="5%" class="">Branch Name</th>  
                    <th width="5%" class="">Department Name</th> 
                    <th width="5%" class="">Section Name</th> 
                    <th width="5%" class="">Shift</th> 
                    <th width="5%" class="">Basic</th> 
                    <th width="5%" class="">Other Allowance</th>
                    <th width="5%" class="">Gross Salary</th> 
                    <th width="5%" class="">OtHour</th> 
                    <th width="5%" class="">OtAmount</th> 
                    <th width="5%" class="">Holiday</th>                     
                    <th width="5%" class="">Present</th> 
                    <th width="5%" class="">Absent</th> 
                    <th width="5%" class="">Net Payable</th>
                    
                </tr>
            </thead>
            <tbody>
                <?php
                $i = 'even';
                $c = 1;
                foreach ($query as $row) {
                    if ($i == 'even') {
                        $i = 'odd';
                    } else {
                        $i = 'even';
                    }

                    $query10 = $this->db->query("select branch_name from add_branch where id='" . $row->branch_name . "'");
                    foreach ($query10->result() as $ab) {
                        $branch = $ab->branch_name;
                    }
                    if (empty($branch)):$branch = "";
                    endif;
                    $query11 = $this->db->query("select name from add_department where id='" . $row->dept . "'");
                    foreach ($query11->result() as $ab1) {
                        $dept = $ab1->name;
                    }
                    if (empty($dept)):$dept = "";
                    endif;
                    $query12 = $this->db->query("select proffession from add_proffession where id='" . $row->section . "'");
                    foreach ($query12->result() as $ab2) {
                        $section = $ab2->proffession;
                    }
                    if (empty($section)):$section = "";
                    endif;
                    $q = $this->db->query("select shift_name from shift where id='" . $row->shift . "'");
                    foreach ($q->result() as $q8) {
                        $shift_name = $q8->shift_name;
                    }
                    if (empty($shift_name)):$shift_name = "";
                    endif;
                    echo "<tr class = " . $i . ">";
                    echo "<td>" . $c . "</td>";
                    echo "<td>" . $row->card_no . "</td>";
                    echo "<td>" . $row->name . "</td>";
                    echo "<td>" . $branch . "</td>";
                    echo "<td>" . $dept . "</td>";
                    echo "<td>" . $section . "</td>";
                    echo "<td>" . $shift_name . "</td>";
                    echo "<td>" . $row->basic . "</td>";
                    echo "<td>" . $row->other_allowance . "</td>";
                    echo "<td>" . $row->gross_salary . "</td>";
                    echo "<td>" . $row->ot_hour . "</td>";
                    echo "<td>" . round($row->ot_amount) . "</td>";
                    echo "<td>" . $row->holiday . "</td>";
                    echo "<td>" . $row->present . "</td>";
                    echo "<td>" . $row->absent . "</td>";
                    echo "<td>" . round($row->net_payable) . "</td>";
                    /*$query3 = $this->db->get_where('salary_payment', array('card_no' => $row->card_no, 'year' => $row->year, 'month' => $row->month));
                    $row3 = $query3->row_array();
                    if ($query3->num_rows() > 0) {
                        echo "<td>Paid</td></tr>";
                    } else {
                        echo "<td>" . anchor('hr/salary/add_salary_payment/' . $row->id, 'Pay Salary') . "</td></tr>";
                    }*/
                    $c++;
                }
                ?>
            </tbody>
        </table>
        <div class="pager" id="pager"></div>
        <div class="table-apply">
        </div>
    </div></div>
<div style="clear: both;"></div>

