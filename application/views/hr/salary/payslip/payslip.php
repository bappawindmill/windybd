<p><input type="button" id="print_button" value="Print" style="margin-top:5px;"><br /><br /></p>

<div class="grid_12">
    <div class="module1">
        <h2><span>View Pay Slip</span></h2>
        <?php echo $this->session->flashdata('message'); ?>
        <div class="module-body" id="invoice">
            <?php foreach ($query as $row): ?>
                <?php
                $months = array('01' => 'January',
                    '02' => 'February',
                    '03' => 'March',
                    '04' => 'April',
                    '05' => 'May',
                    '06' => 'June',
                    '07' => 'July',
                    '08' => 'August',
                    '09' => 'September',
                    '10' => 'October',
                    '11' => 'November',
                    '12' => 'December',
                );
                $monTh = "";
                foreach ($months as $key => $val) {
                    if ($key == $row->month) {
                        $monTh = $val;
                    }
                }
                ?>
                <?php
                $payment_mode = "";
                $bank_name = "";
                $bank_branch = "";
                $account_number = "";
                $empInfo = $this->db->query("SELECT * FROM employeeinfo where id='" . $row->emp_id . "'");
                foreach ($empInfo->result() as $temp):
                    $payment_mode = $temp->payment_mode;
                    $bank_name = $temp->bank_name;
                    $bank_branch = $temp->bank_branch;
                    $account_number = $temp->account_number;
                endforeach;


                $degName = "";
                $query66 = $this->db->query("SELECT id,designation FROM designation where id='" . $row->designation . "'");
                foreach ($query66->result() as $row33) {
                    $degName = ucfirst($row33->designation);
                }
                ?>
                <?php
                $brName = "";
                $query11 = $this->db->query("SELECT id,branch_name FROM add_branch where id='" . $row->branch_name . "'");
                foreach ($query11->result() as $row3) {
                    $brName = ucfirst($row3->branch_name);
                }
                ?>

                <table class="table table-hover table-bordered">
                    <!--<tr>
                        <td colspan="4"><img src="<?php /*echo base_url();*/?>public/images/logo1.png"/></td>
                    </tr>-->
					 <tr>
                        <td colspan="4"style="text-align:center";>Pay slip</td>

                    </tr>
					 <tr>
                        <td colspan="4" style="text-align:center;">Private & Confidential</td>

                    </tr>
                    <tr>
                        <td colspan="2">PAYSLIP FOR THE MONTH OF</td>
                        <td colspan="2"><?php echo $monTh . "'" . $row->year; ?></td>
                    </tr>
                    <tr>
                        <td colspan="2">Name of Employee</td>
                        <td colspan="2"><?php echo ucwords($row->name); ?></td>
                    </tr>
                    <tr>
                        <td colspan="2">Designation</td>
                        <td colspan="2"><?php echo $degName; ?></td>
                    </tr>

                    <tr>
                        <td colspan="2">Gross Salary</td>
                        <td colspan="2">BDT <?php echo $row->gross_salary; ?></td>
                    </tr>
                    <tr>
                        <td colspan="2">Joining Date</td>
                        <td colspan="2"><?php echo date('M d,Y', strtotime($row->join_date)); ?></td>
                    </tr>
                    <?php if ($payment_mode == '2'):

                         $bankName = "";
                        $query10 = $this->db->query("select bank_name from bank where id='" . $bank_name . "'");
                        foreach ($query10->result() as $temp2):
                            $bankName = $temp2->bank_name;
                        endforeach;

                        ?>
                        <tr>
                            <td colspan="2">Bank Name</td>
                            <td colspan="2"><?php echo ucfirst($bankName); ?></td>
                        </tr>
                        <tr>
                            <td colspan="2">Account No</td>
                            <td colspan="2"><?php echo $account_number;?></td>
                        </tr>
                    <?php endif; ?>

                </table>


                <table class="table table-bordered table-hover">

                    <tr>
                        <th>Payment Heads</th>
                        <th>Amount</th>
                        <th>Payment Heads</th>
                        <th>Amount</th>
                    </tr>

                    <tr>
                        <td>Basic Salary</td>
                        <td>BDT <?php echo $row->basic; ?></td>
                        <td>PF Contribution (Office+Employee)</td>
                        <td>BDT <?php echo $row->total_pf; ?></td>
                    </tr>

                    <tr>
                        <td>House Rent Allowance (60%)</td>
                        <td>BDT <?php echo $row->house_rent; ?></td>
                        <td>Adjustment ( Absent )</td>
                        <td>BDT <?php echo $row->absent_deduction; ?></td>
                    </tr>

                    <tr>
                        <td>Medical Allowance (20%)</td>
                        <td>BDT <?php echo $row->medical; ?></td>
                        <td>Advance Adjustment (I.O.U)</td>
                        <td>BDT <?php echo $row->advance; ?></td>
                    </tr>

                    <tr>
                        <td>Conveyance Allowance (10%)</td>
                        <td>BDT <?php echo $row->conveyance; ?></td>
                        <td>Loan Adjustment (negative)</td>
                        <td>BDT <?php echo $row->loan; ?></td>
                    </tr>

                    <tr>
                        <td>Special Allowance (10%)</td>
                        <td>BDT <?php echo $row->special; ?></td>
                        <td>Tax Deducted at Source</td>
                        <td>BDT <?php echo $row->tax; ?></td>
                    </tr>

                    <tr>
                        <td>PF Contribution (Windmill-10%)</td>
                        <td>BDT <?php echo $row->pf_employeer; ?></td>
                        <td>KPI</td>
                        <td>BDT <?php echo $row->kpi; ?></td>
                    </tr>



                    <tr>
                        <td>Arrear</td>
                        <td>BDT <?php echo $row->arrear; ?></td>
                        <td></td>
                        <td></td>
                    </tr>

                    <tr>
                        <td>Mobile Allowance ( Fixed )</td>
                        <td>BDT <?php echo $row->mobile_bill; ?></td>
                        <td></td>
                        <td></td>
                    </tr>
                    <tr>
                        <td>Half Yearly Bonus</td>
                        <td>BDT <?php echo $row->bonus; ?></td>
                        <td style="color:red;"></td>
                        <td style="color:red;"></td>
                    </tr>
                    <tr>
                        <td>KPI/Performance</td>
                        <td>BDT <?php echo $row->kpi_add; ?></td>
                        <td style="color:red;"></td>
                        <td style="color:red;"></td>
                    </tr>
                    <tr>
                        <td>OT</td>
                        <td>BDT <?php echo $row->ot; ?></td>
                        <td style="color:red;"></td>
                        <td style="color:red;"></td>
                    </tr>



                    <tr>
                        <td><p><br /></td>
                        <td><p><br /></td>
                        <td><p><br /></td>
                        <td><p><br /></td>
                    </tr>

                    <tr>
                        <td>Total Gross Pay</td>
                        <td>BDT <?php echo $row->gross_payable ?></td>


                        <td>Total Deduction</td>
                        <td>BDT <?php echo $row->total_deduction_amount; ?></td>
                    </tr>


                </table>

                <table class="table table-bordered table-hover">
                    <tr>
                        <td colspan="2"> Net Amount Transferred By
                              <?php if ($payment_mode == '2'):echo "Bank";else:echo "Cach";endif;?> </td>
                        <td colspan="2">BDT <?php echo $row->net_salary; ?></td>
                    </tr>
                </table>

                <table class="table table-hover table-bordered">
                    <tr>
                        <th> Note 1 : For Any clarification please contact hr@windmill.com.bd<br />
						Note 2 :  This is a system generated report and requires no manual signature.
						 </th>
                    </tr>
                </table>

            <?php endforeach; ?>

        </div></div></div>
<div style="clear: both;"></div>