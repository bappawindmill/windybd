<h3><i class="fa fa-angle-right"></i> Payslip</h3>

<!-- BASIC FORM ELELEMNTS -->
<div class="row">
    <div class="col-sm-12">
        <div class="content-panel">
            <?php
            echo form_open('hr/payslip/viewed');?>
            <?php
            if($this->session->flashdata('message')==NULL){

            }else{ ?>
                <div class="alert-success alert">
                    <?php echo $this->session->flashdata('message') ?>
                </div> <?php
            }
            ?>
            <?php
            $months = array(
                ''=>'Select',
                '01' => 'January',
                '02' => 'February',
                '03' => 'March',
                '04' => 'April',
                '05' => 'May',
                '06' => 'June',
                '07' => 'July',
                '08' => 'August',
                '09' => 'September',
                '10' => 'October',
                '11' => 'November',
                '12' => 'December',
            );
            $query3 = $this->db->query("select id,branch_name from add_branch");

            ?>
            <div class="col-md-12">
                <div class="form-group col-sm-4">
                    <label for="branch_name">Employee ID :</label>
                        <input class="form-control" type="text" name="card_no" />
                    <div class="alert-danger"><?php echo ucwords(form_error('card_no')); ?></div>
                </div>
                <div class="form-group col-sm-4">
                    <label for="">Year:</label>
                    <select class="form-control" name="year">
                        <?php for ($i = 2012; $i <= date('Y'); $i++) { ?>
                            <option value="<?php echo $i; ?> "><?php echo $i; ?></option>
                        <?php } ?>
                    </select>
                    <div class="alert-danger"><?php echo ucwords(form_error('year')); ?></div>
                </div>
                <div class="form-group col-sm-4">
                    <label for="">month:</label>
                    <?php echo form_dropdown('month', $months, date('m'),'class="form-control"'); ?>
                    <div class="alert-danger"><?php echo ucwords(form_error('month')); ?></div>
                </div>
            </div>
            <span>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
                <input type="hidden" name="id" value="" />&nbsp;&nbsp;&nbsp;&nbsp;
                <input type="submit" name="mysubmit" value="Submit"  class="btn btn-primary">
            </span>

            </form>
            <p></p>
        </div>
    </div><!-- col-lg-12-->
</div><!-- /row -->