<p></p>
<input class="btn btn-primary btn-sm" type="button" id="print_button" value="Print">
<script language="javascript">
    function confirmSubmit() {
        var agree = confirm("Are you sure to delete this record?");
        if (agree)
            return true;
        else
            return false;
    }
</script>

<h3><i class="fa fa-angle-right"></i> View Loan Details</h3>
<?php
if($this->session->flashdata('message')==NULL){

}else{ ?>
    <div class="alert-success alert">
        <?php echo $this->session->flashdata('message') ?>
    </div> <?php
}
?>
<?php

$monthArray = array(
    '01' => 'January',
    '02' => 'February',
    '03' => 'March',
    '04' => 'April',
    '05' => 'May',
    '06' => 'June',
    '07' => 'July',
    '08' => 'August',
    '09' => 'September',
    '10' => 'October',
    '11' => 'November',
    '12' => 'December',
);
?>
<div class="row">
    <div class="col-sm-12">
        <div class="content-panel">
            <!--<section id="flip-scroll">-->
            <section id="">
                <div id="invoice">
                <table  id="myTable"  class="table table-hover table-bordered">
                    <thead>
                        <tr class="info">
                            <th>SL</th>
                            <th>Company </th>
                            <th>Department</th>
                            <th>Employee ID No</th>
                            <th>Emp. Name</th>
                            <th>Loan Issue Date</th>
                            <th>Loan Amount</th>
                            <th>Monthly Installment</th>
                            <th>Deduct</th>
                            <th>Remain</th>
                            <th>Remarks</th>
                            <th>Action</th>
                        </tr>
                    </thead>
                    <tbody>
                    <?php
                    $i = 'even';
                    $c = 1;
                    foreach ($query as $row) {
                        if ($i == 'even') {
                            $i = 'odd';
                        } else {
                            $i = 'even';
                        }
                        $branch = "";
                        $query10 = $this->db->query("select branch_name from add_branch where id='" . $row->branch_name . "'");
                        foreach ($query10->result() as $ab) {
                            $branch = $ab->branch_name;
                        }
                        $dept_name = "";
                        $query = $this->db->query("select name from add_department where id='" . $row->dept . "'");
                        foreach ($query->result() as $row3) {
                            $dept_name = $row3->name;
                        }
                        $cardNo = "";
                        $sUserName="";
                        $eInfo = $this->db->query("select * from employeeinfo where id='" . $row->emp_id . "'");
                        foreach ($eInfo->result() as $row61) {
                            $cardNo = $row61->card_no;
                            $sUserName = $row61->name;
                        }
                        echo "<tr class = " . $i . ">";
                        echo "<td>" . $c . "</td>";
                        echo "<td>" . $branch . "</td>";
                        echo "<td>" . $dept_name . "</td>";
                        echo "<td>" . $cardNo . "</td>";
                        echo "<td>" . $sUserName . "</td>";
                        echo "<td>" . $row->issue_date . "</td>";
                        echo "<td>" . $row->loan_amount . "</td>";
                        echo "<td>" . $row->monthly_installment . "</td>";
                        echo "<td>" . $row->total_deduct . "</td>";
                        echo "<td>" . $row->remain_amount . "</td>";
                        echo "<td>" . $row->remarks . "</td>";
                        ?>
                        <td>
                            <a class="btn btn-primary btn-xs" href="<?php echo base_url(); ?>hr/loan/details/<?php echo $row->id; ?> " title="Details">View</a>&nbsp;&nbsp;&nbsp;
                            <a class="btn btn-danger btn-xs" onclick="return confirmSubmit()" href="<?php echo base_url() ?>hr/loan/delete/<?php echo $row->id; ?>/<?php echo $row->branch_name; ?>/<?php echo $row->dept; ?>" title="Delete the Content">Delete</a>
                        </td>
                        <?php
                        $c++;
                    }

                    ?>
                    </tbody>
                </table>
                </div>
                <?php echo form_close(); ?>

                <div class="text-center">
                    <?php echo $this->pagination->create_links(); ?>
                </div>
                <div class="text-center">
                    <?php
                    echo "Total Records: ".$this->pagination_library->total_rows." page ".(($this->pagination->cur_page) ? $this->pagination->cur_page : "1")." of ". ceil(($this->pagination_library->total_rows)/($this->pagination_library->per_page));
                    ?>
                </div>
            </section>
        </div><! --/content-panel -->
    </div><!-- /col-md-12 -->
</div><!-- row -->