<link rel="stylesheet" type="text/css" href="<?php echo base_url(); ?>public/css/t1.css" />
<style type="text/css">
    <!--
    .style1 {color: #FF0000}
    -->
</style>
<script type="text/javascript">
    window.onload = function(){
        new JsDatePick({
            useMode:2,
            target:"issue_date",
            dateFormat:"%Y-%m-%d"
        });
        new JsDatePick({
            useMode:2,
            target:"end_date",
            dateFormat:"%Y-%m-%d"
        });
    }
</script>

<script language="javascript">
    function confirmSubmit() {
        var agree=confirm("Are you sure to delete this record?");
        if (agree)
            return true;
        else
            return false;
    }
</script>
<div class="grid_12">
    <div class="module1">
        <div class="module">
            <h2><span>View Incentive</span></h2>
            <div class="module-table-body">
                <table id="myTable" class="" border="0" >
                    <thead>
                        <tr>
                            <th class="header">SL No</th>
                            <th class="header">Company</th>
                            <th class="header">Department</th> 
                             <th class="header">ID No</th> 
                              <th class="header"style="text-align:left">Name</th> 
                            <th class="header">Year</th> 
                            <th class="header">Month</th> 
                            <th class="header">Incentive Amount</th> 
                            <th class="header">Action</th> 
                        </tr>
                    </thead>
                    <tbody>
                        <?php
                        $i = 'even';
                        $c = 1;
                        foreach ($query as $row) {
                            if ($i == 'even') {
                                $i = 'odd';
                            } else {
                                $i = 'even';
                            }
                            $branch = "";
                            $query10 = $this->db->query("select branch_name from add_branch where id='" . $row->branch_name . "'");
                            foreach ($query10->result() as $ab) {
                                $branch = $ab->branch_name;
                            }
                            $dept_name = "";
                            $query = $this->db->query("select sName from TB_USER_DEPT where nDepartmentIdn='" . $row->dept . "'");
                            foreach ($query->result() as $row3) {
                                $dept_name = $row3->sName;
                            }
                            $sUserName = "";
                            $query51 = $this->db->query("select sUserName from TB_USER where nUserIdn='" . $row->emp_id . "'");
                            foreach ($query51->result() as $row6) {
                                $sUserName = $row6->sUserName;
                            }
                            $card_no = "";
                            $query515 = $this->db->query("select card_no from employeeinfo where emp_id='" . $row->emp_id . "'");
                            foreach ($query515->result() as $row61) {
                                $card_no = $row61->card_no;
                            }


                            echo "<tr class = " . $i . ">";
                            echo "<td>" . $c . "</td>";
                            echo "<td>" . $branch . "</td>";
                            echo "<td>" . $dept_name . "</td>";
                             echo "<td>" . $card_no . "</td>";
                             echo "<td style='text-align:left'>" . $sUserName . "</td>";
                           
                            echo "<td>" . $row->year . "</td>";
                            echo "<td>" . $row->month . "</td>";
                            echo "<td>" . $row->amount . "</td>";
                            ?>
                        <td><a onclick="return confirmSubmit()" href="<?php echo base_url() ?>hr/deduction/delete/<?php echo $row->id; ?>/<?php echo $bname; ?>/<?php echo $dname; ?>" title="Delete the Content"><img src="<?php echo base_url(); ?>public/images/bin.gif" border="0" align="absmiddle" height="20" alt="<?php echo "#"; ?>"/></a></td>					

                        <?php
                        $c++;
                    }
                    ?>
                    </tbody>
                </table>
                <div class="pager" id="pager"></div>
                <div class="table-apply">
                </div>
            </div></div>
        <div style="clear: both;"></div>


