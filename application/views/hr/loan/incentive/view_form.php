<link rel="stylesheet" type="text/css" href="<?php echo base_url(); ?>public/css/t1.css" />
<style type="text/css">
    <!--
    .style1 {color: #FF0000}
    -->
</style>
<script type="text/javascript">
    window.onload = function(){
        new JsDatePick({
            useMode:2,
            target:"issue_date",
            dateFormat:"%Y-%m-%d"
        });
        new JsDatePick({
            useMode:2,
            target:"end_date",
            dateFormat:"%Y-%m-%d"
        });
    }
</script>
<script>
    function h_check()
    {
       
        var myvar = document.getElementById("hourly").checked;
        if(myvar)
        {
            $("#daily").attr("disabled",true);
            $("#h").show();
        }
        else
        {
            $("#daily").removeAttr("disabled");
            $("#h").hide();
        }
    }
    function d_check()
    {
        var myvar = document.getElementById("daily").checked;
        if(myvar)
        {
            $("#d").show();          
            $("#hourly").attr("disabled",true);;
						
        }
        else
        {
            $("#d").hide();           
            $("#hourly").removeAttr("disabled");
							
        }
    }
</script>

<script type="text/javascript" charset="utf-8">
    $(document).ready(function(){
        $("#leave_type").click(function(){
            var sdate=document.getElementById("start_date").value;
            var edate=document.getElementById("end_date").value;
            if(sdate=="" || edate==""){}
            else{
                var url='<?php echo site_url('hr/holiday_package/getDateDifference'); ?>';
                $("#m").html('Loading....').load(url+"/"+sdate+"/"+edate);
            }	
			   
        })
    })
			   
</script>   
<script type="text/javascript" charset="utf-8"> 
    function getEmpName($val)
    {
        var loadUrl='<?php echo site_url('hr/hr/getEmpName'); ?>';
				
        var name=$val;
			  
        $("#result66").val('Loading....').load(loadUrl+"/"+name);
    }
</script>
<div class="grid_12">
    <div class="module1">
        <h2><span>Incentive</span></h2>
        <?php echo $this->session->flashdata('message'); ?>
        <div class="module-body">
            <?php
            $query3 = $this->db->query("select id,branch_name from add_branch");
            echo form_open('hr/deduction/viewed_incentive');

            echo '<table>';
            echo '<thead><tr><th colspan="2"><b></b></th></tr></thead>';
            ?>
            <tr><th>Branch Name :</th><td>
                    <select name="branch_name" id="crlbranch" class="input-short" style="width:130px;">
                        <option   value="">Select</option>									
                        <?php foreach ($query3->result() as $row3): ?>
                            <option value="<?php echo $row3->id; ?>"><?php echo $row3->branch_name; ?></option>
                        <?php endforeach; ?>
                    </select>
                    <span class="notification-input ni-error"> <?php echo ucwords(form_error('branch_name')); ?></span>
                </td></tr>   
            <tr><th>Department :</th><td><div id='res'>
                        <select style="width:130px;">
                            <option  value="">Select</option>
                        </select>

                    </div><span class="notification-input ni-error"><?php echo ucwords(form_error('dept')); ?></span></td></tr>

            <tr><th>ID No:</th><td>
                    <input type="text" name="card_no" onblur="getEmpName(this.value)" />
                    <span class="notification-input ni-error"><?php echo ucwords(form_error('card_no')); ?></span></td>
            </tr>   
             <tr><th>Name:</th>
			 <td> 
			        <div id="result66"></div>
			</td>
			</tr>
            <?php
            echo '<tr><th></th><td>' . form_submit('mysubmit', 'Submit') . '</td></tr>';

            echo '</table>';
            echo form_close();
            ?>
        </div></div></div>
<div style="clear: both;"></div>

