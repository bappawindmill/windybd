<link rel="stylesheet" type="text/css" href="<?php echo base_url(); ?>public/css/t1.css" />
<style type="text/css">
    <!--
    .style1 {color: #FF0000}
    -->
</style>
<script type="text/javascript">
    window.onload = function(){
        new JsDatePick({
            useMode:2,
            target:"issue_date",
            dateFormat:"%Y-%m-%d"
        });
        new JsDatePick({
            useMode:2,
            target:"end_date",
            dateFormat:"%Y-%m-%d"
        });
    }
</script>
<script>
    function h_check()
    {
       
        var myvar = document.getElementById("hourly").checked;
        if(myvar)
        {
            $("#daily").attr("disabled",true);
            $("#h").show();
        }
        else
        {
            $("#daily").removeAttr("disabled");
            $("#h").hide();
        }
    }
    function d_check()
    {
        var myvar = document.getElementById("daily").checked;
        if(myvar)
        {
            $("#d").show();          
            $("#hourly").attr("disabled",true);;
						
        }
        else
        {
            $("#d").hide();           
            $("#hourly").removeAttr("disabled");
							
        }
    }
</script>

<script type="text/javascript" charset="utf-8">
    $(document).ready(function(){
        $("#leave_type").click(function(){
            var sdate=document.getElementById("start_date").value;
            var edate=document.getElementById("end_date").value;
            if(sdate=="" || edate==""){}
            else{
                var url='<?php echo site_url('hr/holiday_package/getDateDifference'); ?>';
                $("#m").html('Loading....').load(url+"/"+sdate+"/"+edate);
            }	
			   
        })
    })
			   
</script>    
<script type="text/javascript" charset="utf-8"> 
function getEmpName($val)
{
	var loadUrl='<?php echo site_url('hr/hr/getEmpName'); ?>';
	
	var name=$val;
  
	$("#result66").val('Loading....').load(loadUrl+"/"+name);
}
</script>
<div class="grid_12">
    <div class="module1">
        <h2><span>Incentive</span></h2>
        <?php echo $this->session->flashdata('message'); ?>
        <div class="module-body">
            <?php
            $query3 = $this->db->query("select id,branch_name from add_branch");
            ?>
            <?php
            $leaves = array(
                '' => 'Select',
                '1' => 'Causal ',
                '2' => 'Medical',
                '3' => 'Earned',
                '4' => 'Compensation',
                '5' => 'Other'
            );
            echo form_open('hr/deduction/added_incentive');

            echo '<table>';
            echo '<thead><tr><th colspan="2"><b></b></th></tr></thead>';
            $months = array('01' => 'January',
                '02' => 'February',
                '03' => 'March',
                '04' => 'April',
                '05' => 'May',
                '06' => 'June',
                '07' => 'July',
                '08' => 'August',
                '09' => 'September',
                '10' => 'October',
                '11' => 'November',
                '12' => 'December',
            );
            ?>
            <tr><th>ID No:</th><td> 
                    <input type="text" name="card_no"  onblur="getEmpName(this.value)"/>
                    <span class="notification-input ni-error"><?php echo ucwords(form_error('card_no')); ?></span></td>
            </tr>
            <tr><th>Name:</th>
                <td> 
                    <div id="result66"></div>
                </td>
            </tr> 


            <tr><th>Year :</th><td>
                    <input type="text" name="year" value="<?php echo date('Y'); ?>" />
                    <span class="notification-input ni-error"> <?php echo ucwords(form_error('year')); ?></span>
                </td></tr>
            <?php
            $months = array(
                '' => 'Select',
                '01' => 'January',
                '02' => 'February',
                '03' => 'March',
                '04' => 'April',
                '05' => 'May',
                '06' => 'June',
                '07' => 'July',
                '08' => 'August',
                '09' => 'September',
                '10' => 'October',
                '11' => 'November',
                '12' => 'December',
            );
            ?> 
            <tr><th>Month :</th> <td>
            <?php echo form_dropdown('month', $months, date('m')); ?>
                    <span class="notification-input ni-error"> <?php echo ucwords(form_error('month')); ?></span>       
                </td></tr>
            <tr><th>Incentive Amount :</th><td>
                    <input type="text" name="amount" id="amount" />TK
                    <span class="notification-input ni-error"> <?php echo ucwords(form_error('amount')); ?></span>
                </td></tr> 
<?php
echo '<tr><th></th><td>' . form_submit('mysubmit', 'Submit') . '</td></tr>';

echo '</table>';
echo form_close();
?>
        </div></div></div>
<div style="clear: both;"></div>

