<link rel="stylesheet" type="text/css" href="<?php echo base_url(); ?>public/css/t1.css" />
<style type="text/css">
    <!--
    .style1 {color: #FF0000}
    -->
</style>
<script type="text/javascript">
    window.onload = function(){
        new JsDatePick({
            useMode:2,
            target:"issue_date",
            dateFormat:"%Y-%m-%d"
        });
        new JsDatePick({
            useMode:2,
            target:"end_date",
            dateFormat:"%Y-%m-%d"
        });
    }
</script>
<script>
   function h_check()
    {
       
        var myvar = document.getElementById("hourly").checked;
        if(myvar)
        {
            $("#daily").attr("disabled",true);
            $("#h").show();
        }
        else
        {
            $("#daily").removeAttr("disabled");
            $("#h").hide();
        }
    }
    function d_check()
    {
        var myvar = document.getElementById("daily").checked;
        if(myvar)
        {
            $("#d").show();          
            $("#hourly").attr("disabled",true);;
						
        }
        else
        {
            $("#d").hide();           
            $("#hourly").removeAttr("disabled");
							
        }
    }
</script>

<script type="text/javascript" charset="utf-8">
    $(document).ready(function(){
        $("#leave_type").click(function(){
            var sdate=document.getElementById("start_date").value;
            var edate=document.getElementById("end_date").value;
            if(sdate=="" || edate==""){}
            else{
                var url='<?php echo site_url('hr/holiday_package/getDateDifference'); ?>';
                $("#m").html('Loading....').load(url+"/"+sdate+"/"+edate);
            }	
			   
        })
    })
			   
</script>    
<div class="grid_12">
    <div class="module1">
        <h2><span>Incentive</span></h2>
        <?php echo $this->session->flashdata('message'); ?>
        <div class="module-body">
            <?php
            $query3 = $this->db->query("select id,branch_name from add_branch");
            ?>
            <?php
            $leaves = array(
                '' => 'Select',
                '1' => 'Causal ',
                '2' => 'Medical',
                '3' => 'Earned',
                '4' => 'Compensation',
                '5' => 'Other'
            );
            echo form_open('hr/deduction/added_incentive');

            echo '<table>';
            echo '<thead><tr><th colspan="2"><b></b></th></tr></thead>';
            $months = array('01' => 'January',
                '02' => 'February',
                '03' => 'March',
                '04' => 'April',
                '05' => 'May',
                '06' => 'June',
                '07' => 'July',
                '08' => 'August',
                '09' => 'September',
                '10' => 'October',
                '11' => 'November',
                '12' => 'December',
            );
            ?>
            <tr><th>Company :</th><td>
                    <select name="branch_name" id="crlbranch" class="input-short" style="width:130px;">
                        <option   value="">Select</option>									
                        <?php foreach ($query3->result() as $row3): ?>
                            <option value="<?php echo $row3->id; ?>"><?php echo $row3->branch_name; ?></option>
                        <?php endforeach; ?>
                    </select>
                    <span class="notification-input ni-error"> <?php echo ucwords(form_error('branch_name')); ?></span>
                </td></tr> 
            <tr>
                <th>Department Name<span class="style1"></span></th>
                <td><div id='res'></div></td>

            </tr>             

            <tr>
                <th>Employee Name<span class="style1">*</span></th> 
                <td><div id='result1'></div></td>
            </tr>      
			  
         
                				<tr><th>Year :</th><td>
                     <select name="year" style="width:160px;" class="input-short">
					  <option value="">Select</option>
                        <?php for ($i = 2012; $i <= date('Y'); $i++) { ?>                
                            <option value="<?php echo $i; ?> "><?php echo $i; ?></option>
                        <?php } ?>                
                    </select>
                    <span class="notification-input ni-error"> <?php echo ucwords(form_error('year')); ?></span>
               </td></tr>
			   <tr><th>Month :</th><td>
                    <select name="month">
					    <option value="">Select</option>
                        <option value="01">January</option>
                        <option value="02">February</option>
                        <option value="03">March</option>
                        <option value="04">April</option>
                        <option value="05">May</option>
                        <option value="06">June</option>
                        <option value="07">July</option>
                        <option value="08">August</option>
                        <option value="09">September</option>
                        <option value="10">October</option>
                        <option value="11">November</option>
                        <option value="12">December</option>
                    </select>

                    <span class="notification-input ni-error"> <?php echo ucwords(form_error('month')); ?></span>
               </td></tr>
              <tr><th>Incentive Amount :</th><td>
                    <input type="text" name="amount" id="amount" />TK
                    <span class="notification-input ni-error"> <?php echo ucwords(form_error('amount')); ?></span>
            </td></tr> 

				
			
			
			

            <?php
            echo '<tr><th></th><td>' . form_submit('mysubmit', 'Submit') . '</td></tr>';

            echo '</table>';
            echo form_close();
            ?>
        </div></div></div>
<div style="clear: both;"></div>
<div class="module">
    <h2><span>View Incentive</span></h2>
    <div class="module-table-body">
        <table id="myTable" class="" border="1" >
            <thead>
                <tr>
                    <th class="header">SL No</th>
					<th class="header">Company</th>
                    <th class="header">Department</th> 
                    <th class="header">Name</th> 
                    <th class="header">Incentive Amount</th> 
                    <th class="header">Year</th> 
                    <th class="header">Month</th> 
                </tr>
            </thead>
            <tbody>
                <?php
                $i = 'even';
                $c = 1;
                foreach ($query as $row) {
                    if ($i == 'even') {
                        $i = 'odd';
                    } else {
                        $i = 'even';
                    }
                     $branch="";
                    $query10 = $this->db->query("select branch_name from add_branch where id='" . $row->branch_name."'");
                    foreach ($query10->result() as $ab) {
                        $branch = $ab->branch_name;
                    }
					$dept_name="";
					 $query = $this->db->query("select sName from TB_USER_DEPT where nDepartmentIdn='" . $row->dept . "'");
                foreach ($query->result() as $row3) {
                    $dept_name = $row3->sName;
                }
				$sUserName="";
				 $query51 = $this->db->query("select sUserName from TB_USER where nUserIdn='" . $row->emp_id . "'");
                foreach ($query51->result() as $row6) {
                    $sUserName = $row6->sUserName;
                }
                   

                    echo "<tr class = " . $i . ">";
                    echo "<td>" . $c . "</td>";
                    echo "<td>" . $branch . "</td>";
                    echo "<td>" . $dept_name . "</td>";
					echo "<td>" . $sUserName . "</td>";
					echo "<td>" . $row->amount . "</td>";
					echo "<td>" . $row->year . "</td>";
					echo "<td>" . $row->month . "</td>";
					
                    ?>

                <?php

                $c++;
            }
            ?>
            </tbody>
        </table>
        <div class="pager" id="pager"></div>
        <div class="table-apply">
        </div>
    </div></div>
<div style="clear: both;"></div>


