<h3><i class="fa fa-angle-right"></i> View Loan Details:</h3>

<div class="row">
    <div class="col-sm-12">
        <div class="content-panel">
            <!--<section id="flip-scroll">-->
            <section id="">
                <?php
                $this->load->model('hr/loan_model');
                $i = 'even';
                $c = 1;
                foreach ($query as $row):
                if ($i == 'even') {
                    $i = 'odd';
                } else {
                    $i = 'even';
                }
                $branch = "";
                $query10 = $this->db->query("select branch_name from add_branch where id='" . $row->branch_name . "'");
                foreach ($query10->result() as $ab) {
                    $branch = $ab->branch_name;
                }
                $dept_name = "";
                $query = $this->db->query("select name from add_department where id='" . $row->dept . "'");
                foreach ($query->result() as $row3) {
                    $dept_name = $row3->name;
                }
                $cardNo = "";
                $sUserName="";
                $eInfo = $this->db->query("select * from employeeinfo where id='" . $row->emp_id . "'");
                foreach ($eInfo->result() as $row61) {
                    $cardNo = $row61->card_no;
                    $sUserName = $row61->name;
                }
                ?>
                <table  id="myTable"  class="table table-hover">
                    <thead>
                    <tr>
                        <th>SBU: </th>
                        <td><?php echo $branch; ?></td>
                        <th>Department: </th>
                        <td><?php echo $dept_name; ?></td>
                        <th>Name: </th>
                        <td><?php echo $sUserName; ?></td>
                    </tr>
                    </thead>
                    <tr>
                        <th>ID No: </th>
                        <td><?php echo $cardNo; ?></td>
                        <th>Loan Issue Date: </th>
                        <td><?php echo $row->issue_date; ?></td>
                        <th>Loan Amount: </th>
                        <td><?php echo $row->loan_amount; ?></td>
                    </tr>
                    <tbody>
                    <tr>
                        <th>Monthly Installment: </th>
                        <td><?php echo $row->monthly_installment; ?></td>
                        <th>Total Deduct: </th>
                        <td><?php echo $row->total_deduct; ?></td>
                        <th>Remain Amount: </th>
                        <td><?php echo $row->remain_amount; ?></td>
                    </tr>
                    <tr>
                        <th>Remarks: </th>
                        <td><?php echo $row->remarks; ?></td>
                        <th>Deduction start: </th>
                        <td><?php echo $this->loan_model->getMonthName($row->month); ?>,<?php echo $row->year; ?></td>
                        <th> </th>
                        <td></td>
                    </tr>

                    </tbody>
                </table>
                    <?php

                endforeach; ?>
                <div class="text-center">
                    <?php echo $this->pagination->create_links(); ?>
                </div>
            </section>
        </div><! --/content-panel -->
    </div><!-- /col-md-12 -->
</div><!-- row -->





<h3><i class="fa fa-angle-right"></i> Leave Summary:</h3>

<div class="row">


    <div class="col-sm-12">
        <div class="content-panel">
            <!--<section id="flip-scroll">-->
            <section id=""> <?php
                if (!empty($query)) {
                    $c1 = 1;
                    $total1 = 0;
                    ?>
                    <table class="table table-hover">
                        <tr>
                            <th>SL No</th>
                            <th>Year</th>
                            <th>Month</th>
                            <th>Amount</th>
                        </tr>
                        <?php
                        foreach ($query1 as $temp1) {
                            echo "<tr >
                            <td>" . $c1 . "</td>";
                            echo "<td> " . $temp1->year . "</td>";
                            echo "<td>" . $this->loan_model->getMonthName($temp1->month) . "</td>";
                            echo "<td>" . $temp1->amount . "</td></tr>";
                            $c1 = $c1 + 1;
                        }
                    }
                    ?>
                </table>
                <div class="text-center">
                    <?php echo $this->pagination->create_links(); ?>
                </div>
            </section>
        </div><! --/content-panel -->
    </div><!-- /col-md-12 -->
</div><!-- row -->