<link rel="stylesheet" type="text/css" href="<?php echo base_url(); ?>public/css/t1.css" />
<style type="text/css">
    <!--
    .style1 {color: #FF0000}
    -->
</style>
<script type="text/javascript" src="<?php echo base_url(); ?>/public/js/jquery.jqprint.0.3.js"></script>
<script>
    $(function(){
        $("#print_button").click( function() {
            $("#invoice").jqprint();
        });
    });
</script>
<input type="button" id="print_button" value="Print" style="margin-top:5px; ;">
<?php
 $this->load->model('hr/loan_model');
$i = 'even';
$c = 1;
foreach ($query as $row):
    if ($i == 'even') {
        $i = 'odd';
    } else {
        $i = 'even';
    }
    $branch = "";
	$query10 = $this->db->query("select branch_name from add_branch where id='" . $row->branch_name . "'");
	foreach ($query10->result() as $ab) {
		$branch = $ab->branch_name;
	}
	$dept_name = "";
	$query = $this->db->query("select name from add_department where id='" . $row->dept . "'");
	foreach ($query->result() as $row3) {
		$dept_name = $row3->name;
	}
   
	$cardNo = "";
	$sUserName="";
	$eInfo = $this->db->query("select * from employeeinfo where id='" . $row->emp_id . "'");
	foreach ($eInfo->result() as $row61) {
		$cardNo = $row61->card_no;
		 $sUserName = $row61->name;
	}

    ?>
    <div id="invoice" style="float:left; width:900px">
        <div align="center" style=" margin-right: 50px; margin-left: 50px; border-bottom: 1px solid #000000; padding-bottom: 5px;"><h4><?php echo ucfirst($branch); ?></h4>Loan Report of <b><?php echo $sUserName; ?> </b><br /> </div><br />


        <p style=" font-weight: bold; padding-left: 40px; margin: 20px">Loan Details:</p>
        <table width="90%">
            <tr>
                <th>SBU: </th>
                <td><?php echo $branch; ?></td>
                <th width="133">Department: </th>
                <td width="133"><?php echo $dept_name; ?></td>
                <th width="133">Name: </th>
                <td width="133"><?php echo $sUserName; ?></td>
            </tr>
            <tr>
			    <th>ID No: </th>
                <td><?php echo $cardNo; ?></td>
                <th>Loan Issue Date: </th>
                <td><?php echo $row->issue_date; ?></td>
                <th width="133">Loan Amount: </th>
                <td width="133"><?php echo $row->loan_amount; ?></td>
               
            </tr>

            <tr>
			 <th width="133">Monthly Installment: </th>
                <td width="133"><?php echo $row->monthly_installment; ?></td>
                <th>Total Deduct: </th>
                <td><?php echo $row->total_deduct; ?></td>
                <th width="133">Remain Amount: </th>
                <td width="133"><?php echo $row->remain_amount; ?></td>
               
            </tr>
            <tr>
			 <th width="133">Remarks: </th>
                <td width="133"><?php echo $row->remarks; ?></td>
                <th>Deduction start: </th>
                <td><?php echo $this->loan_model->getMonthName($row->month); ?>,<?php echo $row->year; ?></td>
				  <th> </th>
                <td></td>
            </tr>
        </table>

        <?php
    endforeach;

    if (!empty($query)) {

        $c1 = 1;
        $total1 = 0;
        ?>
        <p style=" font-weight: bold; padding-left: 40px; margin: 20px">Deduction Details:</p>
        <table width="90%">
            <tr>
                <th style="text-align:center">SL No</th>
                <th style="text-align:center">Year</th>
                <th style="text-align:center">Month</th>
                <th style="text-align:center">Amount</th>
            </tr>
            <?php
            foreach ($query1 as $temp1) {
                echo "<tr >
                <td style='text-align:center'>" . $c1 . "</td>";
                echo "<td style='text-align:center'> " . $temp1->year . "</td>";
                echo "<td style='text-align:center'>" . $this->loan_model->getMonthName($temp1->month) . "</td>";
                echo "<td style='text-align:center'>" . $temp1->amount . "</td></tr>";

                $c1 = $c1 + 1;
            }
        }
        ?>
    </table>
</div>
<div style="clear: both;"></div>
</div>

