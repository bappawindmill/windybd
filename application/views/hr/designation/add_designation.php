<h3><i class="fa fa-angle-right"></i> Create New Employee Type</h3>

<!-- BASIC FORM ELELEMNTS -->
<div class="row">
    <div class="col-sm-12">
        <div class="content-panel">
            <?php
            echo form_open('hr/department/added_designation');?>
            <div class="alert-danger"><?php echo $this->session->flashdata('message'); ?> </div>
            <div class="col-md-12">
                <div class="form-group col-sm-6">
                    <label for="name">Designation:</label>
                    <input value=""  type="text" name="designation" class="form-control" id=" ">
                    <div class="alert-danger"><?php echo ucwords(form_error('designation')); ?></div>
                </div>
                <?php
                    $query3 = $this->db->query("select id,emp_type from emp_type");
                ?>
                <div class="form-group col-sm-6">
                    <label for="emp_type">SBU Name:</label>
                    <select name="emp_type" id="emp_type" class="form-control">
                        <option value="">select</option>
                        <?php foreach ($query3->result() as $row) { ?>
                            <option value="<?php echo $row->id; ?>"><?php echo $row->emp_type; ?></option>
                            <?php
                        }
                        ?>
                    </select>
                    <div class="alert-danger"><?php echo ucwords(form_error('emp_type')); ?></div>
                </div>
            </div>
                <span>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
                    <input type="hidden" name="id" value="" />&nbsp;&nbsp;&nbsp;&nbsp;
                    <input type="submit" name="mysubmit" value="Submit"  class="btn btn-primary">
                    <a class="btn btn-theme" href="<?php echo base_url(); ?>hr/department/designation_list">Cancel</a>
                </span>
            </form>
            <p></p>
        </div>
    </div><!-- col-lg-12-->
</div><!-- /row -->