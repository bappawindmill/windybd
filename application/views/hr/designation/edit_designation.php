<h3><i class="fa fa-angle-right"></i> Designation Edit</h3>

<!-- BASIC FORM ELELEMNTS -->
<div class="row">
    <div class="col-sm-12">
        <div class="content-panel">
            <?php
            echo form_open('hr/department/designation_edit');?>
            <div class="alert-danger"><?php echo $this->session->flashdata('message'); ?> </div>
            <div class="col-md-12">
                <div class="form-group col-sm-6">
                    <label for="">Designation:</label>
                    <input value="<?php echo $fname['value']; ?>"  type="text" name="name" class="form-control" id="" required>
                    <div class="alert-danger"><?php echo ucwords(form_error('name')); ?></div>
                </div>
                <?php
                $query1 = $this->db->query("select id,emp_type from emp_type");
                ?>
                <div class="form-group col-sm-6">
                    <label for="department_id">Employee Type:</label>
                    <select name="emp_type" id="emp_type" class="form-control" required>
                        <option value="">select</option>
                        <?php foreach ($query1->result() as $row) { ?>
                            <option <?php if (($row->id) == ($emptype['value'])): ?> selected="selected" value="<?php echo $emptype['value']; ?>" <?php else: ?>  value="<?php echo $row->id; ?>" <?php endif; ?>><?php echo $row->emp_type; ?></option>
                            <?php
                        }
                        ?>
                    </select>
                    <div class="alert-danger"><?php echo ucwords(form_error('emp_type')); ?></div>
                </div>
            </div>
                <span>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
                    <input type="hidden" name="id" value="<?php echo $fid['value']; ?>" />&nbsp;&nbsp;&nbsp;&nbsp;
                    <input type="submit" name="mysubmit" value="Update"  class="btn btn-primary">
                    <a class="btn btn-theme" href="<?php echo base_url(); ?>hr/department/designation_list">Cancel</a></span>
            </span>
            </form>
        </div>
    </div><!-- col-lg-12-->
</div><!-- /row -->