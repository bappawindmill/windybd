<h3><i class="fa fa-angle-right"></i> Create New Employee Type</h3>

<!-- BASIC FORM ELELEMNTS -->
<div class="row">
    <div class="col-sm-12">
        <div class="content-panel">
            <?php
            echo form_open('hr/department/added_emp_type');?>
            <div class="alert-danger"><?php echo $this->session->flashdata('message'); ?> </div>
            <div class="col-md-12">
                <div class="form-group col-sm-6">
                    <label for="name">Employee Type:</label>
                    <input value=""  type="text" name="emp_type" class="form-control" id=" ">
                    <div class="alert-danger"><?php echo ucwords(form_error('emp_type')); ?></div>
                </div>
            </div>
                <span>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
                    <input type="hidden" name="id" value="" />&nbsp;&nbsp;&nbsp;&nbsp;
                    <input type="submit" name="mysubmit" value="Submit"  class="btn btn-primary">
                    <a class="btn btn-theme" href="<?php echo base_url(); ?>hr/department/emp_type_list">Cancel</a>
                </span>
            </form>
            <p></p>
        </div>
    </div><!-- col-lg-12-->
</div><!-- /row -->