<h3><i class="fa fa-angle-right"></i> Employee Type Edit</h3>

<!-- BASIC FORM ELELEMNTS -->
<div class="row">
    <div class="col-sm-12">
        <div class="content-panel">
            <?php
            echo form_open('hr/department/emptype_edit');?>
            <div class="alert-danger"><?php echo $this->session->flashdata('message'); ?> </div>
            <div class="col-md-12">
                <div class="form-group col-sm-12">
                    <label for="">Employee Type:</label>
                    <input value="<?php echo $fname['value'];?>"  type="text" name="name" class="form-control" id="" required>
                    <div class="alert-danger"><?php echo ucwords(form_error('name')); ?></div>
                </div>
            </div>
                <span>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
                    <input type="hidden" name="id" value="<?php echo $fid['value']; ?>" />&nbsp;&nbsp;&nbsp;&nbsp;
                    <input type="submit" name="mysubmit" value="Update"  class="btn btn-primary">
                    <a class="btn btn-theme" href="<?php echo base_url(); ?>hr/department/emp_type_list">Cancel</a></span>
            </span>
            </form>
        </div>
    </div><!-- col-lg-12-->
</div><!-- /row -->