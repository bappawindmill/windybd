<script language="javascript">
    function confirmSubmit() {
        var agree = confirm("Are you sure to delete this record?");
        if (agree)
            return true;
        else
            return false;
    }
</script>

<?php
if($this->session->flashdata('message')==NULL){

}else{ ?>
    <div class="alert-success alert">
        <?php echo $this->session->flashdata('message') ?>
    </div> <?php
}
?>
<?php
$branch_name = "";

$query = $this->db->query("select branch_name from add_branch where id='" . $bname . "'");

foreach ($query->result() as $row3) {

    $branch_name = $row3->branch_name;

}

$dept_name = "";

$query1 = $this->db->query("select name from add_department where id='" . $dname . "'");

foreach ($query1->result() as $row31) {

    $dept_name = $row31->name;

}

$monthArray = array(
    '01' => 'January',
    '02' => 'February',
    '03' => 'March',
    '04' => 'April',
    '05' => 'May',
    '06' => 'June',
    '07' => 'July',
    '08' => 'August',
    '09' => 'September',
    '10' => 'October',
    '11' => 'November',
    '12' => 'December',
);
?>
<!--<p style="text-align:center"> Year : <?php /*echo $syear;*/?><br/>
    Month : <?php
/*    foreach($monthArray as $key=>$val):
        if($key==$smonth){echo $val;}
    endforeach;
    */?>
</p>-->
<div class="row">
    <div class="col-xs-4 col-sm-4 search_pos"><h3><i class="fa fa-angle-right"></i> View Employee Advance Info</h3></div>
    <div class="col-xs-4 col-sm-4 search_pos">
        <h3><i class="fa "></i>Year:<?php echo $this->input->get_post("year"); ?> &nbsp;&nbsp;Month:<?php
            foreach($monthArray as $key=>$val):
                if($key==$smonth){echo $val;}
            endforeach;
            ?></h3>
    </div>
    <div class="col-xs-4 col-sm-4 search_pos1"><p></p>
        <form class="form-inline">
            <div class="form-group has-feedback pull-right">
                <input type="text" id="searchKey" name="search" class="form-control" placeholder="Search by Emp Name" />
                <input type="hidden" name="branch_name" value="<?php echo $bname;?>" />
                <input type="hidden" name="dept" value="<?php echo $dname;?>" />
                <input type="hidden" name="year" value="<?php echo $syear;?>" />
                <input type="hidden" name="month" value="<?php echo $smonth;?>" />
                <i class="glyphicon glyphicon-search form-control-feedback"></i>
            </div>
        </form>
        <div class="pull-right"><a href="<?php echo base_url();?>hr/kpi_deduction/add" class="btn btn-info" role="button" >Reset</a>&nbsp;</div>
    </div>
</div>
<div class="row">
    <div class="col-sm-12">
        <div class="content-panel">
            <!--<section id="flip-scroll">-->
            <section id="">
                <table  id="myTable"  class="table table-hover table-bordered">
                    <thead>
                    <tr class="info">
                        <th>SL</th>
                        <th>SBU </th>
                        <th>Department</th>
                        <th>Emp. Name</th>
                        <th>Employee ID No</th>
                        <th>KPI Deduction Amount</th>
                        <th>Remarks</th>
                    </tr>
                    </thead>
                    <tbody>
                    <?php

                    echo form_open('hr/kpi_deduction/changed');

                    $i = 'even';

                    $c = 1;

                    foreach ($query9 as $row) {?>



                    <tr class = "">

                        <td><?php echo  $c ;?></td>

                        <td><?php echo $branch_name ;?></td>

                        <td><?php echo $dept_name ;?></td>

                        <td><?php echo $row->name ;?></td>

                        <td><?php echo $row->card_no ;?></td>

                        <?php

                        $kpi="";

                        $remarks="";

                        $aInfo = $this->db->query("select * from kpi where emp_id='" . $row->id . "'and year='".$syear."'and month='".$smonth."'");

                        foreach ($aInfo->result() as $advInfo) {

                            $kpi = $advInfo->amount;

                            $remarks = $advInfo->remarks;

                        }

                        ?>

                        <td><input type="text" name="kpi[]" value="<?php echo $kpi;?>" /></td>

                        <td><input type="text" name="remarks[]" value="<?php echo $remarks;?>" /></td>

                        <input type="hidden" name="empid[]" value="<?php echo $row->id;?>" />

                        <?php

                        $c++;

                        }

                        ?>
                    </tbody>
                </table>
                <table>
                    <thead>
                    <tr>
                        <td>
                            <input type="hidden" name="branch" value="<?php echo $bname;?>"/>
                            <input type="hidden" name="dept" value="<?php echo $dname;?>"/>
                            <input type="hidden" name="year" value="<?php echo $syear;?>"/>
                            <input type="hidden" name="month" value="<?php echo $smonth;?>"/>
                    </tr>
                    <thead>
                </table>
                <input class="btn btn-primary" type="submit" name="change" value="Save" />
                <?php echo form_close(); ?>

                <div class="text-center">
                    <?php echo $this->pagination->create_links(); ?>
                </div>
                <div class="text-center">
                    <?php
                    echo "Total Records: ".$this->pagination_library->total_rows." page ".(($this->pagination->cur_page) ? $this->pagination->cur_page : "1")." of ". ceil(($this->pagination_library->total_rows)/($this->pagination_library->per_page));
                    ?>
                </div>
            </section>
        </div><! --/content-panel -->
    </div><!-- /col-md-12 -->
</div><!-- row -->