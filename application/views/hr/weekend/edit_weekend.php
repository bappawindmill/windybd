<h3><i class="fa fa-angle-right"></i> Weekend Edit</h3>

<!-- BASIC FORM ELELEMNTS -->
<div class="row">
    <div class="col-sm-12">
        <div class="content-panel">
                <?php
                echo form_open('hr/weekend/weekend_edit');?>
                <?php
                if($this->session->flashdata('message')==NULL){

                }else{ ?>
                    <div class="alert-success alert">
                        <?php echo $this->session->flashdata('message') ?>
                    </div> <?php
                }
                ?>
                <?php foreach ($query as $temp):endforeach; ?>
                <div class="col-md-12">
                    <div class="form-group col-sm-6">
                        <label for="emp_type">SBU Name:</label>
                        <?php echo form_dropdown('branch_name', $optionBranch,$temp->branch_name, "id = 'crlbranch' class='form-control' required"); ?>
                        <div class="alert-danger"><?php echo ucwords(form_error('branch_name')); ?></div>

                    </div>
                    <div class="form-group col-sm-6">
                        <label for="name">Weekend Date:</label>
                        <input value="<?php echo $temp->weekend_date ?>"  type="text" name="weekend_date" id="weekend_date" class="form-control" required>
                        <div class="alert-danger"><?php echo ucwords(form_error('weekend_date')); ?></div>
                    </div>
                </div>
                    <span>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
                        <input type="hidden" name="id" value="<?php echo $temp->id; ?>" />&nbsp;&nbsp;&nbsp;&nbsp;
                        <input type="submit" name="mysubmit" value="Update"  class="btn btn-primary">
                        <a class="btn btn-theme" href="<?php echo base_url(); ?>hr/weekend/weekend_list">Cancel</a></span>
                </span>
            </form>
        </div>
    </div><!-- col-lg-12-->
</div><!-- /row -->