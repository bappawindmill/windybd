<h3><i class="fa fa-angle-right"></i> Create New Employee Type</h3>

<!-- BASIC FORM ELELEMNTS -->
<div class="row">
    <div class="col-sm-12">
        <div class="content-panel">
            <?php
            echo form_open('hr/weekend/added_weekend');?>
            <?php
            if($this->session->flashdata('message')==NULL){

            } else { ?>
                <div class="alert-success alert">
                    <?php echo $this->session->flashdata('message') ?>
                </div> <?php
            }
            ?>
            <div class="col-md-12">
                <div class="form-group col-sm-6">
                    <label for="emp_type">SBU Name:</label>
                    <?php echo form_dropdown('branch_name', $optionBranch,set_value('branch_name'), "id = 'crlbranch' class='form-control'"); ?>
                    <div class="alert-danger"><?php echo ucwords(form_error('branch_name')); ?></div>

                </div>

                <div class="form-group col-sm-6">
                    <label for="name">Weekend Date:</label>
                    <input value=""  type="text" name="weekend_date" id="weekend_date" class="form-control">
                    <div class="alert-danger"><?php echo ucwords(form_error('weekend_date')); ?></div>
                </div>
            </div>
                <span>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
                    <input type="hidden" name="id" value="" />&nbsp;&nbsp;&nbsp;&nbsp;
                    <input type="submit" name="mysubmit" value="Submit"  class="btn btn-primary">
                    <a class="btn btn-theme" href="<?php echo base_url(); ?>hr/weekend/weekend_list">Cancel</a>
                </span>
            </form>
            <p></p>
        </div>
    </div><!-- col-lg-12-->
</div><!-- /row -->