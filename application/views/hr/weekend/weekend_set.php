<link href='<?php echo base_url(); ?>assets/fullcalendar/fullcalendar.min.css' rel='stylesheet' />
<link href='<?php echo base_url(); ?>assets/fullcalendar/fullcalendar.print.min.css' rel='stylesheet' media='print' />


<script src='<?php echo base_url(); ?>assets/fullcalendar/lib/moment.min.js'></script>
<script src='<?php echo base_url(); ?>assets/fullcalendar/fullcalendar.min.js'></script>

<style>

    #calendar {
        max-width: 900px;
        margin: 0 auto;
        color:black;
    }
    .event_delete
    {
        /* border: 1px solid #ff184b;
         background: #ff1016;*/


    }
</style>


<h3><i class="fa fa-angle-right"></i> Set Weekend For A Year</h3>

<!-- BASIC FORM ELELEMNTS -->
<div class="row">
    <div class="col-sm-12 content-panel">
        <div class="content-panel">
            <?php
            echo form_open('hr/weekend/insert_weekend');?>
            <?php
            if($this->session->flashdata('message')==NULL){

            } else { ?>
                <div class="alert-success alert">
                    <?php echo $this->session->flashdata('message') ?>
                </div> <?php
            }
            ?>
            <div class="col-md-12">
                <div class="form-group col-md-3">
                    <label for="emp_type">SBU Name:</label>
                    <?php echo form_dropdown('branch_name', $optionBranch,set_value('branch_name'), "id = 'crlbranch' class='form-control'"); ?>
                    <div class="alert-danger"><?php echo ucwords(form_error('branch_name')); ?></div>
                </div>

                <div class="form-group col-md-3">
                    <label for="name">Weekend Year:</label>
                    <input value=""  type="text" name="weekend_year" id="" class="form-control">
                    <div class="alert-danger"><?php echo ucwords(form_error('weekend_year')); ?></div>
                </div>

                <div class="form-group col-md-3">
                    <label for="name">Set Days:</label>
                    <select name="weekend_days_select[]" class="form-control" multiple required>
                        <option value="Sat">Saturday</option>
                        <option value="Sun">Sunday</option>
                        <option value="Mon">Monday</option>
                        <option value="Tue">Tuesday</option>
                        <option value="Wed">Wednesday</option>
                        <option value="Thu">Thuresday</option>
                        <option value="Fri">Friday</option>
                    </select>
                    <div class="alert-danger"><?php echo ucwords(form_error('weekend_date')); ?></div>
                </div>
                <div class="form-group col-md-3">
                    <label for="name">Submit</label>
                    <div>
                    <input type="submit" name="mysubmit" value="Generate Weekend"  class="btn btn-primary"></div>
                </div>
            </div>
                <!--<span>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
                    <input type="hidden" name="id" value="" />&nbsp;&nbsp;&nbsp;&nbsp;
                    <input type="submit" name="mysubmit" value="Submit"  class="btn btn-primary">
                    <a class="btn btn-theme" href="<?php /*echo base_url(); */?>hr/weekend/weekend_list">Cancel</a>
                </span>-->
            </form>
            <p></p>
        </div>
    </div><!-- col-lg-12-->
</div><!-- /row -->

<h3><i class="fa fa-angle-right"></i> View Weekend Calendar</h3>
<div class="pull-left">
    <?php $query3 = $this->db->query("select id,branch_name from add_branch"); ?>
    <select name="empTypeSelect" id="branch_Select" class="form-control" style="float:right;" onchange="branch_wise_weekend()">
        <option   value="">Get Branch Wise Weekend</option>
        <?php foreach ($query3->result() as $row3): ?>
            <option value="<?php echo $row3->id; ?>"><?php echo $row3->branch_name; ?></option>
        <?php endforeach; ?>
    </select>
</div>
<div class="pull-left"><a href="<?php echo base_url();?>hr/weekend/insert_weekend" class="btn btn-info" role="button" >Reset</a>&nbsp;</div>
<div class="pull-right"> <a href="<?php echo base_url();?>hr/weekend/weekend_list" class="btn btn-info" role="button" >View All Weekend List</a>&nbsp; </div>
<div class="row">
    <div class="col-sm-12">
        <div class="content-panel">
            <!--<section id="flip-scroll">-->
            <section id="">
                <div id='calendar'></div>
            </section>
        </div><! --/content-panel -->
    </div><!-- /col-md-12 -->
</div>
<?php
foreach($query1 as $row){
    ucfirst($row->branch_name) ;
    $row->weekend_date ;
}

//show($query1);
?>

<script>
    $(document).ready(function() {
        $('#calendar').fullCalendar({
            header: {
                left: 'prev,next today',
                center: 'title',
                right: 'month,agendaWeek,agendaDay'
            },
            defaultDate: '<?php echo date("Y-m-d") ?>',
            navLinks: true, // can click day/week names to navigate views
            selectable: true,
            selectHelper: true,

            editable: true,
            eventLimit: true, // allow "more" link when too many events
            events: [

                <?php foreach($query1 as $key => $row):?>
                {
                    id: "<?php echo $row->id; ?>",
                    title: "<?php echo ucfirst($row->bname); ?>",
                    start: "<?php echo ucfirst($row->weekend_date); ?>",
                    editable:true

                }<?php if(key_exists(($key+1),$query1)):?>,<?php endif;?>
                <?php endforeach;?>
            ]
        });

    });

</script>