<h3><i class="fa fa-angle-right"></i> Change Status</h3>

<!-- BASIC FORM ELELEMNTS -->
<div class="row">
    <div class="col-sm-12">
        <div class="content-panel">
            <?php
            $status = array('0' => 'Active',
                '1' => 'Inactive'
            );
            foreach ($query as $t):endforeach;

            echo form_open_multipart('hr/info/change_status');?>
            <?php
            if($this->session->flashdata('message')==NULL){

            }else{ ?>
                <div class="alert-success alert">
                    <?php echo $this->session->flashdata('message') ?>
                </div> <?php
            }
            ?>
            <?php
            $query12 = $this->db->query("SELECT id,branch_name FROM add_branch");
            ?>
            <div class="col-md-12">
                <div class="form-group col-sm-12">
                    <label for="package_name">Leave Package Name:</label>
                    <?php echo form_dropdown('status', $status, $t->status, 'class="form-control"') ?>
                    <div class="alert-danger"><?php echo ucwords(form_error('status')); ?></div>
                </div>
            </div>
                <span>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
                    <input type="hidden" name="id" value="<?php echo $t->id ?>" />&nbsp;&nbsp;&nbsp;&nbsp;
                    <input type="submit" name="mysubmit" value="Submit"  class="btn btn-primary">
                    <a class="btn btn-theme" href="<?php echo base_url(); ?>hr/leave_package/leave_package_list">Cancel</a>
                </span>

            </form>
            <p></p>
        </div>
    </div><!-- col-lg-12-->
</div><!-- /row -->