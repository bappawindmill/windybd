<h3><i class="fa fa-angle-right"></i> Employee General And Official Info</h3>

<?php

$attendence = array('yes' => 'yes',

    'no' => 'no'

);



$pf = array(

    '1' => 'No',

    '2' => 'Yes'

);

$tax = array(

    '' => 'Select',

    '1' => 'No',

    '2' => 'Yes'

);

$gender1 = array(

    '' => 'Select',

    'Male' => 'Male',

    'Female' => 'Female'

);



$msatatus = array(

    '' => 'Select',

    'Yes' => 'Yes',

    'No' => 'No'

);

$bg = array(

    '' => 'Select',

    'A+' => 'A+',

    'A-' => 'A-',

    'B+' => 'B+',

    'B-' => 'B-',

    'O+' => 'O+',

    'O-' => 'O-',

    'AB+' => 'AB+',

);

$category = array(

    '' => 'Select',

    '1' => 'Temporary',

    '2' => 'Probation Period',

    '3' => 'Permanent'

);
?>

<!-- BASIC FORM ELELEMNTS -->
<div class="row">
    <div class="col-sm-12">
        <div class="content-panel">
            <?php

            foreach ($query as $t):endforeach;
            echo form_open_multipart('hr/info/employee_general_info_edit/'.$id);
            ?>
            <?php
            if($this->session->flashdata('message')==NULL){

            }else{ ?>
                <div class="alert-success alert">
                    <?php echo $this->session->flashdata('message') ?>
                </div> <?php
            }
            ?>
            <div class="col-md-12">
                <div class="form-group col-sm-6">

                    <table class="table table-bordered">
                        <thead>
                            <tr>
                                <th colspan="2" style=" background-color:#3D9EE2; color:white; font-weight:bold">
                                    <b>General Information</b>
                                </th>
                            </tr>
                        </thead>
                        <tr>
                            <th>EMPLOYEE ID No :</th>
                            <td>
                                <input class="form-control" type="text" name="card_no" value="<?php echo $t->card_no; ?>" class="input-short" <?php if ($this->session->userdata("utype") != '1'): ?> readonly="readonly"<?php endif; ?> />
                                <span style="color:red;"></span>
                                <span class="notification-input ni-error"> <?php echo ucwords(form_error('card_no')); ?></span>
                            </td>
                        </tr>
                        <tr>
                            <th>Secret No :</th>
                            <td>
                                <input class="form-control" type="text" value="<?php echo $t->secrete_no; ?>" name="secrete_no" <?php if ($this->session->userdata("utype") != '1'): ?> readonly="readonly"<?php endif; ?> />
                                <span style="color:red;"></span> <span class="notification-input ni-error"> <?php echo ucwords(form_error('secrete_no')); ?></span>
                            </td>
                        </tr>

                        <tr><th>Name :</th>
                            <td><?php echo form_input('name', $t->name, 'class="form-control"') ?><span style="color:red;">*</span>
                                <span class="notification-input ni-error"> <?php echo ucwords(form_error('name')); ?></span>
                            </td>
                        </tr>
                        <tr>
                            <th>Date Of Birth :</th><td>
                                <input class="form-control" type="text" name="date_of_birth" id="date_of_birth" value="<?php echo $t->dob; ?>"/>
                            </td>
                        </tr>

                        <tr><th>Gender :</th><td><?php echo form_dropdown('gender', $gender1,  $t->gender) ?></td></tr>

                        <tr><th>Photo :</th><td><input type="file" name="userfile" class="input-short" /></td></tr>

                        <tr><th>Marital_status :</th><td><?php echo form_dropdown('marital_status', $msatatus, $t->marital_status) ?></td></tr>

                        <tr><th>nationality :</th><td> <?php echo form_input('nationality', $t->nationality, 'nationality') ?></td></tr>

                        <tr><th>Blood Group :</th><td><?php echo form_dropdown('blood', $bg, $t->blood) ?></td></tr>

                        <tr><th>telephone :</th><td><?php echo form_input('telephone', $t->telephone, 'telephone') ?></td></tr>

                        <tr><th>contact person :</th><td><?php echo form_input('contact_person', $t->contact_person, 'contact_person') ?> </td></tr>

                        <tr><th>relation :</th><td> <?php echo form_input('relation', $t->relation, 'relation') ?></td></tr>

                        <tr><th>National ID :</th><td> <?php echo form_input('national_id', $t->national_id, 'national_id') ?></td></tr>

                        <tr>

                            <th>office email :</th><td> <?php echo form_input('email', $t->email, 'email') ?>  <span style="color:red;">*</span>

                                <span class="notification-input ni-error"> <?php echo ucwords(form_error('email')); ?></span>

                            </td>

                        </tr>

                        <tr>

                            <th>Personal email :</th><td> <?php echo form_input('personal_email', $t->personal_email, 'personal_email') ?>

                                <span style="color:red;"></span>

                                <span class="notification-input ni-error"> <?php echo ucwords(form_error('personal_email')); ?></span>

                            </td>

                        </tr>



                        <tr><th>Emergency_no :</th><td><?php echo form_input('emergency_no', $t->emergency_no, 'class="input-short"') ?></td></tr>

                        <tr><th>join date :</th><td><input type="text" name="join_date" id="weekend_date" class="input-short" value="<?php echo $t->join_date; ?>"  /></td></tr>

                        <tr><th style="vertical-align:middle">Present Address :</th><td>

                                <textarea rows="3" cols="16" style="resize:none" name="present_address" ><?php echo $t->present_address; ?></textarea><span style="color:red;"></span>

                                <span class="notification-input ni-error"> <?php echo ucwords(form_error('present_address')); ?></span>

                            </td></tr>

                        <tr><th style="vertical-align:middle">Permanent Address :</th><td>

                                <textarea rows="3" cols="16" style="resize:none" name="permanent_address"><?php echo $t->permanent_address; ?></textarea><span style="color:red;"></span>

                                <span class="notification-input ni-error"> <?php echo ucwords(form_error('permanent_address')); ?></span>

                            </td></tr>



                        <tr><th>Emp Category: </th><td>

                                <?php echo form_dropdown('category', $category, $t->category); ?>

                                <span style="color:red;"></span>

                                <span class="notification-input ni-error"> <?php echo ucwords(form_error('category')); ?></span>

                            </td></tr>

                        <tr><th>Confirmation date :</th><td><input type="text" name="confirmation_date" id="start_date" class="input-short" value="<?php echo $t->confirmation_date; ?>"  /></td></tr>

                        <tr><th>Notice Period: </th><td>  <input type="text" value="<?php echo $t->notice_period; ?>" class="input-short" name="notice_period" id="notice_period">

                                <span style="color:red;">days</span></td> </span></tr>



                        <tr><th>Attendence Rules :</th><td> <?php echo form_dropdown('attendence_rules', $attendence, $t->attendence_rules, 'style=width:130px') ?></td></tr>



                        <tr><th>note :</th><td><?php echo form_input('note', $t->note, 'note') ?></td></tr>

                        <tr><th>Name of reference :</th><td>

                                <input type="text" class="input-short" name="reference_name" id="reference_name" value="<?php echo $t->reference_name; ?>">

                                <span class="notification-input ni-error"> <?php echo ucwords(form_error('reference_name')); ?></span>

                            </td></tr>

                        <tr><th> Contact No :</th><td>

                                <input type="text" class="input-short" value="<?php echo $t->referee_contact_no; ?>" name="referee_contact_no" id="notice_period">

                                <span class="notification-input ni-error"> <?php echo ucwords(form_error('referee_contact_no')); ?></span>

                            </td></tr>





                    </table>

                </div>



                <div class="form-group col-sm-6">
                    <table class="table table-bordered">

                        <thead><tr><th colspan="2" style=" background-color:#3D9EE2; color:white; font-weight:bold"><b>Employee official Profile</b></th></tr></thead>



                        <tr><th>Leave Package :</th><td><select class="form-control" name='leave_name' id='ctlJob'>";

                                    <?php

                                    $l = $this->db->query("SELECT id,package_name FROM add_leave");

                                    foreach ($l->result() as $l1):

                                        ?>

                                        <option <?php if (($l1->id) == ($t->leave_name)): ?> selected="selected" value="<?php echo $t->leave_name; ?>" <?php else: ?>  value="<?php echo $l1->id; ?>" <?php endif; ?>><?php echo $l1->package_name; ?></option>

                                    <?php endforeach; ?>

                                </select>

                            </td></tr>





                        <tr>

                        <tr><th>SBU Name :</th>

                            <td>

                                <?php $query66 = $this->db->query("SELECT id,branch_name FROM add_branch"); ?>



                                <select class="form-control" name='branch_name' id='crlbranch'>";

                                    <option value="">Select</option>

                                    <?php foreach ($query66->result() as $row88): ?>

                                        <option <?php if (($row88->id) == ($t->branch_name)): ?> selected="selected" value="<?php echo $t->branch_name; ?>" <?php else: ?>  value="<?php echo $row88->id; ?>" <?php endif; ?>><?php echo $row88->branch_name; ?></option>

                                    <?php endforeach; ?>

                                </select>

                            </td>

                        </tr>



                        <tr><th>Dpartment :</th>

                            <td>

                                <?php

                                $d1 = $this->db->query("select id,name from add_department where id='" . $t->dept . "'");

                                $sName = "";

                                foreach ($d1->result() as $d11):

                                    $sName = $d11->name;

                                endforeach;

                                ?>

                                <div id='res'>

                                    <!--<select name='dept' >

                                        <option value="<?php echo $t->dept; ?>"><?php echo $sName; ?></option>

                                    </select> -->

                                </div>

                            </td>

                        </tr>

                        <tr>

                            <th>Section :</th>

                            <td>

                                <?php

                                $p = $this->db->query("select proffession from add_proffession where id='" . $t->proffession . "'");

                                $p6 = "";

                                foreach ($p->result() as $t1):

                                    $p6 = $t1->proffession;

                                endforeach;

                                ?>

                                <div id='result'>

                                    <select class="form-control" name="proffession">

                                        <option value="<?php echo $t->proffession; ?>"><?php echo $p6; ?></option>

                                    </select>

                                </div>

                            </td>

                        </tr>



                        <tr><th>Employee Type :</th><td>

                                <?php $query27 = $this->db->query("SELECT id,emp_type FROM emp_type"); ?>

                                <select class="form-control" name="emp_type" id="emp_type">

                                    <?php foreach ($query27->result() as $d_info): ?>

                                        <option <?php if (($d_info->id) == ($t->emp_type)): ?> selected="selected" value="<?php echo $t->emp_type; ?>" <?php else: ?>  value="<?php echo $d_info->id; ?>" <?php endif; ?>><?php echo $d_info->emp_type; ?></option>



                                    <?php endforeach; ?>



                                </select>

                                <span class="notification-input ni-error"> <?php echo ucwords(form_error('emp_type')); ?></span>

                            </td>

                        </tr>

                        <?php

                        $d9 = $this->db->query("select designation from designation where id='" . $t->designation . "'");

                        $d8 = "";

                        foreach ($d9->result() as $t10):

                            $d8 = $t10->designation;

                        endforeach;

                        ?>

                        <tr><th>Designation :</th>
                            <td>
                                <div id='ds'></div>
                                <span class="notification-input ni-error"> <?php echo ucwords(form_error('designation')); ?></span>
                            </td>
                        </tr>
                        <tr><th>Duty Schedule :</th><td>
                                <?php $scheduleInfo = $this->db->query("SELECT * FROM schedule"); ?>
                                <select class="form-control" name="duty_schedule" id="duty_schedule">

                                    <option value="">Select</option>

                                    <?php foreach ($scheduleInfo->result() as $schedule_info): ?>

                                        <option <?php if (($schedule_info->id) == ($t->duty_schedule)): ?> selected="selected" value="<?php echo $t->duty_schedule; ?>" <?php else: ?>  value="<?php echo $schedule_info->id; ?>" <?php endif; ?>><?php echo $schedule_info->schedule_title; ?></option>

                                    <?php endforeach; ?>

                                </select>

                                <span class="notification-input ni-error"> <?php echo ucwords(form_error('duty_schedule')); ?></span>

                            </td>

                        </tr>





                        <table class="table table-bordered">

                            <thead><tr><th colspan="2" style=" background-color:#3D9EE2; color:white; font-weight:bold"><b>Employee Login Info</b></th></tr></thead>

                            <tr>

                                <th colspan="2" style="font-size:10px; color: #993366; font-weight:bold; text-align:center">Please select supervisor name and designation</th>

                            </tr>

                            <tr>
                                <th>Supervisor Deg</th>
                                <td>
                                    <?php $degInfo = $this->db->query("SELECT id,designation FROM designation"); ?>
                                    <select name="sup_designation" id="empDesignation" style="width:130px" onchange="javascript:getSupervisor(this.value)">
                                        <option value="">Select</option>
                                        <?php foreach ($degInfo->result() as $deg): ?>
                                            <option <?php if (($deg->id) == ($t->sup_designation)): ?> selected="selected" value="<?php echo $t->sup_designation; ?>" <?php else: ?>  value="<?php echo $deg->id; ?>" <?php endif; ?>><?php echo $deg->designation; ?></option>
                                        <?php endforeach; ?>
                                    </select>
                                    <span class="notification-input ni-error"><?php echo ucwords(form_error('empDesignation')); ?></span>
                                </td>
                            </tr>

                            <tr>
                                <th>Supervisor Name : </th>
                                <td>
                                    <div id="resultSupervisor">
                                        <?php $e = $this->db->query("SELECT id,name FROM employeeinfo where designation='" . $t->sup_designation . "'"); ?>
                                        <select name="supervisor" id="supervisor" style="width:130px" >
                                            <option value="">Select</option>
                                            <?php foreach ($e->result() as $e1): ?>
                                                <option <?php if (($e1->id) == ($t->supervisor)): ?> selected="selected" value="<?php echo $t->supervisor; ?>" <?php else: ?>  value="<?php echo $e1->id; ?>" <?php endif; ?>><?php echo $e1->name; ?></option>
                                            <?php endforeach; ?>
                                        </select>
                                    </div>
                                    <span class="notification-input ni-error"><?php echo ucwords(form_error('supervisor')); ?></span>
                                </td>
                            </tr>
                            <tr>
                                <th colspan="2" style="font-size:10px; color: #993366; font-weight:bold; text-align:center">Please Enter Employee username and pass for login </th>
                            </tr>
                            <tr><th>Username :</th><td><?php echo form_input('username', $t->username, 'class="input-short"'); ?><span style="color:red;">*</span>

                                    <span class="notification-input ni-error"> <?php echo ucwords(form_error('username')); ?></span></td></tr>

                            <tr><th>Password :</th><td><?php echo form_input('password', $t->password, 'class="input-short"'); ?><span style="color:red;">*</span>

                                    <span class="notification-input ni-error"> <?php echo ucwords(form_error('password')); ?></span></td></tr>



                            <?php if ($this->session->userdata("finance_prev") == '1'): ?>

                            <tr><th colspan="2" style=" background-color:#3D9EE2; color:white; font-weight:bold"><center><b>Employee PF Info</center></b></th></tr>

                            <tr><th>Provident Fund:</th><td><?php echo form_dropdown('provident_fund', $pf, $t->provident_fund, 'style=width:130px') ?></td></tr>

                            <tr><th>Prev Balance(off) :</th><td><?php echo form_input('employeer_pf_balance', $t->employeer_pf_balance, 'class="input-short"'); ?>

                                    <span class="notification-input ni-error"> <?php echo ucwords(form_error('employeer_pf_balance')); ?></span></td></tr>

                            <tr>

                                <th>Prev Balance(emp) :</th><td><?php echo form_input('employee_pf_balance', $t->employee_pf_balance, 'class="input-short"'); ?>

                                    <span class="notification-input ni-error"> <?php echo ucwords(form_error('employee_pf_balance')); ?></span></td>

                            </tr>

                            <tr>

                                <th>Timeframe :</th><td><?php echo form_input('timeframe', $t->timeframe, 'class="input-short"'); ?>

                                    <span class="notification-input ni-error"> <?php echo ucwords(form_error('timeframe')); ?></span></td>

                            </tr>

                            <tr ><th colspan="2" style=" background-color:#3D9EE2; color:white; font-weight:bold"><center><b>Employee payment mode</center></b></th></tr>

                            <tr><th>Mobile Bill :</th><td><?php echo form_input('mobile_bill', $t->mobile_bill, 'class="input-short"'); ?>

                                    <span class="notification-input ni-error"> <?php echo ucwords(form_error('mobile_bill')); ?></span></td></tr>

                            <tr><th>Tex Deduction:</th><td><?php echo form_dropdown('tax_deduction', $tax, $t->tax_deduction, 'style=width:130px') ?></td></tr>

                            <tr><th>Tax Amount :</th><td><?php echo form_input('tax_percentage', $t->tax_percentage, 'class="input-short"'); ?>

                                    <span class="notification-input ni-error"> <?php echo ucwords(form_error('tax_percentage')); ?></span></td></tr>

                            <tr><th>Payment Mode:</th>

                                <td>

                                    Cash<input type="checkbox" <?php if ($t->payment_mode == "1"): ?>checked="checked"<?php endif; ?> name="cash_payment" value=1 id="cash_payment" onClick="return check_value();">

                                    Bank<input type="checkbox" <?php if ($t->payment_mode == "2"): ?>checked="checked" <?php endif; ?> name="bank_payment" value=2 id="bank_payment" onClick="return check();">

                                </td>

                            </tr>

                            <?php

                            if ($t->payment_mode == "2") {

                                $a = "block";

                            } else {

                                $a = "none";

                            }



                            $query00 = $this->db->query("select id,bank_name from bank");

                            ?>

                            <tr><th>Bank Name: </th><td>

                                    <select name="bank_name" id="bank_name" style=" width:130px;">

                                        <option value="select">select</option>

                                        <?php foreach ($query00->result() as $info2): ?>

                                            <option <?php if (($info2->id) == ($t->bank_name)): ?> selected="selected" value="<?php echo $t->bank_name; ?>" <?php else: ?>  value="<?php echo $info2->id; ?>" <?php endif; ?>><?php echo $info2->bank_name; ?></option>

                                        <?php endforeach; ?>

                                    </select>

                                </td></tr>



                            <tr><th>Branch Name: </th><td> <div id="result99">

                                        <?php

                                        $b = $this->db->query("select id,branch_name from branch_info where id='" . $t->bank_branch . "'");



                                        $bname = "";

                                        foreach ($b->result() as $b1):

                                            $bname = $b1->branch_name;

                                        endforeach;

                                        ?>



                                        <select name="bank_branch" id="bank_branch" style="width:130px; display:<?php echo $a; ?>">

                                            <option value="<?php echo $t->branch_name; ?>"><?php echo $bname; ?></option>

                                        </select>







                                    </div></td></tr>

                            <tr><th>Account Number: </th><td>  <input type="text" name="account_number" id="account_number" value="<?php echo $t->account_number; ?>" style="display: <?php echo $a; ?>;"></td></tr>

                            <!-- <tr><th>Salary Package :</th><td>

                            <?php //$query660 = $this->db->query("SELECT id,package_name FROM sapackage"); ?>

                                    <select name='sapackage'  style='width:130px;'>

                                        <option value="0">Select</option>

                            <?php //foreach ($query660->result() as $row808): ?>

                                            <option <?php if (($row808->id) == ($t->sapackage)): ?> selected="selected" value="<?php echo $t->sapackage; ?>" <?php else: ?>  value="<?php echo $row808->id; ?>" <?php endif; ?>><?php echo $row808->package_name; ?></option>



                            <?php //endforeach; ?>

                                    </select>

                                </td></tr>

                            <tr>-->

                            <tr><th colspan="2" style="text-align:left">Custom Salary Package:

                                    <input type="checkbox" name="package" value="1" id="package" onClick="check_package_edit()">

                                </th>

                            </tr>



                            <tr id="p1" style="display:none" >

                                <th style="width:147px">Basic </th>

                                <td>

                                    <input type="text" name="basic" value="<?php echo $t->basic; ?>" onblur="calculateAuto(this.value)" />

                                </td>

                            </tr>

                            <tr id="p2" style="display:none">

                                <th style="width:147px">House Rent </th>

                                <td>

                                    <input type="text" name="house_rent" id="house_rent" value="<?php echo $t->house_rent; ?>" readonly="readonly" />

                                </td>

                            </tr>

                            <tr id="p3" style="display:none">

                                <th style="width:147px">Medical</th>

                                <td>

                                    <input type="text" name="medical" id="medical"value="<?php echo $t->medical; ?>" readonly="readonly" />

                                </td>

                            </tr>

                            <tr id="p4" style="display:none">

                                <th style="width:147px">Transfort</th>

                                <td>

                                    <input type="text" name="conveyance" id="conveyance" value="<?php echo $t->conveyance; ?>" readonly="readonly"/>

                                </td>

                            </tr>

                            <tr id="p5" style="display:none">

                                <th style="width:147px">Special</th>

                                <td>

                                    <input type="text" name="special" id="special" value="<?php echo $t->special; ?>" readonly="readonly"/>

                                </td>

                            </tr>

                            <tr id="p6" style="display:none">

                                <th style="width:147px">Salary Step</th>

                                <td>

                                    <input type="text" name="salary_step" id="salary_step" value="<?php echo $t->salary_step; ?>" />

                                </td>

                            </tr>

                            <tr id="p7" style="display:none">

                                <th style="width:147px">Salary Band</th>

                                <td>

                                    <input type="text" name="salary_band" id="salary_band" value="<?php echo $t->salary_band; ?>" />

                                </td>

                            </tr>

                        </table>

                    <?php endif;?>
                </div>
            </div>
                <span>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
                    <input type="hidden" name="id" value="<?php echo $t->id ?>" />&nbsp;&nbsp;&nbsp;&nbsp;
                    <input type="submit" name="mysubmit" value="Submit"  class="btn btn-primary">
                </span>
            </form>
            <p></p>
        </div>
    </div><!-- col-lg-12-->
</div><!-- /row -->