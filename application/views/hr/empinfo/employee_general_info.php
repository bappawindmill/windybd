<h3><i class="fa fa-angle-right"></i> Employee General And Official Info</h3>

<?php

$attendence = array('yes' => 'yes',

    'no' => 'no'

);



$pf = array(

    '1' => 'No',

    '2' => 'Yes'

);

$tax = array(

    '' => 'Select',

    '1' => 'No',

    '2' => 'Yes'

);

$gender1 = array(

    '' => 'Select',

    'Male' => 'Male',

    'Female' => 'Female'

);



$msatatus = array(

    '' => 'Select',

    'Yes' => 'Yes',

    'No' => 'No'

);

$bg = array(

    '' => 'Select',

    'A+' => 'A+',

    'A-' => 'A-',

    'B+' => 'B+',

    'B-' => 'B-',

    'O+' => 'O+',

    'O-' => 'O-',

    'AB+' => 'AB+',

);

$category = array(

    '' => 'Select',

    '1' => 'Temporary',

    '2' => 'Probation Period',

    '3' => 'Permanent'

);
?>

<!-- BASIC FORM ELELEMNTS -->
<div class="row">
    <div class="col-sm-12">
        <div class="content-panel">
            <?php
                echo form_open_multipart('hr/info/added_employee_general_info');
            ?>
            <?php
                if($this->session->flashdata('message')==NULL){

                }else{ ?>
                    <div class="alert-success alert">
                        <?php echo $this->session->flashdata('message') ?>
                    </div> <?php
                }
            ?>
                <div class="col-md-12">
                    <div class="form-group col-sm-6">

                        <table class="table table-bordered">
                            <thead>
                            <tr><th colspan="2" style="background-color:#3D9EE2; color:white; font-weight:bold"><b>General Information</b></th></tr>
                            </thead>
                            <tr>
                                <th>EMPLOYEE ID No :</th>
                                <td>
                                    <input type="text" name="card_no" class="input-short" <?php if ($this->session->userdata("utype") != '1'): ?> readonly="readonly"<?php endif; ?> value="<?php echo $this->input->post('card_no') ?>" /><span style="color:red;">*</span>
                                    <span class="notification-input ni-error"> <?php echo ucwords(form_error('card_no')); ?></span>
                                </td>
                            </tr>

                            <tr>
                                <th>Secret No :</th>
                                <td>
                                    <input type="text" name="secrete_no" class="input-short" <?php if ($this->session->userdata("utype") != '1'): ?> readonly="readonly"<?php endif; ?>  value="<?php echo $this->input->post('secrete_no') ?>"/><span style="color:red;">*</span>
                                    <span style="color:red;"></span> <span class="notification-input ni-error"> <?php echo ucwords(form_error('secrete_no')); ?></span>
                                </td>
                            </tr>

                            <tr>
                                <th>FirstName :</th>
                                    <td>
                                        <?php echo form_input('firstname', $this->input->post('firstname'), 'class="input-short"') ?><span style="color:red;">*</span>
                                        <span class="notification-input ni-error"> <?php echo ucwords(form_error('firstname')); ?></span>
                                </td>
                            </tr>

                            <tr>
                                <th>LastName :</th>
                                    <td>
                                        <?php echo form_input('lastname',$this->input->post('lastname'), 'class="input-short"') ?>
                                        <span style="color:red;">*</span> <span class="notification-input ni-error"> <?php echo ucwords(form_error('lastname')); ?></span>
                                    </td>
                            </tr>
                            <tr>
                                <th>Date Of Birth :</th>
                                <td>
                                    <select name="year" style="width:60px;" class="input-short">
                                        <?php for ($i = 1920; $i <= date('Y'); $i++) { ?>
                                            <option value="<?php echo $i; ?> " <?php if($this->input->post('year') == $i){ echo "selected";}?> > <?php echo $i; ?></option>
                                        <?php } ?>
                                    </select>

                                    <select name="month">
                                        <option value="01" <?php if($this->input->post('month') == "01"):?> selected <?php endif;?> >January</option>
                                        <option value="02" <?php if($this->input->post('month') == "02"):?> selected <?php endif;?> >February</option>

                                        <option value="03" <?php if($this->input->post('month') == "03"):?> selected <?php endif;?>>March</option>

                                        <option value="04" <?php if($this->input->post('month') == "04"):?> selected <?php endif;?>>April</option>

                                        <option value="05" <?php if($this->input->post('month') == "05"):?> selected <?php endif;?>>May</option>

                                        <option value="06" <?php if($this->input->post('month') == "06"):?> selected <?php endif;?>>June</option>

                                        <option value="07" <?php if($this->input->post('month') == "07"):?> selected <?php endif;?>>July</option>

                                        <option value="08" <?php if($this->input->post('month') == "08"):?> selected <?php endif;?>>August</option>

                                        <option value="09" <?php if($this->input->post('month') == "09"):?> selected <?php endif;?>>September</option>

                                        <option value="10" <?php if($this->input->post('month') == "10"):?> selected <?php endif;?>>October</option>

                                        <option value="11" <?php if($this->input->post('month') == "11"):?> selected <?php endif;?>>November</option>

                                        <option value="12" <?php if($this->input->post('month') == "12"):?> selected <?php endif;?>>December</option>

                                    </select>

                                    <select name="day" style="width:60px;">

                                        <?php for ($i = 1; $i <= 31; $i++) { ?>

                                            <option value="<?php echo $i; ?> " <?php if($this->input->post('day') == $i){ echo "selected";}?> ><?php echo $i; ?></option>

                                        <?php } ?>

                                    </select>



                                </td>
                            </tr>

                            <tr><th>gender :</th><td><?php echo form_dropdown('gender', $gender1,$this->input->post('gender'), 'class="input-short"') ?></td></tr>

                            <tr><th>photo :</th><td><input type="file" name="userfile" class="input-short" /></td></tr>

                            <tr><th>marital_status :</th><td><?php echo form_dropdown('marital_status', $msatatus,$this->input->post('marital_status'), 'class="input-short"') ?></td></tr>

                            <tr><th>nationality :</th><td> <?php echo form_input('nationality', $this->input->post('nationality'), 'class="input-short"') ?></td></tr>

                            <tr><th>Blood Group :</th><td><?php echo form_dropdown('blood', $bg, $this->input->post('blood') ,'class="input-short"') ?></td></tr>

                            <tr><th>telephone :</th><td><?php echo form_input('telephone', $this->input->post('telephone'), 'class="input-short"') ?></td></tr>

                            <tr><th>contact person :</th><td><?php echo form_input('contact_person',  $this->input->post('contact_person'), 'class="input-short"') ?> </td></tr>

                            <tr><th>relation :</th><td> <?php echo form_input('relation',  $this->input->post('relation'), 'class="input-short"') ?></td></tr>

                            <tr><th>National ID :</th><td> <?php echo form_input('national_id',  $this->input->post('national_id'), 'class="input-short"') ?></td></tr>

                            <tr>

                                <th>office email :</th><td> <?php echo form_input('email',  $this->input->post('email'), 'class="input-short"') ?>  <span style="color:red;">*</span>

                                    <span class="notification-input ni-error"> <?php echo ucwords(form_error('email')); ?></span>

                                </td>

                            </tr>

                            <tr>

                                <th>Personal email :</th><td> <?php echo form_input('personal_email',  $this->input->post('personal_email'), 'class="input-short"') ?>  <span style="color:red;"></span>

                                    <span class="notification-input ni-error"> <?php echo ucwords(form_error('personal_email')); ?></span>

                                </td>

                            </tr>

                            <tr><th>Emergency_no :</th><td><?php echo form_input('emergency_no',  $this->input->post('emergency_no'), 'class="input-short"') ?></td></tr>

                            <tr><th>join date :</th><td><input type="text" name="join_date" id="weekend_date" class="input-short" value="<?php echo $this->input->post('join_date') ?>"  /></td></tr>

                            <tr><th style="vertical-align:middle">Present Address :</th><td>

                                    <textarea rows="3" cols="16" style="resize:none" name="present_address"><?php echo $this->input->post('present_address') ?></textarea><span style="color:red;"></span>

                                    <span class="notification-input ni-error"> <?php echo ucwords(form_error('present_address')); ?></span>

                                </td>
                            </tr>

                            <tr><th style="vertical-align:middle">Permanent Address :</th><td>

                                    <textarea rows="3" cols="16" style="resize:none" name="permanent_address"><?php echo $this->input->post('permanent_address') ?></textarea><span style="color:red;"></span>

                                    <span class="notification-input ni-error"> <?php echo ucwords(form_error('permanent_address')); ?></span>

                                </td>
                            </tr>

                            <tr><th>Emp Category: </th><td>

                                    <?php echo form_dropdown('category', $category,$this->input->post('category'), 'class="input-short"'); ?>

                                    <span style="color:red;">*</span>

                                    <span class="notification-input ni-error"> <?php echo ucwords(form_error('category')); ?></span>

                                </td>
                            </tr>

                            <tr>
                                <th>Confirmation date :</th>
                                <td><input type="text" name="confirmation_date" id="start_date" class="input-short" value="<?php echo $this->input->post('confirmation_date') ?>"  /></td>
                            </tr>

                            <tr>
                                <th>Notice Period: </th>
                                <td>  <input type="text" class="input-short" name="notice_period" id="notice_period" value="<?php echo $this->input->post('notice_period') ?>">

                                    <span style="color:red;">days</span>
                                </td> </span>
                            </tr>

                            <tr>
                                <th>Attendence Rules :</th>
                                <td> <?php echo form_dropdown('attendence_rules', $attendence, $this->input->post('attendence_rules'), 'style=width:130px') ?>
                                </td>
                            </tr>

                            <tr>
                                <th>note :</th>
                                <td><?php echo form_input('note', $this->input->post('note'), 'note') ?></td>
                            </tr>

                            <tr>
                                <th>Name of reference :</th>
                                <td>
                                    <input type="text" class="input-short" name="reference_name" id="notice_period" value="<?php echo $this->input->post('reference_name') ?>">
                                    <span class="notification-input ni-error"> <?php echo ucwords(form_error('reference_name')); ?></span>
                                </td>
                            </tr>

                            <tr>
                                <th> Contact No :</th>
                                <td>
                                    <input type="text" class="input-short" name="referee_contact_no" id="notice_period" value="<?php echo $this->input->post('referee_contact_no') ?>">
                                    <span class="notification-input ni-error"> <?php echo ucwords(form_error('referee_contact_no')); ?></span>
                                </td>
                            </tr>
                        </table>

                    </div>

                    <div class="form-group col-sm-6">
                        <table class="table table-bordered">
                            <thead>
                                <tr><th colspan="2" style=" background-color:#3D9EE2; color:white; font-weight:bold"><b>Employee official Profile</b></th></tr>
                            </thead>
                            <?php
                            $query3 = $this->db->query("SELECT id,package_name FROM add_leave");
                            $dataStr3 = "<tr><th>Leave Package :</th><td><select name='leave_name' style='width:130px;'>";
                            foreach ($query3->result() as $row3) {
                                $dataStr3.="<option value='" . $row3->id . "'";
                                if($this->input->post('leave_name')==$row3->id){
                                    $dataStr3.=" Selected";
                                }
                                $dataStr3.=">" . $row3->package_name . "</option>";
                            }
                            $dataStr3.='</select></td></tr>';
                            echo $dataStr3;
                            $query66 = $this->db->query("SELECT id,branch_name FROM add_branch");
                            $dataStr12 = "<tr><th>SBU Name :</th><td>
			<select name='branch_name' id='crlbranch' style='width:130px;'>";
                            $dataStr12.="<option value=''>select</option>";
                            foreach ($query66->result() as $row33) {
                                $dataStr12.="<option value='" . $row33->id . "'";
                                if($this->input->post('branch_name')==$row33->id){
                                    $dataStr12.=" Selected";
                                }
                                $dataStr12.=">" . $row33->branch_name . "</option>";
                            }
                            $dataStr12.='</select>'
                            ?>
                            <?php
                            '</td>';
                            '</tr>';
                            echo $dataStr12
                            ?>
                            <span class="notification-input ni-error"> <?php echo ucwords(form_error('branch_name')); ?></span>

                            <tr><th>department :</th><td>

                                    <div id='res'>

                                        <!--<select name="dept" id="ctlDep" onchange="app(this.value)" style="width:130px;">

								<option value="<?php echo $this->input->post('dept'); ?>"><?php echo $this->input->post('dept'); ?></option>

							</select>-->

                                    </div>

                                    <span class="notification-input ni-error"> <?php echo ucwords(form_error('dept')); ?></span>

                                </td>
                            </tr>

                            <!-- {DEPARTMENT}-->
                            <?php
                            $query2 = $this->db->query("SELECT id,package_name FROM salary_package where status=0");
                            $dataStr2 = "<tr><th>Salary Package :</th><td><select name='salary_package' style='width:130px;'>";
                            foreach ($query2->result() as $row2) {
                                $dataStr2.="<option value='" . $row2->id . "'";
                                $dataStr2.=">" . $row2->package_name . "</option>";
                            }
                            $dataStr2.='</select></td></tr>';
                            ?>
                            <tr>
                                <th>section :</th>
                                    <td>
                                        <div id='result'></div>
                                        <span class="notification-input ni-error"> <?php echo ucwords(form_error('proffession')); ?></span>
                                    </td>
                            </tr>

                            <tr>
                                <th>Employee Type :</th>
                                <td>
                                    <?php $query27 = $this->db->query("SELECT id,emp_type FROM emp_type"); ?>
                                    <select name="emp_type" id="emp_type" style="width:130px">
                                        <option value="">Select</option>
                                        <?php foreach ($query27->result() as $d_info): ?>
                                            <option value="<?php echo $d_info->id; ?>" <?php if($this->input->post('emp_type')==$d_info->id){echo 'selected';} ?> ><?php echo $d_info->emp_type; ?></option>
                                        <?php endforeach; ?>
                                    </select>
                                    <span class="notification-input ni-error"> <?php echo ucwords(form_error('emp_type')); ?></span>
                                </td>
                            </tr>
                            <tr>
                                <th>Emp Designation :</th>
                                <td>
                                    <div id='ds'></div>
                                    <span class="notification-input ni-error"> <?php echo ucwords(form_error('designation')); ?></span>
                                </td>
                            </tr>

                            <tr>
                                <th>Duty Schedule:</th>
                                <td>
                                    <?php $scheduleInfo = $this->db->query("SELECT * FROM schedule"); ?>

                                    <select name="duty_schedule" id="duty_schedule" style="width:130px">

                                        <option value="">Select</option>

                                        <?php foreach ($scheduleInfo->result() as $schedule_info): ?>

                                            <option value="<?php echo $schedule_info->id;  ?>" <?php if($this->input->post('duty_schedule')==$schedule_info->id){echo 'selected';} ?>><?php echo $schedule_info->schedule_title; ?></option>

                                        <?php endforeach; ?>

                                    </select>

                                    <span class="notification-input ni-error"> <?php echo ucwords(form_error('duty_schedule')); ?></span>

                                </td>

                            </tr>



                            <table class="table table-bordered">
                                <thead><tr><th colspan="2" style=" background-color:#3D9EE2; color:white; font-weight:bold"><b>Employee Login Info</b></th></tr></thead>
                                <tr>
                                    <th colspan="2" style="font-size:10px; color: #993366; font-weight:bold; text-align:center">Please select supervisor name and designation</th>
                                </tr>

                                <tr>
                                    <th>Supervisor Deg</th>
                                    <td>
                                        <?php $degInfo = $this->db->query("SELECT id,designation FROM designation"); ?>
                                        <select name="sup_designation" id="empDesignation" style="width:130px" onchange="javascript:getSupervisor(this.value)">
                                            <option value="">Select</option>
                                            <?php foreach ($degInfo->result() as $deg): ?>
                                                <option value="<?php echo $deg->id; ?>" <?php if($this->input->post('sup_designation')==$deg->id){echo 'selected';} ?> ><?php echo $deg->designation; ?></option>
                                            <?php endforeach; ?>
                                        </select>
                                        <span class="notification-input ni-error"><?php echo ucwords(form_error('empDesignation')); ?></span>
                                    </td>
                                </tr>

                                <tr>
                                    <th>Supervisor Name: </th>
                                    <td>
                                        <div id="resultSupervisor"></div>
                                        <span class="notification-input ni-error"><?php echo ucwords(form_error('supervisor')); ?></span>
                                    </td>
                                </tr>



                                <tr>

                                    <th colspan="2" style="font-size:10px; color: #993366; font-weight:bold; text-align:center">Please Enter Employee username and pass for login </th>

                                </tr>



                                <tr>
                                    <th>Username :</th>
                                    <td>
                                        <?php echo form_input('username', $this->input->post('username'), 'class="input-short"'); ?><span style="color:red;">*</span>
                                        <span class="notification-input ni-error"> <?php echo ucwords(form_error('username')); ?></span>
                                    </td>
                                </tr>

                                <tr>
                                    <th>Password :</th>
                                    <td>
                                        <?php echo form_input('password', $this->input->post('password'), 'class="input-short"'); ?><span style="color:red;">*</span>
                                        <span class="notification-input ni-error"> <?php echo ucwords(form_error('password')); ?></span>
                                    </td>
                                </tr>



                                <?php if ($this->session->userdata("finance_prev") == '1'): ?>



                                <tr><th colspan="2" style=" background-color:#3D9EE2; color:white; font-weight:bold"><center><b>Employee PF Info</center></b></th></tr>

                                <tr><th>Provident Fund:</th><td><?php echo form_dropdown('provident_fund', $pf, $this->input->post('provident_fund'), 'style=width:130px') ?></td></tr>

                                <tr><th>Prev Balance(off) :</th><td><?php echo form_input('employeer_pf_balance',  $this->input->post('employeer_pf_balance'), 'class="input-short"'); ?>

                                        <span class="notification-input ni-error"> <?php echo ucwords(form_error('employeer_pf_balance')); ?></span></td></tr>

                                <tr>

                                    <th>Prev Balance(emp) :</th>
                                    <td><?php echo form_input('employee_pf_balance',  $this->input->post('employee_pf_balance'), 'class="input-short"'); ?>

                                        <span class="notification-input ni-error"> <?php echo ucwords(form_error('employee_pf_balance')); ?></span>
                                    </td>

                                </tr>

                                <tr>

                                    <th>Timeframe :</th>
                                    <td><?php echo form_input('timeframe',  $this->input->post('timeframe'), 'class="input-short"'); ?>

                                        <span class="notification-input ni-error"> <?php echo ucwords(form_error('timeframe')); ?></span>
                                    </td>

                                </tr>



                                <tr><th colspan="2" style=" background-color:#3D9EE2; color:white; font-weight:bold"><center><b>Employee payment mode</center></b></th></tr>

                                <tr><th>Mobile Bill :</th><td><?php echo form_input('mobile_bill',  $this->input->post('mobile_bill'), 'class="input-short"'); ?>

                                        <span class="notification-input ni-error"> <?php echo ucwords(form_error('mobile_bill')); ?></span>
                                    </td>
                                </tr>

                                <tr><th>Tax Deduction:</th><td><?php echo form_dropdown('tax_deduction', $tax, $this->input->post('tax_deduction'), 'style=width:130px') ?></td></tr>

                                <tr><th>Tax Amount :</th><td><?php echo form_input('tax_percentage',  $this->input->post('tax_percentage'), 'class="input-short"'); ?>

                                        <span class="notification-input ni-error"> <?php echo ucwords(form_error('tax_percentage')); ?></span>
                                    </td>
                                </tr>

                                <tr>
                                    <th>Payment Mode:</th>
                                    <td>
                                        Cash<input type="checkbox" name="cash_payment" value=1 id="cash_payment" onClick="return check_value();" <?php if($this->input->post('cash_payment')==1){echo "checked";}?>>

                                        Bank<input type="checkbox" name="bank_payment" value=2 id="bank_payment" onClick="return check();"  <?php if($this->input->post('bank_payment')==2){echo "checked";}?> />
                                    </td>
                                </tr>
                                <?php

                                $query00 = $this->db->query("select id,bank_name from bank");

                                ?>

                                <tr>
                                    <th>Bank Name: </th>
                                    <td>
                                        <select name="bank_name" id="bank_name" style="display: none; width:130px;">
                                            <option value="select">select</option>
                                            <?php foreach ($query00->result() as $info2): ?>
                                                <option value="<?php echo $info2->id; ?>" <?php if($this->input->post("bank_name")==$info2->id){echo 'selected';} ?>><?php echo $info2->bank_name; ?></option>
                                            <?php endforeach; ?>
                                        </select>
                                    </td>

                                </tr>

                                <tr><th>Branch Name: </th><td> <div id="result99"></div></td></tr>

                                <tr><th>Account Number: </th><td>  <input type="text" name="account_number" id="account_number" style="display: none;" value="<?php echo $this->input->post('account_number'); ?>"></td></tr>
                                <script>
                                    function calculateAuto($totalAmount) {

                                        var basic = $totalAmount;

                                        if (basic != "") {

                                            var houseRent_amount = parseFloat(Math.round(basic * 60) / 100).toFixed(2);

                                            var medical_amount = parseFloat(Math.round(basic * 20) / 100).toFixed(2);

                                            var conveyance_amount = parseFloat(Math.round(basic * 10) / 100).toFixed(2);

                                            var special_amount = parseFloat(Math.round(basic * 10) / 100).toFixed(2);





                                            var houseRent = document.getElementById("house_rent");

                                            var medical = document.getElementById("medical");

                                            var conveyance = document.getElementById("conveyance");

                                            var special = document.getElementById("special");



                                            houseRent.value = houseRent_amount;

                                            medical.value = medical_amount;

                                            conveyance.value = conveyance_amount;

                                            special.value = special_amount;

                                        }

                                    }

                                </script>

                                <tr>
                                    <th>Custom Salary Package: </th>
                                    <td> <div style="float:left; width:230px;">
                                            <input type="checkbox" name="package" value="1" id="package" onClick="check_package()" <?php if($this->input->post('package')){ echo 'checked';}?> >
                                    </td>
                                </tr>

                            </table>



                            <div id="package1" style="display:none">

                                <table>
                                    <tr>
                                        <th style="width:147px">Basic </th>
                                        <td><input type="text" name="basic" onblur="calculateAuto(this.value)" value="<?php echo $this->input->post('basic'); ?>" /></td>
                                    </tr>

                                    <tr>
                                        <th style="width:147px">House Rent </th>
                                        <td><input type="text" name="house_rent" id="house_rent" readonly="readonly"  value="<?php echo $this->input->post('house_rent'); ?>" /></td>
                                    </tr>

                                    <tr>

                                        <th style="width:147px">Medical</th>



                                        <td><input type="text" name="medical" id="medical" readonly="readonly"  value="<?php echo $this->input->post('medical'); ?>" /></td>

                                    </tr>

                                    <tr>

                                        <th style="width:147px">Transport</th>



                                        <td><input type="text" name="conveyance" id="conveyance" readonly="readonly"  value="<?php echo $this->input->post('conveyance'); ?>" /></td>

                                    </tr>

                                    <tr>
                                        <th style="width:147px">Special</th>
                                        <td><input type="text" name="special" id="special" readonly="readonly" value="<?php echo $this->input->post('special'); ?>" /></td>
                                    </tr>

                                    <tr>
                                        <th style="width:147px">Salary Step</th>
                                        <td><input type="text" name="salary_step" id="salary_step"  value="<?php echo $this->input->post('salary_step'); ?>" /></td>
                                    </tr>

                                    <tr>
                                        <th style="width:147px">Salary Band</th>
                                        <td><input type="text" name="salary_band" id="salary_band"  value="<?php echo $this->input->post('salary_band'); ?>" /></td>
                                    </tr>

                                </table>

                                <?php endif; ?>

                            </div>

                        </table>
                    </div>
                </div>
                <span>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
                    <input type="hidden" name="id" value="" />&nbsp;&nbsp;&nbsp;&nbsp;
                    <input type="submit" name="mysubmit" value="Submit"  class="btn btn-primary">
                </span>
            </form>
            <p></p>
        </div>
    </div><!-- col-lg-12-->
</div><!-- /row -->