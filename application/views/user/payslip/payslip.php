<?php

    /*    echo '<pre>';
    var_dump($query);
    echo '<pre>';
    exit;*/
    foreach ($montyly_salary_info as $row):

    $months = array(
        '01' => 'January',
        '02' => 'February',
        '03' => 'March',
        '04' => 'April',
        '05' => 'May',
        '06' => 'June',
        '07' => 'July',
        '08' => 'August',
        '09' => 'September',
        '10' => 'October',
        '11' => 'November',
        '12' => 'December',
    );

    $monTh = "";
    foreach ($months as $key => $val) {
        if ($key == $row->month) {
            $monTh = $val;
        }
    }

    $payment_mode = "";
    $bank_name = "";
    $bank_branch = "";
    $account_number = "";
    //$empInfo = $this->db->query("SELECT * FROM employeeinfo where id='" . $row->emp_id . "'");
    $emp_info = $this->payslip_model->get_emp_info($row->emp_id);

    foreach ($emp_info as $temp):
        $payment_mode = $temp->payment_mode;
        $bank_name = $temp->bank_name;
        $bank_branch = $temp->bank_branch;
        $account_number = $temp->account_number;
    endforeach;


    $degName = "";
    //$query66 = $this->db->query("SELECT id,designation FROM designation where id='" . $row->designation . "'");
    $emp_desig_info = $this->payslip_model->get_emp_desig($row->designation);
    /*echo '<pre>';
    var_dump($emp_desig);
    echo '</pre>';
    exit;*/
    foreach ($emp_desig_info as $row33) {
        $degName = ucfirst($row33->designation);
    }

    $brName = "";
    //$query11 = $this->db->query("SELECT id,branch_name FROM add_branch where id='" . $row->branch_name . "'");
    $emp_branch_info = $this->payslip_model->get_emp_branch($row->branch_name);
    foreach ($emp_branch_info as $row3) {
        $brName = ucfirst($row3->branch_name);
    }

    ?>
<h3>View Pay Slip</h3>
<div class="row">
    <div class="col-sm-12">
        <div class="content-panel">
            <!--<hr>-->
            <div class="table-responsive">
                <table class="table table-bordered table-hover">
                    <tr>
                        <td colspan="2"><img class="pull-right" src="<?php echo base_url();?>assets/img/logo1.png" height="auto" width="200px" style="padding:10px 0px 10px 0px"/></td>

                    </tr>
                    <tr>
                        <th colspan="2" class="text-center">Pay slip</th>

                    </tr>
                    <tr>
                        <th colspan="2" class="text-center">Private & Confidential</th>

                    </tr>
                    <tr>
                        <th>PAYSLIP FOR THE MONTH OF</th>
                        <th><?php echo $monTh . "'" . $row->year; ?></th>
                    </tr>
                    <tr>
                        <th>Name of Employee</th>
                        <th><?php echo ucwords($row->name); ?></th>
                    </tr>
                    <tr>
                        <th>Designation</th>
                        <th><?php echo $degName; ?></th>
                    </tr>

                    <tr>
                        <th>Gross Salary</th>
                        <th>BDT <?php echo $row->gross_salary; ?></th>
                    </tr>
                    <tr>
                        <th>Joining Date</th>
                        <th><?php echo date('M d,Y', strtotime($row->join_date)); ?></th>
                    </tr>
                    <?php if ($payment_mode == '2'):

                        $bankName = "";
                        //$query10 = $this->db->query("select bank_name from bank where id='" . $bank_name . "'");
                        $bank_info = $this->payslip_model->get_bank_info($bank_name);
                        /*echo '<pre>';
                        var_dump($emp_branch_info);
                        echo '</pre>';
                        exit;*/
                        foreach ($bank_info as $temp2):
                            $bankName = $temp2->bank_name;
                        endforeach;

                        ?>
                        <tr>
                            <th>Bank Name</th>
                            <th><?php echo ucfirst($bankName); ?></th>
                        </tr>
                        <tr>
                            <th>Account No</th>
                            <th><?php echo $account_number;?></th>
                        </tr>
                    <?php endif; ?>

                </table>
                <table class="table table-bordered table-hover">
                    <thead>
                    <tr class="tbl_header_bg_color">
                        <th>Payment Heads</th>
                        <th>Amount</th>
                        <th>Payment Heads</th>
                        <th>Amount</th>
                    </tr>
                    </thead>
                    <tbody>
                    <?php

                    /*$start  = ($this->input->get('start')) ? $this->input->get('start') : 0;

                    if($start){
                        $i = $start+1;
                    }else{
                        $i = '1';
                    }

                    foreach ($holiday_info as $temp) {*/
                        ?>
                        <tr>
                            <td>Basic Salary</td>
                            <td>BDT <?php echo $row->basic; ?></td>
                            <td>PF Contribution (Office+Employee)</td>
                            <td>BDT <?php echo $row->total_pf; ?></td>
                        </tr>

                        <tr>
                            <td>House Rent Allowance (60%)</td>
                            <td>BDT <?php echo $row->house_rent; ?></td>
                            <td>Adjustment ( Absent )</td>
                            <td>BDT <?php echo $row->absent_deduction; ?></td>
                        </tr>

                        <tr>
                            <td>Medical Allowance (20%)</td>
                            <td>BDT <?php echo $row->medical; ?></td>
                            <td>Advance Adjustment (I.O.U)</td>
                            <td>BDT <?php echo $row->advance; ?></td>
                        </tr>

                        <tr>
                            <td>Conveyance Allowance (10%)</td>
                            <td>BDT <?php echo $row->conveyance; ?></td>
                            <td>Loan Adjustment (negative)</td>
                            <td>BDT <?php echo $row->loan; ?></td>
                        </tr>

                        <tr>
                            <td>Special Allowance (10%)</td>
                            <td>BDT <?php echo $row->special; ?></td>
                            <td>Tax Deducted at Source</td>
                            <td>BDT <?php echo $row->tax; ?></td>
                        </tr>

                        <tr>
                            <td>PF Contribution (Windmill-10%)</td>
                            <td>BDT <?php echo $row->pf_employeer; ?></td>
                            <td>KPI</td>
                            <td>BDT <?php echo $row->kpi; ?></td>
                        </tr>



                        <tr>
                            <td>Arrear</td>
                            <td>BDT <?php echo $row->arrear; ?></td>
                            <td></td>
                            <td></td>
                        </tr>

                        <tr>
                            <td>Mobile Allowance ( Fixed )</td>
                            <td>BDT <?php echo $row->mobile_bill; ?></td>
                            <td></td>
                            <td></td>
                        </tr>
                        <tr>
                            <td>Half Yearly Bonus</td>
                            <td>BDT <?php echo $row->bonus; ?></td>
                            <td></td>
                            <td></td>
                        </tr>
                        <tr>
                            <td>KPI/Performance</td>
                            <td>BDT <?php echo $row->kpi_add; ?></td>
                            <td></td>
                            <td></td>
                        </tr>
                        <tr>
                            <td>OT</td>
                            <td>BDT <?php echo $row->ot; ?></td>
                            <td></td>
                            <td></td>
                        </tr>
                        <!-- <tr>
                            <td><p><br /></td>
                            <td><p><br /></td>
                            <td><p><br /></td>
                            <td><p><br /></td>
                        </tr> -->

                        <?php
                   /* }*/
                    ?>
                    </tbody>
                </table>
                <table class="table table-bordered">
                    <tr>
                        <td class="text-center"><b>Total Gross Pay</b></td>
                        <td>BDT <?php echo $row->gross_payable ?></td>
                        <td><b>Total Deduction</b></td>
                        <td>BDT <?php echo $row->total_deduction_amount; ?></td>
                    </tr>
                    <tr>
                        <td colspan="2" class="text-center"> <b>Net Amount Transferred By
                            <?php if ($payment_mode == '2'):echo "Bank";else:echo "Cash";endif;?> </b></td>
                        <td colspan="2" class="text-center">BDT <?php echo $row->net_salary; ?></td>
                    </tr>
                    <!--<tr>
                        <td colspan="4" class="td_color">
                            Note 1 : For Any clarification please contact sharif@windmill.com.bd<br />
                            Note 2 :  This is a system generated report and requires no manual signature.
                        </td>
                    </tr>-->
                </table>
                <?php endforeach; ?>
            </div>
        </div><! --/content-panel -->
    </div><!-- /col-md-12 -->
</div><!-- row -->
