

<h3>Change Password</h3>

<div class="row">
    <div class="col-sm-12">
        <div class="content-panel">
            <?php
            echo form_open('user/leave/changed_password');?>
            <?php
            if($this->session->flashdata('message')==NULL){

            }else{ ?>
                <div class="alert-success alert">
                    <?php echo $this->session->flashdata('message') ?>
                </div> <?php
            }
            ?>
            <div class="col-md-12">
                <div class="row">
                    <div class="form-group col-sm-6 <?php if(ucwords(form_error('old_password'))){echo "has-error";}?>">
                        <label for="old_password">Old Password:</label>
                        <input type="password" name="old_password" value="<?php //if($start_date){echo $start_date;}?>" id="old_password" class="form-control">
                        <span class="alert-danger"><?php echo ucwords(form_error('old_password')); ?></span>
                    </div>

                    <div class="form-group col-sm-6 <?php if(ucwords(form_error('new_password'))){echo "has-error";}?>">
                        <label for="new_password">New Password:</label>
                        <input type="password" name="new_password" value="<?php //if($end_date){echo $end_date;}?>" id="new_password" class="form-control">
                        <span class="alert-danger"><?php echo ucwords(form_error('new_password')); ?></span>
                    </div>

                    <div class="form-group col-sm-6 <?php if(ucwords(form_error('confirm_password'))){echo "has-error";}?>">
                        <label for="confirm_password">Confirm Password:</label>
                        <input type="password" name="confirm_password" value="<?php //if($end_date){echo $end_date;}?>" id="confirm_password" class="form-control">
                        <span class="alert-danger"><?php echo ucwords(form_error('confirm_password')); ?></span>
                    </div>

                </div>
            </div>

                <span>&nbsp;&nbsp;&nbsp;&nbsp;
                    <input type="submit" name="mysubmit" value="Submit"  class="btn btn-primary">
                    <a class="btn btn-theme" href="<?php echo base_url(); ?>user/home">Cancel</a>
                </span>
            </form>
            <p></p>
        </div>
    </div><!-- col-lg-12-->
</div><!-- /row -->