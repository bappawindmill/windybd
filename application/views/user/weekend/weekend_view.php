<link href='<?php echo base_url(); ?>assets/fullcalendar/fullcalendar.min.css' rel='stylesheet' />
<link href='<?php echo base_url(); ?>assets/fullcalendar/fullcalendar.print.min.css' rel='stylesheet' media='print' />


<script src='<?php echo base_url(); ?>assets/fullcalendar/lib/moment.min.js'></script>
<script src='<?php echo base_url(); ?>assets/fullcalendar/fullcalendar.min.js'></script>

<style>

    #calendar {
        max-width: 900px;
        margin: 0 auto;
        color:black;
    }
    .event_delete
    {
        /* border: 1px solid #ff184b;
         background: #ff1016;*/


    }
</style>
<h3><i class="fa fa-angle-right"></i> View Weekend Calendar</h3>
<div class="row">
    <div class="col-sm-12">
        <div class="content-panel">
            <!--<section id="flip-scroll">-->
            <section id="">
                <div id='calendar'></div>
            </section>
        </div><! --/content-panel -->
    </div><!-- /col-md-12 -->
</div>
<?php
foreach($weekend_info as $row){
    ucfirst($row->branch_name) ;
    $row->weekend_date ;
}
?>

<script>
    $(document).ready(function() {
        $('#calendar').fullCalendar({
            header: {
                left: 'prev,next today',
                center: 'title',
                right: 'month,agendaWeek,agendaDay'
            },
            defaultDate: '<?php echo date("Y-m-d") ?>',
            navLinks: true, // can click day/week names to navigate views
            selectable: true,
            selectHelper: true,

            editable: true,
            eventLimit: true, // allow "more" link when too many events
            events: [

                <?php foreach($weekend_info as $key => $row):?>
                {
                    id: "<?php echo $row->id; ?>",
                    title: "<?php echo ucfirst('Weekend'); ?>",
                    start: "<?php echo ucfirst($row->weekend_date); ?>",
                    editable:true

                }<?php if(key_exists(($key+1),$weekend_info)):?>,<?php endif;?>
                <?php endforeach;?>
            ]
        });

    });

</script>