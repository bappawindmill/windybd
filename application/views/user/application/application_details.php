<script type="text/javascript" src="<?php echo base_url(); ?>/public/js/jquery.jqprint.0.3.js"></script>
<script>
    $(function() {
        $("#print_button").click(function() {
            $("#invoice").jqprint();
        });
    });
</script>
<div class="module">
    <div style="width:800px;margin:0 auto" id="invoice">
        <div style="width:800px; height:60px">
            <div style="width:800px; text-align: center; font-size: 18px; font-weight: bold; color:blue">Leave Application Details</div>

        </div>
    </div>
    <div class="module-table-body">
        <style>
            table tr{ border:1px solid #EEEEEE;}
            table tr th{font-size: 11px; font-weight: bold; text-align: center}
            table tr td{font-size: 11px;text-align: center }
        </style>
        <?php
        $sup = array(
            '' => 'Select',
            '1' => 'Approve',
            '2' => 'Deny'
        );
        $sup1 = array(
            '1' => ''
        );
        $hr = array(
            '' => 'Select',
            '1' => 'Approve',
            '2' => 'Deny'
        );
        ?>
        <table border="1" bgcolor="" cellpadding="0" cellspacing="0" align="center">
            <thead>
                <tr class="tbl_header_bg_color">
                    <th width="5%" style="text-align:center">Date</th>                       
                    <th width="5%" style="text-align:center">Entry time</th>                                     
                    <th width="5%" style="text-align:center">Exit time</th> 
                    <th width="5%" style="text-align:center">Status</th>                  
                    <th width="5%" style="text-align:center">EmpRemarks</th>
                    <th width="5%" style="text-align:center">SupRemarks</th>
                    <th width="5%" style="text-align:center">SupStatus</th>
                    <th width="5%" style="text-align:center">Hr Remarks</th>

                </tr>
            </thead>
            <tbody>
                <?php
                echo form_open("user/attendence/update");
                $c = 1;
                if ($month == '01') {
                    $total_month_day = 31;
                } elseif ($month == '02') {
                    $total_month_day = 28;
                } elseif ($month == '03') {
                    $total_month_day = 31;
                } elseif ($month == '04') {
                    $total_month_day = 30;
                } elseif ($month == "05") {
                    $total_month_day = 31;
                } elseif ($month == "06") {
                    $total_month_day = 30;
                } elseif ($month == "07") {
                    $total_month_day = 31;
                } elseif ($month == "08") {
                    $total_month_day = 31;
                } elseif ($month == "09") {
                    $total_month_day = 30;
                } elseif ($month == "10") {
                    $total_month_day = 31;
                } elseif ($month == "11") {
                    $total_month_day = 30;
                } elseif ($month == "12") {
                    $total_month_day = 31;
                } else {
                    $total_month_day = 30;
                }
                $branch = "";
                $duty_schedule = 0;
               // $empId = $this->session->userdata("employeeId");
                $query9 = $this->attendence_model->getEmployeeInfo($empId);
                foreach ($query9 as $row):
                    $branch = $row->branch_name;
                    $duty_schedule = $row->duty_schedule;
                endforeach;
                $total_start_time = "";
                $total_end_time = "";
                $total_late_time = 0;

                $scheduleInfo = $this->attendence_model->getSchedule($duty_schedule);
                if (count($scheduleInfo)):
                    foreach ($scheduleInfo as $sInfo):
                        $start_time = $sInfo->start_time;
                        $end_time = $sInfo->end_time;
                        $late_time = $sInfo->late_time;
                    endforeach;
                    //convert start time to minute
                    $explode_start_time = explode(":", $start_time);
                    $start_hour = $explode_start_time[0];
                    $start_hour_to_minute = $start_hour * 60;
                    $start_minute = $explode_start_time[1];
                    $total_start_time = $start_hour_to_minute + $start_minute;
                    //convert end time to minute
                    $explode_end_time = explode(":", $end_time);
                    $end_hour = $explode_end_time[0];
                    $end_hour_to_minute = $end_hour * 60;
                    $end_minute = $explode_end_time[1];
                    $total_end_time = $end_hour_to_minute + $end_minute;
                    //convert Late time to minute
                    $explode_late_time = explode(":", $late_time);
                    $late_hour = $explode_late_time[0];
                    $late_hour_to_minute = $late_hour * 60;
                    $late_minute = $explode_late_time[1];
                    $total_late_time = $late_hour_to_minute + $late_minute;

                endif;

                $leave = 0;
                $holiday = 0;
                $weekend = 0;
                $absent = 0;
                $present = 0;
                $late = 0;

//start for loop
                for ($i = 1; $i <= $total_month_day; $i++) {
                    if ($i <= 9) {
                        $day = "0" . $i;
                    } else {
                        $day = $i;
                    }
                    $sdate = $year . "-" . $month . "-" . $day . "";

                    $empId = $empId;
                   // $total_late_time = "09:10";
                    //Retrive In time data
                    $this->load->model("user/attendence_model");
                    $timeInInfo = $this->attendence_model->getTimeLogInfo($empId, $sdate);
                    //echo "<pre>";print_r($timeInInfo);

                    $att_time_in = "";
                    $att_time_out = "";
                    $totalInTime = "";
                    $emp_comment = "";
                    $sup_comment = "";
                    $status = "";
                    $hr_comment = "";
                    $a = "";
                    foreach ($timeInInfo as $row3) {
                        $att_time_in = $row3->timein;
                        $att_time_out = $row3->timeout;
                        $emp_comment = $row3->emp_comment;
                        $sup_comment = $row3->sup_comment;
                        $hr_comment = $row3->hr_comment;
                    }

                    if ($att_time_in != "") {
                        $explodeTimeIn = explode(":", $att_time_in);
                        $InHour = $explodeTimeIn[0];
                        $InHourToMinute = $InHour * 60;
                        $InHourminute = $explodeTimeIn[1];
                        $totalInTime = $InHourToMinute + $InHourminute;
                    }
                    //check weekend,holiday and leave

                    $holidayInfo = $this->attendence_model->getHoliday($sdate);
                    if (count($holidayInfo)) {
                        $holiday = $holiday + 1;
                        $status = "<p class=\"text-success\">Holiday</p>";
                        $status1 = "holiday";
                        $a = 1;
                    } else {
                        $weekendInfo = $this->attendence_model->getWeekend($sdate, $branch);
                        if (count($weekendInfo)) {
                            $weekend = $weekend + 1;
                            $status = "<p class=\"text-success\">Weekend</p>";
                            $status1 = "weekend";
                            $a = 1;
                        } else {
                            $leaveInfo = $this->attendence_model->getLeaveInfo($sdate, $empId);
                            if (count($leaveInfo)) {
                                $leave = $leave + 1;
                                $status = "<p class=\"text-success\">Leave</p>";
                                $status1 = "leave";
                                $a = 1;
                            } else {
                                if ($att_time_in == "" && $att_time_out == "") {
                                    $absent = $absent + 1;
                                    $status = "<p class=\"text-danger\">Absent</p>";
                                    $status1 = "absent";
                                    $a = "";
                                } else {
                                    $present = $present + 1;
                                    $status = "<p class=\"text-info\">Present</p>";
                                    $status1 = "present";
                                    $a = 1;
                                    if ($total_late_time != 0 && $totalInTime != ""):
                                        if ($totalInTime > $total_late_time) {
                                            $late = $late + 1;
                                            // $late_duration = $timein - $total_late_time;
                                            //$total_late_minute = $total_late_minute + $late_duration;

                                            $status = "<p class=\"text-info\">Late</p>";
                                            $status1 = "late";
                                            $a = "";
                                        }
                                    endif;
                                }
                            }
                        }
                    }
                    //end weelend,holiday,leave check 
                    ?>

                    <tr class=" <?php echo $i ?>" style="width:90%">
                        <td><input type="text" readonly="readonly" name="att_date[]" style="width:100px" value="<?php echo $sdate; ?>" /></td>
                        <td><input type="text" name="timein[]" readonly="readonly"  style="width:100px"value="<?php echo $att_time_in; ?>" /></td>  
                        <td><input type="text" name="timeout[]" readonly="readonly" style="width:100px" value="<?php echo $att_time_out; ?>" /></td>  
                        <td>
                            <input type="hidden" readonly="readonly" name="status[]" style="width:100px" value="<?php echo $status1; ?>" />
                            <?php echo $status; ?></td> 


                        <td style="width:250px">
                            <input type="text" readonly="readonly" name="emp_comment[]" value="<?php echo $emp_comment; ?>" style="width:180px" />
                        </td>
                        <td style="width:250px">
                            <input type="text"  name="sup_comment[]" value="<?php echo $sup_comment; ?>" style="width:180px" />
                        </td>
                        <td>

                            <?php
                            if ($a == "1"):
                                echo form_dropdown('sup_status[]', $sup1, $a, "style='width:80px'");
                            else:
                                echo form_dropdown('sup_status[]', $sup, $a, "style='color:red;'");
                            endif;
                            ?>

                        </td> 

                        <td style="width:250px; ">
                            <input type="text" readonly="readonly" name="hr_comment[]" value="<?php echo $hr_comment; ?>" style="width:180px" />
                        </td>


                    </tr>
                    <?php
                }
//end forloop
                ?>
                <tr>
                    <td style="width:250px">
                        <input type="hidden" name="emp_id" value="<?php echo $empId; ?>" style="width:180px" />
                        <input type="hidden" name="year" value="<?php echo $year; ?>" style="width:180px" />
                        <input type="hidden" name="month" value="<?php echo $month; ?>" style="width:180px" />
                        <input type="submit" value="Submit">
                        </form>
                    </td>    
                </tr>   
            </tbody>
        </table>

        <div class="pager" id="pager"></div>
        <div class="table-apply">
        </div>
    </div>

</div>
</div>
<div style="clear: both;"></div>


