<script type="text/javascript" src="<?php echo base_url(); ?>/public/js/jquery.jqprint.0.3.js"></script>
<script>
    $(function(){
        $("#print_button").click( function() {
            $("#invoice").jqprint();
        });
    });
</script>
<div class="module">
    <div style="width:800px;margin:0 auto" id="invoice">
        <div style="width:800px; height:60px">
            <div style="width:800px; text-align: center; font-size: 18px; font-weight: bold; color:blue">Windmill Group</div>

        </div>
    </div>
    <div class="module-table-body">
        <style>
            table tr{ border:1px solid #EEEEEE;}
            table tr th{font-size: 11px; font-weight: bold; text-align: center}
            table tr td{font-size: 11px;text-align: center }
        </style>
        <table border="1" cellpadding="0" cellspacing="0" align="center">
            <thead>
                <tr>
                    <th style="text-align:center">Date</th>                       
                    <th style="text-align:center">Entry time</th>                                     
                    <th style="text-align:center">Exit time</th> 
					 <th style="text-align:center">Status</th>                  
                    <th style="text-align:center">Remarks</th>
                </tr>
            </thead>
            <tbody>
                <?php
                $c = 1;
                $a = "";

                if ($month == '01') {
                    $total_month_day = 31;
                } elseif ($month == '02') {
                    $total_month_day = 28;
                } elseif ($month == '03') {
                    $total_month_day = 31;
                } elseif ($month == '04') {
                    $total_month_day = 30;
                } elseif ($month == "05") {
                    $total_month_day = 31;
                } elseif ($month == "06") {
                    $total_month_day = 30;
                } elseif ($month == "07") {
                    $total_month_day = 31;
                } elseif ($month == "08") {
                    $total_month_day = 31;
                } elseif ($month == "09") {
                    $total_month_day = 30;
                } elseif ($month == "10") {
                    $total_month_day = 31;
                } elseif ($month == "11") {
                    $total_month_day = 30;
                } elseif ($month == "12") {
                    $total_month_day = 31;
                } else {
                    $total_month_day = 30;
                }
                //start for loop
                for ($i = 1; $i <= $total_month_day; $i++) {
                    if ($i <= 9) {
                        $day = "0" . $i;
                    } else {
                        $day = $i;
                    }
                    $sdate = $year . "-" . $month . "-" . $day . "";


                    $query88 = $this->db->query("select * from attendence where card_no='" .$this->session->userdata("card_no") . "'and att_date='" . $sdate . "'");
                    foreach ($query88->result() as $row3) {

                        $att_time_in = $row3->start_time;
                        $att_time_out = $row3->end_time;
                       
                    }
                    if (!isset($att_time_in)):$att_time_in = "00:00";
                    endif;
                    if (!isset($att_time_out)):$att_time_out = "00:00";
                    endif;
                 
                    echo "<tr class = " . $i . ">";

                    echo "<td>" . $sdate . "</td>";
                    echo "<td>" . $att_time_in . "</td>";                   
                    echo "<td>" . $att_time_out . "</td>";                 
                    echo "<td>Absent</td>";     
                    ?>
                <td style="width:250px">
                  <input type="text" name="" style="width:180px" />
                </td>
				</tr>
                <?php
            }
            //end forloop
            ?>
            </tbody>
        </table>
        <div class="pager" id="pager"></div>
        <div class="table-apply">
        </div>
    </div>
    
</div>
</div>
<div style="clear: both;"></div>


