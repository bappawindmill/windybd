<script type="text/javascript" src="<?php echo base_url(); ?>/public/js/jquery.jqprint.0.3.js"></script>
<script>
    $(function() {
        $("#print_button").click(function() {
            $("#invoice").jqprint();
        });
    });
</script>
<div class="module">
    <div style="width:800px;margin:0 auto" id="invoice">
        <div style="width:800px; height:60px">
            <div style="width:800px; text-align: center; font-size: 18px; font-weight: bold; color:blue">Windmill Group</div>

        </div>
    </div>
    <div class="module-table-body">
        <style>
            table tr{ border:1px solid #EEEEEE;}
            table tr th{font-size: 11px; font-weight: bold; text-align: center}
            table tr td{font-size: 11px;text-align: center }
        </style>
        <table border="1" b bgcolor="" cellpadding="0" cellspacing="0" align="center">
            <thead>
                <tr>
                    <th style="text-align:center">Date</th>                       
                    <th style="text-align:center">Entry time</th>                                     
                    <th style="text-align:center">Exit time</th> 
                    <th style="text-align:center">Status</th>                  
                    <th style="text-align:center">Remarks</th>
                </tr>
            </thead>
            <tbody>
                <?php
                echo form_open("user/attendence/viewLog");
                $c = 1;
                if ($month == '01') {
                    $total_month_day = 31;
                } elseif ($month == '02') {
                    $total_month_day = 28;
                } elseif ($month == '03') {
                    $total_month_day = 31;
                } elseif ($month == '04') {
                    $total_month_day = 30;
                } elseif ($month == "05") {
                    $total_month_day = 31;
                } elseif ($month == "06") {
                    $total_month_day = 30;
                } elseif ($month == "07") {
                    $total_month_day = 31;
                } elseif ($month == "08") {
                    $total_month_day = 31;
                } elseif ($month == "09") {
                    $total_month_day = 30;
                } elseif ($month == "10") {
                    $total_month_day = 31;
                } elseif ($month == "11") {
                    $total_month_day = 30;
                } elseif ($month == "12") {
                    $total_month_day = 31;
                } else {
                    $total_month_day = 30;
                }
                $total_start_time = 0;
                $total_end_time = 0;
                $total_late_time = 0;
                $empId = $this->session->userdata("employeeId");
                $duty_schedule=0;
                $empSchedule = $this->attendence_model->getEmpInfo($empId);
                foreach($empSchedule as $eInfo):
                   $duty_schedule = $eInfo->duty_schedule;
                endforeach;
               
                $scheduleInfo = $this->attendence_model->getSchedule($duty_schedule);
              
                if (count($scheduleInfo)):
                    foreach ($scheduleInfo as $sInfo):
                        $start_time = $sInfo->start_time;
                        $late_time = $sInfo->late_time;
                        $end_time = $sInfo->end_time;
                    endforeach;
                    //convert start time to minute
                    $explode_start_time = explode(":", $start_time);
                    $start_hour = $explode_start_time[0];
                    $start_hour_to_minute = $start_hour * 60;
                    $start_minute = $explode_start_time[1];
                    $total_start_time = $start_hour_to_minute + $start_minute;
                    //convert end time to minute
                    $explode_end_time = explode(":", $end_time);
                    $end_hour = $explode_end_time[0];
                    $end_hour_to_minute = $end_hour * 60;
                    $end_minute = $explode_end_time[1];
                    $total_end_time = $end_hour_to_minute + $end_minute;
                    //convert Late time to minute
                    $explode_late_time = explode(":", $late_time);
                    $late_hour = $explode_late_time[0];
                    $late_hour_to_minute = $late_hour * 60;
                    $late_minute = $explode_late_time[1];
                    $total_late_time = $late_hour_to_minute + $late_minute;
    
                endif;
              

                $leave = 0;
                $holiday = 0;
                $weekend = 0;
                $absent = 0;
                $present = 0;
                $late = 0;
                //start for loop
                for ($i = 1; $i <= $total_month_day; $i++) {
                    if ($i <= 9) {
                        $day = "0" . $i;
                    } else {
                        $day = $i;
                    }
                    $sdate = $year . "-" . $month . "-" . $day . "";
                    $cardNo = $this->session->userdata("card_no");
                    $branch = $this->session->userdata("bName");
                    $empId = $this->session->userdata("employeeId");
                   // $total_late_time = "09:10";
                    //Retrive In time data
                    $this->load->model("user/attendence_model");
                    $timeInInfo = $this->attendence_model->getTimeInLog($cardNo, $sdate);

                    /* $timeInInfo = $this->db->query("select top(1) * from attendence where card_no='" . $this->session->userdata("secreteNo") . "'and att_date='" . $sdate . "'order by id asc"); */
                    $att_time_in = "";
                    $att_time_out = "";
                    $totalInTime = "";
                    foreach ($timeInInfo as $row3) {
                        $att_time_in = $row3->att_time;
                    }

                    if ($att_time_in != "") {
                        $explodeTimeIn = explode(":", $att_time_in);
                        $InHour = $explodeTimeIn[0];
                        $InHourToMinute = $InHour * 60;
                        $InHourminute = $explodeTimeIn[1];
                        $totalInTime = $InHourToMinute + $InHourminute;
                    }

                    //Retrive Out time data
                    $timeOutInfo = $this->attendence_model->getTimeOutLog($cardNo, $sdate);
                 // echo "<pre>";print_r($timeOutInfo);
                    /* $timeOutInfo = $this->db->query("select top(1) * from attendence where card_no='" . $this->session->userdata("secreteNo") . "'and att_date='" . $sdate . "'order by id DESC"); */

                    foreach ($timeOutInfo as $row31) {
                        $att_time_out = $row31->att_time;
                    }
                    if ($att_time_in == $att_time_out) {
                        $att_time_out = "";
                    }

                    //check weekend,holiday and leave

                    $holidayInfo = $this->attendence_model->getHoliday($sdate);

                    // $holidayInfo = $this->db->query("select * from add_holiday where  branch_name='" . $temp->branch_name . "'and '" . $sdate . "' between start_date and end_date ");
                    //echo "<pre>";print_r($holiday->result());
                    if (count($holidayInfo)) {
                        $holiday = $holiday + 1;
                        $status = "<font style='color:#006633'>Holiday</font>";
                        $status1 = "holiday";
                    } else {

                        $weekendInfo = $this->attendence_model->getWeekend($sdate, $branch);
                        //  $weekendInfo = $this->db->query("select * from add_weekend where dept='" . $temp->dept . "'and branch_name='" . $temp->branch_name . "'and weekend='" . $weekDay . "'");
                        if (count($weekendInfo)) {
                            $weekend = $weekend + 1;
                            $status = "<font style='color:#006633'>Weekend</font>";
                            $status1 = "weekend";
                        } else {
                            $leaveInfo = $this->attendence_model->getLeaveInfo($sdate, $empId);
                            // $leave1 = $this->db->query("select * from leave_management where emp_id='" . $temp->emp_id . "'and '" . $sdate . "' between start_date and end_date ");
                            if (count($leaveInfo)) {
                                $leave = $leave + 1;
                                $status = "<font style='color:green'>leave</font>";
                                $status1 = "leave";
                            } else {
                                if ($att_time_in == "" && $att_time_out == "") {
                                    $absent = $absent + 1;
                                    $status = "<font style='color:red'>Absent</font>";
                                    $status1 = "absent";
                                } else {
                                    $present = $present + 1;
                                    $status = "<font style='color:#990033'>Present</font>";
                                    $status1 = "present";
                                     if ($total_late_time != 0 && $totalInTime != 0):
                                        if ($totalInTime > $total_late_time) {
                                           // echo "<br/>". $totalInTime."latetime=".$total_late_time;
                                            $late = $late + 1;
                                            // $late_duration = $timein - $total_late_time;
                                            //$total_late_minute = $total_late_minute + $late_duration;

                                            $status = "<font style='color:#990033'>late</font>";
                                            $status1 = "late";
                                        }
                                    endif;
                                }
                            }
                        }
                    }
                    //end weelend,holiday,leave check
                    $dateComment = "";
                    $loginfo = $this->db->query("select comment from DailyLog where emp_id ='" . $empId . "'and log_date ='" . $sdate . "'");
                    if (count($loginfo)):
                        foreach ($loginfo->result() as $c1Info):
                            $dateComment = $c1Info->comment;
                        endforeach;
                    endif;
                    ?>
                    <tr class=" <?php echo $i ?>">
                        <td><?php echo $sdate; ?><input type="hidden" name="att_date[]" value="<?php echo $sdate; ?>" /></td>
                        <td><?php echo $att_time_in; ?><input type="hidden" name="timein[]" value="<?php echo $att_time_in; ?>" /></td>  
                        <td><?php echo $att_time_out; ?><input type="hidden" name="timeout[]" value="<?php echo $att_time_out; ?>" /></td>  
                        <td><?php echo $status; ?><input type="hidden" name="status[]" value="<?php echo $status1;    ?>" /></td>     
                        <td style="width:250px">
                            <?php echo $dateComment; ?>
                            <input type="hidden" name="comment[]" style="width:180px" value="<?php echo $dateComment; ?>" />
                            <input type="hidden" name="year" value="<?php echo $year; ?>" style="width:180px" />
                            <input type="hidden" name="month" value="<?php echo $month; ?>" style="width:180px" />
                        </td>
                    </tr>
                    <?php
                }
//end forloop
                ?>
                <tr>
                    <td style="width:250px">
                        <?php
                        $s_status = "";
                        $query = $this->db->query("select * from submit_control");
                        foreach ($query->result() as $t):
                            $s_status = $t->status;
                        endforeach;
                        ?>
                        <input type="submit"  value="Submit"  <?php if ($s_status == '2'): ?> disabled="disabled" <?php endif; ?>>
                        </form>
                    </td>    
                </tr>
            </tbody>
        </table>

        <div class="pager" id="pager"></div>
        <div class="table-apply">
        </div>
    </div>

</div>
</div>
<div style="clear: both;"></div>


