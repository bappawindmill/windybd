<?php

?>

<h3>Viewed Attendance</h3>
<div class="row">
    <div class="col-sm-12">
        <div class="content-panel">
            <!--<hr>-->
            <div class="table-responsive">
                <table class="table table-bordered table-hover">
                    <thead>
                    <tr class="tbl_header_bg_color">
                        <th>Date</th>
                        <th>Day</th>
                        <th>Entry time</th>
                        <th>Exit time</th>
                        <th>Status</th>
                        <!--<th>Late Time</th>-->
                        <!--<th>OT Time</th>-->
                        <th>Remarks</th>
                    </tr>
                    </thead>
                    <tbody>
                    <?php
                    echo form_open("user/attendence/viewLog");
                    $c = 1;
                    if ($month == '01') {
                        $total_month_day = 31;
                    } elseif ($month == '02') {
                        $total_month_day = 28;
                    } elseif ($month == '03') {
                        $total_month_day = 31;
                    } elseif ($month == '04') {
                        $total_month_day = 30;
                    } elseif ($month == "05") {
                        $total_month_day = 31;
                    } elseif ($month == "06") {
                        $total_month_day = 30;
                    } elseif ($month == "07") {
                        $total_month_day = 31;
                    } elseif ($month == "08") {
                        $total_month_day = 31;
                    } elseif ($month == "09") {
                        $total_month_day = 30;
                    } elseif ($month == "10") {
                        $total_month_day = 31;
                    } elseif ($month == "11") {
                        $total_month_day = 30;
                    } elseif ($month == "12") {
                        $total_month_day = 31;
                    } else {
                        $total_month_day = 30;
                    }
                    $total_start_time = 0;
                    $total_end_time = 0;
                    $total_late_time = 0;
                    $empId = $this->session->userdata("employeeId");
                    $duty_schedule=0;
                    $empSchedule = $this->attendence_model->getEmpInfo($empId);
                    //echo '<pre>';
                    //var_dump($empSchedule);
                    //echo '<pre>';
                    //exit;
                    foreach($empSchedule as $eInfo):
                        $duty_schedule = $eInfo->duty_schedule;
                    endforeach;

                    $scheduleInfo = $this->attendence_model->getSchedule($duty_schedule);
                    if (count($scheduleInfo)){
                        foreach ($scheduleInfo as $sInfo){
                            $start_time = $sInfo->start_time;
                            $late_time = $sInfo->late_time;
                            $end_time = $sInfo->end_time;
                        }
                        //convert start time to minute
                        $explode_start_time = explode(":", $start_time);
                        $start_hour = $explode_start_time[0];
                        $start_hour_to_minute = $start_hour * 60;
                        $start_minute = $explode_start_time[1];
                        $total_start_time = $start_hour_to_minute + $start_minute;
                        //convert end time to minute
                        $explode_end_time = explode(":", $end_time);
                        $end_hour = $explode_end_time[0];
                        $end_hour_to_minute = $end_hour * 60;
                        $end_minute = $explode_end_time[1];
                        $total_end_time = $end_hour_to_minute + $end_minute;
                        //convert Late time to minute
                        $explode_late_time = explode(":", $late_time);
                        $late_hour = $explode_late_time[0];
                        $late_hour_to_minute = $late_hour * 60;
                        $late_minute = $explode_late_time[1];
                        $total_late_time = $late_hour_to_minute + $late_minute;
                    }

                    $leave = 0;
                    $holiday = 0;
                    $weekend = 0;
                    $absent = 0;
                    $present = 0;
                    $late = 0;
                    //start for loop
                    for ($i = 1; $i <= $total_month_day; $i++) {
                        if ($i <= 9) {
                            $day = "0" . $i;
                        } else {
                            $day = $i;
                        }
                        $sdate = $year . "-" . $month . "-" . $day . "";
                        $cardNo = $this->session->userdata("card_no");
                        $branch = $this->session->userdata("bName");
                        $empId = $this->session->userdata("employeeId");
                        // $total_late_time = "09:10";

                        //$dt = date('1-m-Y',strtotime($att_date));
                        $att_day = date('l',strtotime($sdate));

                        //Retrive In time data
                        $this->load->model("user/attendence_model");
                        $timeInInfo = $this->attendence_model->getTimeInLog($cardNo, $sdate);
                        $att_time_in = "";
                        $att_time_out = "";
                        $totalInTime = "";
                        foreach ($timeInInfo as $row3) {
                            $totalInTime = $row3->pstime;
                        }

                        if ($totalInTime != "") {
                            $devidePunchtime = intval($totalInTime / 100);
                            $mutiplytime = $devidePunchtime * 100;
                            $remainMinute = $totalInTime - $mutiplytime;

                            if($devidePunchtime<10){
                                $devidePunchtime = "0".$devidePunchtime;
                            }
                            if($remainMinute<10){
                                $remainMinute = "0".$remainMinute;
                            }
                            $att_time_in = $devidePunchtime . ":" . $remainMinute;
                        }

                        //Retrive Out time data
                        $totalOutTime = "";
                        $timeOutInfo = $this->attendence_model->getTimeOutLog($cardNo, $sdate);

                        foreach ($timeOutInfo as $row31) {
                            $totalOutTime = $row31->pstime_1;
                        }
                        if ($totalOutTime != "") {
                            $devidePunchtime1 = intval($totalOutTime / 100);
                            $mutiplytime1 = $devidePunchtime1 * 100;
                            $remainMinute1 = $totalOutTime - $mutiplytime1;
                            if($devidePunchtime1<10){
                                $devidePunchtime1 = "0".$devidePunchtime1;
                            }
                            if($remainMinute1<10){
                                $remainMinute1 = "0".$remainMinute1;
                            }

                            $att_time_out = $devidePunchtime1 . ":" . $remainMinute1;

                            /*  $explodeTimeIn = explode(":", $att_time_in);
                              $InHour = $explodeTimeIn[0];
                              $InHourToMinute = $InHour * 60;
                              $InHourminute = $explodeTimeIn[1];
                              $totalInTime = $InHourToMinute + $InHourminute;*/
                        }


                        if ($att_time_in == $att_time_out) {
                            $att_time_out = "";
                        }

                        //check weekend,holiday and leave

                        $holidayInfo = $this->attendence_model->getHoliday($sdate);

                        if (count($holidayInfo)) {
                            $holiday = $holiday + 1;
                            $status = "<p class=\"text-success\">Holiday</p>";
                            $status1 = "holiday";
                        } else {

                            $weekendInfo = $this->attendence_model->getWeekend($sdate, $branch);
                            //  $weekendInfo = $this->db->query("select * from add_weekend where dept='" . $temp->dept . "'and branch_name='" . $temp->branch_name . "'and weekend='" . $weekDay . "'");
                            if (count($weekendInfo)) {
                                $weekend = $weekend + 1;
                                $status = "<p class=\"text-success\">Weekend</p>";
                                $status1 = "weekend";
                            } else {
                                //$leaveInfo = $this->attendence_model->getLeaveInfo($sdate, $empId);
                                $leave1 = $this->db->query("select * from leave_management where hr_status='2' and emp_id='" .$empId . "'and '" . $sdate . "' between start_date and end_date ");
                                //if (count($leaveInfo)) {
                                if ($leave1->num_rows()) {
                                    $leave = $leave + 1;
                                    $status = "<p class=\"text-success\">Leave</p>";
                                    $status1 = "leave";
                                } else {
                                    if ($att_time_in == "" && $att_time_out == "") {
                                        $absent = $absent + 1;
                                        //$status = "<font style='color:red'>Absent</font>";
                                        $status = "<p class=\"text-danger\">Absent</p>";
                                        $status1 = "absent";
                                    } else {
                                        $present = $present + 1;
                                        $status = "<p class=\"text-info\">Present</p>";
                                        $status1 = "present";
                                        $finalInTime = $devidePunchtime*60+$remainMinute ;
                                        if ($total_late_time != 0 && $totalInTime != 0){
                                            if ($finalInTime > $total_late_time) {
                                                $late = $late + 1;
                                                $late_minute_count = $finalInTime - $total_late_time ;
                                                if( $late_minute_count >59 ){
                                                    $minutes = $late_minute_count;
                                                    $zero    = new DateTime('@0');
                                                    $offset  = new DateTime('@' . $minutes * 60);
                                                    $diff    = $zero->diff($offset);
                                                    $late_minute_count= $diff->format('%h Hours, %i Minutes');
                                                } else {
                                                    $minutes = $late_minute_count;
                                                    $zero    = new DateTime('@0');
                                                    $offset  = new DateTime('@' . $minutes * 60);
                                                    $diff    = $zero->diff($offset);
                                                    $late_minute_count= $diff->format('%i Minutes');
                                                }
                                                $status = "<p class=\"text-info\">Late</p>";
                                                $status1 = "late";
                                            }
                                        }
                                    }
                                }
                            }
                        }
                        //end weelend,holiday,leave check

                        // ot $empId, $sdate
                        $this->db->select('*');
                        $this->db->from('ot_sheet');
                        $this->db->where('emp_id',$empId);
                        $this->db->where('ot_date',$sdate);
                        $query = $this->db->get();
                        $ot_row = $query->result() ; //show($ot_row[0]->total_amount);
                        if (count($ot_row)){
                            foreach ($ot_row as $otInfo){
                                $ot_total_hour = $otInfo->total_hour;
                            }
                            $convert_ot_hour = $ot_total_hour * 60 ;
                            if($convert_ot_hour >59){
                                $minutes = $convert_ot_hour;
                                $zero    = new DateTime('@0');
                                $offset  = new DateTime('@' . $minutes * 60);
                                $diff    = $zero->diff($offset);
                                $convert_ot_hour= $diff->format('%h H, %i M');
                            } else if($convert_ot_hour != 0) {
                                $minutes = $convert_ot_hour;
                                $zero    = new DateTime('@0');
                                $offset  = new DateTime('@' . $minutes * 60);
                                $diff    = $zero->diff($offset);
                                $convert_ot_hour= $diff->format('%i M');
                            }
                            $final_ot_hour = $convert_ot_hour;
                        }

                        $dateComment = "";
                        $loginfo = $this->attendence_model->getDailyLogInfo($sdate, $empId);

                        if (count($loginfo)):
                            foreach ($loginfo as $c1Info):
                                $dateComment = $c1Info->comment;
                            endforeach;
                        endif;
                        ?>

                        <tr <?php echo $i ?>>
                            <td><?php echo $sdate; ?><input type="hidden" name="att_date[]" value="<?php echo $sdate; ?>" /></td>
                            <td><?php echo $att_day; ?><input type="hidden" name="att_day[]" value="<?php echo $att_day; ?>" /></td>
                            <td><?php echo $att_time_in; ?><input type="hidden" name="timein[]" value="<?php echo $att_time_in; ?>" /></td>
                            <td><?php echo $att_time_out; ?><input type="hidden" name="timeout[]" value="<?php echo $att_time_out; ?>" /></td>
                            <td><?php echo $status; ?><input type="hidden" name="status[]" value="<?php echo $status1;    ?>" /></td>
                            <!--<td><?php /*echo @$late_minute_count; */?></td>-->
                            <!--<td><?php /*if(@$convert_ot_hour !=0){ echo @$convert_ot_hour; }  */?></td>-->
                            <td id="date__<?php echo $sdate; ?>">
                                <span id="spen_lbl_comment"><?php echo $dateComment; ?></span>
                                <span style="display:none" class="btn btn-success btn-xs btnshow" id="spen_lbl_add" onclick="showDailyLogEdit('<?php echo $sdate; ?>')" >Edit</span>

                                <input class="hidden_spen_lbl_comment" type="hidden" name="comment[]" value="<?php echo $dateComment; ?>" />
                                <input type="hidden" name="year" value="<?php echo $year; ?>" />
                                <input type="hidden" name="month" value="<?php echo $month; ?>" />
                                <input class="enterkey_prevent" type="text" style="display: none;" onblur="setAttDailyLog('<?php echo $sdate; ?>');" id="comment" name="comment1" value="<?php echo $dateComment; ?>" />
                                <input type="hidden" name="emp_id"  id="emp_id" value="<?php echo $empId; ?>">
                                <input type="hidden" name="log_date" id="log_date" value="<?php echo $sdate; ?>">
                                <span id="loader__<?php echo $sdate; ?>"></span>
                            </td>
                        </tr>
                        <?php
                        $late_minute_count="";
                    }
                    ?>
                    <tr>
                        <td colspan="5">
                            <?php
                            $s_status = "";
                            $submitInfo = $this->attendence_model->getSubmitInfo();
                            foreach ($submitInfo as $t){
                                $s_status = $t->status;
                            }
                            ?>
                            <button type="submit" class="btn btn-primary" <?php if ($s_status == '2'): ?> disabled="disabled" <?php endif; ?>>Submit</button>
                            <?php echo form_close();?>
                        </td>
                    </tr>

                    </tbody>

                </table>

            </div>
        </div><! --/content-panel -->
    </div><!-- /col-md-12 -->
</div><!-- row -->