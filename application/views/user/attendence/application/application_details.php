<?php
$sup = array(
    '' => 'Select',
    '1' => 'Approve',
    '2' => 'Deny'
);
$sup1 = array(
    '1' => ''
);
$hr = array(
    '' => 'Select',
    '1' => 'Approve',
    '2' => 'Deny'
);
?>
<h3>Attendance Details:</h3>
<div class="row">
    <div class="col-sm-12">
        <div class="content-panel">
            <!--<hr>-->
            <div class="table-responsive">
                <table class="table">
                    <thead>
                    <tr class="tbl_header_bg_color">
                        <th>Date</th>
                        <th>Entry time</th>
                        <th>Exit time</th>
                        <th>Status</th>
                        <th>EmpRemarks</th>
                        <th>SupRemarks</th>
                        <th>SupStatus</th>
                        <th>Hr Remarks</th>
                    </tr>
                    </thead>
                    <tbody>
                    <?php
                    echo form_open("user/attendence/update");
                    $c = 1;
                    if ($month == '01') {
                        $total_month_day = 31;
                    } elseif ($month == '02') {
                        $total_month_day = 28;
                    } elseif ($month == '03') {
                        $total_month_day = 31;
                    } elseif ($month == '04') {
                        $total_month_day = 30;
                    } elseif ($month == "05") {
                        $total_month_day = 31;
                    } elseif ($month == "06") {
                        $total_month_day = 30;
                    } elseif ($month == "07") {
                        $total_month_day = 31;
                    } elseif ($month == "08") {
                        $total_month_day = 31;
                    } elseif ($month == "09") {
                        $total_month_day = 30;
                    } elseif ($month == "10") {
                        $total_month_day = 31;
                    } elseif ($month == "11") {
                        $total_month_day = 30;
                    } elseif ($month == "12") {
                        $total_month_day = 31;
                    } else {
                        $total_month_day = 30;
                    }
                    $branch = "";
                    $duty_schedule = 0;
                    $empInfo = $this->attendence_model->getEmployeeInfo($empId);
                    /*echo '<pre>';
                    var_dump($empInfo);
                    echo '</pre>';
                    exit;*/
                    foreach ($empInfo as $row):
                        $branch = $row->branch_name;
                        $duty_schedule = $row->duty_schedule;
                    endforeach;
                    $total_start_time = "";
                    $total_end_time = "";
                    $total_late_time = 0;

                    $scheduleInfo = $this->attendence_model->getSchedule($duty_schedule);
                    if (count($scheduleInfo)):
                        foreach ($scheduleInfo as $sInfo):
                            $start_time = $sInfo->start_time;
                            $end_time = $sInfo->end_time;
                            $late_time = $sInfo->late_time;
                        endforeach;
                        //convert start time to minute
                        $explode_start_time = explode(":", $start_time);
                        $start_hour = $explode_start_time[0];
                        $start_hour_to_minute = $start_hour * 60;
                        $start_minute = $explode_start_time[1];
                        $total_start_time = $start_hour_to_minute + $start_minute;
                        //convert end time to minute
                        $explode_end_time = explode(":", $end_time);
                        $end_hour = $explode_end_time[0];
                        $end_hour_to_minute = $end_hour * 60;
                        $end_minute = $explode_end_time[1];
                        $total_end_time = $end_hour_to_minute + $end_minute;
                        //convert Late time to minute
                        $explode_late_time = explode(":", $late_time);
                        $late_hour = $explode_late_time[0];
                        $late_hour_to_minute = $late_hour * 60;
                        $late_minute = $explode_late_time[1];
                        $total_late_time = $late_hour_to_minute + $late_minute;

                    endif;

                    $leave = 0;
                    $holiday = 0;
                    $weekend = 0;
                    $absent = 0;
                    $present = 0;
                    $late = 0;

                    //start for loop
                    for ($i = 1; $i <= $total_month_day; $i++) {
                        if ($i <= 9) {
                            $day = "0" . $i;
                        } else {
                            $day = $i;
                        }
                        $sdate = $year . "-" . $month . "-" . $day . "";

                        $empId = $empId;
                        // $total_late_time = "09:10";
                        //Retrive In time data
                        //$this->load->model("user/attendence_model");
                        $timeInInfo = $this->attendence_model->getTimeLogInfo($empId, $sdate);
                        //echo "<pre>";print_r($timeInInfo);

                        $att_time_in = "";
                        $att_time_out = "";
                        $totalInTime = "";
                        $emp_comment = "";
                        $sup_comment = "";
                        $status = "";
                        $hr_comment = "";
                        $a = "";
                        foreach ($timeInInfo as $row3) {
                            $att_time_in = $row3->timein;
                            $att_time_out = $row3->timeout;
                            $emp_comment = $row3->emp_comment;
                            $sup_comment = $row3->sup_comment;
                            $hr_comment = $row3->hr_comment;
                        }

                        if ($att_time_in != "") {
                            $explodeTimeIn = explode(":", $att_time_in);
                            $InHour = $explodeTimeIn[0];
                            $InHourToMinute = $InHour * 60;
                            $InHourminute = $explodeTimeIn[1];
                            $totalInTime = $InHourToMinute + $InHourminute;
                        }
                        //check weekend,holiday and leave

                        $holidayInfo = $this->attendence_model->getHoliday($sdate);
                       /* echo '<pre>';
                    var_dump($holidayInfo);
                    echo '</pre>';
                    exit;*/
                        if (count($holidayInfo)) {
                            $holiday = $holiday + 1;
                            $status = "<p class=\"text-success\">Holiday</p>";
                            $status1 = "holiday";
                            $a = 1;
                        } else {
                            $weekendInfo = $this->attendence_model->getWeekend($sdate, $branch);
                            if (count($weekendInfo)) {
                                $weekend = $weekend + 1;
                                $status = "<p class=\"text-success\">Weekend</p>";
                                $status1 = "weekend";
                                $a = 1;
                            } else {
                                //$leaveInfo = $this->attendence_model->getLeaveInfo($sdate, $empId);
                                $leave1 = $this->db->query("select * from leave_management where hr_status='2' and emp_id='" .$empId . "'and '" . $sdate . "' between start_date and end_date ");
                                //if (count($leaveInfo)) {
                                if ($leave1->num_rows()) {
                                    $leave = $leave + 1;
                                    $status = "<p class=\"text-success\">Leave</p>";
                                    $status1 = "leave";
                                    $a = 1;
                                } else {
                                    if ($att_time_in == "" && $att_time_out == "") {
                                        $absent = $absent + 1;
                                        $status = "<p class=\"text-danger\">Absent</p>";
                                        $status1 = "absent";
                                        $a = "";
                                    } else {
                                        $present = $present + 1;
                                        $status = "<p class=\"text-success\">Present</p>";
                                        $status1 = "present";
                                        $a = 1;
                                        if ($total_late_time != 0 && $totalInTime != ""):
                                            if ($totalInTime > $total_late_time) {
                                                $late = $late + 1;
                                                // $late_duration = $timein - $total_late_time;
                                                //$total_late_minute = $total_late_minute + $late_duration;

                                                $status = "<p class=\"text-info\">Late</p>";
                                                $status1 = "late";
                                                $a = "";
                                            }
                                        endif;
                                    }
                                }
                            }
                        }
                        ?>
                        <tr class=" <?php echo $i ?>">
                            <td><input type="text" class="form-control" readonly="readonly" name="att_date[]" value="<?php echo $sdate; ?>" /></td>
                            <td><input type="text" class="form-control" name="timein[]" readonly="readonly" value="<?php echo $att_time_in; ?>" /></td>
                            <td><input type="text" class="form-control" name="timeout[]" readonly="readonly" value="<?php echo $att_time_out; ?>" /></td>
                            <td><?php echo $status; ?><input type="hidden" readonly="readonly" name="status[]" value="<?php echo $status1; ?>" /></td>
                            <td><input type="text" class="form-control" readonly="readonly" name="emp_comment[]" value="<?php echo $emp_comment; ?>" /></td>
                            <td>
                                <input type="text" class="form-control"  name="sup_comment[]" value="<?php echo $sup_comment; ?>" />
                            </td>
                            <td>
                                <?php
                                if ($a == "1"):
                                    echo form_dropdown('sup_status[]', $sup1, $a, "class='form-control'");
                                else:
                                    echo form_dropdown('sup_status[]', $sup, $a, "class='form-control'");
                                endif;
                                ?>
                            </td>
                            <td>
                                <input type="text" class="form-control" readonly="readonly" name="hr_comment[]" value="<?php echo $hr_comment; ?>" />
                            </td>
                        </tr>
                        <?php
                    }
                    ?>
                        <tr>
                            <td>
                                <input type="hidden" name="emp_id" value="<?php echo $empId; ?>" />
                                <input type="hidden" name="year" value="<?php echo $year; ?>" />
                                <input type="hidden" name="month" value="<?php echo $month; ?>" />
                                <button type="submit" class="btn btn-primary">Submit</button>
                                </form>
                            </td>
                        </tr>
                    </tbody>
                </table>
            </div>
        </div><! --/content-panel -->
    </div><!-- /col-md-12 -->
</div><!-- row -->
