<h3>Add New Log</h3>

<div class="row">
    <div class="col-sm-12">
        <div class="content-panel">
            <?php
            echo form_open('user/attendence/addedLog');?>
            <?php
            if($this->session->flashdata('message')==NULL){

            }else{ ?>
                <div class="alert-success alert">
                    <?php echo $this->session->flashdata('message') ?>
                </div> <?php
            }
            ?>
            <div class="col-md-12">
                <div class="row">
                    <div class="form-group col-sm-6 <?php if(ucwords(form_error('log_date'))){echo "has-error";}?>">
                        <label for="start_date">Date:</label>
                        <input type="text" name="log_date" value="<?php if($log_date){echo $log_date;}?>" id="start_date" class="form-control">
                        <span class="alert-danger"><?php echo ucwords(form_error('log_date')); ?></span>
                    </div>

                    <div class="form-group col-sm-6 <?php if(ucwords(form_error('comment'))){echo "has-error";}?>">
                        <label for="end_date">Commit:</label>
                        <input type="text" name="comment" value="<?php if($comment){echo $comment;}?>" id="comment" class="form-control">
                        <span class="alert-danger"><?php echo ucwords(form_error('comment')); ?></span>
                    </div>
                </div>
            </div>
                <span>&nbsp;&nbsp;&nbsp;&nbsp;
                    <input type="submit" name="mysubmit" value="Submit"  class="btn btn-primary">
                    <a class="btn btn-theme" href="<?php echo base_url(); ?>user/attendence/added_log_form">Cancel</a>
                </span>
            </form>
            <p></p>
        </div>
    </div><!-- col-lg-12-->
</div><!-- /row -->