<?php
    $months = array(
        '' => 'Select',
        '01' => 'January',
        '02' => 'February',
        '03' => 'March',
        '04' => 'April',
        '05' => 'May',
        '06' => 'June',
        '07' => 'July',
        '08' => 'August',
        '09' => 'September',
        '10' => 'October',
        '11' => 'November',
        '12' => 'December',
    );
    $leaves = array(
        '' => 'Select',
        '1' => 'Causal ',
        '2' => 'Sick',
        '3' => 'Earned',
    );
?>

<!-- <div class="row">
    <div class="form-panel">
        <P class="text_decoration_heading">Note:</P>
        <P class="text_decoration">* Start And End Date must be in same year and in same quartile.</P>
        <P class="text_decoration">* End Date must be greater then or equal to Start Date.</P>
        <P class="text_decoration">* Previous Date are allowed only for sick leave.</P>
        <P class="text_decoration">* You have to Apply at least two days ago.</P>
        <P class="text_decoration">* Don't choose Holiday and weekend days for leave.</P>
    </div>

</div> -->

<h5>Leave Summary:<?php echo date('Y') ?></h5>
<div class="row">
    <div class="col-sm-12">
        <div class="content-panel">
            <!--<hr>-->
            <div class="table-responsive">
                <table class="table table-bordered">


                    <tr><td colspan="3">
                            <table class="table">
                                <thead>
                                <tr>
                                    <td colspan="2"><b>Leave Package</b></td>
                                    <td colspan="2"><b>Leave Spent</b></td>
                                    <td colspan="2"><b>Leave Balance</b></td>
                                </tr>
                                </thead>
                                <?php
                                foreach($leave_package_info as $data){
                                    //echo '<pre>';
                                    //var_dump($data);
                                    //echo '</pre>';
                                    //exit;

                                    $package_name=$data->package_name;
                                    $casual=$data->casual;
                                    $medical=$data->medical;
                                    $earned=$data->earned;
                                }

                                $t_casual = 0;
                                $t_medical = 0;
                                $t_earned = 0;
                                $ex_with_pay = 0;
                                $ex_without_pay = 0;
                                foreach ($leave_details_info as $row4) {
                                    $duration_show = $row4->leave_duration;
                                    $extra_days = $row4->extra_days;
                                    $leave_type_show = $row4->leave_type;

                                    if ($leave_type_show == '1') {
                                        $t_casual = $t_casual + $duration_show;
                                    } else {
                                        $t_casual = $t_casual;
                                    }
                                    if ($leave_type_show == '2') {
                                        $t_medical = $t_medical + $duration_show;
                                    } else {
                                        $t_medical = $t_medical;
                                    }
                                    if ($leave_type_show == '3') {
                                        $t_earned = $t_earned + $duration_show;
                                    } else {
                                        $t_earned = $t_earned;
                                    }

                                    if ($row4->pay_type == "1") {
                                        $ex_with_pay = $ex_with_pay + $extra_days;
                                    } elseif ($row4->pay_type == "2") {
                                        $ex_without_pay = $ex_without_pay + $extra_days;
                                    } else {

                                    }
                                }

                                $cas_exists = $casual - $t_casual;
                                if ($cas_exists < 0) {
                                    $cas_exists = 0;
                                }
                                $medical_exists = $medical - $t_medical;
                                if ($medical_exists < 0) {
                                    $medical_exists = 0;
                                }
                                $earned_exists = $earned - $t_earned;
                                if ($earned_exists < 0) {
                                    $earned_exists = 0;
                                }
                                ?>
                                <tr>
                                    <th class="tbl_header_bg_color">Package Name: </th>
                                    <td><?php echo $package_name; ?></td>
                                    <th></th>
                                    <td></td>
                                    <th></th>
                                    <td></td>
                                </tr>

                                <tr>
                                    <th class="tbl_header_bg_color">Casual Leave: </th>
                                    <td><?php echo $casual; ?></td>
                                    <th class="tbl_header_bg_color">Casual: </th>
                                    <td><?php echo $t_casual; ?></td>
                                    <th class="tbl_header_bg_color">Casual: </th>
                                    <td><?php echo $cas_exists; ?></td>
                                </tr>
                                <tr>
                                    <th class="tbl_header_bg_color">Sick Leave: </th>
                                    <td><?php echo $medical; ?></td>
                                    <th class="tbl_header_bg_color">Sick: </th>
                                    <td><?php echo $t_medical; ?></td>
                                    <th class="tbl_header_bg_color">Sick: </th>
                                    <td><?php echo $medical_exists; ?></td>
                                </tr>
                                <tr>
                                    <th class="tbl_header_bg_color">Earned Leave: </th>
                                    <td><?php echo $earned; ?></td>
                                    <th class="tbl_header_bg_color">Earned: </th>
                                    <td><?php echo $t_earned; ?></td>
                                    <th class="tbl_header_bg_color">Earned: </th>
                                    <td><?php echo $earned_exists; ?></td>
                                </tr>

                                <tr>
                                    <th>Total:</th>
                                    <td><?php echo $casual + $medical + $earned; ?></td>
                                    <th>Total:</th>
                                    <td><?php echo $t_casual + $t_medical + $t_earned; ?></td>
                                    <th>Total:</th>
                                    <td><?php echo $cas_exists + $medical_exists + $earned_exists; ?></td>
                                </tr>
                            </table></td>
                    </tr>
                </table>
            </div>
        </div><! --/content-panel -->
    </div><!-- /col-md-12 -->
</div><!-- row -->


<h5>Leave Schedule</h5>
<div class="row">
    <div class="col-sm-12">
        <div class="content-panel">
            <!--<hr>-->
            <div class="table-responsive">
                <table class="table table-bordered table-hover">
                    <thead>
                    <tr class="tbl_header_bg_color">
                        <th>SL No</th>
                        <th>Start Date</th>
                        <th>End Date</th>
                        <th>Casual</th>
                        <th>Earned</th>
                        <th>Sick</th>
                        <th>Total</th>
                    </tr>
                    </thead>
                    <tbody>
                    <?php
                    $i=1;
                    foreach ($schedule_info as $temp1) {
                        ?>
                        <tr>
                            <td><?php echo $i++;?></td>
                            <td><?php echo $temp1->month_start;?></td>
                            <td><?php echo $temp1->month_end;?></td>
                            <td><?php echo $temp1->casual;?></td>
                            <td><?php echo $temp1->earned;?></td>
                            <td><?php echo $temp1->medical?></td>
                            <td><?php echo $temp1->days;?></td>
                        </tr>
                        <?php
                    }
                    ?>
                    </tbody>
                </table>
            </div>
        </div><! --/content-panel -->
    </div><!-- /col-md-12 -->
</div><!-- row -->


<h3><i class="fa fa-angle-right"></i>Leave Application</h3>
<div class="leave_condition">
    <P class="text_decoration_heading">Note:</P>
    <P class="text_decoration">* Start And End Date must be in same year and in same quartile.</P>
    <P class="text_decoration">* End Date must be greater then or equal to Start Date.</P>
    <P class="text_decoration">* Previous Date are allowed only for sick leave.</P>
    <P class="text_decoration">* You have to Apply at least two days ago.</P>
    <P class="text_decoration">* Don't choose Holiday and weekend days for leave.</P>
</div>

<div class="row">
    <div class="col-sm-12">
        <div class="content-panel">
            <?php
            echo form_open('user/leave/applied');?>
            <?php
            if($this->session->flashdata('message')==NULL){

            } else { ?>
                <div class="alert-success alert">
                    <?php echo $this->session->flashdata('message') ?>
                </div> <?php
            }
            ?>
            <div class="col-md-12">
                <div class="row">
                    <div class="form-group col-sm-6 <?php if(ucwords(form_error('start_date'))){echo "has-error";}?>">
                        <label for="start_date">Start Date:</label>
                        <input type="text" name="start_date" value="<?php if($start_date){echo $start_date;}?>" id="start_date" class="form-control">
                        <span class="alert-danger"><?php echo ucwords(form_error('start_date')); ?></span>
                    </div>

                    <div class="form-group col-sm-6 <?php if(ucwords(form_error('end_date'))){echo "has-error";}?>">
                        <label for="end_date">End Date:</label>
                        <input type="text" name="end_date" value="<?php if($end_date){echo $end_date;}?>" id="end_date" class="form-control">
                        <span class="alert-danger"><?php echo ucwords(form_error('end_date')); ?></span>
                    </div>

                    <div class="form-group col-sm-6 <?php if(ucwords(form_error('leave_type'))){echo "has-error";}?>">
                        <label for="leave_type">Leave Type:</label>

                        <?php echo form_dropdown('leave_type', $leaves, $leave_type, 'id="leave_type" class="form-control"') ?>
                        <span class="alert-danger"><?php echo ucwords(form_error('leave_type')); ?></span>
                    </div>

                    <div class="form-group col-sm-6">
                        <label for="">Duration:</label>
                        <div id="m">
                            <input type="text" readonly="readonly"  name="duration" value="<?php if($duration){echo $duration;}?>" id="duration" class="form-control">
                        </div>
                        <span class="alert-danger"><?php //echo ucwords(form_error('duration')); ?></span>
                    </div>
                </div>
            </div>
            <div class="col-md-12">
                <div class="row">
                    <div class="form-group col-sm-12">
                        <p class="authorized_heading">* Please select your Authorized  person according to Designation*</p>
                    </div>
                    <div class="form-group col-sm-6">
                        <?php
                            $degInfo = $this->leave_model->get_deg_info();
                        ?>
                        <label for="leave_type">Authorized Person Deg:</label>

                        <input type="hidden" name="hidden_nominated_person" id="hidden_nominated_person" value="<?php echo $this->input->get_post("nominated_person");?>" >
                        <select name="sup_designation" id="empDesignation" class="form-control" onchange="javascript:getSupervisor(this.value)">
                            <option value="">Select</option>
                            <?php foreach ($degInfo as $deg): ?>
                                <option <?php  if($sup_designation == $deg->id){ echo "selected" ;} ?> value="<?php echo $deg->id; ?>"><?php echo $deg->designation; ?></option>
                            <?php endforeach; ?>
                        </select>

                        <script>

                            $( document ).ready(function() {
                                var empDesignation = $("#empDesignation").val();

                                if(empDesignation)
                                {
                                    getSupervisor(empDesignation)


                                }
                            });
                        </script>

                        <span class="alert-danger"><?php echo ucwords(form_error('empDesignation')); ?></span>
                    </div>

                    <div class="form-group col-sm-6">
                        <label for="">Authorized Person Name:</label>
                        <div id="resultSupervisor">
                            <!--<input type="text" readonly="readonly" name="nominated_person" value="<?php /*if($nominated_person){echo $nominated_person;}*/?>" id="nominated_person" class="form-control">-->
                        </div>
                        <span class="alert-danger"><?php echo ucwords(form_error('supervisor')); ?></span>
                    </div>
                </div>

            </div>
                <span>&nbsp;&nbsp;&nbsp;&nbsp;
                    <input type="submit" name="mysubmit" value="Submit"  class="btn btn-primary">
                    <a class="btn btn-theme" href="<?php echo base_url(); ?>user/home">Cancel</a>
                </span>
            </form>
            <p></p>
        </div>
    </div><!-- col-lg-12-->
</div><!-- /row -->