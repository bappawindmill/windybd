<?php
    if (count($application_info)):

    foreach ($application_info as $t):endforeach;
    $empId = $t->emp_id;
    $year = $t->year;
    $nid = $t->nominated_person;
    //$query = $this->db->query("select * from employeeinfo where id='" . $t->emp_id . "'");
    $emp_info = $this->leave_model->get_emp($empId);

    foreach ($emp_info as $temp):endforeach;
    $leave_package_id = $temp->leave_name;
    $emp_id = $temp->id;
    $dept_id = $temp->dept;
    $desig_id = $temp->designation;

    $package_name = "";
    $casual = "";
    $medical = "";
    $earned = "";
    //$res1 = $this->db->query("SELECT * FROM add_leave WHERE id='" . $temp->leave_name . "'");
    $leave_package_info = $this->leave_model->all_leave_package_info($leave_package_id);

    foreach ($leave_package_info as $row1) {
        $package_name = $row1->package_name;
        $casual = $row1->casual;
        $medical = $row1->medical;
        $earned = $row1->earned;
    }

    //$query4 = $this->db->query("SELECT * FROM leave_management WHERE emp_id='" . $temp->id . "'  AND year='" . $t->year . "'and hr_status=2");
    $leave_details_info = $this->leave_model->report_leave_details($emp_id, $year);

    $t_casual = 0;
    $t_medical = 0;
    $t_earned = 0;
    $ex_with_pay = 0;
    $ex_without_pay = 0;
    foreach ($leave_details_info as $row4) {
        $duration = $row4->leave_duration;
        $extra_days = $row4->extra_days;
        $leave_type = $row4->leave_type;
        if ($leave_type == '1') {
            $t_casual = $t_casual + $duration;
        } else {
            $t_casual = $t_casual;
        }
        if ($leave_type == '2') {
            $t_medical = $t_medical + $duration;
        } else {
            $t_medical = $t_medical;
        }
        if ($leave_type == '3') {
            $t_earned = $t_earned + $duration;
        } else {
            $t_earned = $t_earned;
        }
        if ($row4->pay_type == "1") {
            $ex_with_pay = $ex_with_pay + $extra_days;
        } elseif ($row4->pay_type == "2") {
            $ex_without_pay = $ex_without_pay + $extra_days;
        } else {

        }
    }
    $cas_exists = $casual - $t_casual;
    $medical_exists = $medical - $t_medical;
    $earned_exists = $earned - $t_earned;

    if ($cas_exists < 0) {
        $cas_exists = 0;
    }

    if ($medical_exists < 0) {
        $medical_exists = 0;
    }

    if ($earned_exists < 0) {
        $earned_exists = 0;
    }

    $dept_name = "";
    //$query8 = $this->db->query("select name from add_department where id='" . $temp->dept . "'");
    $department_info = $this->leave_model->get_department_info($dept_id);
    foreach ($department_info as $row3) {
        $dept_name = $row3->name;
    }
    $emp_deg = "";
    //$ed = $this->db->query("select designation from designation where id='" . $temp->designation . "'");
    $designation_info = $this->leave_model->get_designation_info($desig_id);
    foreach ($designation_info as $d3) {
        $emp_deg = $d3->designation;
    }
    $npersonName = "";
    $npersonDeg = "";
    //$aInfo = $this->db->query("select * from employeeinfo where id='" . $t->nominated_person . "'");
    $nominated_info = $this->leave_model->get_emp_info1($nid);
    foreach ($nominated_info as $a1Info) {
        $npersonName = $a1Info->name;
        $npersonDeg = $a1Info->designation;
    }
    $npersonDegName = "";
    //$ndegInfo = $this->db->query("select designation from designation where id='" . $npersonDeg . "'");
    $nominated_desig_info = $this->leave_model->get_designation_info($npersonDeg);
        /*echo '<pre>';
        var_dump($nominated_desig_info);
        echo '<pre>';
        exit;*/
    foreach ($nominated_desig_info as $d9) {
        $npersonDegName = $d9->designation;
    }
?>

<h3>Leave Application Details:</h3>
<div class="row">
    <div class="col-sm-12">
        <div class="content-panel">
            <!--<hr>-->
            <div class="table-responsive">
                <table class="table table-bordered table-hover">
                    <tr>
                        <td><strong>EmployeeID No</strong></td>
                        <td><?php echo $temp->card_no; ?></td>
                        <td><strong>Application Submission Date</strong></td>
                        <td><?php echo $t->app_date; ?></td>
                    </tr>
                    <tr>
                        <td><strong>Name</strong></td>
                        <td><?php echo $temp->name; ?></td>

                        <td><strong>Designation</strong></td>
                        <td><?php echo $emp_deg; ?></td>
                    </tr>
                    <tr>
                        <td><strong>Department</strong></td>
                        <td><?php echo $dept_name; ?></td>
                        <td><strong>Leave Type</strong></td>
                        <td>
                            <?php
                            if ($t->leave_type == '1') {

                                $a = "Casual";
                            } elseif ($t->leave_type == '2') {

                                $a = "Medical";
                            } elseif ($t->leave_type == '3') {

                                $a = "Earned";
                            } else {
                                $a = "";
                            }
                            echo $a;
                            ?>
                        </td>
                    </tr>
                    <tr>
                        <td><strong>Leave Period</strong></td>
                        <td><strong>From</strong>: <?php echo $t->start_date; ?></td>
                        <td><strong>To</strong>: <?php echo $t->end_date; ?></td>
                        <td><strong>Total Duration </strong>: <?php echo $t->leave_duration; ?></td>
                    </tr>
                    <tr>
                        <td colspan="2"><strong>Authorized Person Name : <?php echo $npersonName; ?></strong></td>
                        <td colspan="2"><strong>Authorized Person Designation : <?php echo $npersonDegName; ?></strong></td>

                    </tr>


                    <?php if ($t->extra_days > 0): ?>
                        <tr>
                            <td><strong>Extra Days</strong>: <?php echo $t->extra_days; ?></td>
                            <td><strong>Payment Type</strong>: <?php
                                if ($t->pay_type == "1"):echo "With  Pay";
                                else:echo "Without Pay";
                                endif;
                                ?></td>
                        </tr>
                    <?php endif; ?>
                </table>
            </div>
        </div><! --/content-panel -->
    </div><!-- /col-md-12 -->
</div><!-- row -->
<br/><br/>


<h3><i class="fa fa-angle-right"></i> Leave Summary:</h3>
<div class="row">
    <div class="col-sm-12">
        <div class="content-panel">
            <!--<hr>-->
            <div class="table-responsive">

                            <table class="table table-bordered table-hover">
                                <thead>
                                <tr>
                                    <td colspan="2"><b>Leave Package</b></td>
                                    <td colspan="2"><b>Leave Spent</b></td>
                                    <td colspan="2"><b>Leave Balance</b></td>
                                </tr>
                                </thead>
                                <tr>
                                    <th class="tbl_header_bg_color">Package Name: </th>
                                    <td><?php echo $package_name; ?></td>
                                    <th></th>
                                    <td></td>
                                    <th></th>
                                    <td></td>
                                </tr>

                                <tr>
                                    <th class="tbl_header_bg_color">Casual Leave: </th>
                                    <td><?php echo $casual; ?></td>
                                    <th class="tbl_header_bg_color">Casual: </th>
                                    <td><?php echo $t_casual; ?></td>
                                    <th class="tbl_header_bg_color">Casual: </th>
                                    <td><?php echo $cas_exists; ?></td>
                                </tr>
                                <tr>
                                    <th class="tbl_header_bg_color">Sick Leave: </th>
                                    <td><?php echo $medical; ?></td>
                                    <th class="tbl_header_bg_color">Sick: </th>
                                    <td><?php echo $t_medical; ?></td>
                                    <th class="tbl_header_bg_color">Sick: </th>
                                    <td><?php echo $medical_exists; ?></td>
                                </tr>
                                <tr>
                                    <th class="tbl_header_bg_color">Earned Leave: </th>
                                    <td><?php echo $earned; ?></td>
                                    <th class="tbl_header_bg_color">Earned: </th>
                                    <td><?php echo $t_earned; ?></td>
                                    <th class="tbl_header_bg_color">Earned: </th>
                                    <td><?php echo $earned_exists; ?></td>
                                </tr>

                                <tr>
                                    <th>Total:</th>
                                    <td><?php echo $casual + $medical + $earned; ?></td>
                                    <th>Total:</th>
                                    <td><?php echo $t_casual + $t_medical + $t_earned; ?></td>
                                    <th>Total:</th>
                                    <td><?php echo $cas_exists + $medical_exists + $earned_exists; ?></td>
                                </tr>
                            </table>
            </div>
        </div><! --/content-panel -->
    </div><!-- /col-md-12 -->
</div><!-- row -->
<br/><br/>

<div class="row">
    <div class="col-sm-12">
        <div class="content-panel">
            <!--<hr>-->
            <div class="table-responsive">
                <table class="table table-bordered">
                    <thead>
                    <tr>
                        <th class="tbl_header_bg_color">Total With Pay :</th>
                        <td><?php echo $ex_with_pay;?> Days</td>
                    </tr>
                    </thead>
                    <tbody>

                    <tr>
                        <th class="tbl_header_bg_color">Total without Pay :</th>
                        <td><?php echo $ex_without_pay;?> Days</td>
                    </tr>

                    </tbody>
                </table>
            </div>
        </div><! --/content-panel -->
    </div><!-- /col-md-12 -->
</div><!-- row -->
<br/><br/>
<a href="<?php echo base_url(); ?>user/leave/approve/<?php echo $t->id; ?>"><button class="btn btn-primary">Request For Approval</button></a>
<a href="<?php echo base_url(); ?>user/leave/deny/<?php echo $t->id; ?>/<?php echo $temp->id; ?>"><button class="btn btn-primary" >Deny</button></a>
<?php endif; ?>