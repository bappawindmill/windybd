<div class="grid_12">
    <div class="module1">
        <h2><span>Leave Management</span></h2>
        <div class="module-body">
            <?php
            $pay_type = array(
                '2' => 'Without Pay',                
                '3' => 'Not Apply'
            );
            $pay_type1 = array(
                '2' => 'Without Pay',
                '3' => 'Not Apply'
            );
            echo form_open('user/leave/confirmation');
            ?>
            <input type="hidden" name="start_date" value="<?php echo $start_date; ?>" />
            <input type="hidden" name="end_date" value="<?php echo $end_date; ?>" />
            <input type="hidden" name="leave_type" value="<?php echo $leave_type; ?>" />
            <input type="hidden" name="leave_duration" value="<?php echo $leave_duration;?>" />
            <input type="hidden" name="year" value="<?php echo $year; ?>" />
            <input type="hidden" name="extra_days" value="<?php echo $extra_days; ?>" />
            <input type="hidden" name="nominated_person" value="<?php echo $nominated_person; ?>" />
            <input type="hidden" name="quarter_id" value="<?php echo $quater_id; ?>" />
            <tr ><th ></th><td>

                    <font style="color:#993333; font-weight:bold" > <?PHP echo $message; ?><br /><br/>
                    If you want to  Apply for <?php echo $extra_days; ?> Days leave By Without pay Please select without pay from the dropdown and submit your application.<br/>
                   
                    Otherwise if your are not agree to apply please select not apply and click submit. 

                    </font><br /><br />

                    <?php
                    echo form_dropdown('pay_type', $pay_type, 'pay_type', 'id="leave_type"');
                    ?>
                    <span class="notification-input ni-error"> <?php echo ucwords(form_error('pay_type')); ?></span>
                </td></tr>
                    <?php
                    echo '<tr><th></th><td>' . form_submit('mysubmit', 'Submit','class=btn-primary') . '</td></tr>';

                    echo '</table>';
                    echo form_close();
                    ?>
        </div></div></div>
<div style="clear: both;"></div>
