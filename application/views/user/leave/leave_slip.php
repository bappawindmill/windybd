
        <div>
            <h5 style="text-align: center"><b>Windmill Group</b></h5>
            <h5 style="text-align: center">Leave Report of <b><?php echo $this->session->userdata('name');?></b></h5>
            <h5 style="text-align: center">For the Year of <?php echo $year;?></h5>
        </div>
        <hr/>
        <div class="col-sm-12">
            <h5 class="pull-left"><b>Employee Name:</b> <?php echo $this->session->userdata('name');?></h5>

            <h5 class="pull-right">Year: <?php echo $year;?></h5>
        </div>
        <br/><br/>
        <h3>Leave Details</h3>
        <div class="row">
            <div class="col-sm-12">
                <div class="content-panel">
                    <!--<hr>-->
                    <div class="table-responsive">
                        <table class="table table-bordered table-hover">
                            <thead>
                            <tr class="tbl_header_bg_color">
                                <th>SL No</th>
                                <th>Start Date</th>
                                <th>End Date</th>
                                <th>Leave Type</th>
                                <th>Total Duration</th>
                                <th>Extra Days</th>
                                <th>Payment Type</th>
                            </tr>
                            </thead>
                            <tbody>
                            <?php
                            //var_dump($leave_details_info[0]);exit;
                            $i=1;
                            foreach ($leave_details_info as $temp1) {
                                ?>
                                <tr>
                                    <td><?php echo $i++;?></td>
                                    <td><?php echo $temp1->start_date;?></td>
                                    <td><?php echo $temp1->end_date;?></td>
                                    <td>
                                        <?php
                                        if ($temp1->leave_type == '1') {

                                            $a = "Casual";
                                        } elseif ($temp1->leave_type == '2') {

                                            $a = "Sick";
                                        } elseif ($temp1->leave_type == '3') {

                                            $a = "Earned";
                                        } else {
                                            $a = "";
                                        }
                                        echo $a;
                                        ?>
                                    </td>
                                    <td><?php echo $temp1->leave_duration;?></td>
                                    <td><?php echo $temp1->extra_days?></td>
                                    <td>
                                        <?php
                                        if ($temp1->pay_type == "1") {
                                            echo "With  Pay";
                                        } elseif ($temp1->pay_type == "2") {
                                            echo "Without Pay";
                                        } else {
                                            echo "";
                                        }
                                        ?>
                                    </td>
                                </tr>
                                <?php
                            }
                            ?>
                            </tbody>
                        </table>
                    </div>
                </div><! --/content-panel -->
            </div><!-- /col-md-12 -->
        </div><!-- row -->
        <br/><br/>

        <h3><i class="fa fa-angle-right"></i> Leave Summary:</h3>
        <div class="row">
            <div class="col-sm-12">
                <div class="content-panel">
                    <!--<hr>-->
                    <div class="table-responsive">
                        <table class="table table-bordered">


                            <tr><td colspan="3">
                                    <table class="table">
                                        <thead>
											<tr>
												<td colspan="2"><b>Leave Package</b></td>
												<td colspan="2"><b>Leave Spent</b></td>
												<td colspan="2"><b>Leave Balance</b></td>
											</tr>
										</thead>
										<?php
											foreach($leave_package_info as $data){
												//echo '<pre>';
												//var_dump($data);
												//echo '</pre>';
												//exit;
												$package_name=$data->package_name;
												$casual=$data->casual;
												$medical=$data->medical;
												$earned=$data->earned;
											}

											$t_casual = 0;
											$t_medical = 0;
											$t_earned = 0;
											$ex_with_pay = 0;
											$ex_without_pay = 0;
											foreach ($leave_details_info as $row4) {
												$duration = $row4->leave_duration;
												$extra_days = $row4->extra_days;
												$leave_type = $row4->leave_type;

												if ($leave_type == '1') {
													$t_casual = $t_casual + $duration;
												} else {
													$t_casual = $t_casual;
												}
												if ($leave_type == '2') {
													$t_medical = $t_medical + $duration;
												} else {
													$t_medical = $t_medical;
												}
												if ($leave_type == '3') {
													$t_earned = $t_earned + $duration;
												} else {
													$t_earned = $t_earned;
												}

												if ($row4->pay_type == "1") {
													$ex_with_pay = $ex_with_pay + $extra_days;
												} elseif ($row4->pay_type == "2") {
													$ex_without_pay = $ex_without_pay + $extra_days;
												} else {

												}
											}

											$cas_exists = $casual - $t_casual;
											if ($cas_exists < 0) {
												$cas_exists = 0;
											}
											$medical_exists = $medical - $t_medical;
											if ($medical_exists < 0) {
												$medical_exists = 0;
											}
											$earned_exists = $earned - $t_earned;
											if ($earned_exists < 0) {
												$earned_exists = 0;
											}
										?>
                                        <tr>
                                            <th class="tbl_header_bg_color">Package Name: </th>
                                            <td><?php echo $package_name; ?></td>
                                            <th></th>
                                            <td></td>
                                            <th></th>
                                            <td></td>
                                        </tr>

                                        <tr>
                                            <th class="tbl_header_bg_color">Casual Leave: </th>
                                            <td><?php echo $casual; ?></td>
                                            <th class="tbl_header_bg_color">Casual: </th>
                                            <td><?php echo $t_casual; ?></td>
                                            <th class="tbl_header_bg_color">Casual: </th>
                                            <td><?php echo $cas_exists; ?></td>
                                        </tr>
                                        <tr>
                                            <th class="tbl_header_bg_color">Sick Leave: </th>
                                            <td><?php echo $medical; ?></td>
                                            <th class="tbl_header_bg_color">Sick: </th>
                                            <td><?php echo $t_medical; ?></td>
                                            <th class="tbl_header_bg_color">Sick: </th>
                                            <td><?php echo $medical_exists; ?></td>
                                        </tr>
                                        <tr>
                                            <th class="tbl_header_bg_color">Earned Leave: </th>
                                            <td><?php echo $earned; ?></td>
                                            <th class="tbl_header_bg_color">Earned: </th>
                                            <td><?php echo $t_earned; ?></td>
                                            <th class="tbl_header_bg_color">Earned: </th>
                                            <td><?php echo $earned_exists; ?></td>
                                        </tr>

                                        <tr>
                                            <th>Total:</th>
                                            <td><?php echo $casual + $medical + $earned; ?></td>
                                            <th>Total:</th>
                                            <td><?php echo $t_casual + $t_medical + $t_earned; ?></td>
                                            <th>Total:</th>
                                            <td><?php echo $cas_exists + $medical_exists + $earned_exists; ?></td>
                                        </tr>
                                    </table></td>
                            </tr>
                        </table>
                    </div>
                </div><! --/content-panel -->
            </div><!-- /col-md-12 -->
        </div><!-- row -->
        <br/><br/>

        <!--<div class="row">
            <div class="col-sm-12">
                <div class="content-panel">

                    <div class="table-responsive">
                        <table class="table">
                            <thead>
                            <tr>
                                <th class="tbl_header_bg_color">Total With Pay :</th>
                                <td><?php /*echo $ex_with_pay;*/?> Days</td>
                            </tr>
                            </thead>
                            <tbody>

                                <tr>
                                    <th class="tbl_header_bg_color">Total without Pay :</th>
                                    <td><?php /*echo $ex_without_pay;*/?> Days</td>
                                </tr>

                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
        </div>-->
        <br/><br/>

