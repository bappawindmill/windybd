<?php

class Attendence_model extends CI_Model {

    function Attendence_model() {
        parent::__construct();
    }

    function get_attendence_request($id) {
        $this->db->select('*');
        $this->db->from('attendence_request');
        $this->db->where('supervisor_id',$id);
        $this->db->where('sup_status','1');
        $query=$this->db->get();
        return $query->result();
    }

	function get_emp_info($id) {
        $this->db->select('*');
        $this->db->from('employeeinfo');
        $this->db->where('id',$id);
        $query=$this->db->get();
        return $query->result();
    }

    function checkSubmission($year, $month, $empId) {
        $this->db->select('*');
        $this->db->from('Attendence_emp');
        $this->db->where('emp_id',$empId);
        $this->db->where('year',$year);
        $this->db->where('month',$month);
        $query=$this->db->get();
        return $query->result();
    }

    function getEmpInfo($empId) {
        $this->db->select('*');
        $this->db->from('employeeinfo');
        $this->db->where('id',$empId);
        $query=$this->db->get();
        return $query->result();
    }

    function getSchedule($duty_schedule) {
        $this->db->select('*');
        $this->db->from('schedule');
        $this->db->where('id',$duty_schedule);
        $query=$this->db->get();
        return $query->result();
    }

    function getTimeInLog($cardNo, $sdate) {
        $this->db->select('min(timein_int) as pstime');
        $this->db->from('Attendence');
        $this->db->where('employee_id',$cardNo);
        $this->db->where('att_date',$sdate);
        $query=$this->db->get();
        return $query->result();
    }

    function getTimeOutLog($cardNo, $sdate) {
        $this->db->select('max(timein_int) as pstime_1');
        $this->db->from('Attendence');
        $this->db->where('employee_id',$cardNo);
        $this->db->where('att_date',$sdate);
        $query=$this->db->get();
        return $query->result();
    }

    function getTimeLogInfo($empId, $sdate) {
        $this->db->select('*');
        $this->db->from('Attendence_emp');
        $this->db->where('emp_id',$empId);
        $this->db->where('att_date',$sdate);
        $query = $this->db->get();
        return $query->result();
    }

    function getHoliday($sdate) {
        $this->db->select('*');
        $this->db->from('add_holiday');
        $this->db->where('start_date',$sdate);
        $query=$this->db->get();
        return $query->result();
    }

    function getWeekend($sdate, $branch) {
        $this->db->select('*');
        $this->db->from('add_weekend');
        $this->db->where('branch_name',$branch);
        $this->db->where('weekend_date',$sdate);
        $query=$this->db->get();
        return $query->result();
    }

    function getLeaveInfo($sdate, $empId) {
        $this->db->select('*');
        $this->db->from('leave_management');
        $this->db->where('emp_id',$empId);
        $this->db->where('start_date >=',$sdate );
        $this->db->where('end_date <=',$sdate );
        $this->db->where('hr_status','2');
        $query=$this->db->get();
        return $query->result();
    }

    function getDailyLogInfo($sdate, $empId) {
        $this->db->select('*');
        $this->db->from('DailyLog');
        $this->db->where('emp_id',$empId);
        $this->db->where('log_date',$sdate);
        $query=$this->db->get();
        return $query->result();
    }

    function getSubmitInfo() {
        $this->db->select('*');
        $this->db->from('submit_control');
        $query=$this->db->get();
        return $query->result();
    }

    function getTimeLog($empId, $sdate) {
        $this->db->select('*');
        $this->db->from('Attendence_emp');
        $this->db->where('emp_id',$empId);
        $this->db->where('att_date',$sdate );
        $query=$this->db->get();
        return $query->result();
    }

    function attendenceInsert($AttData) {
        $this->db->insert('Attendence_emp', $AttData);
    }

    function getEmployeeInfo($emp_id) {
        $this->db->select('*');
        $this->db->from('employeeinfo');
        $this->db->where('id',$emp_id);
        $query=$this->db->get();
        return $query->result();
    }

    function att_request_insert($requestData) {
        $this->db->insert('attendence_request', $requestData);
    }

    function updateAttendence($AttData, $emp_id, $att_date) {
        $cond = "emp_id = '$emp_id'  and att_date = '$att_date'";
        $this->db->where($cond);
        $this->db->update("Attendence_emp", $AttData);
    }

    function updateSupStatus($supStatus, $year, $month, $emp_id) {
        $cond = "emp_id = '$emp_id'  and year = '$year' and month = '$month'";
        $this->db->where($cond);
        $this->db->update("attendence_request", $supStatus);
    }

    function viewDailyLog($empId) {
        $limit  = $this->pagination_library->per_page;
        $search = $this->input->get('search');
        $start  = ($this->input->get('start')) ? $this->input->get('start') : 0;
        $search_q = ($search) ? "?search=".$search : '';
        $this->db->where('emp_id',$empId);
        $this->db->like('log_date',$search);
        $this->db->limit($limit,$start);
        $this->db->order_by('id','desc');
        $query = $this->db->get('DailyLog');
        $this->db->where('emp_id',$empId);
        $this->db->like('log_date',$search);
        $this->db->limit(0);
        $this->pagination_library->total_rows =  $this->db->count_all_results("DailyLog");
        $this->pagination_library->base_url = site_url('user/attendence/addLog').$search_q;
        return $query->result();
    }

    function DailyLogInsert($data) {
        $this->db->insert('DailyLog', $data);
    }

    function deleteDailyLog($id) {
        $this->db->delete('DailyLog', array('id' => $id));
    }


    // for hr
    function getEmployeeInfoByCardHr($card_no) {
        $query = $this->db->get_where('employeeinfo', array('card_no' => $card_no));
        return $query->result();
    }

    function getTimeInLogHr($cardNo, $sdate) {
        // $cond = "card_no = '$cardNo' and att_date = '$sdate'";
        // $this->db->select('top(1)*');
        //$this->db->select('*');
        // $this->db->from('attendence');
        //$this->db->order_by('id ASC');
        //$this->db->where($cond);
        // $query = $this->db->get();
        $query = $this->db->query("select * from Attendence where employee_id='" . $cardNo . "'and att_date='" . $sdate . "' order by id ASC LIMIT 1");
        // $query = $this->db->query("select * from Attendence where card_no='" . $cardNo . "'and att_date='" . $sdate . "' order by id ASC LIMIT 1");

        return $query->result();
    }

    function getTimeOutLogHr($cardNo, $sdate) {
        //$cond = "card_no = '$cardNo' and att_date = '$sdate'";
        // $this->db->select('top(1)*');
        //$this->db->from('attendence');
        // $this->db->order_by('id DESC');
        // $this->db->where($cond);
        // $query = $this->db->get();
        $query = $this->db->query("select * from Attendence where employee_id='" . $cardNo . "'and att_date='" . $sdate . "' order by id DESC LIMIT 1");
        // $query = $this->db->query("select * from Attendence where card_no='" . $cardNo . "'and att_date='" . $sdate . "' order by id DESC LIMIT 1");
        return $query->result();
    }


}
