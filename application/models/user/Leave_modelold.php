<?php

class Leave_model extends CI_Model
{

    function report_leave_details($emp_id, $year) {
        $this->db->select('*');
        $this->db->from('leave_management');
        $this->db->where('emp_id',$emp_id);
		$this->db->where('year',$year);
		$this->db->where('hr_status',2);
        $query=$this->db->get();
        return $query->result();
    }

    function report_leave_package($emp_id) {
        $this->db->select('leave_name');
        $this->db->from('employeeinfo');
        $this->db->where('id',$emp_id);
        $query=$this->db->get();
        return $query->result();
    }

    function all_leave_package_info($leave_package_id) {
        $this->db->select('*');
		$this->db->from('add_leave');
		$this->db->where('id',$leave_package_id);
        $query=$this->db->get();
        return $query->result();
    }

    function get_schedule() {
        $this->db->select('*');
        $this->db->from('leave_schedule');
        $query=$this->db->get();
        return $query->result();
    }

    function show_leave_status($emp_id){
        $this->db->select('*');
        $this->db->from('leave_management');
        $this->db->where('emp_id',$emp_id);
        $query=$this->db->get();
        return $query->result();
    }

    function view_leave_application_request($emp_id){
        $this->db->select('emp.name,emp.card_no,lm.id,lm.leave_type,lm.start_date,lm.end_date,lm.leave_duration,lm.emp_id');
        $this->db->from('leave_management as lm');
        $this->db->join('employeeinfo as emp', 'emp.id = lm.emp_id', 'left');
        $this->db->where('emp.supervisor',$emp_id);
        $this->db->where('lm.supervisor_status','1');
        $this->db->where('lm.hr_status','1');
        $this->db->order_by('lm.id', 'ASC');
        $query=$this->db->get();
        return $query->result();
    }

    function get_deg_info(){
        $this->db->select('*');
        $this->db->from('designation');
        $query=$this->db->get();
        return $query->result();
    }

    function get_supInfo_for_leave_apply($id){
        $this->db->select('*');
        $this->db->from('employeeinfo');
        $this->db->where('designation',$id);
        $query=$this->db->get();
        return $query->result();
    }

    function view_holiday(){

        $limit  = $this->pagination_library->per_page;
        $search = $this->input->get('search');
        $start  = ($this->input->get('start')) ? $this->input->get('start') : 0;
        $search_q = ($search) ? "?search=".$search : '';
        $this->db->like('title',$search);
        $this->db->limit($limit,$start);
        $this->db->order_by('id','desc');
        $query = $this->db->get('add_holiday');

        $this->db->like('title',$search);
        $this->db->limit(0);
        $this->pagination_library->total_rows =  $this->db->count_all_results("add_holiday");
        $this->pagination_library->base_url = site_url('user/leave/holiday').$search_q;
        return $query->result();
    }

    function view_weekend(){

        $limit  = $this->pagination_library->per_page;
        $search = $this->input->get('search');
        $start  = ($this->input->get('start')) ? $this->input->get('start') : 0;
        $bName  = ($this->session->userdata('bName')) ? $this->session->userdata('bName') : '';
        //var_dump($bName);exit;
        $search_q = ($search) ? "?search=".$search : '';
        $this->db->where('branch_name',$bName);
        $this->db->like('weekend_date',$search);
        $this->db->limit($limit,$start);
        $this->db->order_by('id','desc');
        $query = $this->db->get('add_weekend');
        
        $this->db->where('branch_name',$bName);
        $this->db->like('weekend_date',$search);
        $this->db->limit(0);
        $this->pagination_library->total_rows =  $this->db->count_all_results("add_weekend");
        $this->pagination_library->base_url = site_url('user/leave/weekend').$search_q;
        return $query->result();
        
        /*$this->db->select('*');
        $this->db->from('add_weekend');
        $query = $this->db->get();
        return $query->result();*/
    }

    function get_holiday_info_for_leave_application($start_date,$end_date){
        $this->db->select('*');
        $this->db->from('add_holiday');
        $this->db->where('start_date >=',$start_date);
        $this->db->where('start_date <=',$end_date);
        $query=$this->db->get();
        return $query->result();
    }
    
    function get_weekend_info_for_leave_application($start_date,$end_date,$branch_name){
        $this->db->select('*');
        $this->db->from('add_weekend');
        $this->db->where('weekend_date >=',$start_date);
        $this->db->where('weekend_date <=',$end_date);
        $this->db->where('branch_name <=',$branch_name);
        $query=$this->db->get();
        return $query->result();
    }

    function get_category_info_for_leave_application($id){
        $this->db->select('*');
        $this->db->from('employeeinfo');
        $this->db->where('id',$id);
        $query=$this->db->get();
        return $query->result();
    }

    function get_leave_duration_info_for_leave_application($syear,$leave_type,$id){
        $this->db->select('*');
        $this->db->from('leave_management');
        $this->db->where('year',$syear);
        $this->db->where('leave_type',$leave_type);
        $this->db->where('emp_id',$id);
        $this->db->where('hr_status','2');
        $query=$this->db->get();
        return $query->result();
    }

    function get_leave_package_info_for_leave_application($leavePackage){
        $this->db->select('*');
        $this->db->from('add_leave');
        $this->db->where('id',$leavePackage);
        $query=$this->db->get();
        return $query->result();
    }

    function get_leave_schedule_info_for_leave_application($start_date){
        $this->db->select('*');
        $this->db->from('leave_schedule');
        $this->db->where('start_date >=',$start_date);
        $this->db->where('end_date <=',$start_date);
        $query=$this->db->get();
        return $query->row();
    }

    function get_leaveSchedule1_info_for_leave_application($end_date){
        $this->db->select('*');
        $this->db->from('leave_schedule');
        $this->db->where('start_date >=',$end_date);
        $this->db->where('end_date <=',$end_date);
        $query=$this->db->get();
        return $query->result();
    }

    function get_firstQuarter_info_for_leave_application($syear,$cNo,$ltype){
        $this->db->select('*');
        $this->db->from('leave_management');
        $this->db->where('year',$syear);
        $this->db->where('emp_id',$cNo);
        $this->db->where('leave_type',$ltype);
        $this->db->where('quarter_id','1');
        $this->db->where('hr_status','2');
        $query=$this->db->get();
        return $query->result();
    }

    function get_secondQuarter_info_for_leave_application($syear,$cNo,$ltype){
        $this->db->select('*');
        $this->db->from('leave_management');
        $this->db->where('year',$syear);
        $this->db->where('emp_id',$cNo);
        $this->db->where('leave_type',$ltype);
        $this->db->where('quarter_id','2');
        $this->db->where('hr_status','2');
        $query=$this->db->get();
        return $query->result();
    }

    function get_thirdQuarter_info_for_leave_application($syear,$cNo,$ltype){
        $this->db->select('*');
        $this->db->from('leave_management');
        $this->db->where('year',$syear);
        $this->db->where('emp_id',$cNo);
        $this->db->where('leave_type',$ltype);
        $this->db->where('quarter_id','3');
        $this->db->where('hr_status','2');
        $query=$this->db->get();
        return $query->result();
    }

    function get_emp_info1($nid) {
        $this->db->select('*');
        $this->db->from('employeeinfo');
        $this->db->where('id',$nid);
        $query=$this->db->get();
        return $query->result();
    }

    function get_emp($empId){
        $this->db->select('*');
        $this->db->from('employeeinfo');
        $this->db->where('id',$empId);
        $query=$this->db->get();
        return $query->result();
    }

    function get_application_info($id) {
        $this->db->select('*');
        $this->db->from('leave_management');
        $this->db->where('id', $id);
        $query = $this->db->get();
        return $query->result();
    }
    
    function get_department_info($dept_id){
        $this->db->select('name');
        $this->db->from('add_department');
        $this->db->where('id', $dept_id);
        $query = $this->db->get();
        return $query->result();
    }

    function get_designation_info($desig_id){
        $this->db->select('designation');
        $this->db->from('designation');
        $this->db->where('id', $desig_id);
        $query = $this->db->get();
        return $query->result();
    }

    function get_leave_manage($id) {
        $this->db->select('*');
        $this->db->from('leave_management');
        $this->db->where('id', $id);
        $query = $this->db->get();
        return $query->result();
    }

    function update_leave_management($data, $id) {
        $this->db->where('id', $id);
        $this->db->update('leave_management', $data);
    }

    function password_update($data, $id) {
        $this->db->where('id', $id);
        $this->db->update('employeeinfo', $data);
    }
}