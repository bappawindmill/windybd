<?php

/*
 * ***********************************************************************************************************
 * 																											*
 * 		Authentication library for Code Igniter.															*
 * 		@File Type		Model Public Class																	*
 * 		@Author			Kazi Sanchoy Ahmed (B.Sc in CSE)													*
 * 		@Email			sanchoy7@gmail.com		                                     						*
 * 		@Profession		Web Application Developer & Programmer												*
 * 		@Based on		CL Auth by Jason Ashdown (http://http://www.jasonashdown.co.uk/)					*
 * 																											*
 * ***********************************************************************************************************
 */

class Shows extends CI_Model {

    function Shows() {
        parent::__construct();
        $this->_prefix = $this->config->item('DX_table_prefix');
        //$this->_branch_table = $this->_prefix.$this->config->item('DX_branch_table');	
    }

    function getThisValue($cond = FALSE, $tableName = '', $limit = '', $column_name = '', $order = 'id ASC') {
        $tableName = $this->_prefix . $tableName;
        $this->db->select($column_name, FALSE);
        if ($cond) {
            $this->db->where($cond);
        }
        $this->db->order_by($order);
        return $this->db->get($tableName, $limit)->result();
    }

    function getAll($cond = FALSE, $tableName = '', $limit = '', $order = 'id ASC') {
        $tableName = $this->_prefix . $tableName;
        $this->db->select(" * ", FALSE);
        if ($cond) {
            $this->db->where($cond);
        }
        $this->db->order_by($order);
        return $this->db->get($tableName, $limit)->result();
    }

    function EntryAll($tableName = '', $tabledata = '') {
        $this->db->insert($tableName, $tabledata);
    }

    function UpdateAll($cond = '', $tableName = '', $tabledata = '') {
        $this->db->where($cond);
        $this->db->update($tableName, $tabledata);
    }

    function DeleteAll($cond = '', $tableName = '') {
        $this->db->where($cond);
        $this->db->delete($tableName);
    }

    function getReceivedWorkOrderProduct($contentId) {
        $this->db->select("*");
        $this->db->from('received_work_order_product');
        $this->db->join('product', 'product.product_id = received_work_order_product.product_name', 'left');
        $this->db->where('received_work_order_product.work_order_id', $contentId);
        return $this->db->get()->result();
    }
	 function insertThisValue($data = '', $tableName = '') {
        $tableName = $this->_prefix . $tableName;
        //$data['created'] = date('Y-m-d  H:i:s', time());
        return $this->db->insert($tableName, $data);
    }
	  function deleteThisValue($cond = FALSE, $tableName = '') {
        $this->db->where($cond);
        return $this->db->delete($tableName);
    }

    function updateThisValue($data = '', $tableName = '', $cond = '') {
        $tableName = $this->_prefix . $tableName;
        $this->db->where($cond);
        return $this->db->update($tableName, $data);
    }

}
