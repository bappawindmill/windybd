<?php

class Leave_report_model extends CI_Model {

function Doc_model()
	{
		parent::__construct();    
		$this->load->helper('url');           
	}
	function employee_doc_info_insert($data)
	{
		$this->db->insert('doc',$data);
	}
	function view_employee_doc_info($start=0,$end=0)
	{
 		$this->load->database();
		$this->db->limit($end,$start);
		$this->db->order_by('id','desc');
  		$query = $this->db->get('doc');
  		return $query->result();
	} 
	function get_employee_doc_info($id)
	{
 		$this->load->database();
		//$this->db->limit($end,$start);
		//$this->db->order_by('id','desc');
  		$query = $this->db->get_where('doc', array('id'=>$id));
  		return $query->row_array();
	} 
	function getdata($id)
	{
 		$this->load->database();
		//$this->db->limit($end,$start);
		//$this->db->order_by('id','desc');
  		$query = $this->db->get_where('doc', array('id'=>$id));
  		return $query->result();
	} 
	function employee_doc_info_update($data)
	{
  		$this->db->where('id',$this->input->post('id'));
 		$this->db->update('doc',$data);  
	} 
	function salary_insert($data)
	{
		$this->db->insert('salary_package',$data);
	}
	function employee_exp_insert($data)
	{
		$this->db->insert('experience',$data);
	}
	function employee_exp_update($data2,$eid)
	{
  		$this->db->where('id',$eid);
 		$this->db->update('experience',$data2);  
	} 
	function salary_update($data,$id)
	{
  		$this->db->where('id',$id);
 		$this->db->update('salary',$data);  
	} 
	function delete_employee_general_info($id)
	{
 		$this->db->delete('employee_general_info',array('id' => $id)); 
		
	}
	
	function get_salary($id)
	{
 		$this->load->database();
		//$this->db->limit($end,$start);
		//$this->db->order_by('id','desc');
  		$query = $this->db->get_where('salary', array('id'=>$id));
  		return $query->row_array();
	} 
	/*start necessary function for employee report*/
	function emp_detail($id)
	{
 		$this->load->database();
  		$query = $this->db->get_where('employeeinfo', array('id' => $id));
  		return $query->result();
	} 

}	