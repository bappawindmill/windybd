<?php
 class Tax_model extends CI_Model {

	function Tax_model()
	{
		parent::__construct();    
		$this->load->helper('url');           
	}
   function tax_insert($data)
	{
		$this->db->insert('tax',$data);
	}
	function get_tax($id)
	{	
		$query = $this->db->get_where('tax',array('id'=>$id));
  		return $query->row_array();
	}
	
	function view_tax($start=0,$end=0)
	{
 		$this->load->database();
		$this->db->limit($end,$start);
		$this->db->order_by('id','desc');
  		$query = $this->db->get('tax');
  		return $query->result();
	} 
	function edit_tax($data)
	{
  		$this->db->where('id',$this->input->post('id'));
 		$this->db->update('tax',$data);  
	}
	function delete_tax($id)
	{
 		$this->db->delete('tax',array('id' => $id)); 
		//$this->db->delete('experience',array('emp_id' => $id)); 
	}

	
}
?>