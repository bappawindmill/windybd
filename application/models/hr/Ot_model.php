<?php
class Ot_model extends CI_Model {

    function Ot_model() {
        parent::__construct();
        $this->load->helper('url');
    }
    function get_all($branch_name,$dept) {
        $query = $this->db->query("select * from employeeinfo where dept='" . $dept . "' and branch_name='" . $branch_name . "'");
        return $query->result();
    }
	 function getData($cardNo) {
        $query = $this->db->query("select * from employeeinfo where card_no='" . $cardNo . "'");
        return $query->result();
    }

	function update_ot($data,$cond){
	     $this->db->where($cond);
        $this->db->update('ot_sheet', $data);
	}
	function insert_advance($data){
	   $this->db->insert('advance', $data);
	}
	function update_arrear($data,$cond){
	     $this->db->where($cond);
        $this->db->update('arrear', $data);
	}
	function insert_arrear($data){
	   $this->db->insert('arrear', $data);
	}
	function update_kpi($data,$cond){
	     $this->db->where($cond);
        $this->db->update('kpi', $data);
	}
	function insert_ot($data){
	   $this->db->insert('ot_sheet', $data);
	}
}
