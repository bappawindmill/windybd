<?php

class Roster_model extends CI_Model {

    function Roster_model() {
        parent::__construct();
        $this->load->helper('url');
    }

    function shift_insert($data) {
        $this->db->insert('shift', $data);
    }

    function duty_schedule_insert($data) {
        $this->db->insert('duty_schedule', $data);
    }

    function view_shift($start = 0, $perPage = 0) {
        $query = $this->db->query("select * from shift");
        return $query->result();
    }

    function view_schedule($start = 0, $perPage = 0) {
        $query = $this->db->query("select * from duty_schedule");
        return $query->result();
    }

    function get_id($id) {
        $query = $this->db->get_where('shift', array('id' => $id));
        return $query->row_array();
    }

    function get_duty_schedule($id) {
        $query = $this->db->get_where('duty_schedule', array('id' => $id));
        return $query->result();
    }

    function get_all_shift($id) {
        $query = $this->db->get_where('shift', array('id' => $id));
        return $query->result();
    }

    function get_all() {
        $id = $this->input->post("dept");
        $branch_name = $this->input->post("branch_name");
        $proffession = $this->input->post("proffession");
        $query = $this->db->query("select * from employeeinfo where dept='" . $id . "' and branch_name='" . $branch_name . "'and proffession='" . $proffession . "'");
        return $query->result();
    }

    function get_all_emp($branch, $dept, $proffession) {

        $query = $this->db->query("select * from employeeinfo where dept='" . $dept . "' and branch_name='" . $branch . "'and proffession='" . $proffession . "'");
        return $query->result();
    }

    function get_schedule() {
        $branch_name = $this->input->post("branch_name");
        $dept = $this->input->post("dept");
        $section = $this->input->post("proffession");
        $shift_name = $this->input->post("shift_name");
        $query = $this->db->query("select * from duty_schedule where dept='" . $dept . "' and branch_name='" . $branch_name . "'and section='" . $section . "' and shift_name='" . $shift_name . "'");
        return $query->result();
    }

    function edit_shift($data) {
        $this->db->where('id', $this->input->post('id'));
        $this->db->update('shift', $data);
    }

    function delete_shift($id) {
        $this->db->delete('shift', array('id' => $id));
    }

    function delete_schedule($id) {
        $this->db->delete('duty_schedule', array('id' => $id));
    }

    function update_shift($data, $id) {
        $this->db->where('id', $id);
        $this->db->update('employeeinfo', $data);
    }

    function duty_schedule_update($data, $id) {

        $this->db->where('id', $id);
        $this->db->update('duty_schedule', $data);
    }

}
