<?php

class Salary_model extends CI_Model {

    function Salary_model() {
        parent::__construct();
        $this->load->helper('url');
    }

    function view_salary_payment1($id) {
        $query = $this->db->get_where('monthly_salary', array('id' => $id));
        return $query->result();
    }

    function view_salary($start = 0, $end = 0) {
        $this->load->database();
        $this->db->limit($end, $start);
        $this->db->order_by('id', 'desc');
        //$query = $this->db->get('salary');
        $query = $this->db->query('SELECT e.name as ename, s.* FROM employeeinfo as e, salary as s WHERE s.emp_id = e.id ORDER BY id DESC');
        return $query->result();
    }

    function view_salary_payment($start = 0, $end = 0) {
        $this->load->database();
        $this->db->limit($end, $start);
        $this->db->order_by('id', 'desc');
        //$query = $this->db->get('salary_payment');
        $query = $this->db->query('SELECT e.name as ename, s.* FROM employeeinfo as e, salary_payment as s WHERE s.emp_id = e.id ORDER BY id DESC');
        return $query->result();
    }

    function salary_entry($data) {
        $this->db->insert('salary', $data);
    }

    function general_salary_insert($data) {
        $this->db->insert('monthly_salary', $data);
    }

    function pf_amount_entry($data1) {
        $this->db->insert('pf_amount', $data1);
    }

    function get_salary($id) {
        $this->load->database();
        //$this->db->limit($end,$start);
        //$this->db->order_by('id','desc');
        $query = $this->db->get_where('salary', array('id' => $id));
        return $query->row_array();
    }

    function salary_update($data, $id) {
        $this->db->where('id', $id);
        $this->db->update('salary', $data);
    }

    function pf_amount_update($data) {
        $this->db->where('emp_id', $this->input->post('emp_id'));
        $this->db->where('year', $this->input->post('year'));
        $this->db->where('month', $this->input->post('month'));
        $this->db->update('pf_amount', $data);
    }

    function delete_salary($id) {
        $this->db->delete('salary', array('id' => $id));
        /* $this->db->delete('pf_amount',array(
          'year' => $year,'month'=>$month)); */
    }

    function view_department($start = 0, $end = 0) {
        $this->load->database();
        $this->db->limit($end, $start);
        $this->db->order_by('id', 'desc');
        $query = $this->db->get('salary');
        return $query->result();
    }

    function get_salary_pay($id) {
        $this->load->database();
        //$this->db->limit($end,$start);
        //$this->db->order_by('id','desc');
        $query = $this->db->get_where('salary', array('id' => $id));
        return $query->row_array();
    }

    function salary_payment_entry($data) {
        $this->db->insert('salary_payment', $data);
    }

    function salary_echeck() {
        $this->load->database();
        $query = $this->db->get_where('salary', array('year' => $this->input->post('year'),
            'month' => $this->input->post('month'),
            'dept' => $this->input->post('dept'),
            'emp_id' => $this->input->post('emp_id')
                )
        );
        if ($query->num_rows() == 0) {
            return true;
        } else {
            return false;
        }
    }

    function check_pf($emp_id, $month, $year) {
        $query = $this->db->query("select * from pf_amount where emp_id='" . $emp_id . "'and month='" . $month . "'and year='" . $year . "'");


        if ($query->num_rows() > 0) {
            return true;
        } else {
            return false;
        }
    }

    function monthly_salary() {
        $this->load->database();
        $query = $this->db->get_where('salary', array('year' => $this->input->post('year'),
            'month' => $this->input->post('month'),
            'dept' => $this->input->post('dept')
                )
        );
        return $query->result();
    }

    function pay_slip() {
        $this->load->database();
        //$year = $this->input->post('year');
        //$month = $this->input->post('month');
        //$emp_id = $this->input->post('emp_id');
        $query = $this->db->get_where('salary', array('year' => $this->input->post('year'),
            'month' => $this->input->post('month'),
            'dept' => $this->input->post('dept'),
            'emp_id' => $this->input->post('emp_id')
                )
        );
        //$query = $this->db->query('SELECT e.name as ename, s.*, p.* FROM employeeinfo as e, salary_package as p, salary as s WHERE s.emp_id = $emp_id AND e.id = $emp_id AND e.salary_package = p.id AND s.year=$year, s.month=$month');
        return $query->result();
    }

    function pay_bank_slip() {
        $this->load->database();
        $branch_name = $this->input->post('branch_name');
        $dept = $this->input->post('dept');
        $bank_name = $this->input->post('bank_name');
        $bank_branch = $this->input->post('bank_branch');
        $query = $this->db->query("SELECT * FROM employeeinfo  WHERE branch_name ='" . $branch_name . "'and dept='" . $dept . "'and bank_name='" . $bank_name . "'and bank_branch='" . $bank_branch . "'");
        return $query->result();
    }

    function get_all_empinfo() {
        $branch_name = $this->input->post("branch_name");
        $dept = $this->input->post("dept");
        $proffession = $this->input->post("proffession");
        $query = $this->db->query("SELECT * FROM employeeinfo  WHERE branch_name ='" . $branch_name . "'and dept='" . $dept . "'and proffession='" . $proffession . "'");
        return $query->result();
    }

    function check_duty_schedule($shift_name) {
        $branch_name = $this->input->post("branch_name");
        $dept = $this->input->post("dept");
        $proffession = $this->input->post("proffession");
        $shift_name = $shift_name;
        $query = $this->db->query("select * from duty_schedule where dept='" . $dept . "' and branch_name='" . $branch_name . "'and section='" . $proffession . "' and shift_name='" . $shift_name . "'");
        return $query->result();
    }

    function get_monthly_working_hour($card_no) {
        $year = $this->input->post("year");
        $month = $this->input->post("month");
        $start_date = $year . "-" . $month . "-01";
        $end_date = $year . "-" . $month . "-31";
        $card_no = $card_no;
        $query = $this->db->query("select * from attendence where card_no='" . $card_no . "'and att_date>='" . $start_date . "'and att_date<='" . $end_date . "'");
        return $query->result();
    }

    function view_salary_hourly() {
        $branch_name = $this->input->post("branch_name");
        $dept = $this->input->post("dept");

        $month = $this->input->post("month");
        $year = $this->input->post("year");
        $query = $this->db->query("select * from monthly_salary where dept='" . $dept . "' and branch_name='" . $branch_name . "' and month='" . $month . "'and year='" . $year . "'");
        return $query->result();
    }

    function view_slip() {

        $year = $this->input->post('year');
        $month = $this->input->post('month');
        $query = $this->db->get_where('monthly_salary', array('year' => $this->input->post('year'),
            'month' => $this->input->post('month'),
                )
        );
        return $query->result();
    }

    function dept_pay_slip($dept, $year, $month) {
        $query = $this->db->query("select * from monthly_salary where dept='" . $dept . "' and month='" . $month . "'and year='" . $year . "'");
        return $query->result();
    }

    function proffession_pay_slip($proffession, $year, $month) {
        $query = $this->db->query("select * from monthly_salary where section='" . $proffession . "' and month='" . $month . "'and year='" . $year . "'");
        return $query->result();
    }

    function card_pay_slip($card_no, $year, $month) {
        $query = $this->db->query("select * from monthly_salary where card_no='" . $card_no . "' and month='" . $month . "'and year='" . $year . "'");
        return $query->result();
    }

    function pay_slip_all($year, $month) {
        $query = $this->db->query("select * from monthly_salary where  month='" . $month . "'and year='" . $year . "'");
        return $query->result();
    }

    function getArrear($emp_id, $year, $month) {
        $cond = "emp_id = '$emp_id' and month = '$month' and year= '$year'";
        $query = $this->db->get_where('arrear', $cond);
        return $query->result();
    }

    function getKpi($emp_id, $year, $month) {
        $cond = "emp_id = '$emp_id' and month = '$month' and year= '$year'";
        $query = $this->db->get_where('kpi', $cond);
        return $query->result();
    }

    function getKpi1($emp_id, $year, $month) {
        $cond = "emp_id = '$emp_id' and month = '$month' and year= '$year'";
        $query = $this->db->get_where('kpi1', $cond);
        return $query->result();
    }

    function getAdvance($emp_id, $year, $month) {
        $cond = "emp_id = '$emp_id' and month = '$month' and year= '$year'";
        $query = $this->db->get_where('advance', $cond);
        return $query->result();
    }

    function get_loan_info($emp_id) {
        $cond = "emp_id = '$emp_id' and status=0";
        $query = $this->db->get_where('loan', $cond);
        return $query->result();
    }

    function loanInfo($emp_id, $year, $month) {
        $cond = "emp_id = '$emp_id' and year = '$year' and month = '$month'";
        $query = $this->db->get_where('loan_deduction', $cond);
        return $query->result();
    }

    function getPfInfo($emp_id, $year, $month) {
        $cond = "emp_id = '$emp_id' and year = '$year' and month = '$month'";
        $query = $this->db->get_where('pf_amount', $cond);
        return $query->result();
    }

    function getPfRate() {
        $query = $this->db->get("provident_fund");
        return $query->result();
    }

    function getDeptWiseSalary($dept, $year, $month) {
        $cond = "dept = '$dept' and year = '$year' and month = '$month'";
        $this->db->order_by('card_no asc');
        $query = $this->db->get_where('monthly_salary', $cond);
        return $query->result();
    }
    function getBranchWiseSalary($branch, $year, $month) {
        $cond = "branch_name = '$branch' and year = '$year' and month = '$month'";
        $this->db->order_by('branch_name asc,card_no asc');
        $query = $this->db->get_where('monthly_salary', $cond);
        return $query->result();
    }

    function getEmpInfo($emp_id) {
        $cond = "id = '$emp_id' ";
        $query = $this->db->get_where('employeeinfo', $cond);
        return $query->result();
    }

    function getDesignation($deg) {
        $cond = "id = '$deg' ";
        $query = $this->db->get_where('designation', $cond);
        return $query->result();
    }

    function getallDept($branch) {
        $cond = "branch_name = '$branch'";
        $query = $this->db->get_where('add_department', $cond);
        return $query->result();
    }

    function get_ot_info($id, $year, $month) {
        $cond = "emp_id = '$id' and year = '$year' and month = '$month'";
        $query = $this->db->get_where('ot_sheet', $cond);
        return $query->result();
    }

}

