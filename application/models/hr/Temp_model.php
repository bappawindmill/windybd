<?php

class Temp_model extends CI_Model {

    function Temp_model() {
        parent::__construct();
        $this->load->helper('url');
    }

    function getallAcademic($id) {
        $query = $this->db->get_where('academic', array('id' => $id));
        return $query->result();
    }

    function getallQualification($id) {
        $query = $this->db->get_where('qualification', array('id' => $id));
        return $query->result();
    }

    function getallHistory($id) {
        $query = $this->db->get_where('career_history', array('id' => $id));
        return $query->result();
    }

    function getallAppraisal($id) {
        $query = $this->db->get_where('appraisal', array('id' => $id));
        return $query->result();
    }

    function employee_info_update($data, $id) {
        $this->db->where('id', $id);
        $this->db->update('employeeinfo', $data);
    }

    function delete_academic($id) {
        $this->db->delete('academic', array('id' => $id));
    }

    function delete_appraisal($id) {
        $this->db->delete('appraisal', array('id' => $id));
    }

    function delete_history($id) {
        $this->db->delete('career_history', array('id' => $id));
    }

    function delete_qualification($id) {
        $this->db->delete('qualification', array('id' => $id));
    }

    function delete_doc($id) {
        $this->db->delete('doc', array('id' => $id));
    }

}

