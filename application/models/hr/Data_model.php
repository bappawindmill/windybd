<?php

class Data_model extends CI_Model {

    function Data_model() {
        parent::__construct();
        $this->load->helper('url');
    }

    function attendence_insert($data) {
        $this->db->insert('attendence', $data);
    }

    function view_attendence($proffession) {
        $query = $this->db->query("select * from employeeinfo where proffession='" . $proffession . "'");
        return $query->result();
    }

    function view_attendence1($card_no) {
        $query = $this->db->query("select * from employeeinfo where card_no='" . $card_no . "'");
        return $query->result();
    }

    function get_monthly_working_hour1() {

        $start_date = $this->input->post("start_date");
        $end_date = $this->input->post("end_date");
        $card_no = $this->input->post("card_no");
        $query = $this->db->query("select * from attendence where card_no='" . $card_no . "'and att_date>='" . $start_date . "'and att_date<='" . $end_date . "'");
        return $query->result();
    }

    function get_existing_data($a_date) {
        $query = $this->db->query("select * from attendence where att_date='" . $a_date . "'");
        return $query->result();
    }
    function get_emp($card_no){
	    $query = $this->db->query("select * from employeeinfo where card_no='" . $card_no . "'");
        return $query->result();

	}
	function get_emp_section($section){
	 	$query = $this->db->query("select * from employeeinfo where proffession='" . $section . "'");
        return $query->result();


	
	}
    function get_card_no($a_date) {
        $query = $this->db->query("select card_no from employeeinfo where secrete_no='" . $a_date . "'");
        return $query->result();
    }

    function check_card_no($card_no) {
        $query = $this->db->query("select card_no from employeeinfo where card_no='" . $card_no . "'");
        return $query->result();
    }

    function check_duty_schedule($shift_name) {
        $branch_name = $this->input->post("branch_name");
        $dept = $this->input->post("dept");
        $proffession = $this->input->post("proffession");
        $shift_name = $shift_name;
        $query = $this->db->query("select * from duty_schedule where dept='" . $dept . "' and branch_name='" . $branch_name . "'and section='" . $proffession . "' and shift_name='" . $shift_name . "'");
        return $query->result();
    }

    function check_duty_schedule1($branch_name, $dept, $proffession, $shift_name) {
        $query = $this->db->query("select * from duty_schedule where dept='" . $dept . "' and branch_name='" . $branch_name . "'and section='" . $proffession . "' and shift_name='" . $shift_name . "'");
        return $query->result();
    }

    function delete_attendence() {
        $start_date = $this->input->post("start_date");
        $this->db->delete('attendence', array('att_date' => $start_date));
    }

    function check_delete_attendence() {
        $start_date = $this->input->post("start_date");
        $query = $this->db->query("select * from attendence where att_date='" . $start_date . "'");
        return $query->result();
    }

    function get_indetails($inid) {
        $query = $this->db->query("select * from attendence where id='" . $inid . "'");
        return $query->result();
    }

    function get_outdetails($outid) {
        $query = $this->db->query("select * from attendence where id='" . $outid . "'");
        return $query->result();
    }

    function attendence_in_update($data, $inid) {
        $this->db->where('id', $inid);
        $this->db->update('attendence', $data);
    }

    function attendence_out_update($data1, $outid) {
        $this->db->where('id', $outid);
        $this->db->update('attendence', $data1);
    }

    function custom_in_insert($data) {
        $this->db->insert('attendence', $data);
    }

    function get_monthly_attendence() {
        $start_date = $this->input->post("start_date");
        $end_date = $this->input->post("end_date");
        $card_no = $this->input->post("card_no");
        $query = $this->db->query("select * from attendence where card_no='" . $card_no . "'and att_date>='" . $start_date . "'and att_date<='" . $end_date . "'");
        return $query->result();
    }

    function get_monthly_working_hour() {
        $year = $this->input->post("year");
        $month = $this->input->post("month");
        $start_date = $year . "-" . $month . "-01";
        $end_date = $year . "-" . $month . "-31";
        $card_no = $this->input->post("card_no");
        $query = $this->db->query("select * from attendence where card_no='" . $card_no . "'and att_date>='" . $start_date . "'and att_date<='" . $end_date . "'");
        return $query->result();
    }

    function view_attendence_shift($proffession, $shift_name) {
        $query = $this->db->query("select * from employeeinfo where proffession='" . $proffession . "'and shift_name='" . $shift_name . "'");
        return $query->result();
    }

}

