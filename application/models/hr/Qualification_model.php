<?php

class Qualification_model extends CI_Model {


	function Qualification_model()
	{
		parent::__construct();    
		$this->load->helper('url');           
	}
	function qualification_info_insert($data)
	{
		$this->db->insert('qualification',$data);
	}
	function view_qualification_info($start=0,$end=0)
	{
 		$this->load->database();
		$this->db->limit($end,$start);
		$this->db->order_by('id','desc');
  		$query = $this->db->get('qualification');
  		return $query->result();
	} 
	
	function edit_qualification($data)
	{
  		$this->db->where('id',$this->input->post('id'));
 		$this->db->update('qualification',$data);  
	}
	
	
	function delete_qualification_info($id)
	{
 		$this->db->delete('qualification',array('id' => $id)); 
		//$this->db->delete('experience',array('emp_id' => $id)); 
	}
	
	function get_id($id)
	{	
		$query = $this->db->get_where('qualification',array('id'=>$id));
  		return $query->row_array();
	}

}	