<?php class Department_model extends CI_Model {


	function Department_model()
	{
		parent::__construct();    
		$this->load->helper('url');           
	}
   
	function department_insert($data)
	{
		$this->db->insert('add_department',$data);
	}
	
	
	
	function view_department($start=0,$end=0)
	{
 		$this->load->database();
		$this->db->limit($end,$start);
		$this->db->order_by('id','desc');
  		$query = $this->db->get('add_department');
  		return $query->result();
	} 
	function get_id($id)
	{	
		$query = $this->db->get_where('add_department',array('id'=>$id));
  		return $query->row_array();
	}
	
	function edit_department($data)
	{
  		$this->db->where('id',$this->input->post('id'));
 		$this->db->update('add_department',$data);  
	}
	
	function delete_department($id)
	{
 		$this->db->delete('add_department',array('id' => $id));  
	}
}
?>