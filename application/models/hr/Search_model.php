<?php

class Search_model extends CI_Model {

    function Search_model() {
        parent::__construct();
        $this->load->helper('url');
    }

    function view_salary($start=0, $end=0) {
        $this->load->database();
        $this->db->limit($end, $start);
        $this->db->order_by('id', 'desc');
        //$query = $this->db->get('salary');
        $query = $this->db->query('SELECT e.name as ename, s.* FROM employeeinfo as e, salary as s WHERE s.emp_id = e.id ORDER BY id DESC');
        return $query->result();
    }

    function view_salary_payment($start=0, $end=0) {
        $this->load->database();
        $this->db->limit($end, $start);
        $this->db->order_by('id', 'desc');
        //$query = $this->db->get('salary_payment');
        $query = $this->db->query('SELECT e.name as ename, s.* FROM employeeinfo as e, salary_payment as s WHERE s.emp_id = e.id ORDER BY id DESC');
        return $query->result();
    }

    function salary_entry($data) {
        $this->db->insert('salary', $data);
    }

    function get_salary($id) {
        $this->load->database();
        //$this->db->limit($end,$start);
        //$this->db->order_by('id','desc');
        $query = $this->db->get_where('salary', array('id' => $id));
        return $query->row_array();
    }

    function salary_update($data, $id) {
        $this->db->where('id', $id);
        $this->db->update('salary', $data);
    }

    function delete_salary($id) {
        $this->db->delete('salary', array('id' => $id));
    }

    function view_department($start=0, $end=0) {
        $this->load->database();
        $this->db->limit($end, $start);
        $this->db->order_by('id', 'desc');
        $query = $this->db->get('salary');
        return $query->result();
    }

    function get_salary_pay($id) {
        $this->load->database();
        //$this->db->limit($end,$start);
        //$this->db->order_by('id','desc');
        $query = $this->db->get_where('salary', array('id' => $id));
        return $query->row_array();
    }

    function salary_payment_entry($data) {
        $this->db->insert('salary_payment', $data);
    }

    function salary_echeck() {
        $this->load->database();
        $query = $this->db->get_where('salary', array('year' => $this->input->post('year'),
                    'month' => $this->input->post('month'),
                    'dept' => $this->input->post('dept'),
                    'emp_id' => $this->input->post('emp_id')
                        )
        );
        if ($query->num_rows() == 0) {
            return true;
        } else {
            return false;
        }
    }

    function monthly_salary() {

        $this->load->database();
        $query = $this->db->get_where('salary', array('year' => $this->input->post('year'),
                    'month' => $this->input->post('month'),
                    'dept' => $this->input->post('dept')
                        )
        );
        return $query->result();
    }

   /* function pgetall() {
        $this->load->database();
        echo $id = $this->input->post("id");
        echo $name = $this->input->post("name");
        echo $proffession = $this->input->post("proffession");
        echo $telephone = $this->input->post("telephone");
        $query = $this->db->query("select * from employeeinfo where id='" . $id . "' or proffession='" . $proffession . "' or telephone='" . $telephone . "' or name like '%" . $name . "%'");
        return $query->result();
    }*/

  function getall() {
        $id = $this->input->post("id");
        $name = $this->input->post("name");
        $proffession = $this->input->post("proffession");
        $telephone = $this->input->post("telephone");
        if (!empty($id)) {
            $query = "c.id like '%$id%'";
            if (!empty($name)) {
                $query .= " And c.name like '%$name%'";
            }
            if (!empty($proffession)) {
                $query .= " And c.proffession like '%$proffession%'";
            }
            if (!empty($telephone)) {
                $query .= " And c.telephone like '$telephone'";
            }
        } elseif (!empty($name)) {
            $query = "c.name like '%$name%'";
            if (!empty($id)) {
                $query .= " And c.id like '%$id%'";
            }
            if (!empty($proffession)) {
                $query .= " And c.proffession like '$proffession'";
            }
            if (!empty($telephone)) {
                $query .= " And c.telephone like '$telephone'";
            }
        } elseif (!empty($proffession)) {
            $query = "c.proffession like '%$proffession%'";
            if (!empty($id)) {
                $query .= " And c.id like '$id'";
            }
            if (!empty($name)) {
                $query .= " And c.name like '$name'";
            }
            if (!empty($telephone)) {
                $query .= "And c.telephone like '%$telephone%'";
            }
        } elseif (!empty($telephone)) {
            $query = "c.telephone like '$telephone'";
            if (!empty($id)) {
                $query .= " And c.id like '$id'";
            }
            if (!empty($name)) {
                $query.= " And c.name like '%$name%'";
            }
            if (!empty($proffession)) {
                $query .= " And c.proffession like '%$proffession%'";
            }
        } elseif (empty($name) & empty($id) & empty($proffession) & empty($telephone)) {
            $query = "";
        }
		  if (empty($query)) {
            $query = "c.id like '% %'";
        } 
        $this->db->select('*');
        $this->db->from('employeeinfo c');
        $this->db->where($query);
        $res = $this->db->get();
        return $res->result();
    }

    //name LIKE '%John%'
    function pay_slip() {
        $this->load->database();
        //$year = $this->input->post('year');
        //$month = $this->input->post('month');
        //$emp_id = $this->input->post('emp_id');
        $query = $this->db->get_where('salary', array('year' => $this->input->post('year'),
                    'month' => $this->input->post('month'),
                    'dept' => $this->input->post('dept'),
                    'emp_id' => $this->input->post('emp_id')
                        )
        );
        //$query = $this->db->query('SELECT e.name as ename, s.*, p.* FROM employeeinfo as e, salary_package as p, salary as s WHERE s.emp_id = $emp_id AND e.id = $emp_id AND e.salary_package = p.id AND s.year=$year, s.month=$month');
        return $query->result();
    }

}

