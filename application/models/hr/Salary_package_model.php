<?php

class Salary_package_model extends CI_Model {


	function Salary_package_model()
	{
		parent::__construct();    
		$this->load->helper('url');           
	}
	function salary_insert($data)
	{
		$this->db->insert('salary_package',$data);
	}
	
	function view_salary_package($start=0,$end=0)
	{
 		$this->load->database();
		$this->db->limit($end,$start);
  		$this->db->where('status',0);
		$this->db->order_by('id','desc');
  		$query = $this->db->get('salary_package');
  		return $query->result();
	} 
	
	function edit_salary_package($data)
	{
  		$this->db->where('id',$this->input->post('id'));
 		$this->db->update('salary_package',$data);  
	} 
	
	function get_salary_package($id)
	{	
		$query = $this->db->get_where('salary_package',array('id'=>$id));
  		return $query->row_array();
	}
	
	function delete_salary_package($id)
	{
 		$this->db->delete('salary_package',array('id' => $id));  
	}
	
	function package_info_insert($data)
	{
		$this->db->insert('add_salary_package',$data);
	}
  function view_all_salary_package(){
	  $query=$this->db->query("select * from add_salary_package order by package_name asc");
	  return $query->result();
	}
	function delete_sapackage($id){
      $this->db->delete('add_salary_package',array('id' => $id));  	
	}
}
