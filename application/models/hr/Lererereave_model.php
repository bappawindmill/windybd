<?php

class Leave_model extends CI_Model {


        function Leave_model()
        {
                parent::__construct();    
                $this->load->helper('url');           
        }
        /*Start Add Leave*/ 
        function leave_insert($data)
        {
                $this->db->insert('add_leave',$data);
        }
        function leave_manage_insert($data)
        {
                $this->db->insert('leave_management',$data);
        }
        function view_leave_manage($start=0,$end=0)
        {
                 //$this->load->database();
                //$this->db->limit($end,$start);
                //$this->db->order_by('id','desc');
                $query = $this->db->query('SELECT l.*, e.name as ename FROM leave_management as l, employeeinfo as e WHERE l.emp_id = e.id ORDER BY l.id DESC');
                  //$query = $this->db->get('leave_management');
                  return $query->result();
        } 
        function view_leave($start=0,$end=0)
        {
                 $this->load->database();
                $this->db->limit($end,$start);
                $this->db->order_by('id','desc');
                  $query = $this->db->get('add_leave');
                  return $query->result();
        } 
        /*End Add Leave*/ 
        function get_leave_manage($id)
        {        
                $query = $this->db->get_where('leave_management',array('id'=>$id));
                  return $query->row_array();
        }
        function edit_leave_manage($data)
        {
                  $this->db->where('id',$this->input->post('id'));
                 $this->db->update('leave_management',$data);  
        }
        
        function delete_leave_management($id)
        {
                 $this->db->delete('leave_management',array('id' => $id)); 
        }
        function get_leave($id)
        {        
                $query = $this->db->get_where('add_leave',array('id'=>$id));
                  return $query->row_array();
        }
        
        function edit_leave($data)
        {
                  $this->db->where('id',$this->input->post('id'));
                 $this->db->update('add_leave',$data);  
        } 
        
        function delete_leave($id)
        {
                 $this->db->delete('add_leave',array('id' => $id));  
        }
        function report_leave($data)
        {
                 $this->load->database();
                 $query = $this->db->get_where('leave_management', array('year' => $this->input->post('year'),
                                                                         'month' => $this->input->post('month'),
                                                                         'dept' => $this->input->post('dept'),
                                                                         'emp_id' => $this->input->post('emp_id')
                                                                                                          )
                                                                          );
                  return $query->result();
        }

}
