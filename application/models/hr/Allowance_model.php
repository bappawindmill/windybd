<?php class Allowance_model extends CI_Model {


	function Allowance_model()
	{
		parent::__construct();    
		$this->load->helper('url');           
	}
   
	function allowance_insert($data)
	{
		$this->db->insert('add_allowance',$data);
	}
	
	
	
	function view_allowance($start=0,$end=0)
	{
 		$this->load->database();
		$this->db->limit($end,$start);
		$this->db->order_by('id','desc');
  		$query = $this->db->get('add_allowance');
  		return $query->result();
	} 
	function get_id($id)
	{	
		$query = $this->db->get_where('add_allowance',array('id'=>$id));
  		return $query->row_array();
	}
	
	function edit_allowance($data)
	{
  		$this->db->where('id',$this->input->post('id'));
 		$this->db->update('add_allowance',$data);  
	}
	
	function delete_allowance($id)
	{
 		$this->db->delete('add_allowance',array('id' => $id));  
	}
}
?>