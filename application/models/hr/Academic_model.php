<?php

class Academic_model extends CI_Model {


	function Academic_model()
	{
		parent::__construct();    
		$this->load->helper('url');           
	}
	function academic_info_insert($data)
	{
		$this->db->insert('academic',$data);
	}
	function view_academic_info($start=0,$end=0)
	{
 		$this->load->database();
		$this->db->limit($end,$start);
		$this->db->order_by('id','desc');
  		$query = $this->db->get('academic');
  		return $query->result();
	} 
	
	function edit_academic($data)
	{
  		$this->db->where('id',$this->input->post('id'));
 		$this->db->update('academic',$data);  
	}
	
	
	function delete_academic_info($id)
	{
 		$this->db->delete('academic',array('id' => $id)); 
		//$this->db->delete('experience',array('emp_id' => $id)); 
	}
	
	function get_id($id)
	{	
		$query = $this->db->get_where('academic',array('id'=>$id));
  		return $query->row_array();
	}

}	