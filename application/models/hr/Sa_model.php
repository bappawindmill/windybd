<?php

class sa_model extends CI_Model {


	function sa_model()
	{
		parent::__construct();    
		$this->load->helper('url');           
	}
	function package_insert($data)
	{
		$this->db->insert('sapackage',$data);
	}
	
	function view()
	{
		$this->db->order_by('id','desc');
  		$query = $this->db->get('sapackage');
  		return $query->result();
	} 
	/*End Add Leave*/ 
	
	function get_package($id)
	{	
		$query = $this->db->get_where('sapackage',array('id'=>$id));
  		return $query->result();
	}
	
	function edit($data)
	{
  		$this->db->where('id',$this->input->post('id'));
 		$this->db->update('sapackage',$data);  
	} 
	
	function delete($id)
	{
 		$this->db->delete('sapackage',array('id' => $id));  
	}

}