<?php
class Office_info_model extends CI_Model {

function Office_info_model()
	{
		parent::__construct();    
		$this->load->helper('url');           
	}
	
	function view_employee_office_info($start=0,$end=0)
	{
 		$this->load->database();
		$this->db->limit($end,$start);
		$this->db->order_by('id','desc');
  		$query = $this->db->get('employee_official_info');
  		return $query->result();
	} 
	function salary_insert($data)
	{
		$this->db->insert('salary_package',$data);
	}
	function employee_office_info_insert($data)
	{
		$this->db->insert('employee_official_info',$data);
	}
	function delete_employee_office_info($id)
	{
 		$this->db->delete('employee_official_info',array('id' => $id)); 
		//$this->db->delete('experience',array('emp_id' => $id)); 
	}
function get_employee_office_info($id)
	{
 		$this->load->database();
		//$this->db->limit($end,$start);
		//$this->db->order_by('id','desc');
  		$query = $this->db->get_where('employee_official_info', array('id'=>$id));
  		return $query->row_array();
	} 
function employee_office_info_update($data)
	{
  		$this->db->where('id',$this->input->post('id'));
 		$this->db->update('employee_official_info',$data);  
	} 	
}