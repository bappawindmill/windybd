<?php

class Appraisal_model extends CI_Model {

    function Appraisal_model() {
        parent::__construct();
        $this->load->helper('url');
    }

    function employee_general_info_insert($data) {
        $this->db->insert('employee_general_info', $data);
    }

    function pf_insert($data) {
        $this->db->insert('pf_details', $data);
    }

    function custom_package_update($data3, $key) {
        $this->db->where('id', $key);
        $this->db->update('add_salary_package', $data3);
    }

    function view_employee_general_info($start = 0, $end = 0) {
        $this->load->database();
        $this->db->limit($end, $start);
        $this->db->order_by('id', 'desc');
        $query = $this->db->get('employeeinfo');
        return $query->result();
    }

    function get_employee_general_info($id) {
        $this->load->database();
        //$this->db->limit($end,$start);
        //$this->db->order_by('id','desc');
        $query = $this->db->get_where('employeeinfo', array('id' => $id));
        return $query->result();
    }

    function employee_info_insert($data) {
        $this->db->insert('employeeinfo', $data);
    }

    function employee_general_info_update($data) {
        $this->db->where('id', $this->input->post('id'));
        $this->db->update('employeeinfo', $data);
    }

    function employee_pf_info_update($data1) {
        $this->db->where('emp_id', $this->input->post('id'));
        $this->db->update('pf_details', $data1);
    }

    function salary_insert($data) {
        $this->db->insert('salary_package', $data);
    }

    function employee_exp_insert($data) {
        $this->db->insert('experience', $data);
    }

    function employee_exp_update($data2, $eid) {
        $this->db->where('id', $eid);
        $this->db->update('experience', $data2);
    }

    function salary_update($data, $id) {
        $this->db->where('id', $id);
        $this->db->update('salary', $data);
    }

    function delete_employee_general_info($id) {
        $this->db->delete('employeeinfo', array('id' => $id));
    }

    function get_salary($id) {
        $this->load->database();
        //$this->db->limit($end,$start);
        //$this->db->order_by('id','desc');
        $query = $this->db->get_where('salary', array('id' => $id));
        return $query->row_array();
    }

    /* start necessary function for employee report */

    function emp_detail($id) {
        $this->load->database();
        $query = $this->db->get_where('employeeinfo', array('id' => $id));
        return $query->result();
    }

    function salary_package_insert($data1) {
        $this->db->insert('salary_package', $data1);
    }

    function custom_package_insert($data3) {
        $this->db->insert('add_salary_package', $data3);
    }

    function get_all_employee() {
        $id = $this->input->post("dept");
        $branch_name = $this->input->post("branch_name");
        $proffession = $this->input->post("proffession");
        $query = $this->db->query("select * from employeeinfo where dept='" . $id . "' and branch_name='" . $branch_name . "'and proffession='" . $proffession . "'");
        return $query->result();
    }

    function update_secrete_no($data, $id) {
        $this->db->where('id', $id);
        $this->db->update('employeeinfo', $data);
    }

    function get_section($section) {
        $this->_table = 'add_proffession';
        $this->db->where('id', $section);
        $query = $this->db->get($this->_table);
        return $query->result();
    }

    function get_section_dept($dept) {
        $this->_table = 'add_proffession';
        $this->db->where('department', $dept);
        $query = $this->db->get($this->_table);
        return $query->result();
    }

    function get_all_section() {
        $this->_table = 'add_proffession';
        $this->db->order_by($this->_table . '.department', 'ASC');
        $query = $this->db->get($this->_table);
        return $query->result();
    }

    function get_all_employee1($section) {
        $this->_table = 'employeeinfo';
        $this->db->where('proffession', $section);
        $query = $this->db->get($this->_table);
        return $query->result();
    }

    function empDetail($card_no) {
        $query = $this->db->get_where('employeeinfo', array('card_no' => $card_no));
        return $query->result();
    }

    function get_temp_info($emp_id) {
        $query = $this->db->get_where('appraisal_temp', array('emp_id' => $emp_id));
        return $query->result();
    }

}

