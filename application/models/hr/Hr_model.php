<?php

class Hr_model extends CI_Model {


	function Hr_model()
	{
		parent::__construct();    
		$this->load->helper('url');           
	}
   
	function department_insert($data)
	{
		$this->db->insert('add_department',$data);
	}
	function user_insert($data)
	{
		$this->db->insert('user',$data);
	}
	function view_user($start=0,$end=0)
	{
 		$this->load->database();
		$this->db->limit($end,$start);
		$this->db->order_by('id','desc');
  		$query = $this->db->get('user');
  		return $query->result();
	} 
	
	function view_department($start=0,$end=0)
	{
 		$this->load->database();
		$this->db->limit($end,$start);
		$this->db->order_by('id','desc');
  		$query = $this->db->get('add_department');
  		return $query->result();
	} 
	
	function edit_department($data)
	{
  		$this->db->where('id',$this->input->post('id'));
 		$this->db->update('add_department',$data);  
	}
	
	function delete_department($id)
	{
 		$this->db->delete('add_department',array('id' => $id));  
	}
	
	function ot_insert($data)
	{
		$this->db->insert('add_ot',$data);
	}
	
	function view_ot($start=0,$end=0)
	{
 		$this->load->database();
		$this->db->limit($end,$start);
		$this->db->order_by('id','desc');
  		$query = $this->db->get('add_ot');
  		return $query->result();
	} 
	
	function get_ot($id)
	{	
		$query = $this->db->get_where('add_ot',array('id'=>$id));
  		return $query->row_array();
	}
	
	function edit_ot($data)
	{
  		$this->db->where('id',$this->input->post('id'));
 		$this->db->update('add_ot',$data);  
	}
	
	
	
/*	function get_id($id)
	{	
		$query = $this->db->get_where('add_department',array('id'=>$id));
  		return $query->row_array();
	}*/
	
	function get_proffession($id)
	{	
		$query = $this->db->get_where('add_proffession',array('id'=>$id));
  		return $query->row_array();
	}
	
	
	
	/*Start Add Proffession*/ 
	function proffession_insert($data)
	{
		$this->db->insert('add_proffession',$data);
	}
	
	function view_proffession($start=0,$end=0)
	{
 		/*$this->load->database();
		$this->db->limit($end,$start);
		$this->db->order_by('id','desc');
  		$query = $this->db->get('add_proffession'); */
		$query = $this->db->query("SELECT p.id as pid, d.id as did, d.name as dname, p.proffession as pname FROM add_proffession as p, add_department as d WHERE p.department=d.id ORDER BY pid DESC LIMIT $start,$end");
		
  		return $query->result();
	} 
	/*End Add Proffession*/ 
	
	function edit_proffession($data)
	{
  		$this->db->where('id',$this->input->post('id'));
 		$this->db->update('add_proffession',$data);  
	}
	
	function delete_proffession($id)
	{
 		$this->db->delete('add_proffession',array('id' => $id));  
	}
	
	/*Start Add Salary Package*/ 
	function salary_insert($data)
	{
		$this->db->insert('salary_package',$data);
	}
	
	function view_salary_package($start=0,$end=0)
	{
 		$this->load->database();
		$this->db->limit($end,$start);
		$this->db->order_by('id','desc');
  		$query = $this->db->get('salary_package');
  		return $query->result();
	} 
	/*End Add Salary Package*/
	
	function edit_salary_package($data)
	{
  		$this->db->where('id',$this->input->post('id'));
 		$this->db->update('salary_package',$data);  
	} 
	
	function get_salary_package($id)
	{	
		$query = $this->db->get_where('salary_package',array('id'=>$id));
  		return $query->row_array();
	}
	
	function delete_salary_package($id)
	{
 		$this->db->delete('salary_package',array('id' => $id));  
	}
	
	/*Start Add Leave*/ 
	function leave_insert($data)
	{
		$this->db->insert('add_leave',$data);
	}
	
	function view_leave($start=0,$end=0)
	{
 		$this->load->database();
		$this->db->limit($end,$start);
		$this->db->order_by('id','desc');
  		$query = $this->db->get('add_leave');
  		return $query->result();
	} 
	/*End Add Leave*/ 
	
	function get_leave($id)
	{	
		$query = $this->db->get_where('add_leave',array('id'=>$id));
  		return $query->row_array();
	}
	
	function edit_leave($data)
	{
  		$this->db->where('id',$this->input->post('id'));
 		$this->db->update('add_leave',$data);  
	} 
	
	function delete_leave($id)
	{
 		$this->db->delete('add_leave',array('id' => $id));  
	}
	
	/*Start Add employee_info*/ 
	function employee_info_insert($data)
	{
		$this->db->insert('employeeinfo',$data);
	}
	
	/*Start Add employee_Exp*/ 
	function employee_exp_insert($data)
	{
		$this->db->insert('experience',$data);
	}
	
	function view_employee_info($start=0,$end=0)
	{
 		$this->load->database();
		$this->db->limit($end,$start);
		$this->db->order_by('id','desc');
  		$query = $this->db->get('employeeinfo');
  		return $query->result();
	} 
	/*End Add employee_info*/ 
	
	function get_employee_info($id)
	{
 		$this->load->database();
		//$this->db->limit($end,$start);
		//$this->db->order_by('id','desc');
  		$query = $this->db->get_where('employeeinfo', array('id'=>$id));
  		return $query->row_array();
	} 
	
	function employee_info_update($data,$id)
	{
  		$this->db->where('id',$id);
 		$this->db->update('employeeinfo',$data);  
	} 
	
	function employee_exp_update($data2,$eid)
	{
  		$this->db->where('id',$eid);
 		$this->db->update('experience',$data2);  
	} 
	
	function salary_update($data,$id)
	{
  		$this->db->where('id',$id);
 		$this->db->update('salary',$data);  
	} 
	
	/*function salary_update($data,$s_id)
	{
  		$this->db->where('id',$eid);
 		$this->db->update('salary_package',$data);  
	}*/ 
	
	function delete_employee_info($id)
	{
 		$this->db->delete('employeeinfo',array('id' => $id)); 
		$this->db->delete('experience',array('emp_id' => $id)); 
	}
	
	function get_salary($id)
	{
 		$this->load->database();
		//$this->db->limit($end,$start);
		//$this->db->order_by('id','desc');
  		$query = $this->db->get_where('salary', array('id'=>$id));
  		return $query->row_array();
	} 
	
	function get_salary_pay($id)
	{
 		$this->load->database();
		//$this->db->limit($end,$start);
		//$this->db->order_by('id','desc');
  		$query = $this->db->get_where('salary', array('id'=>$id));
  		return $query->row_array();
	}
	
	/*Start Add Leave Management*/ 
	function leave_manage_insert($data)
	{
		$this->db->insert('leave_management',$data);
	}
	
	function view_leave_manage($start=0,$end=0)
	{
 		//$this->load->database();
		//$this->db->limit($end,$start);
		//$this->db->order_by('id','desc');
		$query = $this->db->query('SELECT l.*, e.name as ename FROM leave_management as l, employeeinfo as e WHERE l.emp_id = e.id ORDER BY l.id DESC');
  		//$query = $this->db->get('leave_management');
  		return $query->result();
	} 
	/*End Add Leave Management*/ 
	
	function get_leave_manage($id)
	{	
		$query = $this->db->get_where('leave_management',array('id'=>$id));
  		return $query->row_array();
	}
	
	function edit_leave_manage($data)
	{
  		$this->db->where('id',$this->input->post('id'));
 		$this->db->update('leave_management',$data);  
	}
	
	function delete_leave_management($id)
	{
 		$this->db->delete('leave_management',array('id' => $id)); 
	}
	
	function salary_entry($data)
	{
		$this->db->insert('salary',$data);
	}
	
	function view_salary($start=0,$end=0)
	{
 		$this->load->database();
		$this->db->limit($end,$start);
		$this->db->order_by('id','desc');
  		//$query = $this->db->get('salary');
		$query = $this->db->query('SELECT e.name as ename, s.* FROM employeeinfo as e, salary as s WHERE s.emp_id = e.id ORDER BY id DESC');
  		return $query->result();
	} 
	
	function delete_salary($id)
	{
 		$this->db->delete('salary',array('id' => $id)); 
	}
	
	function view_salary_payment($start=0,$end=0)
	{
 		$this->load->database();
		$this->db->limit($end,$start);
		$this->db->order_by('id','desc');
  		//$query = $this->db->get('salary_payment');
		$query = $this->db->query('SELECT e.name as ename, s.* FROM employeeinfo as e, salary_payment as s WHERE s.emp_id = e.id ORDER BY id DESC');
  		return $query->result();
	} 
	
	function delete_salary_payment($id)
	{
 		$this->db->delete('salary_payment',array('id' => $id)); 
	}
	
	function salary_payment_entry($data)
	{
		$this->db->insert('salary_payment',$data);
	}
	
	function emp_detail($id)
	{
 		$this->load->database();
  		$query = $this->db->get_where('employeeinfo', array('id' => $id));
  		return $query->result();
	} 
	
	function monthly_salary()
	{
 		$this->load->database();
  		$query = $this->db->get_where('salary', array('year' => $this->input->post('year'),
													  'month' => $this->input->post('month'),
													  'dept' => $this->input->post('dept')
													  )
									  );
  		return $query->result();
	} 
	
	function pay_slip()
	{
 		$this->load->database();
		//$year = $this->input->post('year');
		//$month = $this->input->post('month');
		//$emp_id = $this->input->post('emp_id');
		$query = $this->db->get_where('salary', array('year' => $this->input->post('year'),
													  'month' => $this->input->post('month'),
													  'dept' => $this->input->post('dept'),
													  'emp_id' => $this->input->post('emp_id')
													  )
									  );
  		//$query = $this->db->query('SELECT e.name as ename, s.*, p.* FROM employeeinfo as e, salary_package as p, salary as s WHERE s.emp_id = $emp_id AND e.id = $emp_id AND e.salary_package = p.id AND s.year=$year, s.month=$month');
  		return $query->result();
	} 
	
	function salary_echeck()
	{
		$this->load->database();
		$query = $this->db->get_where('salary', array('year' => $this->input->post('year'),
													  'month' => $this->input->post('month'),
													  'dept' => $this->input->post('dept'),
													  'emp_id' => $this->input->post('emp_id')
													  )
									  );
		if($query->num_rows() == 0)
		{
			return true;
		}				
		else
		{
			return false;
		}					  
	}

}