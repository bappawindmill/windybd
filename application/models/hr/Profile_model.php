<?php

class profile_model extends CI_Model {

function profile_model()
	{
		parent::__construct();    
		$this->load->helper('url');           
	}
	function employee_general_profile_insert($data)
	{
		$this->db->insert('employee_general_profile',$data);
	}
	function view_profile($start=0,$end=0)
	{
 		$this->load->database();
		$this->db->limit($end,$start);
		$this->db->order_by('id','desc');
  		$query = $this->db->get('company_profile');
  		return $query->result();
	} 
	function get_profile($id)
	{
 		$this->load->database();
		//$this->db->limit($end,$start);
		//$this->db->order_by('id','desc');
  		$query = $this->db->get_where('company_profile', array('id'=>$id));
  		return $query->row_array();
	} 
	function profile_insert($data)
	{
		$this->db->insert('company_profile',$data);
	}
	function profile_update($data)
	{
  		$this->db->where('id',$this->input->post('id'));
 		$this->db->update('company_profile',$data);  
	} 
	function salary_insert($data)
	{
		$this->db->insert('salary_package',$data);
	}
	function employee_exp_insert($data)
	{
		$this->db->insert('experience',$data);
	}
	function employee_exp_update($data2,$eid)
	{
  		$this->db->where('id',$eid);
 		$this->db->update('experience',$data2);  
	} 
	function salary_update($data,$id)
	{
  		$this->db->where('id',$id);
 		$this->db->update('salary',$data);  
	} 
	function delete_employee_general_profile($id)
	{
 		$this->db->delete('employeeprofile',array('id' => $id)); 
		
	}
	
	function get_salary($id)
	{
 		$this->load->database();
		//$this->db->limit($end,$start);
		//$this->db->order_by('id','desc');
  		$query = $this->db->get_where('salary', array('id'=>$id));
  		return $query->row_array();
	} 
	/*start necessary function for employee report*/
	function emp_detail($id)
	{
 		$this->load->database();
  		$query = $this->db->get_where('employeeprofile', array('id' => $id));
  		return $query->result();
	} 

}	