<?php

class Ot_model extends CI_Model {


	function Ot_model()
	{
		parent::__construct();    
		$this->load->helper('url');           
	}
	function ot_insert($data)
	{
		$this->db->insert('add_ot',$data);
	}
	function get_ot($id)
	{	
		$query = $this->db->get_where('add_ot',array('id'=>$id));
  		return $query->row_array();
	}
	
	function edit_ot($data)
	{
  		$this->db->where('id',$this->input->post('id'));
 		$this->db->update('add_ot',$data);  
	}
}	