<?php
class history_model extends CI_Model {
function history_model()
	{
		parent::__construct();    
		$this->load->helper('url');           
	}
function history_info_insert($data)
	{
		$this->db->insert('career_history',$data);
	}
function view_history_info($start=0,$end=0)
	{
 		$this->load->database();
		$this->db->limit($end,$start);
		$this->db->order_by('id','desc');
  		$query = $this->db->get('career_history');
  		return $query->result();
	} 
	
function edit_history($data)
	{
  		$this->db->where('id',$this->input->post('id'));
 		$this->db->update('career_history',$data);  
	}
	
	
function delete_history_info($id)
	{
 		$this->db->delete('career_history',array('id' => $id)); 
		//$this->db->delete('experience',array('emp_id' => $id)); 
	}
	
function get_id($id)
	{	
		$query = $this->db->get_where('career_history',array('id'=>$id));
  		return $query->row_array();
	}
function getinfo($emp_id)
	{	
		$query = $this->db->get_where('career_history',array('emp_id'=>$this->input->post("emp_id")));
  		return $query->result();
	}


}	